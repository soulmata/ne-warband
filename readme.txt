﻿Supposed SVN structure:

  / --+-- [docs]  - mod documentation, plans, design docs etc.
      |
      +-- [trunc] - currently developed version.
      |
      +-- [v610b] - branch for stable release v0.610b.

All stable releases that we keep for whatever purpose get the branch name "v" + %version% + %suffix% + %subversion%, where:
  1. %version% is the 3-digit mod version (610 for v0.610).
  2. %suffix% is the release quality ("a" for alpha, "b" for beta, "c" for release candidate, "r" for release).
  3. %subversion% is the build number for specific version.

Examples:

  v608b6 - 6th build of Beta release 0.608.
  v903c  - release candidate 0.903
  v1000r - official release version 1.000.
