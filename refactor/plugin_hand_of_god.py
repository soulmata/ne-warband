﻿from compiler import *
register_plugin()

simple_triggers = [

    (0,
        [
            (map_free),
            (neq, "$g_controlled_party", 0),
            (party_is_active, "$g_controlled_party"),
            (set_fixed_point_multiplier, 1000),
            (party_get_position, pos60, "$g_controlled_party"),
            (position_get_x, ":dx", pos60),
            (position_get_y, ":dy", pos60),
            (assign, ":movement", 0),
            (assign, ":d", 200),
            (try_begin),
                (this_or_next|key_is_down, key_left_control),
                (key_is_down, key_right_control),
                (val_div, ":d", 10),
            (else_try),
                (this_or_next|key_is_down, key_left_shift),
                (key_is_down, key_right_shift),
                (val_mul, ":d", 5),
            (try_end),
            (try_begin),
                (key_is_down, key_up),
                (val_add, ":dy", ":d"),
                (assign, ":movement", 1),
            (try_end),
            (try_begin),
                (key_is_down, key_down),
                (val_sub, ":dy", ":d"),
                (assign, ":movement", 1),
            (try_end),
            (try_begin),
                (key_is_down, key_left),
                (val_sub, ":dx", ":d"),
                (assign, ":movement", 1),
            (try_end),
            (try_begin),
                (key_is_down, key_right),
                (val_add, ":dx", ":d"),
                (assign, ":movement", 1),
            (try_end),
            (eq, ":movement", 1),
            (init_position, pos61),
            (position_set_x, pos61, ":dx"),
            (position_set_y, pos61, ":dy"),
            (party_set_position, "$g_controlled_party", pos61),
            (try_begin),
                (party_get_current_terrain, ":terrain", "$g_controlled_party"),
                (this_or_next|eq, ":terrain", 0),
                (this_or_next|eq, ":terrain", 1),
                (this_or_next|eq, ":terrain", 8),
                (eq, ":terrain", 9),
                (party_set_position, "$g_controlled_party", pos60),
            (try_end),
            (set_camera_follow_party, "$g_controlled_party"),
        ]
    ),

    (0, [
    	(key_clicked, key_insert),
    	(map_free),
    	(set_fixed_point_multiplier, 1000),
    	(party_get_position, pos127, "$g_controlled_party"),
    	(position_get_x, reg1, pos127),
    	(position_get_y, reg3, pos127),
    	(store_div, reg0, reg1, 1000),
    	(store_div, reg2, reg3, 1000),
    	(val_mod, reg1, 1000),
    	(val_mod, reg3, 1000),
    	(str_clear, s0),
    	(str_clear, s2),
    	(try_begin),
    		(lt, reg1, 0),
    		(val_add, reg0, 1),
    		(store_sub, reg1, 1000, reg1),
    	(try_end),
    	(try_begin),
    		(lt, reg3, 0),
    		(val_add, reg2, 1),
    		(store_sub, reg3, 1000, reg3),
    	(try_end),
    	(try_begin),
			(lt, reg1, 10),
			(str_store_string, s0, "@00"),
    	(else_try),
			(lt, reg1, 100),
			(str_store_string, s0, "@0"),
    	(try_end),
    	(try_begin),
			(lt, reg3, 10),
			(str_store_string, s2, "@00"),
    	(else_try),
			(lt, reg3, 100),
			(str_store_string, s2, "@0"),
    	(try_end),
    	(str_store_party_name, s5, "$g_controlled_party"),
    	(assign, reg10, "$g_controlled_party"),
    	(display_message, '@ ("spawn_highway_1", "{!}spawn", pf_disabled|pf_is_static, 0, pt.none, fac.outlaws_highway, 0, 0, 0, ({reg0}.{s0}{reg1}, {reg2}.{s2}{reg3}), []),'),
        (assign, "$g_controlled_party", 0),
    ]),

]

injection = {

	'script_game_context_menu': [
		(try_begin),
			(eq, "$g_controlled_party", 0),
		(else_try),
			(eq, ":party_no", "$g_controlled_party"),
			(context_menu_add_item, "@Accept this position", 3),
		(else_try),
			(context_menu_add_item, "@Manipulate this party", 3),
		(try_end),
	],

	'script_game_context_menu_clicked': [
		(eq, ":button_value", 3),
		(try_begin),
			(eq, ":party_no", "$g_controlled_party"),
			(assign, "$g_controlled_party", 0),
			(set_camera_follow_party, "p_main_party"),
			#(display_message, "@New party coordinates set."),
		(else_try),
			(assign, "$g_controlled_party", ":party_no"),
			(str_store_party_name, s5, "$g_controlled_party"),
			#(display_message, "@Party manipulation started."),
		(try_end),
		(else_try),
	],

}
