export_dir = "E:/Native Expansion [DEV]/"
export_dir = "compiled/"
write_id_files = "ID/ID_%s.py"
show_performance_data = False #True


# Working plugins:

import plugin_ms_extension
import plugin_presentations
import plugin_window_manager
import plugin_kt0_autoresolve
import plugin_companions_overseer


# Plugins in development:

#import plugin_data_structures
#import plugin_outlaws
#import plugin_town_menu


# Debugging, testing and auxilliary plugins:

#import plugin_hand_of_god
#import plugin_debug_mode
#import plugin_export
#import plugin_heraldic_config
#import plugin_test
