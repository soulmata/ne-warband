Currently working on:

	[ ] Food & Drinks
	[ ] Books
	[ ] Wounding System
	[ ] Heraldry Refactoring
	[ ] Trade Refactoring
	[ ] Preparation for improved trade goods (item amounts, new resources)
	[ ] Bugfixing issues from previous major release.

[Lav] Code Refactoring.

    [+] Adapt module code to compiler standards.
    [+] Reimplement auto-resolve using 1.161 operations.
    [ ] Refactor entity slots (cleanup and re-organization).
    [5] Implement data structures plugin.
    [ ] Modify game data structures to use data structures plugin.
    [ ] Get rid of Battlefield Tactics kit.
    [1] Major code cleanup and restructuring.
    [+] Create party templates for static parties to discern them by party template instead of slot.
    [+] Re-write extra-text item script (pre-generate strings on 1st run, output afterwards).
    [+] Fix Companions Overseer to use MS Extension and Presentation plugins.
    [+] Re-write spearwall triggers using 1.161 operations to retrieve weapons type and reach.
    [+] Re-write scripts "replace_scene_items_with_spawn_items..." using new spawn marker mechanic and get rid of potential slot range overflow bug.
    [ ] Re-write troop relations system so that relation data won't rely on actual troop references. Hint: check slot_troop_relations_begin uses.

[Lav] Item Modifiers Refactoring.

    [+] Design new item modifiers.
    [+] Allocate new modifiers to game items.
    [+] Cleanup items with redundant names (Heavy_* etc).
    [+] Reimplement food item modifiers (morale effects and description).
    [+] Reimplement modifiers for NE looting.

[Lav] Items Refactoring.
    
    [+] Rebalance armors (provide unique faction style, add new items, mark obsolete items for deletion).
        [+] Design heraldic leather armor for Swadia
        [+] Fix gambeson coloring.
    [+] Rebalance helmets (provide unique faction style, add new items, mark obsolete items for deletion).
        [+] Retexture hood_new, ladys_hood_new, barbette_new to completely replace hoods and lady turret hats from item_meshes1.brf. DONE PARTIALLY.
        [+] Pick top-tier Swadian helms to equip Paladins, Priests and Cheese Rustlers.
    [+] Rebalance boots and gloves (provide unique faction style, add new items, mark obsolete items for deletion).
        [+] Consider new boots to add (need to expand several factions).
    [+] Rebalance shields (provide unique faction style, add new items, mark obsolete items for deletion).
    [+] Rebalance horses.
    [+] Rebalance melee weapons (replace 1H/2H items with switchables, get rid of unused items, change troops equipment).
    [+] Rebalance ranged weapons (provide unique faction style, add new items, mark obsolete items for deletion).
    [+] Provide uniques and specials as quest rewards.
    [+] Add item variations (re-use items from NE primarily).
    [X] Instead of retextured items sets (viking byrnies etc) use a single item with randomized material (requires 1.161 operations).
    [+] Provide heraldic civilian equipment for lords.
    [+] Balance new heraldic items.
    [+] Add skill modifiers to items.
    [ ] Modify looting code to properly handle unique items (requires 1.161 operations).
    [ ] Kings should frown on others parading stuff that was stolen from them.
    [+] Tweak guarantee flags and skills for troops to properly use their equipment.
    [+] Equip regular faction troops.
    [+] Equip mercenaries.
    [5] Equip outlaws.
    [+] Equip Dark Knights.
    [+] Equip minor faction and miscellaneous troops.
    [+] Equip kings and lords.
    [+] Equip NPC companions.
    [+] Equip town/village walkers.
    [+] Restore liege gifts (marked with TODO: LIEGE_GIFTS).
    [+] Restore weapon trophy after fighting belligerent drunk (marked with TODO: DRUNK TROPHY).
    [+] Restore free club by Ramun the slave trader (marked with TODO: RAMUN GIFT).
    [+] Restore free weapons to lords (marked with TODO: LORDLY FREEBIES).
    [+] Restore DK 2H sword quest rewards (marked with TODO: DK REWARDS).
    [+] Restore mandolin attack sounds (marked with TODO: MINSTREL ATTACK).
    [+] Restore quest rewards for kill elites quest (marked with TODO: ELITE REWARDS).
    [+] Restore free weapons at start (marked with TODO: START ARMED).
    [+] Restore advanced equipment in duels (marked with TODO: DUEL SWORDS).
    [ ] Improve DK weaponry.
    [ ] Restore forest bandit bows.
    [ ] Add more non-factionized weapons (mid- and high-tier).

[] Food and Drinks System.

    [+] Design enhanced foods.
    [+] Design enhanced food modifiers.
    [ ] Implement consumable drinks (requires 1.161 operations).
    [ ] Consider implementing Fodder as horse food.
    [ ] Consider implementing Water as drink for horses, men and prisoners.

[] Books System.

    [ ] Design book effects.
    [ ] Implement dynamic book system.
    [ ] Implement book randomization at game start.
    [ ] Implement new book reading system.
    [ ] Implement companions reading books.

[] Wounding System.

    [ ] Design new wound effects (as skill modifiers).
    [ ] Replace attribute wound effects with skill wound effects.
    [ ] Modify Settings presentation accordingly.

[] Patrol Refactoring.

    [ ] All patrols share the same party template.
    [ ] Dynamic calculation for patrol amounts and strength depending on kingdom stats.
    [ ] Refactor patrol generation.
    [ ] Implement faction, center and lord patrols.
    [ ] Implement patrol behavior.

[] Outlaw Refactoring.

    [ ] Enhanced outlaw troop trees.
    [ ] Outlaw activity dependent on game events.
    [ ] Outlaw parties actually attacking villages.
    [ ] Produce game-design document for outlaws.
    [ ] Factionize outlaws and give them variable behavior (bandits, pirates, raiders).

[] Lesser Factions Refactoring.

    [ ] Mercenary parties.
    [ ] Manhunter parties.

[] Quests Refactoring.

    [ ] List all available quests for a game character instead of randomly picking one.
    [ ] Modify quests relevance and importance depending on current game situation and events.
    [ ] Convert all instant events to kingdom quests (kingdom demanding center, lord demanding fief etc).

[] Advanced Presentations.

    [ ] Design new presentation for center visit.
    [ ] Modify town menu appropriately.
    [ ] Implement the center visit presentation.
    [ ] Fix battlemap presentation to fit with enhanced commands.

[] Buildings System.

    [ ] Need game-design document for new buildings system.
    [ ] Design new buildings and re-design old ones.
    [ ] Use a single bitmask for center buildings. (?)
    [ ] Buildings require resources to build (replaceable by money).
    [ ] Buildings require other buildings to be present.
    [ ] Levelled buildings with improved effects. (?)

[] Faction and Lord AI Improvements.

    [ ] Need research, analysis and design.

[] Royal Court System.

    [ ] Implement court roles.
    [ ] Implement recruitment of minister and advisors.
    [ ] Assignment of court roles among minister and advisors.
    [ ] Calculation of game effects from royal court.

[] Mission Improvements.

    [+] Automatically replace all horse props with actual living breathing horses.
    [X] Additional command for battle: hold THAT position (requires 1.161 operations).
    [ ] Pre-battle deployment system (tactical round).
    [ ] Tweak horse archer AI.
    [ ] Nord Berserker unique handling. Half-naked, super-fast and regenerating. (?)
    [ ] Improve Siege Tower script. (?)
    [ ] Check money earned from victory in battle - reported that money from lord's army of 600 is much lower than a raider party of 40.

[] Heraldry Improvements.

    [ ] Replace map icon flags with heraldic flags (F.I.S.H.).
    [ ] Replace scene banners with heraldic banners.
    [+] Pick background colors for default banners.
    [+] Adapt Banner Standardization Pack.
    [+] Add new banner sheets.
    [ ] Randomize banner replacement for lords when player steals lord's current banner in "prsnt_banner_selection".
    [ ] Randomize banner selection by lords at the start of the game.

[] Living Cities.

    [ ] Add mobile spectators to arenas (use ShaunReno's code or at least general concepts).
    [+] Make walkers clothing varied depending on faction and possibly prosperity.
    [ ] Add more walker types with professional looks (monks, smiths, minor merchants, artisans, mercenaries, soldiers).
    [ ] Add watchers and patrolling soldiers on castle/town walls.
    [ ] Add mobile military patrols to the streets.

[] Trade Refactoring.

    [ ] Update merchandise regeneration triggers to handle unique items.
    [ ] Update merchandise regeneration triggers to take culture and prosperity into account.

[] Miscellaneous Improvements.

[] Bugfixes.

    [+] Missing widget note_t_keys_title in game_variables.txt.
        [+] Use game_variables.txt from latest Native patch and make NE-specific updates.
    [ ] Check mission at the end of train bandits quest - reportedly too few companions are joining.
    [ ] Resolve the situation when player is successfully hunted down by headhunters and cannot escape as they chase him down again and again.

[] General Optimization.

    [ ] Consider sounds optimization from http://forums.taleworlds.com/index.php/topic,174213.0.html
