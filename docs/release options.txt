﻿Possible installation options for the NE mod:

  1. Projectiles in flight look normal / optimized (item_kinds1.txt).
  2. Face codes classic NE / improved by nemchank (troops.txt).
  3. Siege scenes from Native / improved by Lord Samuel (scenes.txt + SceneObj/*.sco).
  4. Texture packs for global map and scenes (Textures/*).
  5. Sound packs (music.txt + Music/*.*). Optimized music (all files converted to *.wav)?
