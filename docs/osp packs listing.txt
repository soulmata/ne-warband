NATIVE-BASED ITEMS (USE ABOVE ALL PRIORITIES IF POSSIBLE).

"Al Mansur - Swords"                                      [sword] - good quality, vanilla textures
"CounterPoint - Polearms"                                 [polearm] - good quality, vanilla textures
"iggorbb - OSP iggorbb"                                   [helm][sword][axe][lance][polearm][bow] - decent quality, vanilla textures (mostly)

PRIMARY ITEM SOURCES:

"_ akosmo - New Warhorses Pack 2"                         [horse] - high quality
"_ Ath0x - Helmets WFaS"                                  [helm] - good quality
"_ cow7488 - Narf's Maximillian Retex"                    [armor] - high quality
"_ Handgunner - 15th Century Brigantines"                 [armor] - high quality
"_ Marcel - Crusader Mail Armor Pack"                     [helm][armor][boots][gloves] - good quality
"_ myriliam - Traditional Horses - Chargers"              [horse] - high quality
"_ Wilk22 - 2 Polish Hussar Helmets"                      [helm] - good quality
"Al Mansur - Helmets"                                     [helm] - good quality
"AlphaDelta - Viking Pack"                                [helm][armor][shield] - good quality
"amade - Bronze Warrior Set"                              [helm][armor][boots][gloves][axe][bow][arrow] - good quality
"Baraban - 15th Century Armour Pack"                      [helm][armor][boots][gloves] - high quality
"Barf - 16th Century Burgonet"                            [helm] - high quality
"Barf - Helmet"                                           [helm] - high quality
"BrustwarzenLenny - Nordic Armors"                        [armor] - very high quality
"BrustwarzenLenny - Round Shields"                        [shield] - high quality
"dia151 - Arabian Equipment"                              [helm][armor][boots][shield][axe][mace] - high quality
"Havoc - Havocafied Bows"                                 [bow] - decent quality, no lods (relies on vanilla lods)
"Kovas - Teutonic&Lithuanian Items"                       [helm][armor][shield][horse][axe][lance][polearm][prop][icon] - good quality
"Life_Erikson - Le Charlemagne's Sword"                   [sword] - high quality
"Life_Erikson - Medieval Swords Pack"                     [sword][polearm] - high quality
"Lucas_the_Benevolent - Weapons Pack I"                   [helm][shield][sword][axe][mace][hammer][polearm][thrown][exotic] - very high quality, some lods missing
"Lucas_the_Benevolent - Weapons Pack II"                  [horse][sword][axe][mace][hammer][polearm] - very high quality, not many lods, has iron_staff model
"Lucas_the_Benevolent - Weapons Pack III Unfinished"      [axe][mace][hammer][polearm] - very high quality
"Mackie - Swords, Axes and Then Some"                     [sword][axe][mace][polearm][exotic] - high quality
"Narf of Picklestink - Men-at-Arms Armour Pack"           [helm][armor][boots] - high quality
"Narf of Picklestink - Plate Armour Pack"                 [helm][armor][boots][gloves][sword] - high quality
"Narf of Picklestink - Rus Armour Pack"                   [helm][armor][boots] - high quality
"Narf of Picklestink - Transitional Armour Pack"          [helm][armor][boots][gloves] - high quality
"Njunja - Eastern Items"                                  [helm][armor][boots][gloves][shield][horse] - very high quality
"Rath0s - Arms and Armour"                                [helm][armor][shield][knife][sword][axe][thrown][prop] - high quality, has torch model
"Runico - 2010 - Swords"                                  [sword] - high quality
"Runico - 2012 - Roman Gladius Pack"                      [sword] - decent quality
"SacredStoneHead - Nord Armor Set"                        [armor][boots] - very high quality, some lods missing
"Sayd Uthman - 640AD Helmets"                             [helm] - high quality
"Shredzorz - 15th Century Weapons"                        [knife][sword][polearm] - decent quality, some lods missing
"Somebody - Quivers and Bolts"                            [bolt] - decent quality
"sonyer - Item Pack"                                      [helm][armor][shield] - high quality
"Spak - Items Pack"                                       [book][helm][armor][boots][gloves][shield][horse][sword][bow][arrow] - very high quality
"Specialist - OSP Models"                                 [sword][polearm][thrown][prop] - good quality, some lods missing
"stephan_dinavoa - Small Plates Armor"                    [armor][boots] - high quality
"thick1988 - Late Medieval Helmets"                       [helm] - high quality
"thick1988 - Visored Helm"                                [helm] - high quality
"Time Golem - Ancient Armory"                             [helm][armor][boots][gloves][shield][sword][axe][polearm] - high quality
"Tumajan - Sturmhaube Helmets Pack"                       [helm] - very high quality
"Waewulf - Light Armor Pack"                              [armor] - very high quality
"wanderer949 - Long Caparisoned Horses"                   [horse] - high quality, lacks specular maps (can be easily added)
"wanderer949 - More Horses"                               [horse] - high quality
"wanderer949 - More Warhorse"                             [horse] - high quality, lacks some specular maps (can be added)
"wanderer949 - Plated Charger"                            [horse] - very high quality
"wanderer949 - Tribal Horses"                             [horse] - high quality

SECONDARY ITEM SOURCES:

"_ akosmo - 28 OSP Warhorses"                             [horse] - decent quality
"_ akosmo - Item Pack"                                    [helm][armor][boots][shield][horse][sword][thrown][bow][crossbow][arrow][bolt] - mediocre quality (mostly tasteless retextures)
"_ akosmo - Lady Dress Pack"                              [armor] - decent quality
"_ akosmo - New Books"                                    [books] - mediocre quality
"_ akosmo - OSP Items Mix"                                [helm][armor][boots][gloves][sword][axe][lance][polearm] - average quality
"_ akosmo - Retex Byrnie Pack"                            [armor] - average quality
"_ akosmo - Retex Weapons Fantasy Style"                  [sword][axe][mace][hammer][lance][polearm][thrown][crossbow][arrow][bolt] - mediocre quality (excessive texturing)
"_ akosmo - Swadian Pack"                                 [helm][armor][boots][horse] - mediocre quality (excessive swadian style)
"_ cow7488 - Colored Tournament Armours"                  [armor] - average quality, simple retexture
"_ shogun65 - Painted Great Helms for 1257"               [helm] - decent quality
"_ Sir Ownzor - Epic Heraldry Mod"                        [armor][icon] - decent quality
"_ von Krieglitz - Heraldic Armor"                        [armor] - average quality
"_ zottlmarsch - Plain Houndskull Bascinet"               [helm] - decent quality
"bogmir - Fantasy Armor Set"                              [head][armor][boots][shield][sword][polearm] - average quality
"bogmir - Heraldic Knight Armor"                          [helm][boots][shield][sword][axe][lance][thrown][bow] - decent quality
"BrustwarzenLenny - Rhodock Stuff"                        [armor][shield] - mediocre quality
"CounterPoint - Crusader Heraldry"                        [armor] - decent quality
"CounterPoint - Oakeshott Sword Pack"                     [sword] - mediocre quality, vanilla textures
"CounterPoint -  Shuriken"                                [thrown] - decent quality
"Gothic Knight - Byzantine Helm"                          [helm] - average quality
"mr.master - WEe's Helmets"                               [helm][shield][sword][axe] - decent quality
"Percus - Helmet"                                         [helm] - average quality
"pino69 - Armor Pack Volume 1"                            [armor] - average quality
"pino69 - Armor Pack Volume 2"                            [armor] - average quality
"Rath0s - Shields"                                        [shield] - average quality
"RookieR. - 64 Different Chargers With Banners"           [horse] - decent quality, deprecated textures (can be added from F-Sheet pack)
"RookieR. - Banner Horses (B-Sheet)"                      [horse] - decent quality, deprecated textures (can be added from F-Sheet pack)
"RookieR. - Banner Horses (F-Sheet)"                      [horse] - decent quality, needs shader tuning
"RookieR. - Exotic Arabians Pack"                         [horse] - average quality
"Runico - 2010 - Weapons"                                 [sword][axe][mace][hammer] - mediocre quality
"Sayd Uthman - Norman Helmets Pack"                       [helm] - decent quality
"SendMeSmile - Smiley Stuff"                              [helm][armor][horse][hammer] - average quality
"ShaunRemo - Vaegir&Sarranid Helmet Replacement"          [helm] - decent quality
"Somebody - Hats"                                         [helm] - mediocre quality
"Somebody - Warhorse Variants"                            [horses] - decent quality, minor modelling/texturing tricks
"stephan_dinavoa - Mail Coif Variation"                   [helm] - decent quality
"Talak - Unique Armoury"                                  [helm][boots][shield][sword][axe][mace][hammer][polearm] - mediocre quality, some lods missing
"wanderer949 - Half Cataphracts"                          [horse] - average quality, lacks specular maps (can be added)
"wanderer949 - Yes More Horses"                           [horse] - decent quality, apparently older version of More Horses
"Wei.Xiadi - Armour Project"                              [helm][armor][boots] - average quality, deprecated textures
"Wei.Xiadi - Litus Armour Project"                        [armor][boots] - average quality
"xenoargh - Camel"                                        [horse] - decent quality
"Yoman Jenkins - Helmets for Cataphracts"                 [helm] - decent quality
"Zimke Zlovoljni - Ficus's Roman Pack"                    [helm][armor][boots][sword] - decent quality
"Zimke Zlovoljni - Kazakh Armor Pack"                     [helm][armor][boots] - decent quality

UNIQUE & SPECIAL ITEM SOURCES:

"_ Addonay - Classical Weaponry"                          [sword][bow][maul] - ultra high quality, no lods
"_ Addonay - Elven Weaponry"                              [sword][bow][arrow] - ultra high quality, no lods, reduced realism
"_ Blobmania - Unofficial 3D Art Competition - Swords"    [sword] - ultra high quality, no lods
"_ Kong Ming - Shashka Model"                             [sword] - very high quality, no lods
"_ Nu-Lz - English Brigandine-and-Plate Armour"           [armor] - very high quality, no lods
"Barf - Elven Gear"                                       [helm][armor][gloves][sword] - ultra high quality, no lods
"BrustwarzenLenny - TES4 OSP Armory - Andragorn"          [sword][axe][mace][hammer] - ultra high quality, no lods, reduced realism
"BrustwarzenLenny - TES4 OSP Armory - Multiple Authors"   [sword][axe] - excessive quality, few lods, little realism
"dejawolf - Medieval Helmets"                             [helm] - high quality, no lods
"dia151 - Lances"                                         [lance][polearm] - high quality, no lods, excessive texture size
"Docm30 - Lord of the Rings Weaponry"                     [sword] - very high quality, no lods
"Gothic Knight - Dark Ages Pack"                          [shield][sword][axe] - very high quality, no lods
"Lucas_the_Benevolent - Maciejowski Greathelm"            [helm] - very high quality, no lods
"Mandible - Peter Pavensie Shield"                        [shield] - high quality, no lods
"Nema - Ancient Swords"                                   [sword] - excessive quality, no lods, low realism
"rgcotl - Rome Pack"                                      [helm][armor][shield][sword][lance][polearm] - very high quality, no lods
"Rigadoon - Boarding Axes"                                [axe] - high quality, no lods
"Rigadoon - Elephant"                                     [horse] - high quality, no lods, excessive poly
"SacredStoneHead - Huscarl Armour"                        [armor] - very high quality, no lods
"scicon - Some Equipment"                                 [shield][sword][axe][lance][polearm][crossbow][bow][arrow] - high quality, excessive texture size, no lods
"Specialist - Macedonian Sarissa"                         [polearm] - high quality, no lods
"thick1988 - Historic Lords Volume 1"                     [helm][armor][boots][gloves][shield][sword] - high quality, no lods
"thick1988 - Historic Lords Volume 2"                     [helm][armor][boots][gloves][shield] - high quality, no lods
"Yamabusi - Battlefield Priests"                          [helm][armor][boots][gloves][polearm] - very high quality, no lods
"Yamabusi - Highlander"                                   [helm][armor][boots][shield][sword] - very high quality, no lods

LAST RESORT ITEM SOURCES:

"_ Kolba - Woad Skins"                                    [armor] - average quality, no lods
"ArRiad - Vaegir&Khergit Maces Mod"                       [mace] - good quality, no lods
"as0017 - Vietnamese Items Part 1"                        [helm][shield][sword][polearm] - good quality, no lods
"as0017 - Vietnamese Items Part 2"                        [armor] - good quality, no lods
"Bloc - Northerner Horses"                                [horse] - decent quality, no lods
"Broken_one - 16th Century Plate Armor"                   [helm][armor] - decent quality, no lods
"claymore833 - 4 Zweihanders"                             [sword] - good quality, no lods
"CounterPoint - Ninja Hoods"                              [helm] - average quality, no lods
"dariel - African Weapons"                                [sword][axe][mace][polearm] - average quality, no lods
"dejawolf - Viking Pack"                                  [helm][armor][shield][sword][axe][polearm][prop][icon] - good quality, no lods
"drakharios - Indo-Persian Armor I"                       [helm][armor] - decent quality, no lods, no bump/specular
"drakharios - Indo-Persian Armor II"                      [armor] - decent quality, no lods, no bump/specular
"drakharios - Indo-Persian Shields"                       [shield] - decent quality, no lods, no bump/specular, interesting thatch tower shields
"faradon - Weaponry"                                      [sword][mace][hammer] - good quality, no lods
"Fredelios - Fred's Bunch of Armours"                     [helm][armor] - decent quality, no lods, excessive poly
"FrisianDude - Combined Helmets"                          [helm] - decent quality, no lods
"Gothic Knight - Late Roman Spangenhelm"                  [helm] - decent quality, no lods
"James - Weapons"                                         [shield][sword][axe][polearm] - decent quality, no lods
"Lor Dric - Lor's Horses"                                 [horse] - average quality, no lods
"Runico - 2010 - Falchions"                               [sword] - average quality, no lods
"Runico - 2010 - Katanas"                                 [sword] - decent quality, no lods
"Runico - 2010 - Special Swords"                          [sword][axe] - average quality, no lods
"Septa Scarabae - Great Helm Add-On"                      [helm] - decent quality, no lods
"Sibylla - Unfinished Pack"                               [helm][sword] - average quality, no lods
"Slytacular - Sly Armour Pack"                            [helm] - decent quality, no lods
"thick1988 - Plumed Tourney Helmets"                      [helm] - decent quality, no lods

UNACCEPTABLE ITEM SOURCES:

"_ gankadank - Light Female Boots"                        [boots] - low quality, limited lods
"_ Killer_Cat - Old Coursers"                             [horse] - low quality
"_ Kolba - Female Armors"                                 [armor] - low quality, no lods
"CounterPoint - Colored Lances"                           [lance] - mediocre quality, no lods
"dariel - African Shields"                                [shield] - mediocre quality, no lods
"FrisianDude - Low Quality Knives"                        [knife] - low quality, no lods
"James&Co - OSP Weapons"                                  [sword][axe][mace][hammer][lance][polearm][crossbow] - varying quality, no lods
"maw - Armor Pack"                                        [armor] - very low quality, no lods, deprecated textures
"maw - Horse Pack M&B"                                    [horse] - mediocre quality, few lods, deprecated textures
"maw - Shield Pack"                                       [shield] - low quality, no lods, deprecated textures
"Rath0s - Weapons Pack"                                   [sword][axe][mace][hammer][lance][polearm][bow][thrown] - mediocre quality, few lods
"RookieR. - 28 Horses"                                    [horse] - mediocre quality, deprecated textures
"Runico - 2010 - Helmets&Shields"                         [helm][shield] - low quality, no lods
"Runico - 2010 - Vikings Pack"                            [sword] - low quality
"Shik - Horses M&B"                                       [horse] - low quality, deprecated textures
"TheMageLord - Weapon Pack"                               [sword][axe] - low quality, no lods
"thick1988 - OSP Item Variants"                           [helm][armor][gloves][horse] - average quality, zero originality

NATIVE ARMORS AND THEIR POLY COUNTS

 812 - heraldic_armors / heraldic_armor_a         BASED ON costumes_b / mail_long_surcoat
 814 - heraldic_armors / heraldic_armor_b         UNIQUE MESH
 848 - heraldic_armors / heraldic_armor_c         UNIQUE MESH
1056 - armors_new_heraldic / heraldic_armor_new_b UNIQUE MESH
1074 - armors_new_heraldic / heraldic_armor_new_a UNIQUE MESH
1084 - armors_new_heraldic / heraldic_armor_new_c UNIQUE MESH
1136 - heraldic_armors / heraldic_armor_d         UNIQUE MESH
1308 - armors_new_heraldic / heraldic_armor_new_d UNIQUE MESH

 410 - item_meshes1 / armor_nomad_b         670 - item_meshes1 / pilgrim_outfit                      1240 - armors_g / mail_shirt_a
 448 - item_meshes1 / shirt                 678 - item_meshes1 / plate_armor                         1246 - bride_dress / bride_dress
 458 - item_meshes1 / leather_vest          682 - item_meshes1 / lady_dress_b                        1254 - armors_f / red_gambeson_a
 458 - item_meshes1 / linen_tunic           688 - item_meshes1 / lamellar_armor                      1264 - armors_f / shirt_a
 462 - item_meshes1 / haubergeon_b          694 - item_meshes1 / black_armor                         1265 - fur_armors_a / nomad_armor_new
 462 - item_meshes1 / nomad_robe_a          750 - deneme / samurai_armor                             1298 - khergit_lady_dress / khergit_lady_dress
 467 - item_meshes1 / armor_nomad           754 - armors_new_b / blue_dress_new                      1300 - sarranid_armor / sarranid_leather_armor
 470 - item_meshes1 / aketon_a              784 - arena_costumes / arena_armorW                      1302 - armors_d / ragged_leather_jerkin
 476 - item_meshes1 / merchant_outf         812 - costumes_b / mail_long_surcoat                     1312 - armors_new_a / mail_long_surcoat_new
 484 - item_meshes1 / leather_jacket        822 - arena_costumes / arena_tunicW                      1312 - fur_armors_a / khergit_armor_new
 484 - item_meshes1 / padded_leather        828 - costumes_c / hauberk_a                             1320 - armors_h / rich_tunic_a
 490 - item_meshes1 / nomad_vest_a          834 - costumes_b / tribal_warrior_outfit_a               1324 - armors_g / haubergeon_c
 496 - item_meshes1 / std_lthr_coat         846 - armors_b / light_mail_and_plate                    1326 - armors_e / lamellar_vest_a
 498 - item_meshes1 / leather_jerkin        860 - armors_new_b / peasant_dress_b_new                 1338 - armors_f / padded_cloth_b
 506 - item_meshes1 / leather_apron         868 - costumes_c / surcoat_over_mail                     1350 - armors_new_a / surcoat_over_mail_new
 510 - item_meshes1 / coat_of_plates        914 - fur_armors_a / nomad_vest_new                      1362 - armors_d / leather_armor_b
 510 - item_meshes1 / mail_shirt            978 - armors_new_a / ragged_outfit_a_new                 1364 - armors_g / brigandine_b
 518 - item_meshes1 / nobleman_outf         992 - armors_b / mail_and_plate                          1376 - arabian_armors / arabian_armor_a
 520 - item_meshes1 / tunic_fur             994 - armors_new_arena / arena_armorW_new                1418 - armors_h / coarse_tunic_a
 530 - item_meshes1 / reinf_jerkin          999 - armors_b / lamellar_armor_a                        1432 - sarranid_armors / sarranid_elite_cavalary
 532 - item_meshes1 / lthr_armor_a         1016 - armors_b / light_leather                           1436 - costumes_d / brown_dress
 538 - item_meshes1 / robe                 1020 - armors_f / leather_vest_a                          1478 - armors_d / thick_coat_a
 540 - item_meshes1 / haubergeon_a         1024 - sarranid_lady_dress / sarranid_lady_dress          1484 - armors_new_a / tribal_warrior_outfit_a_new
 544 - item_meshes1 / white_gambeson       1046 - sarranid_armor / skirmisher_armor                  1494 - armors_g / tattered_leather_armor_a
 550 - item_meshes1 / coarse_tunic         1048 - armors_new_arena / arena_tunicW_new                1508 - full_plate_armor / full_plate_armor
 558 - costumes_c / byrnie_a               1070 - armors_new_b / hauberk_a_new                       1538 - armors_d / leather_armor_a
 562 - item_meshes1 / cvl_costume_a        1078 - armors_c / lamellar_armor_b                        1556 - armors_g / banded_armor_a
 572 - item_meshes1 / padded_armor         1090 - armors_new_b / byrnie_a_new                        1562 - armors_h / tabard_b
 576 - item_meshes1 / tabard_a             1092 - sarranid_armor / sarranian_mail_shirt              1600 - armors_i / lamellar_armor_e
 592 - item_meshes1 / brigandine_a         1102 - armors_d / coat_of_plates_a                        1620 - armors_e / coat_of_plates_b
 594 - item_meshes1 / dress                1110 - sarranid_armor / archers_vest                      1646 - arabian_armors / arabian_armor_b
 610 - item_meshes1 / tourn_armor_a        1134 - armors_c / fur_coat                                1652 - arabian_armors / peasant_man_a
 622 - item_meshes1 / hard_lthr_a          1150 - armors_f / padded_cloth_a                          1720 - costumes_d / red_dress
 624 - item_meshes1 / woolen_dress         1176 - sarranid_armor / sar_robe                          1748 - armors_h / cuir_bouilli_a
 642 - item_meshes1 / court_dress          1182 - armors_c / lamellar_leather                        1783 - armors_e / tunic_armor_a
 648 - costumes_b / ragged_outfit_a        1184 - sarranid_lady_dress / sarranid_common_dress        2062 - armors_e / lamellar_armor_c
 664 - costumes_b / nobleman_outfit_b      1210 - armors_new_a / nobleman_outfit_b_new               2156 - armors_e / lamellar_armor_d
 666 - item_meshes1 / lady_dress_r         1220 - fur_armors_a / leather_jacket_new
