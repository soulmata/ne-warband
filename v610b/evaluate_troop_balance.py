﻿from module_items import *
from module_troops import *
from process_common import convert_to_identifier

def normalize_damage(value):
	result = value & ibf_armor_mask
	dmgtype = value >> iwf_damage_type_bits
	if dmgtype == pierce: result += (result >> 1) # *1.5
	elif dmgtype == blunt: result += (result >> 2) # *1.25
	return result

class Item(object):

	def __init__(self, item):
		self.id = 'itm_%s' % convert_to_identifier(item[0])
		self.name = item[1]
		self.item_type = item[4] & 0xff
		self.weight = get_weight(item[6])
		self.head_armor = get_head_armor(item[6])
		self.body_armor = get_body_armor(item[6])
		self.accuracy = self.leg_armor = get_leg_armor(item[6])
		self.hp = get_hit_points(item[6])
		self.speed = get_speed_rating(item[6])
		self.width = self.length = get_weapon_length(item[6])
		self.horse_speed = self.height = get_missile_speed(item[6])
		self.swing_dmg = normalize_damage(get_swing_damage(item[6]))
		self.thrust_dmg = normalize_damage(get_thrust_damage(item[6]))

_items = [Item(itemdef) for itemdef in items]
balance = []

for troop in troops:
	if (troop[3] & tf_is_hero): continue
	faction = troop[6]
	while faction >= len(balance): balance.append([])
	weapons = []
	bows = []
	arrows = []
	helmets = []
	armors = []
	boots = []
	gloves = []
	horses = []
	for item in troop[]