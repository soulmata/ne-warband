menu_tournament_withdraw_verify = 140
menu_cheat_find_item = 30
menu_past_life_explanation = 18
menu_train_peasants_against_bandits_attack = 153
menu_collect_taxes_complete = 145
menu_captivity_wilderness_check = 202
menu_lord_relations = 22
menu_invite_player_to_faction_without_center = 214
menu_end_game = 38
menu_morale_report = 20
menu_custom_battle_end = 11
menu_collect_taxes_rebels_killed = 146
menu_notification_sortie_possible = 222
menu_hire_adviser = 168
menu_declare_war = 162
menu_collect_taxes_revolt = 149
menu_king_duel_lost = 278
menu_invite_player_to_faction = 216
menu_build_university = 116
menu_lco_view_character = 282
menu_castle_outside = 71
menu_village_infestation_removed = 109
menu_notification_faction_defeated = 247
menu_village_take_food = 124
menu_town_trade = 177
menu_recruit_special_rejection = 104
menu_train_peasants_against_bandits_ready = 151
menu_training_ground = 185
menu_town_talk_locals = 285
menu_lord_duel_lost = 277
menu_join_battle = 60
menu_conscript_volunteers = 106
menu_kill_local_merchant_begin = 253
menu_lco_presentation = 281
menu_castle_entry_denied = 74
menu_companion_report = 23
menu_notification_player_faction_political_issue_resolved_for_player = 259
menu_garden = 252
menu_village_rebellion_result = 110
menu_notification_give_vassal_gift = 267
menu_hire_financial_adviser = 171
menu_hire_logistics_adviser = 172
menu_captivity_start_wilderness_defeat = 197
menu_give_center_to_player = 93
menu_captivity_end_exchanged_with_prisoner = 207
menu_start_phase_4 = 262
menu_establish_court = 264
menu_duel_requested = 275
menu_award_fief = 164
menu_train_peasants_against_bandits_success = 155
menu_give_center_to_player_2 = 94
menu_village_steal_cattle = 122
menu_enemy_slipped_away = 53
menu_battlefields = 66
menu_tournament_bet_confirm = 142
menu_notification_lord_rebellion = 243
menu_center_manage = 114
menu_manage_kingdom = 160
menu_siege_started_defender = 96
menu_declare_war_do = 163
menu_village_loot_no_resist = 126
menu_start_phase_3 = 261
menu_training_ground_selection_details_melee_1 = 186
menu_notification_troop_joined_players_faction = 244
menu_captivity_start_wilderness_surrender = 196
menu_notification_player_faction_deactive = 230
menu_town_tournament_lost = 136
menu_set_brutality = 6
menu_sneak_into_town_suceeded = 180
menu_training_ground_selection_details_ranged_1 = 189
menu_captivity_avoid_wilderness = 194
menu_training_ground_selection_details_ranged_2 = 190
menu_center_improve = 115
menu_notification_lord_restart_freed = 241
menu_train_peasants_against_bandits_training_result = 152
menu_collect_taxes_failed = 147
menu_notification_court_lost = 229
menu_total_defeat = 56
menu_start_game_1 = 12
menu_notification_rebellion = 175
menu_start_game_3 = 2
menu_invite_player_to_faction_swa = 217
menu_leave_faction = 92
menu_notification_give_liege_gift = 268
menu_notification_player_should_consult = 249
menu_collect_taxes = 144
menu_castle_meeting = 75
menu_siege_attack_meets_sally = 78
menu_training_ground_selection_details_melee_2 = 187
menu_cattle_herd_kill_end = 41
menu_requested_castle_granted_to_player_husband = 89
menu_notification_village_raided = 234
menu_price_and_production = 176
menu_tutorial = 3
menu_do_change_color = 174
menu_castle_guard = 72
menu_notification_player_faction_political_issue_resolved = 258
menu_ne_tactical_escape = 52
menu_lord_duel_won = 276
menu_village_steal_cattle_confirm = 121
menu_town_bandits_succeeded = 118
menu_castle_taken_2 = 86
menu_requested_castle_granted_to_another_female = 91
menu_award_fief_selected = 165
menu_notification_peace_declared = 246
menu_faction_relations_report = 27
menu_order_attack_2 = 48
menu_invite_player_to_faction_accepted = 218
menu_hire_civil_adviser = 169
menu_camp_action = 32
menu_enemy_offer_ransom_for_prisoner = 184
menu_auto_return = 19
menu_village_loot_defeat = 128
menu_notification_oath_renounced_faction_defeated = 237
menu_start_phase_2 = 1
menu_village_hunt_down_fugitive_defeated = 107
menu_village_start_attack = 125
menu_minister_confirm = 228
menu_town_tournament_won = 137
menu_assassination_failed = 270
menu_award_fief_both = 167
menu_village_infest_bandits_result = 108
menu_village_loot_continue = 129
menu_encounter_retreat = 46
menu_zendar = 62
menu_salt_mine = 63
menu_cattle_herd = 39
menu_notification_rebels_switched_to_faction = 248
menu_center_construction_abort = 287
menu_change_color = 173
menu_notification_war_declared = 245
menu_assassination_fought_off = 271
menu_notification_treason_indictment = 225
menu_notification_center_lost = 238
menu_captivity_start_castle_defeat = 199
menu_town_tournament = 139
menu_reports = 4
menu_town_tournament_won_by_another = 138
menu_walled_center_manage = 284
menu_training_ground_description = 191
menu_join_order_attack = 61
menu_recruit_nobles_new_system = 102
menu_training_ground_training_result = 192
menu_village_hostile_action = 100
menu_town_bandits_failed = 117
menu_village_wedding_suppressed = 113
menu_camp_cheat = 29
menu_camp_recruit_prisoners = 33
menu_town_trade_assessment_begin = 178
menu_invite_player_to_faction_without_center_swa = 215
menu_castle_meeting_selected = 76
menu_besiegers_camp_with_allies = 70
menu_threaten_war = 280
menu_camp_action_read_book = 35
menu_camp_settings = 5
menu_ne_escape_desparate = 55
menu_custom_battle_scene = 10
menu_bandit_lair = 257
menu_town_assassins_failed = 119
menu_village_wedding_result = 112
menu_village = 99
menu_notification_lord_restart = 242
menu_disembark = 156
menu_center_tax = 131
menu_assassination_prosperity = 272
menu_center_reports = 158
menu_recruit_special_new_system = 103
menu_join_siege_outside = 68
menu_marshall_selection_candidate_ask = 193
menu_castle_besiege = 77
menu_notification_relieved_as_marshal = 265
menu_town_trade_assessment = 179
menu_town_assassins_succeeded = 120
menu_ship_reembark = 157
menu_party_size_report = 26
menu_start_game_0 = 0
menu_captivity_castle_remain = 209
menu_train_peasants_against_bandits = 150
menu_camp_action_read_book_start = 36
menu_sneak_into_town_caught_ran_away = 183
menu_notification_give_crown = 266
menu_recruit_volunteers = 101
menu_construct_ladders = 80
menu_cattle_herd_kill = 40
menu_bar_brawl_begin = 254
menu_start_character_1 = 13
menu_retirement_verify = 37
menu_start_character_3 = 15
menu_start_character_4 = 16
menu_cheat_change_weather = 31
menu_oath_fulfilled = 95
menu_question_peace_offer = 219
menu_training_ground_selection_details_mounted = 188
menu_encounter_retreat_confirm = 45
menu_debug_alert_from_s65 = 255
menu_captivity_end_wilderness_escape = 203
menu_permanent_damage = 58
menu_dk_invasion_start_warning = 88
menu_lord_request_land = 166
menu_castle_entry_granted = 73
menu_choose_skill = 17
menu_town = 133
menu_notification_center_under_siege = 233
menu_captivity_start_wilderness = 195
menu_order_attack_begin = 47
menu_total_victory = 50
menu_notification_troop_left_players_faction = 239
menu_tournament_bet = 141
menu_faction_orders = 24
menu_captivity_start_under_siege_defeat = 200
menu_requested_castle_granted_to_player = 87
menu_notification_one_faction_left = 236
menu_arena_duel_conclusion = 43
menu_kingdom_army_quest_join_siege_order = 212
menu_hire_intel_adviser = 170
menu_notification_of_nemesis = 269
menu_courtship_relations = 21
menu_siege_join_defense = 97
menu_village_take_food_confirm = 123
menu_notification_border_incident = 226
menu_lost_tavern_duel = 263
menu_enter_your_own_castle = 98
menu_castle_attack_walls_with_allies_simulate = 83
menu_notification_player_wedding_day = 231
menu_notification_casus_belli_expired = 223
menu_castle_taken_by_friends = 84
menu_notification_player_feast_in_progress = 250
menu_captivity_end_propose_ransom = 208
menu_king_duel_won = 279
menu_castle_attack_walls_simulate = 82
menu_close = 130
menu_notification_feast_quest_expired = 221
menu_sneak_into_town_caught = 181
menu_captivity_castle_taken_prisoner = 204
menu_captivity_castle_check = 206
menu_dhorak_keep = 67
menu_notification_player_faction_active = 227
menu_pre_join = 59
menu_auto_return_to_map = 256
menu_collect_taxes_revolt_warning = 148
menu_tournament_participants = 143
menu_recruit_special = 105
menu_captivity_start_castle_surrender = 198
menu_train_peasants_against_bandits_attack_result = 154
menu_requested_castle_granted_to_another = 90
menu_set_invasion = 9
menu_test_scene = 65
menu_cannot_enter_court = 134
menu_kingdom_army_quest_messenger = 211
menu_start_phase_2_5 = 260
menu_character_report = 25
menu_lord_released_player = 57
menu_village_loot_complete = 127
menu_start_character_2 = 14
menu_construct_siege_tower = 81
menu_change_kingdom_name = 161
menu_village_rebellion_suppressed = 111
menu_all_center_tax = 132
menu_four_ways_inn = 64
menu_lco_auto_return = 283
menu_kingdom_army_follow_failed = 213
menu_notification_player_kingdom_holds_feast = 232
menu_notification_lady_requests_visit = 251
menu_ask_darkknight_invade = 8
menu_assassination_fiefed = 273
menu_sneak_into_town_caught_dispersed_guards = 182
menu_notification_village_raid_started = 235
menu_notification_breakthrough = 274
menu_ne_failed_escape = 54
menu_arena_duel_fight = 42
menu_castle_taken = 85
menu_castle_besiege_inner_battle = 79
menu_notification_lord_defects = 224
menu_captivity_rescue_lord_taken_prisoner = 205
menu_town_sell_loot = 286
menu_camp = 28
menu_cut_siege_without_fight = 69
menu_blacksmith_forge = 159
menu_camp_no_prisoners = 34
menu_ask_brutality_create = 7
menu_kingdom_army_quest_report_to_army = 210
menu_captivity_wilderness_taken_prisoner = 201
menu_notification_truce_expired = 220
menu_battle_debrief = 49
menu_simple_encounter = 44
menu_lady_visit = 135