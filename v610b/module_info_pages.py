####################################################################################################################
#  Each quest record contains the following fields:
#  1) Info page id: used for referencing info pages in other files. The prefix ip_ is automatically added before each info page id.
#  2) Info page name: Name displayed in the info page screen.
#
####################################################################################################################

info_pages = [
  ("party",        "Party Size and Morale",      "Ready."),
  ("relations",    "Kingdom and Lord Relations", "Ready."),
  ("characters",   "Party Members",              "Ready."),
  ("fiefs",        "Fiefs Status",               "Ready."),
  ("construction", "Construction Projects",      "This report page is not available yet. Expect it to appear in some later version of the Native Expansion Unofficial Patch.^^It will include the listing of all player's fiefs with their current construction projects and estimated completion time."),
  ("alerts",       "Military Alerts",            "This report page is not available yet. Expect it to appear in some later version of the Native Expansion Unofficial Patch.^^It will include the listing of all player's fiefs that are under threat or actual attack by enemy. Note that fiefs which do not have Messenger Posts will only report their status if player is in relative vicinity of them."),
  ("courtship",    "Courtship",                  "This report page is not available yet. Expect it to appear in some later version of the Native Expansion Unofficial Patch.^^It will replicate the information from the Courtship Report in Reports menu, possibly with some additions and extras."),
]
