#from module_constants import *
from ID_factions import *
#from header_operations import *
#from header_triggers import *

from compiler import *
from header_items import  *

####################################################################################################################
#  Each item record contains the following fields:
#  1) Item id: used for referencing items in other files.
#     The prefix itm_ is automatically added before each item id.
#  2) Item name. Name of item as it'll appear in inventory window
#  3) List of meshes.  Each mesh record is a tuple containing the following fields:
#    3.1) Mesh name.
#    3.2) Modifier bits that this mesh matches.
#     Note that the first mesh record is the default.
#  4) Item flags. See header_items.py for a list of available flags.
#  5) Item capabilities. Used for which animations this item is used with. See header_items.py for a list of available flags.
#  6) Item value.
#  7) Item stats: Bitwise-or of various stats about the item such as:
#      weight, abundance, difficulty, head_armor, body_armor,leg_armor, etc...
#  8) Modifier bits: Modifiers that can be applied to this item.
#  9) [Optional] Triggers: List of simple triggers to be associated with the item.
#  10) [Optional] Factions: List of factions that item can be found as merchandise.
####################################################################################################################

# Some constants for ease of use.
imodbits_none = 0
imodbits_horse_basic = imodbit_swaybacked|imodbit_lame|imodbit_spirited|imodbit_heavy|imodbit_stubborn
imodbits_cloth  = imodbit_tattered | imodbit_ragged | imodbit_sturdy | imodbit_thick | imodbit_hardened
imodbits_armor  = imodbit_rusty | imodbit_battered | imodbit_crude | imodbit_thick | imodbit_reinforced |imodbit_lordly
imodbits_plate  = imodbit_cracked | imodbit_rusty | imodbit_battered | imodbit_crude | imodbit_thick | imodbit_reinforced |imodbit_lordly
imodbits_polearm = imodbit_cracked | imodbit_bent | imodbit_balanced
imodbits_shield  = imodbit_cracked | imodbit_battered |imodbit_thick | imodbit_reinforced
imodbits_sword   = imodbit_rusty | imodbit_chipped | imodbit_balanced |imodbit_tempered
imodbits_sword_high   = imodbit_rusty | imodbit_chipped | imodbit_balanced |imodbit_tempered|imodbit_masterwork
imodbits_axe   = imodbit_rusty | imodbit_chipped | imodbit_heavy
imodbits_mace   = imodbit_rusty | imodbit_chipped | imodbit_heavy
imodbits_pick   = imodbit_rusty | imodbit_chipped | imodbit_balanced | imodbit_heavy
imodbits_bow = imodbit_cracked | imodbit_bent | imodbit_strong |imodbit_masterwork
imodbits_crossbow = imodbit_cracked | imodbit_bent | imodbit_masterwork
imodbits_missile   = imodbit_bent | imodbit_large_bag

imodbits_bolts      = imodbit_cracked|imodbit_bent|imodbit_chipped|imodbit_crude|imodbit_fine|imodbit_balanced|imodbit_masterwork|imodbit_heavy|imodbit_large_bag
imodbits_iron_bolts = imodbits_bolts|imodbit_rusty|imodbit_tempered

imodbits_thrown   = imodbit_bent | imodbit_heavy| imodbit_balanced| imodbit_large_bag
imodbits_thrown_minus_heavy = imodbit_bent | imodbit_balanced| imodbit_large_bag

imodbits_horse_good = imodbit_spirited|imodbit_heavy
imodbits_good   = imodbit_sturdy | imodbit_thick | imodbit_hardened | imodbit_reinforced
imodbits_bad    = imodbit_rusty | imodbit_chipped | imodbit_tattered | imodbit_ragged | imodbit_cracked | imodbit_bent
# Replace winged mace/spiked mace with: Flanged mace / Knobbed mace?
# Fauchard (majowski glaive) 
items = [
# item_name, mesh_name, item_properties, item_capabilities, slot_no, cost, bonus_flags, weapon_flags, scale, view_dir, pos_offset
["no_item","INVALID ITEM", [("invalid_item",0)], itp_type_one_handed_wpn|itp_primary|itp_secondary, itc_longsword, 3,weight(1.5)|spd_rtng(103)|weapon_length(90)|swing_damage(16,blunt)|thrust_damage(10,blunt),imodbits_none],

["tutorial_spear", "Spear", [("spear",0)], itp_type_polearm| itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_spear, 0 , weight(4.5)|difficulty(0)|spd_rtng(80) | weapon_length(158)|swing_damage(0 , cut) | thrust_damage(19 ,  pierce),imodbits_polearm ],
["tutorial_club", "Club", [("club",0)], itp_type_one_handed_wpn| itp_primary|itp_wooden_parry|itp_wooden_attack, itc_scimitar, 0 , weight(2.5)|difficulty(0)|spd_rtng(95) | weapon_length(95)|swing_damage(11 , blunt) | thrust_damage(0 ,  pierce),imodbits_none ],
["tutorial_battle_axe", "Battle Axe", [("battle_ax",0)], itp_type_two_handed_wpn| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry, itc_nodachi|itcf_carry_axe_back, 0 , weight(5)|difficulty(0)|spd_rtng(88) | weapon_length(108)|swing_damage(27 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["tutorial_arrows","Arrows", [("arrow",0),("flying_missile",ixmesh_flying_ammo),("quiver", ixmesh_carry)], itp_type_arrows, itcf_carry_quiver_back, 0,weight(3)|abundance(160)|weapon_length(95)|thrust_damage(0,pierce)|max_ammo(20),imodbits_none],
["tutorial_bolts","Bolts", [("bolt",0),("flying_missile",ixmesh_flying_ammo),("bolt_bag", ixmesh_carry),("bolt_bag_b", ixmesh_carry|imodbit_large_bag)], itp_type_bolts, itcf_carry_quiver_right_vertical, 0,weight(2.25)|abundance(90)|weapon_length(55)|thrust_damage(0,pierce)|max_ammo(18),imodbits_none],
["tutorial_short_bow", "Short Bow", [("short_bow",0),("short_bow_carry",ixmesh_carry)], itp_type_bow|itp_two_handed|itp_primary, itcf_shoot_bow|itcf_carry_bow_back, 0, weight(1)|difficulty(0)|spd_rtng(98)|shoot_speed(49)|thrust_damage(12,pierce), imodbits_bow ],
["tutorial_crossbow", "Crossbow", [("crossbow",0)], itp_type_crossbow |itp_primary|itp_two_handed|itp_cant_reload_on_horseback ,itcf_shoot_crossbow|itcf_carry_crossbow_back, 0 , weight(3)|difficulty(0)|spd_rtng(42)|  shoot_speed(68) | thrust_damage(32,pierce)|max_ammo(1),imodbits_crossbow ],
["tutorial_throwing_daggers", "Throwing Daggers", [("throwing_dagger",0)], itp_type_thrown |itp_primary ,itcf_throw_knife, 0 , weight(3.5)|difficulty(0)|spd_rtng(102) | shoot_speed(25) | thrust_damage(16 ,  cut)|max_ammo(14)|weapon_length(0),imodbits_none],
["tutorial_saddle_horse", "Saddle Horse", [("saddle_horse",0)], itp_type_horse, 0, 0,abundance(90)|body_armor(3)|difficulty(0)|horse_speed(40)|horse_maneuver(38)|horse_charge(8),imodbits_horse_basic],
["tutorial_shield", "Kite Shield", [("shield_kite_a",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  118 , weight(2.5)|hit_points(480)|body_armor(1)|spd_rtng(82)|shield_width(150),imodbits_shield ],
["tutorial_staff_no_attack","Staff", [("wooden_staff",0)],itp_type_polearm|itp_offset_lance|itp_primary|itp_penalty_with_shield|itp_wooden_parry|itp_wooden_attack,itc_parry_polearm|itcf_carry_sword_back,9, weight(3.5)|spd_rtng(120) | weapon_length(115)|swing_damage(0,blunt) | thrust_damage(0,blunt),imodbits_none],
["tutorial_staff","Staff", [("wooden_staff",0)],itp_type_polearm|itp_offset_lance|itp_primary|itp_penalty_with_shield|itp_wooden_parry|itp_wooden_attack,itc_staff|itcf_carry_sword_back,9, weight(3.5)|spd_rtng(120) | weapon_length(115)|swing_damage(16,blunt) | thrust_damage(16,blunt),imodbits_none],
["tutorial_sword", "Sword", [("long_sword",0),("scab_longsw_a", ixmesh_carry)], itp_type_one_handed_wpn|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 0 , weight(1.5)|difficulty(0)|spd_rtng(100) | weapon_length(102)|swing_damage(18 , cut) | thrust_damage(15 ,  pierce),imodbits_sword ],
["tutorial_axe", "Axe", [("iron_ax",0)], itp_type_two_handed_wpn| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry, itc_nodachi|itcf_carry_axe_back, 0 , weight(4)|difficulty(0)|spd_rtng(91) | weapon_length(108)|swing_damage(19 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],

["tutorial_dagger","Dagger", [("practice_dagger",0)], itp_type_one_handed_wpn|itp_primary|itp_secondary, itc_longsword, 3,weight(1.5)|spd_rtng(103)|weapon_length(40)|swing_damage(16,blunt)|thrust_damage(10,blunt),imodbits_none],


["horse_meat","Horse Meat", [("raw_meat",0)], itp_type_goods|itp_consumable|itp_food, 0, 12,weight(40)|food_quality(30)|max_ammo(40),imodbits_none],
# Items before this point are hardwired and their order should not be changed!
["practice_sword","Practice Sword", [("practice_sword",0)], itp_type_one_handed_wpn|itp_primary|itp_secondary|itp_wooden_parry|itp_wooden_attack, itc_longsword, 3,weight(1.5)|spd_rtng(103)|weapon_length(90)|swing_damage(22,blunt)|thrust_damage(20,blunt),imodbits_none],
["heavy_practice_sword","Heavy Practice Sword", [("heavy_practicesword",0)], itp_type_two_handed_wpn|itp_two_handed|itp_primary|itp_wooden_parry|itp_wooden_attack, itc_greatsword,    21, weight(6.25)|spd_rtng(94)|weapon_length(128)|swing_damage(30,blunt)|thrust_damage(24,blunt),imodbits_none],
["practice_dagger","Practice Dagger", [("practice_dagger",0)], itp_type_one_handed_wpn|itp_primary|itp_secondary|itp_no_parry|itp_wooden_attack, itc_dagger|itcf_carry_dagger_front_left, 2,weight(0.5)|spd_rtng(110)|weapon_length(47)|swing_damage(16,blunt)|thrust_damage(14,blunt),imodbits_none],
["practice_axe", "Practice Axe", [("hatchet",0)], itp_type_one_handed_wpn| itp_primary|itp_secondary|itp_bonus_against_shield|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip, 24 , weight(2) | spd_rtng(95) | weapon_length(75) | swing_damage(24, blunt) | thrust_damage(0, pierce), imodbits_axe],
["arena_axe", "Axe", [("arena_axe",0)], itp_type_one_handed_wpn|itp_primary|itp_secondary|itp_bonus_against_shield|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip, 137 , weight(1.5)|spd_rtng(100) | weapon_length(69)|swing_damage(24 , blunt) | thrust_damage(0 ,  pierce),imodbits_axe ],
["arena_sword", "Sword", [("arena_sword_one_handed",0),("sword_medieval_b_scabbard", ixmesh_carry),], itp_type_one_handed_wpn|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 243 , weight(1.5)|spd_rtng(99) | weapon_length(95)|swing_damage(22 , blunt) | thrust_damage(20 ,  blunt),imodbits_sword_high ],
["arena_sword_two_handed",  "Two Handed Sword", [("arena_sword_two_handed",0)], itp_type_two_handed_wpn|itp_two_handed|itp_primary, itc_greatsword|itcf_carry_sword_back, 670 , weight(2.75)|spd_rtng(93) | weapon_length(110)|swing_damage(30 , blunt) | thrust_damage(24 ,  blunt),imodbits_sword_high ],
["arena_lance",         "Lance", [("arena_lance",0)], itp_couchable|itp_type_polearm|itp_offset_lance|itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_staff|itcf_carry_spear, 90 , weight(2.5)|spd_rtng(96) | weapon_length(150)|swing_damage(20 , blunt) | thrust_damage(25 ,  blunt),imodbits_polearm ],
["practice_staff","Practice Staff", [("wooden_staff",0)],itp_type_polearm|itp_offset_lance|itp_primary|itp_penalty_with_shield|itp_wooden_parry|itp_wooden_attack,itc_staff|itcf_carry_sword_back,9, weight(2.5)|spd_rtng(103) | weapon_length(118)|swing_damage(18,blunt) | thrust_damage(18,blunt),imodbits_none],
["practice_lance","Practice Lance", [("joust_of_peace",0)], itp_couchable|itp_type_polearm|itp_primary|itp_penalty_with_shield|itp_wooden_parry|itp_wooden_attack, itc_greatlance, 18,weight(4.25)|spd_rtng(58)|weapon_length(240)|swing_damage(0,blunt)|thrust_damage(15,blunt),imodbits_none],
["practice_shield","Practice Shield", [("shield_round_a",0)], itp_type_shield|itp_wooden_parry, itcf_carry_round_shield, 20,weight(3.5)|body_armor(1)|hit_points(200)|spd_rtng(100)|shield_width(50),imodbits_none],
["practice_bow","Practice Bow", [("hunting_bow",0), ("hunting_bow_carry",ixmesh_carry)], itp_type_bow |itp_primary|itp_two_handed,itcf_shoot_bow|itcf_carry_bow_back, 0, weight(1.5)|spd_rtng(90) | shoot_speed(40) | thrust_damage(21, blunt),imodbits_bow ],
##                                                     ("hunting_bow",0)],                  itp_type_bow|itp_two_handed|itp_primary|itp_attach_left_hand, itcf_shoot_bow, 4,weight(1.5)|spd_rtng(90)|shoot_speed(40)|thrust_damage(19,blunt),imodbits_none],
["practice_crossbow", "Practice Crossbow", [("crossbow_a",0)], itp_type_crossbow |itp_primary|itp_two_handed ,itcf_shoot_crossbow|itcf_carry_crossbow_back, 0, weight(3)|spd_rtng(42)| shoot_speed(68) | thrust_damage(32,blunt)|max_ammo(1),imodbits_crossbow],
["practice_javelin", "Practice Javelins", [("javelin",0),("javelins_quiver_new", ixmesh_carry)], itp_type_thrown |itp_primary|itp_next_item_as_melee,itcf_throw_javelin|itcf_carry_quiver_back|itcf_show_holster_when_drawn, 0, weight(5) | spd_rtng(91) | shoot_speed(28) | thrust_damage(27, blunt) | max_ammo(50) | weapon_length(75), imodbits_none],
["practice_javelin_melee", "practice_javelin_melee", [("javelin",0)], itp_type_polearm|itp_primary|itp_penalty_with_shield|itp_wooden_parry , itc_staff, 0, weight(1)|difficulty(0)|spd_rtng(91) |swing_damage(12, blunt)| thrust_damage(14,  blunt)|weapon_length(75),imodbits_polearm ],
["practice_throwing_daggers", "Throwing Daggers", [("throwing_dagger",0)], itp_type_thrown |itp_primary ,itcf_throw_knife, 0 , weight(3.5)|spd_rtng(102) | shoot_speed(25) | thrust_damage(16, blunt)|max_ammo(10)|weapon_length(0),imodbits_none],
["practice_throwing_daggers_100_amount", "Throwing Daggers", [("throwing_dagger",0)], itp_type_thrown |itp_primary ,itcf_throw_knife, 0 , weight(3.5)|spd_rtng(102) | shoot_speed(25) | thrust_damage(16, blunt)|max_ammo(100)|weapon_length(0),imodbits_none],
# ["cheap_shirt","Cheap Shirt", [("shirt",0)], itp_type_body_armor|itp_covers_legs, 0, 4,weight(1.25)|body_armor(3),imodbits_none],
["practice_horse","Practice Horse", [("saddle_horse",0)], itp_type_horse, 0, 37,body_armor(10)|horse_speed(40)|horse_maneuver(37)|horse_charge(14),imodbits_none],
["practice_arrows","Practice Arrows", [("arena_arrow",0),("flying_missile",ixmesh_flying_ammo),("quiver", ixmesh_carry)], itp_type_arrows, itcf_carry_quiver_back, 0,weight(1.5)|weapon_length(95)|max_ammo(80),imodbits_none],
## ["practice_arrows","Practice Arrows", [("arrow",0),("flying_missile",ixmesh_flying_ammo)], itp_type_arrows, 0, 31,weight(1.5)|weapon_length(95)|max_ammo(80),imodbits_none],
["practice_bolts","Practice Bolts", [("bolt",0),("flying_missile",ixmesh_flying_ammo),("bolt_bag", ixmesh_carry),("bolt_bag_b", ixmesh_carry|imodbit_large_bag)], itp_type_bolts, itcf_carry_quiver_right_vertical, 0,weight(2.25)|weapon_length(55)|max_ammo(49),imodbits_none],
["practice_arrows_10_amount","Practice Arrows", [("arrow",0),("flying_missile",ixmesh_flying_ammo),("quiver", ixmesh_carry)], itp_type_arrows, itcf_carry_quiver_back, 0,weight(1.5)|weapon_length(95)|max_ammo(10),imodbits_none],
["practice_arrows_100_amount","Practice Arrows", [("arrow",0),("flying_missile",ixmesh_flying_ammo),("quiver", ixmesh_carry)], itp_type_arrows, itcf_carry_quiver_back, 0,weight(1.5)|weapon_length(95)|max_ammo(100),imodbits_none],
["practice_bolts_9_amount","Practice Bolts", [("bolt",0),("flying_missile",ixmesh_flying_ammo),("bolt_bag", ixmesh_carry),("bolt_bag_b", ixmesh_carry|imodbit_large_bag)], itp_type_bolts, itcf_carry_quiver_right_vertical, 0,weight(2.25)|weapon_length(55)|max_ammo(9),imodbits_none],
["practice_boots", "Practice Boots", [("boot_nomad_a",0)], itp_type_foot_armor|itp_attach_armature|itp_civilian, 0, 40, weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(4), imodbits_cloth ],
["red_tourney_armor","Red Tourney Armor", [("tourn_armor_a",0)], itp_type_body_armor|itp_covers_legs, 0, 152,weight(15.0)|body_armor(20)|leg_armor(6),imodbits_none],
["blue_tourney_armor","Blue Tourney Armor", [("mail_shirt",0)], itp_type_body_armor|itp_covers_legs, 0, 152,weight(15.0)|body_armor(20)|leg_armor(6),imodbits_none],
["green_tourney_armor","Green Tourney Armor", [("leather_vest",0)], itp_type_body_armor|itp_covers_legs, 0, 152,weight(15.0)|body_armor(20)|leg_armor(6),imodbits_none],
["gold_tourney_armor","Gold Tourney Armor", [("padded_armor",0)], itp_type_body_armor|itp_covers_legs, 0, 152,weight(15.0)|body_armor(20)|leg_armor(6),imodbits_none],
["red_tourney_helmet","Red Tourney Helmet",[("flattop_helmet",0)],itp_type_head_armor,0,126, weight(2)|head_armor(16),imodbits_none],
["blue_tourney_helmet","Blue Tourney Helmet",[("segmented_helm",0)],itp_type_head_armor,0,126, weight(2)|head_armor(16),imodbits_none],
["green_tourney_helmet","Green Tourney Helmet",[("hood_c",0)],itp_type_head_armor,0,126, weight(2)|head_armor(16),imodbits_none],
["gold_tourney_helmet","Gold Tourney Helmet",[("hood_a",0)],itp_type_head_armor,0,126, weight(2)|head_armor(16),imodbits_none],

["arena_shield_red", "Shield", [("arena_shield_red",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  42 , weight(2)|hit_points(360)|body_armor(1)|spd_rtng(100)|shield_width(60),imodbits_shield ],
["arena_shield_blue", "Shield", [("arena_shield_blue",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  42 , weight(2)|hit_points(360)|body_armor(1)|spd_rtng(100)|shield_width(60),imodbits_shield ],
["arena_shield_green", "Shield", [("arena_shield_green",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  42 , weight(2)|hit_points(360)|body_armor(1)|spd_rtng(100)|shield_width(60),imodbits_shield ],
["arena_shield_yellow", "Shield", [("arena_shield_yellow",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  42 , weight(2)|hit_points(360)|body_armor(1)|spd_rtng(100)|shield_width(60),imodbits_shield ],

["arena_armor_white", "Arena Armor White", [("arena_armorW_new",0)], itp_type_body_armor  |itp_covers_legs ,0, 650 , weight(16)|abundance(100)|head_armor(0)|body_armor(29)|leg_armor(13), imodbits_armor ],
["arena_armor_red", "Arena Armor Red", [("arena_armorR_new",0)], itp_type_body_armor  |itp_covers_legs ,0, 650 , weight(16)|abundance(100)|head_armor(0)|body_armor(29)|leg_armor(13), imodbits_armor ],
["arena_armor_blue", "Arena Armor Blue", [("arena_armorB_new",0)], itp_type_body_armor  |itp_covers_legs ,0, 650 , weight(16)|abundance(100)|head_armor(0)|body_armor(29)|leg_armor(13), imodbits_armor ],
["arena_armor_green", "Arena Armor Green", [("arena_armorG_new",0)], itp_type_body_armor  |itp_covers_legs ,0, 650 , weight(16)|abundance(100)|head_armor(0)|body_armor(29)|leg_armor(13), imodbits_armor ],
["arena_armor_yellow", "Arena Armor Yellow", [("arena_armorY_new",0)], itp_type_body_armor  |itp_covers_legs ,0, 650 , weight(16)|abundance(100)|head_armor(0)|body_armor(29)|leg_armor(13), imodbits_armor ],
["arena_tunic_white", "Arena Tunic White ", [("arena_tunicW_new",0)], itp_type_body_armor |itp_covers_legs ,0, 47 , weight(2)|abundance(100)|head_armor(0)|body_armor(16)|leg_armor(6), imodbits_cloth ],
["arena_tunic_red", "Arena Tunic Red", [("arena_tunicR_new",0)], itp_type_body_armor |itp_covers_legs ,0, 27 , weight(2)|abundance(100)|head_armor(0)|body_armor(16)|leg_armor(6), imodbits_cloth ],
["arena_tunic_blue", "Arena Tunic Blue", [("arena_tunicB_new",0)], itp_type_body_armor |itp_covers_legs ,0, 27 , weight(2)|abundance(100)|head_armor(0)|body_armor(16)|leg_armor(6), imodbits_cloth ],
["arena_tunic_green", "Arena Tunic Green", [("arena_tunicG_new",0)], itp_type_body_armor |itp_covers_legs ,0, 27 , weight(2)|abundance(100)|head_armor(0)|body_armor(16)|leg_armor(6), imodbits_cloth ],
["arena_tunic_yellow", "Arena Tunic Yellow", [("arena_tunicY_new",0)], itp_type_body_armor |itp_covers_legs ,0, 27 , weight(2)|abundance(100)|head_armor(0)|body_armor(16)|leg_armor(6), imodbits_cloth ],
#headwear
["arena_helmet_red", "Arena Helmet Red", [("arena_helmetR",0)], itp_type_head_armor|itp_fit_to_head ,0, 187 , weight(1.25)|abundance(100)|head_armor(26)|body_armor(0)|leg_armor(0), imodbits_plate ],
["arena_helmet_blue", "Arena Helmet Blue", [("arena_helmetB",0)], itp_type_head_armor|itp_fit_to_head ,0, 187 , weight(1.25)|abundance(100)|head_armor(26)|body_armor(0)|leg_armor(0), imodbits_plate ],
["arena_helmet_green", "Arena Helmet Green", [("arena_helmetG",0)], itp_type_head_armor|itp_fit_to_head ,0, 187 , weight(1.25)|abundance(100)|head_armor(26)|body_armor(0)|leg_armor(0), imodbits_plate ],
["arena_helmet_yellow", "Arena Helmet Yellow", [("arena_helmetY",0)], itp_type_head_armor|itp_fit_to_head ,0, 187 , weight(1.25)|abundance(100)|head_armor(26)|body_armor(0)|leg_armor(0), imodbits_plate ],
["steppe_helmet_white", "Steppe Helmet White", [("steppe_helmetW",0)], itp_type_head_armor|itp_fit_to_head ,0, 187 , weight(1.25)|abundance(100)|head_armor(20)|body_armor(0)|leg_armor(0), imodbits_plate ],
["steppe_helmet_red", "Steppe Helmet Red", [("steppe_helmetR",0)], itp_type_head_armor|itp_fit_to_head ,0, 187 , weight(1.25)|abundance(100)|head_armor(20)|body_armor(0)|leg_armor(0), imodbits_plate ],
["steppe_helmet_blue", "Steppe Helmet Blue", [("steppe_helmetB",0)], itp_type_head_armor|itp_fit_to_head ,0, 187 , weight(1.25)|abundance(100)|head_armor(20)|body_armor(0)|leg_armor(0), imodbits_plate ],
["steppe_helmet_green", "Steppe Helmet Green", [("steppe_helmetG",0)], itp_type_head_armor|itp_fit_to_head ,0, 187 , weight(1.25)|abundance(100)|head_armor(20)|body_armor(0)|leg_armor(0), imodbits_plate ],
["steppe_helmet_yellow", "Steppe Helmet Yellow", [("steppe_helmetY",0)], itp_type_head_armor|itp_fit_to_head ,0, 187 , weight(1.25)|abundance(100)|head_armor(20)|body_armor(0)|leg_armor(0), imodbits_plate ],
["tourney_helm_white", "Tourney Helm White", [("tourney_helmR",0)], itp_type_head_armor|itp_covers_head,0, 760 , weight(2.75)|abundance(100)|head_armor(30)|body_armor(0)|leg_armor(0), imodbits_plate ],
["tourney_helm_red", "Tourney Helm Red", [("tourney_helmR",0)], itp_type_head_armor|itp_covers_head,0, 760 , weight(2.75)|abundance(100)|head_armor(30)|body_armor(0)|leg_armor(0), imodbits_plate ],
["tourney_helm_blue", "Tourney Helm Blue", [("tourney_helmB",0)], itp_type_head_armor|itp_covers_head,0, 760 , weight(2.75)|abundance(100)|head_armor(30)|body_armor(0)|leg_armor(0), imodbits_plate ],
["tourney_helm_green", "Tourney Helm Green", [("tourney_helmG",0)], itp_type_head_armor|itp_covers_head,0, 760 , weight(2.75)|abundance(100)|head_armor(30)|body_armor(0)|leg_armor(0), imodbits_plate ],
["tourney_helm_yellow", "Tourney Helm Yellow", [("tourney_helmY",0)], itp_type_head_armor|itp_covers_head,0, 760 , weight(2.75)|abundance(100)|head_armor(30)|body_armor(0)|leg_armor(0), imodbits_plate ],
["arena_turban_red", "Arena Turban", [("tuareg_open",0)], itp_type_head_armor|itp_fit_to_head ,0, 187 , weight(1.25)|abundance(100)|head_armor(26)|body_armor(0)|leg_armor(0), imodbits_plate ],
["arena_turban_blue", "Arena Turban", [("tuareg_open",0)], itp_type_head_armor|itp_fit_to_head ,0, 187 , weight(1.25)|abundance(100)|head_armor(26)|body_armor(0)|leg_armor(0), imodbits_plate ],
["arena_turban_green", "Arena Turban", [("tuareg_open",0)], itp_type_head_armor|itp_fit_to_head ,0, 187 , weight(1.25)|abundance(100)|head_armor(26)|body_armor(0)|leg_armor(0), imodbits_plate ],
["arena_turban_yellow", "Arena Turban", [("tuareg_open",0)], itp_type_head_armor|itp_fit_to_head ,0, 187 , weight(1.25)|abundance(100)|head_armor(26)|body_armor(0)|leg_armor(0), imodbits_plate ],

# A treatise on The Method of Mechanical Theorems Archimedes

#This book must be at the beginning of readable books
["book_tactics","De Re Militari", [("book_a",0)], itp_type_book|itp_merchandise, 0, 4000,weight(2)|abundance(100),imodbits_none],
["book_persuasion","Rhetorica ad Herennium", [("book_b",0)], itp_type_book|itp_merchandise, 0, 5000,weight(2)|abundance(100),imodbits_none],
["book_leadership","The Life of Alixenus the Great", [("book_d",0)], itp_type_book|itp_merchandise, 0, 4200,weight(2)|abundance(100),imodbits_none],
["book_intelligence","Essays on Logic", [("book_e",0)], itp_type_book|itp_merchandise, 0, 2900,weight(2)|abundance(100),imodbits_none],
["book_trade","A Treatise on the Value of Things", [("book_f",0)], itp_type_book|itp_merchandise, 0, 3100,weight(2)|abundance(100),imodbits_none],
["book_weapon_mastery", "On the Art of Fighting with Swords", [("book_d",0)], itp_type_book|itp_merchandise, 0, 4200,weight(2)|abundance(100),imodbits_none],
["book_engineering","Method of Mechanical Theorems", [("book_open",0)], itp_type_book|itp_merchandise, 0, 4000,weight(2)|abundance(100),imodbits_none],

#Reference books
#This book must be at the beginning of reference books
["book_wound_treatment_reference","The Book of Healing", [("book_c",0)], itp_type_book|itp_merchandise, 0, 3500,weight(2)|abundance(10),imodbits_none],
["book_training_reference","Manual of Arms", [("book_open",0)], itp_type_book|itp_merchandise, 0, 3500,weight(2)|abundance(10),imodbits_none],
["book_surgery_reference","The Great Book of Surgery", [("book_c",0)], itp_type_book|itp_merchandise, 0, 3500,weight(2)|abundance(10),imodbits_none],

# LAV MODIFICATIONS START (TRADE GOODS)

#other trade goods (first one is spice)
#["spice","Spice", [("spice_sack",0)], itp_merchandise|itp_type_goods|itp_consumable, 0, 880,weight(40)|abundance(25)|max_ammo(50),imodbits_none],
#["salt","Salt", [("salt_sack",0)], itp_merchandise|itp_type_goods, 0, 255,weight(50)|abundance(120),imodbits_none],
#
#
##["flour","Flour", [("salt_sack",0)], itp_merchandise|itp_type_goods|itp_consumable, 0, 40,weight(50)|abundance(100)|food_quality(45)|max_ammo(50),imodbits_none],
#
#["oil","Oil", [("oil",0)], itp_merchandise|itp_type_goods|itp_consumable, 0, 450,weight(50)|abundance(60)|max_ammo(50),imodbits_none],
#
#["pottery","Pottery", [("jug",0)], itp_merchandise|itp_type_goods, 0, 100,weight(50)|abundance(90),imodbits_none],
#
#["raw_flax","Flax Bundle", [("raw_flax",0)], itp_merchandise|itp_type_goods, 0, 150,weight(40)|abundance(90),imodbits_none],
#["linen","Linen", [("linen",0)], itp_merchandise|itp_type_goods, 0, 250,weight(40)|abundance(90),imodbits_none],
#
#["wool","Wool", [("wool_sack",0)], itp_merchandise|itp_type_goods, 0, 130,weight(40)|abundance(90),imodbits_none],
#["wool_cloth","Wool Cloth", [("wool_cloth",0)], itp_merchandise|itp_type_goods, 0, 250,weight(40)|abundance(90),imodbits_none],
#
#["raw_silk","Raw Silk", [("raw_silk_bundle",0)], itp_merchandise|itp_type_goods, 0, 600,weight(30)|abundance(90),imodbits_none],
#["raw_dyes","Dyes", [("dyes",0)], itp_merchandise|itp_type_goods, 0, 200,weight(10)|abundance(90),imodbits_none],
#["velvet","Velvet", [("velvet",0)], itp_merchandise|itp_type_goods, 0, 1025,weight(40)|abundance(30),imodbits_none],
#
#["iron","Iron", [("iron",0)], itp_merchandise|itp_type_goods, 0,264,weight(60)|abundance(60),imodbits_none],
#["tools","Tools", [("iron_hammer",0)], itp_merchandise|itp_type_goods, 0, 410,weight(50)|abundance(90),imodbits_none],
#
## NE quests
#["lumber",  "Lumber",  [("timber", 0)],      itp_merchandise|itp_type_goods, 0,  204, weight(60)|abundance( 80), imodbits_none],
## NE end quests
#["raw_leather","Hides", [("leatherwork_inventory",0)], itp_merchandise|itp_type_goods, 0, 120,weight(40)|abundance(90),imodbits_none],
#["leatherwork","Leatherwork", [("leatherwork_frame",0)], itp_merchandise|itp_type_goods, 0, 220,weight(40)|abundance(90),imodbits_none],
#["raw_date_fruit","Date Fruit", [("date_inventory",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 120,weight(40)|food_quality(10)|max_ammo(10),imodbits_none],
#["furs","Furs", [("fur_pack",0)], itp_merchandise|itp_type_goods, 0, 391,weight(40)|abundance(90),imodbits_none],
#
#
## ["dry_bread", "wheat_sack", itp_type_goods|itp_consumable, 0, slt_none,view_goods,95,weight(2),max_ammo(50),imodbits_none],
##foods (first one is smoked_fish)
#["smoked_fish","Smoked Fish", [("smoked_fish",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(110)|food_quality(50)|max_ammo(50),imodbits_none],
#["cheese","Cheese", [("cheese_b",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 75,weight(6)|abundance(110)|food_quality(40)|max_ammo(30),imodbits_none],
#["honey","Honey", [("honey_pot",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 220,weight(5)|abundance(110)|food_quality(40)|max_ammo(30),imodbits_none],
#["sausages","Sausages", [("sausages",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 85,weight(10)|abundance(110)|food_quality(40)|max_ammo(40),imodbits_none],
#["cabbages","Cabbages", [("cabbage",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 30,weight(15)|abundance(110)|food_quality(40)|max_ammo(50),imodbits_none],
#["dried_meat","Dried Meat", [("smoked_meat",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 85,weight(15)|abundance(100)|food_quality(70)|max_ammo(50),imodbits_none],
#["apples","Fruit", [("apple_basket",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 44,weight(20)|abundance(110)|food_quality(40)|max_ammo(50),imodbits_none],
#["raw_grapes","Grapes", [("grapes_inventory",0)], itp_merchandise|itp_consumable|itp_type_goods, 0, 75,weight(40)|abundance(90)|food_quality(10)|max_ammo(10),imodbits_none], #x2 for wine
#["raw_olives","Olives", [("olive_inventory",0)], itp_merchandise|itp_consumable|itp_type_goods, 0, 100,weight(40)|abundance(90)|food_quality(10)|max_ammo(10),imodbits_none], #x3 for oil
#["grain","Grain", [("wheat_sack",0)], itp_merchandise|itp_type_goods|itp_consumable, 0, 30,weight(30)|abundance(110)|food_quality(40)|max_ammo(50),imodbits_none],
## NE misc
## wine and ale shouldn't be consumables, to prevent low quality feasts
#["hardtack",    "Hardtack",    [("bread_slice_a", 0)],   itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 100, weight( 5)|abundance(110)|food_quality(40)|max_ammo(150), imodbits_none],
#["wine","Wine", [("amphora_slim",0)], itp_merchandise|itp_type_goods, 0, 220,weight(30)|abundance(60)|max_ammo(50),imodbits_none],
#["ale","Ale", [("ale_barrel",0)], itp_merchandise|itp_type_goods, 0, 120,weight(30)|abundance(70)|max_ammo(50),imodbits_none],
## Ne end misc
#["cattle_meat","Beef", [("raw_meat",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 80,weight(20)|abundance(100)|food_quality(80)|max_ammo(50),imodbits_none],
#["bread","Bread", [("bread_a",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 50,weight(30)|abundance(110)|food_quality(40)|max_ammo(50),imodbits_none],
#["chicken","Chicken", [("chicken",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 95,weight(10)|abundance(110)|food_quality(40)|max_ammo(50),imodbits_none],
#["pork","Pork", [("pork",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 75,weight(15)|abundance(100)|food_quality(70)|max_ammo(50),imodbits_none],
#["butter","Butter", [("butter_pot",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 150,weight(6)|abundance(110)|food_quality(40)|max_ammo(30),imodbits_none],

#other trade goods (first one is spice)
["spice","Spice", [("spice_sack",0)], itp_merchandise|itp_type_goods|itp_consumable, 0, 880,weight(40)|abundance(25)|max_ammo(50),imodbit_fine|imodbit_strong|imodbit_lordly|imodbit_well_made|imodbit_exquisite|imodbit_large_bag],
["salt","Salt", [("salt_sack",0)], itp_merchandise|itp_type_goods, 0, 255,weight(50)|abundance(120),imodbit_fine|imodbit_well_made|imodbit_exquisite|imodbit_rough|imodbit_large_bag],
["oil","Oil", [("oil",0)], itp_merchandise|itp_type_goods|itp_consumable, 0, 450,weight(50)|abundance(60)|max_ammo(50),imodbit_fine|imodbit_thick|imodbit_lordly|imodbit_well_made|imodbit_exquisite],
["pottery","Pottery", [("jug",0)], itp_merchandise|itp_type_goods, 0, 100,weight(50)|abundance(90),imodbit_battered|imodbit_crude|imodbit_fine|imodbit_masterwork|imodbit_strong|imodbit_sturdy|imodbit_hardened|imodbit_lordly|imodbit_well_made|imodbit_exquisite|imodbit_rough],
["raw_flax","Flax Bundle", [("raw_flax",0)], itp_merchandise|itp_type_goods, 0, 150,weight(40)|abundance(90),imodbit_fine|imodbit_strong|imodbit_sturdy|imodbit_exquisite|imodbit_rough],
["linen","Linen", [("linen",0)], itp_merchandise|itp_type_goods, 0, 250,weight(40)|abundance(90),imodbit_battered|imodbit_fine|imodbit_masterwork|imodbit_strong|imodbit_tattered|imodbit_ragged|imodbit_sturdy|imodbit_thick|imodbit_lordly|imodbit_well_made|imodbit_exquisite|imodbit_rough],
["wool","Wool", [("wool_sack",0)], itp_merchandise|itp_type_goods, 0, 130,weight(40)|abundance(90),imodbit_fine|imodbit_strong|imodbit_sturdy|imodbit_exquisite|imodbit_rough|imodbit_large_bag],
["wool_cloth","Wool Cloth", [("wool_cloth",0)], itp_merchandise|itp_type_goods, 0, 250,weight(40)|abundance(90),imodbit_battered|imodbit_fine|imodbit_masterwork|imodbit_strong|imodbit_tattered|imodbit_ragged|imodbit_sturdy|imodbit_thick|imodbit_lordly|imodbit_well_made|imodbit_exquisite|imodbit_rough],
["raw_silk","Raw Silk", [("raw_silk_bundle",0)], itp_merchandise|itp_type_goods, 0, 600,weight(30)|abundance(90),imodbit_fine|imodbit_strong|imodbit_sturdy|imodbit_exquisite|imodbit_rough],
["raw_dyes","Dyes", [("dyes",0)], itp_merchandise|itp_type_goods, 0, 200,weight(10)|abundance(90),imodbit_fine|imodbit_strong|imodbit_well_made|imodbit_exquisite],
["velvet","Velvet", [("velvet",0)], itp_merchandise|itp_type_goods, 0, 1025,weight(40)|abundance(30),imodbit_fine|imodbit_masterwork|imodbit_strong|imodbit_tattered|imodbit_ragged|imodbit_sturdy|imodbit_thick|imodbit_lordly|imodbit_well_made|imodbit_exquisite],
["iron","Iron", [("iron",0)], itp_merchandise|itp_type_goods, 0,264,weight(60)|abundance(60),imodbit_rusty|imodbit_fine|imodbit_tempered|imodbit_hardened],
["tools","Tools", [("iron_hammer",0)], itp_merchandise|itp_type_goods, 0, 410,weight(50)|abundance(90),imodbit_rusty|imodbit_bent|imodbit_chipped|imodbit_battered|imodbit_crude|imodbit_fine|imodbit_tempered|imodbit_masterwork|imodbit_strong|imodbit_hardened|imodbit_well_made|imodbit_rough],
# NE quests
["lumber", "Lumber", [("timber", 0)], itp_merchandise|itp_type_goods, 0, 204, weight(60)|abundance( 80), imodbit_crude|imodbit_fine|imodbit_strong|imodbit_hardened|imodbit_reinforced|imodbit_rough],
# NE end quests
["raw_leather","Hides", [("leatherwork_inventory",0)], itp_merchandise|itp_type_goods, 0, 120,weight(40)|abundance(90),imodbit_battered|imodbit_fine|imodbit_strong|imodbit_sturdy|imodbit_thick|imodbit_exquisite|imodbit_rough],
["leatherwork","Leatherwork", [("leatherwork_frame",0)], itp_merchandise|itp_type_goods, 0, 220,weight(40)|abundance(90),imodbit_battered|imodbit_fine|imodbit_masterwork|imodbit_strong|imodbit_tattered|imodbit_ragged|imodbit_sturdy|imodbit_thick|imodbit_hardened|imodbit_reinforced|imodbit_lordly|imodbit_well_made|imodbit_exquisite|imodbit_rough],
["raw_date_fruit","Date Fruit", [("date_inventory",0),("dates_basket",ixmesh_inventory)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 120, weight(25)|food_quality(50)|max_ammo(50),imodbit_fine|imodbit_exquisite],
["furs","Furs", [("fur_pack",0)], itp_merchandise|itp_type_goods, 0, 391,weight(40)|abundance(90),imodbit_battered|imodbit_fine|imodbit_tattered|imodbit_ragged|imodbit_sturdy|imodbit_thick|imodbit_lordly|imodbit_exquisite|imodbit_large_bag],

#foods (first one is smoked_fish)
["smoked_fish","Smoked Fish", [("smoked_fish",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(110)|food_quality(50)|max_ammo(50),imodbit_fine|imodbit_lordly|imodbit_exquisite],
["cheese","Cheese", [("cheese_b",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 75,weight(6)|abundance(110)|food_quality(40)|max_ammo(30),imodbit_fine|imodbit_lordly|imodbit_well_made|imodbit_exquisite],
["honey","Honey", [("honey_pot",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 220,weight(5)|abundance(110)|food_quality(40)|max_ammo(30),imodbit_fine|imodbit_lordly|imodbit_exquisite],
["sausages","Sausages", [("sausages",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 85,weight(10)|abundance(110)|food_quality(40)|max_ammo(40),imodbit_fine|imodbit_lordly|imodbit_well_made|imodbit_exquisite],
["cabbages","Cabbages", [("cabbage",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 30,weight(15)|abundance(110)|food_quality(40)|max_ammo(50),imodbit_fine|imodbit_lordly|imodbit_exquisite],
["dried_meat","Dried Meat", [("smoked_meat",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 85,weight(15)|abundance(100)|food_quality(70)|max_ammo(50),imodbit_fine],
["apples","Fruit", [("apple_basket",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 44,weight(20)|abundance(110)|food_quality(40)|max_ammo(50),imodbit_fine|imodbit_lordly|imodbit_exquisite],
["raw_grapes","Grapes", [("grapes_inventory",0),("grapes_basket",ixmesh_inventory)], itp_merchandise|itp_consumable|itp_type_goods, 0, 75, weight(20)|abundance(90)|food_quality(20)|max_ammo(40),imodbit_fine|imodbit_lordly|imodbit_exquisite],
["raw_olives","Olives", [("olive_inventory",0),("olives_basket",ixmesh_inventory)], itp_merchandise|itp_consumable|itp_type_goods, 0, 100, weight(15)|abundance(90)|food_quality(25)|max_ammo(35),imodbit_fine|imodbit_lordly|imodbit_exquisite],
["grain","Grain", [("wheat_sack",0)], itp_merchandise|itp_type_goods|itp_consumable, 0, 30,weight(30)|abundance(110)|food_quality(40)|max_ammo(50),imodbit_fine|imodbit_large_bag],
# NE misc
# wine and ale shouldn't be consumables, to prevent low quality feasts
["hardtack","Hardtack",[("bread_slice_a", 0)],itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 100, weight( 5)|abundance(110)|food_quality(40)|max_ammo(150),imodbit_well_made],
["wine","Wine", [("amphora_slim",0)], itp_merchandise|itp_type_goods, 0, 220,weight(30)|abundance(60)|max_ammo(50),imodbit_fine|imodbit_strong|imodbit_lordly|imodbit_well_made|imodbit_exquisite],
["ale","Ale", [("ale_barrel",0)], itp_merchandise|itp_type_goods, 0, 120,weight(30)|abundance(70)|max_ammo(50),imodbit_fine|imodbit_strong|imodbit_lordly|imodbit_well_made|imodbit_exquisite],
# Ne end misc
["cattle_meat","Beef", [("raw_meat",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 80,weight(20)|abundance(100)|food_quality(80)|max_ammo(50),imodbit_plain],
["bread","Bread", [("bread_a",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 50,weight(30)|abundance(110)|food_quality(40)|max_ammo(50),imodbit_fine|imodbit_lordly|imodbit_well_made|imodbit_exquisite],
["chicken","Chicken", [("chicken",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 95,weight(10)|abundance(110)|food_quality(40)|max_ammo(50),imodbit_plain],
["pork","Pork", [("pork",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 75,weight(15)|abundance(100)|food_quality(70)|max_ammo(50),imodbit_plain],
["butter","Butter", [("butter_pot",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 150,weight(6)|abundance(110)|food_quality(40)|max_ammo(30),imodbit_fine|imodbit_lordly|imodbit_well_made|imodbit_exquisite],

# AYNIL:
#    ["food_fresh_fish",  "Fish", [("fish_a", 0)],               itp_merchandise|itp_type_goods|itp_consumable, 0,  1, weight(15)|abundance(100)|max_ammo(40), imodbit.fine|imodbit.delicious|imodbit.magnificent|imodbit.exquisite|imodbit.lordly], 
#    ["food_fried_fish",  "Fried Fish", [("fish_roasted_a", 0)], itp_merchandise|itp_type_goods|itp_consumable, 0,  1, weight(15)|abundance(100)|max_ammo(40), imodbit.fine|imodbit.delicious|imodbit.magnificent|imodbit.exquisite|imodbit.lordly], 
#    ["food_garlic",      "Garlic", [("garlic", 0)],             itp_merchandise|itp_type_goods|itp_consumable, 0,  1, weight(15)|abundance(100)|max_ammo(10), imodbit.fine|imodbit.delicious|imodbit.strong|imodbit.magnificent|imodbit.exquisite|imodbit.lordly], 
#    ["water", "Water", [("barrel", 0)], itp_merchandise|itp_type_goods|itp_consumable, 0, 25, weight(30)|abundance(100)|max_ammo(50), imodbits_none], 

# LAV MODIFICATIONS END (TRADE GOODS)

#Would like to remove flour altogether and reduce chicken, pork and butter (perishables) to non-trade items. Apples could perhaps become a generic "fruit", also representing dried fruit and grapes
# Armagan: changed order so that it'll be easier to remove them from trade goods if necessary.
#************************************************************************************************
# ITEMS before this point are hardcoded into item_codes.h and their order should not be changed!
#************************************************************************************************

# Quest Items

["siege_supply","Supplies", [("ale_barrel",0)], itp_type_goods, 0, 96,weight(40)|abundance(70),imodbits_none],
["quest_wine","Wine", [("amphora_slim",0)], itp_type_goods, 0, 46,weight(40)|abundance(60)|max_ammo(50),imodbits_none],
["quest_ale","Ale", [("ale_barrel",0)], itp_type_goods, 0, 31,weight(40)|abundance(70)|max_ammo(50),imodbits_none],


# Tutorial Items

["tutorial_sword", "Sword", [("long_sword",0),("scab_longsw_a", ixmesh_carry)], itp_type_one_handed_wpn|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 0 , weight(1.5)|difficulty(0)|spd_rtng(100) | weapon_length(102)|swing_damage(18 , cut) | thrust_damage(15 ,  pierce),imodbits_sword ],
["tutorial_axe", "Axe", [("iron_ax",0)], itp_type_two_handed_wpn| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry, itc_nodachi|itcf_carry_axe_back, 0 , weight(4)|difficulty(0)|spd_rtng(91) | weapon_length(108)|swing_damage(19 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["tutorial_spear", "Spear", [("spear",0)], itp_type_polearm| itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_spear, 0 , weight(4.5)|difficulty(0)|spd_rtng(80) | weapon_length(158)|swing_damage(0 , cut) | thrust_damage(19 ,  pierce),imodbits_polearm ],
["tutorial_club", "Club", [("club",0)], itp_type_one_handed_wpn| itp_primary|itp_wooden_parry|itp_wooden_attack, itc_scimitar, 0 , weight(2.5)|difficulty(0)|spd_rtng(95) | weapon_length(95)|swing_damage(11 , blunt) | thrust_damage(0 ,  pierce),imodbits_none ],
["tutorial_battle_axe", "Battle Axe", [("battle_ax",0)], itp_type_two_handed_wpn| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry, itc_nodachi|itcf_carry_axe_back, 0 , weight(5)|difficulty(0)|spd_rtng(88) | weapon_length(108)|swing_damage(27 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["tutorial_arrows","Arrows", [("arrow",0),("flying_missile",ixmesh_flying_ammo),("quiver", ixmesh_carry)], itp_type_arrows, itcf_carry_quiver_back, 0,weight(3)|abundance(160)|weapon_length(95)|thrust_damage(0,pierce)|max_ammo(20),imodbits_none],
["tutorial_bolts","Bolts", [("bolt",0),("flying_missile",ixmesh_flying_ammo),("bolt_bag", ixmesh_carry),("bolt_bag_b", ixmesh_carry|imodbit_large_bag)], itp_type_bolts, itcf_carry_quiver_right_vertical, 0,weight(2.25)|abundance(90)|weapon_length(63)|thrust_damage(0,pierce)|max_ammo(18),imodbits_none],
["tutorial_short_bow", "Short Bow", [("short_bow",0),("short_bow_carry",ixmesh_carry)], itp_type_bow |itp_primary|itp_two_handed ,itcf_shoot_bow|itcf_carry_bow_back, 0 , weight(1)|difficulty(0)|spd_rtng(98) | shoot_speed(49) | thrust_damage(12 ,  pierce  ),imodbits_bow ],
["tutorial_crossbow", "Crossbow", [("crossbow_a",0)], itp_type_crossbow |itp_primary|itp_two_handed|itp_cant_reload_on_horseback ,itcf_shoot_crossbow|itcf_carry_crossbow_back, 0 , weight(3)|difficulty(0)|spd_rtng(42)|  shoot_speed(68) | thrust_damage(32,pierce)|max_ammo(1),imodbits_crossbow ],
["tutorial_throwing_daggers", "Throwing Daggers", [("throwing_dagger",0)], itp_type_thrown |itp_primary ,itcf_throw_knife, 0 , weight(3.5)|difficulty(0)|spd_rtng(102) | shoot_speed(25) | thrust_damage(16 ,  cut)|max_ammo(14)|weapon_length(0),imodbits_none],
["tutorial_saddle_horse", "Saddle Horse", [("saddle_horse",0)], itp_type_horse, 0, 0,abundance(90)|body_armor(3)|difficulty(0)|horse_speed(40)|horse_maneuver(38)|horse_charge(8),imodbits_horse_basic],
["tutorial_shield", "Kite Shield", [("shield_kite_a",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  118 , weight(2.5)|hit_points(480)|body_armor(1)|spd_rtng(82)|shield_width(150),imodbits_shield ],
["tutorial_staff_no_attack","Staff", [("wooden_staff",0)],itp_type_polearm|itp_offset_lance|itp_primary|itp_penalty_with_shield|itp_wooden_parry|itp_wooden_attack,itc_parry_polearm|itcf_carry_sword_back,9, weight(3.5)|spd_rtng(120) | weapon_length(115)|swing_damage(0,blunt) | thrust_damage(0,blunt),imodbits_none],
["tutorial_staff","Staff", [("wooden_staff",0)],itp_type_polearm|itp_offset_lance|itp_primary|itp_penalty_with_shield|itp_wooden_parry|itp_wooden_attack,itc_staff|itcf_carry_sword_back,9, weight(3.5)|spd_rtng(120) | weapon_length(115)|swing_damage(16,blunt) | thrust_damage(16,blunt),imodbits_none],

# Horses: sumpter horse/ pack horse, saddle horse, steppe horse, warm blood, geldling, stallion,   war mount, charger,
# Carthorse, hunter, heavy hunter, hackney, palfrey, courser, destrier.
# ["sumpter_horse","Sumpter Horse", [("sumpter_horse",0)], itp_merchandise|itp_type_horse, 0, 134,abundance(90)|hit_points(100)|body_armor(14)|difficulty(1)|horse_speed(37)|horse_maneuver(39)|horse_charge(9)|horse_scale(100),imodbits_horse_basic],
# ["saddle_horse","Saddle Horse", [("saddle_horse",0),("horse_c",imodbits_horse_good)], itp_merchandise|itp_type_horse, 0, 240,abundance(90)|hit_points(100)|body_armor(8)|difficulty(1)|horse_speed(45)|horse_maneuver(44)|horse_charge(10)|horse_scale(104),imodbits_horse_basic],
# ["steppe_horse","Steppe Horse", [("steppe_horse",0)], itp_merchandise|itp_type_horse, 0, 192,abundance(80)|hit_points(120)|body_armor(10)|difficulty(2)|horse_speed(40)|horse_maneuver(51)|horse_charge(8)|horse_scale(98),imodbits_horse_basic, [], [fac_kingdom_2, fac_kingdom_3]],
# ["arabian_horse_a","Desert Horse", [("arabian_horse_a",0)], itp_merchandise|itp_type_horse, 0, 550,abundance(80)|hit_points(110)|body_armor(10)|difficulty(2)|horse_speed(42)|horse_maneuver(50)|horse_charge(12)|horse_scale(100),imodbits_horse_basic|imodbit_champion, [], [fac_kingdom_3, fac_kingdom_6]],
# ["courser","Courser", [("courser",0)], itp_merchandise|itp_type_horse, 0, 600,abundance(70)|body_armor(12)|hit_points(110)|difficulty(2)|horse_speed(50)|horse_maneuver(44)|horse_charge(12)|horse_scale(106),imodbits_horse_basic|imodbit_champion],
# ["arabian_horse_b","Sarranid Horse", [("arabian_horse_b",0)], itp_merchandise|itp_type_horse, 0, 700,abundance(80)|hit_points(120)|body_armor(10)|difficulty(3)|horse_speed(43)|horse_maneuver(54)|horse_charge(16)|horse_scale(100),imodbits_horse_basic|imodbit_champion, [], [fac_kingdom_6]],
# ["hunter","Hunter", [("hunting_horse",0),("hunting_horse",imodbits_horse_good)], itp_merchandise|itp_type_horse, 0, 810,abundance(60)|hit_points(160)|body_armor(18)|difficulty(3)|horse_speed(43)|horse_maneuver(44)|horse_charge(24)|horse_scale(108),imodbits_horse_basic|imodbit_champion],
# ["warhorse","War Horse", [("warhorse_chain",0)], itp_merchandise|itp_type_horse, 0, 1224,abundance(50)|hit_points(165)|body_armor(40)|difficulty(4)|horse_speed(40)|horse_maneuver(41)|horse_charge(28)|horse_scale(110),imodbits_horse_basic|imodbit_champion],
# ["charger","Charger", [("charger_new",0)], itp_merchandise|itp_type_horse, 0, 1811,abundance(40)|hit_points(165)|body_armor(58)|difficulty(4)|horse_speed(40)|horse_maneuver(44)|horse_charge(32)|horse_scale(112),imodbits_horse_basic|imodbit_champion, [], [fac_kingdom_1, fac_kingdom_5]],
# ["warhorse_sarranid","Sarranian War Horse", [("warhorse_sarranid",0)], itp_merchandise|itp_type_horse, 0, 1811,abundance(40)|hit_points(165)|body_armor(58)|difficulty(4)|horse_speed(40)|horse_maneuver(44)|horse_charge(32)|horse_scale(112),imodbits_horse_basic|imodbit_champion, [], [fac_kingdom_6]],
# ["warhorse_steppe","Steppe Charger", [("warhorse_steppe",0)], itp_merchandise|itp_type_horse, 0, 1400,abundance(45)|hit_points(150)|body_armor(40)|difficulty(4)|horse_speed(40)|horse_maneuver(50)|horse_charge(28)|horse_scale(112),imodbits_horse_basic|imodbit_champion, [], [fac_kingdom_3,fac_kingdom_2]],

["sumpter_horse",    "Farm Horse",         [("sumpter_horse", 0)],                                         itp_merchandise|itp_type_horse, 0,  164, abundance(90)|hit_points( 60)|body_armor( 17)|difficulty(1)|horse_speed(35)|horse_maneuver(35)|horse_charge(10), imodbits_horse_basic],
["saddle_horse",     "Saddle Horse",       [("saddle_horse", 0), ("horse_c", imodbits_horse_good)],        itp_merchandise|itp_type_horse, 0,  312, abundance(90)|hit_points( 60)|body_armor( 14)|difficulty(1)|horse_speed(40)|horse_maneuver(35)|horse_charge(14), imodbits_horse_basic],
["steppe_horse",     "Steppe Horse",       [("steppe_horse", 0)],                                          itp_merchandise|itp_type_horse, 0,  492, abundance(80)|hit_points( 65)|body_armor( 15)|difficulty(2)|horse_speed(48)|horse_maneuver(48)|horse_charge(17), imodbits_horse_basic, [], [fac_kingdom_2, fac_kingdom_3, fac_kingdom_6]],
["courser",          "Courser",            [("courser", 0)],                                               itp_merchandise|itp_type_horse, 0,  723, abundance(70)|hit_points( 65)|body_armor( 16)|difficulty(2)|horse_speed(52)|horse_maneuver(48)|horse_charge(20), imodbits_horse_basic|imodbit_champion],
["arabian_horse_a",  "Desert Horse",       [("arabian_horse_a",0)],                                        itp_merchandise|itp_type_horse, 0, 550,abundance(80)|hit_points(70)|body_armor(10)|difficulty(2)|horse_speed(42)|horse_maneuver(50)|horse_charge(12)|horse_scale(100),imodbits_horse_basic|imodbit_champion, [], [fac_kingdom_3, fac_kingdom_6]],
["arabian_horse_b",  "Sarranid Horse",     [("arabian_horse_b",0)],                                        itp_merchandise|itp_type_horse, 0, 700,abundance(80)|hit_points(70)|body_armor(10)|difficulty(3)|horse_speed(43)|horse_maneuver(54)|horse_charge(16)|horse_scale(100),imodbits_horse_basic|imodbit_champion, [], [fac_kingdom_6]],
["hunter",           "Hunter",             [("hunting_horse", 0), ("hunting_horse", imodbits_horse_good)], itp_merchandise|itp_type_horse, 0,  734, abundance(60)|hit_points( 70)|body_armor( 29)|difficulty(3)|horse_speed(43)|horse_maneuver(43)|horse_charge(26), imodbits_horse_basic|imodbit_champion],
["warhorse",         "Warhorse",           [("warhorse", 0)],                                              itp_merchandise|itp_type_horse, 0, 1224, abundance(50)|hit_points( 60)|body_armor( 52)|difficulty(4)|horse_speed(40)|horse_maneuver(40)|horse_charge(45), imodbits_horse_basic|imodbit_champion],
["warhorse_steppe",  "Steppe Charger",     [("warhorse_steppe",0)],                                        itp_merchandise|itp_type_horse, 0, 1400,abundance(45)|hit_points(80)|body_armor(40)|difficulty(4)|horse_speed(40)|horse_maneuver(50)|horse_charge(28)|horse_scale(112),imodbits_horse_basic|imodbit_champion, [], [fac_kingdom_3, fac_kingdom_2]],
["charger",          "Charger",            [("charger_new", 0)],                                           itp_merchandise|itp_type_horse, 0, 2411, abundance(40)|hit_points( 80)|body_armor( 65)|difficulty(4)|horse_speed(42)|horse_maneuver(42)|horse_charge(55), imodbits_horse_basic|imodbit_champion],
["warhorse_sarranid","Sarranian War Horse",[("warhorse_sarranid",0)],                                      itp_merchandise|itp_type_horse, 0, 1811,abundance(40)|hit_points(90)|body_armor(58)|difficulty(4)|horse_speed(40)|horse_maneuver(44)|horse_charge(32)|horse_scale(112),imodbits_horse_basic|imodbit_champion, [], [fac_kingdom_6]],

	
# New Pellagus horses
	["warhorse_nord", "Nord Warhorse", 			  [("warhorse", 0)], itp_type_horse|itp_merchandise, 0, 700, 0|abundance(150)|body_armor(52)|hit_points(90)|horse_maneuver(37)|horse_speed(32)|weapon_length(110)|horse_charge(22), imodbits_horse_basic|imodbit_champion, [], [fac_kingdom_4]],
	["charger_nord", "Nord Charger", [("charger", 0)], itp_type_horse|itp_merchandise, 0, 800, 0|abundance(120)|body_armor(65)|hit_points(100)|horse_maneuver(36)|horse_speed(34)|weapon_length(110)|horse_charge(30), imodbits_horse_basic|imodbit_champion, [], [fac_kingdom_4]],
	["larktin_charger", "Lady Larktin's Charger", [("DK_armour_charger", 0)], itp_type_horse|itp_unique, 0, 2411, 0|abundance(40)|body_armor(110)|difficulty(4)|hit_points(320)|horse_maneuver(51)|horse_speed(51)|weapon_length(111)|horse_charge(70), imodbits_horse_basic|imodbit_champion ],
	["jarls_warhorse", "Mail Armour Warhorse", [("mail_armour_charger1", 0)], itp_type_horse|itp_merchandise, 0, 3411, 0|abundance(40)|body_armor(73)|difficulty(3)|hit_points(85)|horse_maneuver(40)|horse_speed(35)|weapon_length(111)|horse_charge(48), imodbits_horse_basic|imodbit_champion ],
	["jarls_charger", "Mail Armour Charger", [("mail_armour_charger2", 0)], itp_type_horse|itp_merchandise, 0, 4321, 0|abundance(40)|body_armor(78)|difficulty(4)|hit_points(90)|horse_maneuver(42)|horse_speed(38)|weapon_length(111)|horse_charge(55), imodbits_horse_basic|imodbit_champion ],
	["knights_steed", "Knight's Steed", [("barded_armour_charger2", 0)], itp_type_horse|itp_merchandise, 0, 3921, 0|abundance(30)|body_armor(70)|difficulty(4)|hit_points(80)|horse_maneuver(44)|horse_speed(42)|weapon_length(112)|horse_charge(39), imodbits_horse_good|imodbit_stubborn|imodbit_lame|imodbit_champion, [], [fac_kingdom_1, fac_kingdom_2, fac_kingdom_5] ],
	["knights_charger", "Knight's Charger", [("barded_armour_charger1", 0)], itp_type_horse|itp_merchandise, 0, 4521, 0|abundance(40)|body_armor(65)|difficulty(5)|hit_points(80)|horse_maneuver(42)|horse_speed(44)|weapon_length(110)|horse_charge(40), imodbits_horse_basic|imodbit_champion, [], [fac_kingdom_1] ],
	["dukes_charger", "Duke's Charger", [("plate_armour_charger1", 0)], itp_type_horse|itp_merchandise, 0, 5899, 0|abundance(20)|body_armor(70)|difficulty(6)|hit_points(80)|horse_maneuver(43)|horse_speed(49)|weapon_length(114)|horse_charge(45), imodbits_horse_basic|imodbit_champion, [], [fac_kingdom_1, fac_kingdom_2] ],
	["leather_armoured_warhorse", "Leather Armoured Warhorse", [("leather_armour_charger1", 0)], itp_type_horse|itp_merchandise, 0, 2511, 0|abundance(40)|body_armor(60)|difficulty(3)|hit_points(70)|horse_maneuver(40)|horse_speed(45)|weapon_length(110)|horse_charge(30), imodbits_horse_basic|imodbit_champion ],
	["rhodok_warhorse", "Rhodok Warhorse", [("rhodok_plate_armour_charger1", 0)], itp_type_horse|itp_merchandise, 0, 3411, 0|abundance(40)|body_armor(80)|difficulty(3)|hit_points(85)|horse_maneuver(38)|horse_speed(33)|weapon_length(115)|horse_charge(45), imodbits_horse_basic|imodbit_champion, [], [fac_kingdom_5] ],
	["heavy_plated_armour_charger", "Plated Armour Warhorse", [("heavy_plated_armour_charger", 0)], itp_type_horse|itp_merchandise, 0, 3831, 0|abundance(30)|body_armor(90)|difficulty(4)|hit_points(90)|horse_maneuver(33)|horse_speed(35)|weapon_length(117)|horse_charge(55), imodbits_horse_basic|imodbit_champion ],
	["vaegir_warhorse", "Vaegir Warhorse", [("scale_armour_charger_silver", 0)], itp_type_horse|itp_merchandise, 0, 3631, 0|abundance(50)|body_armor(70)|difficulty(3)|hit_points(75)|horse_maneuver(46)|horse_speed(42)|weapon_length(112)|horse_charge(45), imodbits_horse_basic|imodbit_champion, [], [fac_kingdom_1, fac_kingdom_2, fac_kingdom_3] ],
	["vaegir_charger", "Vaegir Charger", [("scale_armour_charger_gold", 0)], itp_type_horse|itp_merchandise, 0, 4431, 0|abundance(30)|body_armor(72)|difficulty(5)|hit_points(80)|horse_maneuver(48)|horse_speed(45)|weapon_length(112)|horse_charge(50), imodbits_horse_basic|imodbit_champion, [], [fac_kingdom_2] ],
	["armoured_courser", "Armoured Courser", [("new_courser_1", 0)], itp_type_horse|itp_merchandise, 0, 799, 0|abundance(60)|body_armor(20)|difficulty(2)|hit_points(65)|horse_maneuver(47)|horse_speed(52)|weapon_length(105)|horse_charge(20), imodbits_horse_basic|imodbit_champion ],
	["armoured_hunter", "Armoured Hunter", [("new_hunting_horse_1", 0), ("new_hunting_horse_1", imodbits_none)], itp_type_horse|itp_merchandise, 0, 794, 0|abundance(50)|body_armor(32)|difficulty(3)|hit_points(70)|horse_maneuver(42)|horse_speed(43)|weapon_length(108)|horse_charge(27), imodbits_horse_basic|imodbit_champion ],
	["dk_charger", "Dark Knight Charger", [("DK_armour_charger", 0)], itp_type_horse, 0, 2811, 0|abundance(90)|body_armor(65)|difficulty(4)|hit_points(80)|horse_maneuver(42)|horse_speed(42)|weapon_length(117)|horse_charge(55), imodbits_horse_basic|imodbit_champion ],
	["hospitaller_charger", "Hospitaller Charger", [("hospitaller_charger", 0)], itp_type_horse|itp_merchandise, 0, 2811, 0|abundance(1)|body_armor(65)|difficulty(4)|hit_points(80)|horse_maneuver(42)|horse_speed(42)|weapon_length(114)|horse_charge(55), imodbits_horse_basic|imodbit_champion, [], [fac_kingdom_1] ],
	["templar_charger", "Templar Charger", [("templar_charger", 0)], itp_type_horse|itp_merchandise, 0, 2811, 0|abundance(1)|body_armor(65)|difficulty(4)|hit_points(80)|horse_maneuver(42)|horse_speed(42)|weapon_length(113)|horse_charge(55), imodbits_horse_basic|imodbit_champion, [], [fac_kingdom_1] ],
	["sarr_elite","Sarranian Purebreed", [("warhorse_sarranid", 0)], itp_type_horse|itp_merchandise, 0, 38811, 0|abundance(1)|body_armor(58)|difficulty(4)|hit_points(145)|horse_maneuver(44)|horse_speed(40)|weapon_length(113)|horse_scale(112),imodbits_horse_basic|imodbit_champion, [], [fac_kingdom_6] ],

#whalebone crossbow, yew bow, war bow, arming sword
#["arrows","Arrows", [("arrow",0),("flying_missile",ixmesh_flying_ammo),("quiver", ixmesh_carry)], itp_type_arrows|itp_merchandise|itp_default_ammo, itcf_carry_quiver_back, 72,weight(3)|abundance(160)|weapon_length(95)|thrust_damage(1,pierce)|max_ammo(30),imodbits_missile],
["khergit_arrows", "Khergit Arrows", [("arrow_b",0),("flying_missile",ixmesh_flying_ammo),("quiver_b",ixmesh_carry)], itp_type_arrows|itp_merchandise, itcf_carry_quiver_back_right, 280, weight(2.5)|abundance(80)|weapon_length(95)|thrust_damage(18,pierce)|max_ammo(50), imodbits_iron_bolts],
#["barbed_arrows", "Barbed Arrows", [("barbed_arrow",0),("flying_missile",ixmesh_flying_ammo),("quiver_d",ixmesh_carry)], itp_type_arrows|itp_merchandise, itcf_carry_quiver_back_right, 120, weight(2)|abundance(40)|weapon_length(95)|thrust_damage(4,pierce)|max_ammo(50), imodbits_missile ],
["bodkin_arrows", "Bodkin Arrows", [("piercing_arrow",0),("flying_missile",ixmesh_flying_ammo),("quiver_c",ixmesh_carry)], itp_type_arrows|itp_merchandise, itcf_carry_quiver_back_right, 400, weight(3)|abundance(100)|weapon_length(95)|thrust_damage(24,pierce)|max_ammo(50), imodbits_iron_bolts],

# ["bolts","Bolts", [("bolt",0),("flying_missile",ixmesh_flying_ammo),("bolt_bag", ixmesh_carry),("bolt_bag_b", ixmesh_carry|imodbit_large_bag)], itp_type_bolts|itp_merchandise|itp_default_ammo|itp_can_penetrate_shield, itcf_carry_quiver_right_vertical, 64,weight(2.25)|abundance(90)|weapon_length(63)|thrust_damage(1,pierce)|max_ammo(29),imodbits_missile],
# ["steel_bolts","Steel Bolts", [("bolt",0),("flying_missile",ixmesh_flying_ammo),("bolt_bag_c", ixmesh_carry)], itp_type_bolts|itp_merchandise|itp_can_penetrate_shield, itcf_carry_quiver_right_vertical, 210,weight(2.5)|abundance(20)|weapon_length(63)|thrust_damage(2,pierce)|max_ammo(29),imodbits_missile],
["cartridges","Cartridges", [("cartridge_a",0)], itp_type_bullets|itp_merchandise|itp_can_penetrate_shield|itp_default_ammo, 0, 41,weight(2.25)|abundance(90)|weapon_length(3)|thrust_damage(1,pierce)|max_ammo(50),imodbits_missile],

["pilgrim_disguise", "Pilgrim Disguise", [("pilgrim_outfit",0)], itp_type_body_armor|itp_covers_legs|itp_civilian|0, 0, 300, weight(6)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["pilgrim_hood", "Pilgrim Hood", [("pilgrim_hood",0)], itp_type_head_armor|itp_civilian|0, 0, 20, weight(1)|abundance(100)|head_armor(4)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth ],

# ARMOR
#handwear
["leather_gloves","Leather Gloves", [("leather_gloves_L",0)], itp_merchandise|itp_type_hand_armor,0, 90, weight(0.25)|abundance(120)|body_armor(2)|difficulty(0),imodbits_cloth],
["mail_mittens", "Mail Mittens", [("mail_mittens_L",0)], itp_type_hand_armor|itp_merchandise, 0, 200, weight(1)|abundance(120)|body_armor(4)|difficulty(6), imodbits_armor ],
["scale_gauntlets", "Scale Gauntlets", [("scale_gauntlets_b_L",0)], itp_type_hand_armor|itp_merchandise, 0, 450, weight(1.5)|abundance(100)|body_armor(6)|difficulty(6), imodbits_armor ],
["lamellar_gauntlets", "Lamellar Gauntlets", [("scale_gauntlets_a_L",0)], itp_type_hand_armor|itp_merchandise, 0, 800, weight(2)|abundance(80)|body_armor(8)|difficulty(8), imodbits_armor ],
["gauntlets", "Gauntlets", [("gauntlets_L",0)], itp_type_hand_armor|itp_merchandise, 0, 1250, weight(2.5)|abundance(60)|body_armor(10)|difficulty(10), imodbits_armor ],

#footwear
["wrapping_boots", "Wrapping Boots", [("wrapping_boots_a",0)], itp_type_foot_armor|itp_attach_armature|itp_merchandise|itp_civilian, 0, 40, weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(4)|difficulty(0), imodbits_cloth ],
["woolen_hose", "Woolen Hose", [("woolen_hose_a",0)], itp_type_foot_armor|itp_attach_armature|itp_merchandise|itp_civilian, 0, 20, weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(2)|difficulty(0), imodbits_cloth ],
["blue_hose", "Blue Hose", [("blue_hose_a",0)], itp_type_foot_armor|itp_attach_armature|itp_merchandise|itp_civilian, 0, 20, weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(2)|difficulty(0), imodbits_cloth ],
["hunter_boots", "Hunter Boots", [("hunter_boots_a",0)], itp_type_foot_armor|itp_attach_armature|itp_merchandise|itp_civilian, 0, 120, weight(3)|abundance(120)|head_armor(0)|body_armor(0)|leg_armor(8)|difficulty(0), imodbits_cloth ],
["hide_boots", "Hide Boots", [("hide_boots_a",0)], itp_type_foot_armor|itp_attach_armature|itp_merchandise|itp_civilian, 0, 150, weight(3)|abundance(120)|head_armor(0)|body_armor(0)|leg_armor(10)|difficulty(0), imodbits_cloth ],
["ankle_boots", "Ankle Boots", [("ankle_boots_a_new",0)], itp_type_foot_armor|itp_attach_armature|itp_merchandise|itp_civilian, 0, 40, weight(2)|abundance(120)|head_armor(0)|body_armor(0)|leg_armor(4)|difficulty(0), imodbits_cloth ],
["nomad_boots", "Nomad Boots", [("nomad_boots_a",0)], itp_type_foot_armor|itp_attach_armature|itp_merchandise|itp_civilian, 0, 60, weight(2)|abundance(120)|head_armor(0)|body_armor(0)|leg_armor(6)|difficulty(0), imodbits_cloth ],
["leather_boots", "Leather Boots", [("leather_boots_a",0)], itp_type_foot_armor|itp_attach_armature|itp_merchandise|itp_civilian, 0, 350, weight(5)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(14)|difficulty(6), imodbits_cloth ],
["splinted_leather_greaves", "Splinted Leather Greaves", [("leather_greaves_a",0)], itp_type_foot_armor|itp_attach_armature|itp_merchandise, 0, 540, weight(6)|abundance(80)|head_armor(0)|body_armor(0)|leg_armor(16)|difficulty(6), imodbits_armor ],
["mail_chausses", "Mail Chausses", [("mail_chausses_a",0)], itp_type_foot_armor|itp_attach_armature|itp_merchandise, 0, 780, weight(6.5)|abundance(60)|head_armor(0)|body_armor(0)|leg_armor(24)|difficulty(8), imodbits_armor ],
["splinted_greaves", "Splinted Greaves", [("splinted_greaves_a",0)], itp_type_foot_armor|itp_attach_armature|itp_merchandise, 0, 1200, weight(8)|abundance(40)|head_armor(0)|body_armor(0)|leg_armor(30)|difficulty(12), imodbits_armor ],
["mail_boots", "Mail Boots", [("mail_boots_a",0)], itp_type_foot_armor|itp_attach_armature|itp_merchandise, 0, 600, weight(6)|abundance(60)|head_armor(0)|body_armor(0)|leg_armor(20)|difficulty(8), imodbits_armor ],
["iron_greaves", "Iron Greaves", [("iron_greaves_a",0)], itp_type_foot_armor|itp_attach_armature|itp_merchandise, 0, 1280, weight(8)|abundance(40)|head_armor(0)|body_armor(0)|leg_armor(32)|difficulty(12), imodbits_armor ],
# ["black_greaves", "Black Greaves", [("black_greaves",0)], itp_type_foot_armor  | itp_attach_armature,0,
# 2361 , weight(3.5)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(35)|difficulty(0) ,imodbits_armor ],
["khergit_leather_boots", "Khergit Leather Boots", [("khergit_leather_boots",0)], itp_type_foot_armor|itp_attach_armature|itp_merchandise|itp_civilian, 0, 400, weight(5)|abundance(80)|head_armor(0)|body_armor(0)|leg_armor(14)|difficulty(6), imodbits_cloth ],
["sarranid_boots_a", "Sarranid Shoes", [("sarranid_shoes",0)], itp_type_foot_armor|itp_attach_armature|itp_civilian, 0, 20, weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(2)|difficulty(0), imodbits_cloth ],
["sarranid_boots_b", "Sarranid Leather Boots", [("sarranid_boots",0)], itp_type_foot_armor|itp_attach_armature|itp_merchandise|itp_civilian, 0, 400, weight(5)|abundance(80)|head_armor(0)|body_armor(0)|leg_armor(16)|difficulty(6), imodbits_cloth ],
["sarranid_boots_c", "Plated Boots", [("sarranid_camel_boots",0)], itp_type_foot_armor|itp_attach_armature|itp_merchandise|itp_civilian, 0, 910, weight(7)|abundance(50)|head_armor(0)|body_armor(0)|leg_armor(28)|difficulty(10), imodbits_armor ],
["sarranid_boots_d", "Sarranid Mail Boots", [("sarranid_mail_chausses",0)], itp_type_foot_armor|itp_attach_armature|itp_merchandise|itp_civilian, 0, 715, weight(6.5)|abundance(60)|head_armor(0)|body_armor(0)|leg_armor(22)|difficulty(8), imodbits_armor ],

["sarranid_head_cloth", "Lady Head Cloth", [("tulbent",0)], itp_type_head_armor|itp_attach_armature|itp_doesnt_cover_hair|itp_civilian, 0, 20, weight(1)|abundance(100)|head_armor(4)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["sarranid_head_cloth_b", "Lady Head Cloth", [("tulbent_b",0)], itp_type_head_armor|itp_attach_armature|itp_doesnt_cover_hair|itp_civilian, 0, 20, weight(1)|abundance(100)|head_armor(4)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["sarranid_felt_head_cloth", "Head Cloth", [("common_tulbent",0)], itp_type_head_armor|itp_attach_armature|itp_civilian, 0, 20, weight(1)|abundance(100)|head_armor(4)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["sarranid_felt_head_cloth_b", "Head Cloth", [("common_tulbent_b",0)], itp_type_head_armor|itp_attach_armature|itp_civilian, 0, 20, weight(1)|abundance(100)|head_armor(4)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth ],


#bodywear
["lady_dress_ruby", "Lady Dress", [("lady_dress_r",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 240, weight(6)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["lady_dress_green", "Lady Dress", [("lady_dress_g",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 240, weight(6)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["lady_dress_blue", "Lady Dress", [("lady_dress_b",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 240, weight(6)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["red_dress", "Red Dress", [("red_dress",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 240, weight(6)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["brown_dress", "Brown Dress", [("brown_dress",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 240, weight(6)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["green_dress", "Green Dress", [("green_dress",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 240, weight(6)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["khergit_lady_dress", "Khergit Lady Dress", [("khergit_lady_dress",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 240, weight(6)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["khergit_lady_dress_b", "Khergit Leather Lady Dress", [("khergit_lady_dress_b",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 300, weight(6)|abundance(100)|head_armor(0)|body_armor(10)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["sarranid_lady_dress", "Sarranid Lady Dress", [("sarranid_lady_dress",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 240, weight(6)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["sarranid_lady_dress_b", "Sarranid Lady Dress", [("sarranid_lady_dress_b",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 240, weight(6)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["sarranid_common_dress", "Sarranid Dress", [("sarranid_common_dress",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 240, weight(6)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["sarranid_common_dress_b", "Sarranid Dress", [("sarranid_common_dress_b",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 240, weight(6)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["courtly_outfit", "Courtly Outfit", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 240, weight(6)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["nobleman_outfit", "Nobleman Outfit", [("nobleman_outfit_b_new",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 240, weight(6)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["nomad_armor", "Nomad Armor", [("nomad_armor_new",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 225, weight(7)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["khergit_armor", "Khergit Armor", [("khergit_armor_new",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 225, weight(7)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["leather_jacket", "Leather Jacket", [("leather_jacket_new",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs|itp_civilian, 0, 360, weight(8)|abundance(100)|head_armor(0)|body_armor(16)|leg_armor(0)|difficulty(0), imodbits_cloth ],

#NEW:
["rawhide_coat", "Rawhide Coat", [("coat_of_plates_b",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs|itp_civilian, 0, 200, weight(6.5)|abundance(100)|head_armor(0)|body_armor(12)|leg_armor(0)|difficulty(0), imodbits_cloth ],
#NEW: was lthr_armor_a
["leather_armor", "Leather Armor", [("tattered_leather_armor_a",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 520, weight(9.5)|abundance(140)|head_armor(0)|body_armor(22)|leg_armor(0)|difficulty(6), imodbits_cloth ],
["fur_coat", "Fur Coat", [("fur_coat",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs|itp_civilian, 0, 240, weight(6)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(0)|difficulty(0), imodbits_cloth ],



#for future:
["coat", "Coat", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 240, weight(6)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["leather_coat", "Leather Coat", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 225, weight(6.5)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["mail_coat", "Noble Coat", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 240, weight(6)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["long_mail_coat", "Noble Long Coat", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 240, weight(6)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["mail_with_tunic_red", "Mail with Tunic", [("arena_armorR_new",0)], itp_type_body_armor|itp_covers_legs, 0, 1445, weight(17)|abundance(100)|head_armor(0)|body_armor(34)|leg_armor(6)|difficulty(10), imodbits_armor ],
["mail_with_tunic_green", "Mail with Tunic", [("arena_armorG_new",0)], itp_type_body_armor|itp_covers_legs, 0, 1445, weight(17)|abundance(100)|head_armor(0)|body_armor(34)|leg_armor(6)|difficulty(10), imodbits_armor ],
["hide_coat", "Hide Coat", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 200, weight(6.5)|abundance(100)|head_armor(0)|body_armor(12)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["merchant_outfit", "Merchant Outfit", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 240, weight(6)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["homespun_dress", "Homespun Dress", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 240, weight(6)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["thick_coat", "Court Coat", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 240, weight(6)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["coat_with_cape", "Coat with Cape", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 240, weight(6)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["steppe_outfit", "Steppe Outfit", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 200, weight(6.5)|abundance(100)|head_armor(0)|body_armor(12)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["nordic_outfit", "Nordic Outfit", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 200, weight(6.5)|abundance(100)|head_armor(0)|body_armor(12)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["nordic_armor", "Nordic Armor", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 200, weight(6.5)|abundance(100)|head_armor(0)|body_armor(12)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["hide_armor", "Hide Armor", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 360, weight(8)|abundance(100)|head_armor(0)|body_armor(16)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["cloaked_tunic", "Cloaked Tunic", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 240, weight(6)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["sleeveless_tunic", "Sleeveless Tunic", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 240, weight(6)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["sleeveless_leather_tunic", "Sleeveless Leather Tunic", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 300, weight(6)|abundance(100)|head_armor(0)|body_armor(10)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["linen_shirt", "Linen Shirt", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 240, weight(6)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["wool_coat", "Wool Coat", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 240, weight(6)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(0)|difficulty(0), imodbits_cloth ],
#end

["dress", "Dress", [("dress",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs|itp_civilian, 0, 240, weight(6)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["blue_dress", "Blue Dress", [("blue_dress_new",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs|itp_civilian, 0, 240, weight(6)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["peasant_dress", "Peasant Dress", [("peasant_dress_b_new",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs|itp_civilian, 0, 240, weight(6)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["woolen_dress", "Woolen Dress", [("woolen_dress",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs|itp_civilian, 0, 240, weight(6)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["shirt", "Shirt", [("shirt",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs|itp_civilian, 0, 240, weight(6)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(0)|difficulty(0), imodbits_cloth ],
#NEW: was "linen_tunic"
["linen_tunic", "Linen Tunic", [("shirt_a",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs|itp_civilian, 0, 240, weight(6)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(0)|difficulty(0), imodbits_cloth ],
#NEW was cvl_costume_a
["short_tunic", "Red Tunic", [("rich_tunic_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 240, weight(6)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(0)|difficulty(0), imodbits_cloth ],
#TODO:
["red_shirt", "Red Shirt", [("rich_tunic_a",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs|itp_civilian, 0, 240, weight(6)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["red_tunic", "Red Tunic", [("arena_tunicR_new",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs|itp_civilian, 0, 240, weight(6)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(0)|difficulty(0), imodbits_cloth ],

["green_tunic", "Green Tunic", [("arena_tunicG_new",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs|itp_civilian, 0, 240, weight(6)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["blue_tunic", "Blue Tunic", [("arena_tunicB_new",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs|itp_civilian, 0, 240, weight(6)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["robe", "Robe", [("robe",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs|itp_civilian, 0, 300, weight(6)|abundance(100)|head_armor(0)|body_armor(10)|leg_armor(0)|difficulty(0), imodbits_cloth ],
#NEW: was coarse_tunic
["coarse_tunic", "Tunic with vest", [("coarse_tunic_a",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs|itp_civilian, 0, 240, weight(6)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["leather_apron", "Leather Apron", [("leather_apron",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs|itp_civilian, 0, 240, weight(6)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(0)|difficulty(0), imodbits_cloth ],
#NEW: was tabard_a
["tabard", "Tabard", [("tabard_b",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs|itp_civilian, 0, 240, weight(6)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(0)|difficulty(0), imodbits_cloth ],
#NEW: was leather_vest
["leather_vest", "Leather Vest", [("leather_vest_a",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs|itp_civilian, 0, 180, weight(6)|abundance(100)|head_armor(0)|body_armor(12)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["steppe_armor", "Steppe Armor", [("lamellar_leather",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 225, weight(7)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["gambeson", "Gambeson", [("white_gambeson",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs|itp_civilian, 0, 280, weight(7.5)|abundance(100)|head_armor(0)|body_armor(16)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["blue_gambeson", "Blue Gambeson", [("blue_gambeson",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs|itp_civilian, 0, 280, weight(7.5)|abundance(100)|head_armor(0)|body_armor(16)|leg_armor(0)|difficulty(0), imodbits_cloth ],
#NEW: was red_gambeson
["red_gambeson", "Red Gambeson", [("red_gambeson_a",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs|itp_civilian, 0, 280, weight(7.5)|abundance(100)|head_armor(0)|body_armor(16)|leg_armor(0)|difficulty(0), imodbits_cloth ],
#NEW: was aketon_a
["padded_cloth", "Aketon", [("padded_cloth_a",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 280, weight(7.5)|abundance(100)|head_armor(0)|body_armor(16)|leg_armor(0)|difficulty(0), imodbits_cloth ],
#NEW:
["aketon_green", "Padded Cloth", [("padded_cloth_b",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 280, weight(7.5)|abundance(100)|head_armor(0)|body_armor(16)|leg_armor(0)|difficulty(0), imodbits_cloth ],
#NEW: was "leather_jerkin"
["leather_jerkin", "Leather Jerkin", [("ragged_leather_jerkin",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs|itp_civilian, 0, 360, weight(8)|abundance(100)|head_armor(0)|body_armor(16)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["nomad_vest", "Nomad Vest", [("nomad_vest_new",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs|itp_civilian, 0, 280, weight(7.5)|abundance(100)|head_armor(0)|body_armor(16)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["ragged_outfit", "Padded Outfit", [("ragged_outfit_a_new",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs|itp_civilian, 0, 180, weight(6)|abundance(100)|head_armor(0)|body_armor(12)|leg_armor(0)|difficulty(0), imodbits_cloth ],
#NEW: was padded_leather
["padded_leather", "Padded Leather", [("leather_armor_b",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs|itp_civilian, 0, 560, weight(10)|abundance(140)|head_armor(0)|body_armor(24)|leg_armor(0)|difficulty(6), imodbits_cloth ],
["tribal_warrior_outfit", "Tribal Warrior Outfit", [("tribal_warrior_outfit_a_new",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs|itp_civilian, 0, 280, weight(7.5)|abundance(100)|head_armor(0)|body_armor(16)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["nomad_robe", "Nomad Robe", [("nomad_robe_a",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs|itp_civilian, 0, 280, weight(7.5)|abundance(100)|head_armor(0)|body_armor(16)|leg_armor(0)|difficulty(0), imodbits_cloth ],
#["heraldric_armor", "Heraldric Armor", [("tourn_armor_a",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 442 , weight(17)|abundance(100)|head_armor(0)|body_armor(35)|leg_armor(8)|difficulty(7) ,imodbits_armor ],
#NEW: was "std_lthr_coat"
["studded_leather_coat", "Studded Leather Coat", [("leather_armor_a",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 980, weight(14)|abundance(140)|head_armor(0)|body_armor(28)|leg_armor(2)|difficulty(8), imodbits_armor ],

["byrnie", "Byrnie", [("byrnie_a_new",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 1445, weight(17)|abundance(120)|head_armor(0)|body_armor(34)|leg_armor(6)|difficulty(10), imodbits_armor ],
#["blackwhite_surcoat", "Black and White Surcoat", [("surcoat_blackwhite",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 348 , weight(16)|abundance(100)|head_armor(0)|body_armor(33)|leg_armor(8)|difficulty(7) ,imodbits_armor ],
#["green_surcoat", "Green Surcoat", [("surcoat_green",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 348 , weight(16)|abundance(100)|head_armor(0)|body_armor(33)|leg_armor(8)|difficulty(7) ,imodbits_armor ],
#["blue_surcoat", "Blue Surcoat", [("surcoat_blue",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 350 , weight(16)|abundance(100)|head_armor(0)|body_armor(33)|leg_armor(8)|difficulty(7) ,imodbits_armor ],
#["red_surcoat", "Red Surcoat", [("surcoat_red",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 350 , weight(16)|abundance(100)|head_armor(0)|body_armor(33)|leg_armor(8)|difficulty(7) ,imodbits_armor ],
#NEW: was "haubergeon_a"
["haubergeon", "Haubergeon", [("haubergeon_c",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 1445, weight(17)|abundance(120)|head_armor(0)|body_armor(34)|leg_armor(6)|difficulty(10), imodbits_armor ],

["lamellar_vest", "Lamellar Vest", [("lamellar_vest_a",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs|itp_civilian, 0, 1125, weight(15)|abundance(120)|head_armor(0)|body_armor(30)|leg_armor(4)|difficulty(8), imodbits_armor ],

["lamellar_vest_khergit", "Khergit Lamellar Vest", [("lamellar_vest_b",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 1125, weight(15)|abundance(120)|head_armor(0)|body_armor(30)|leg_armor(4)|difficulty(8), imodbits_armor ],

#NEW: was mail_shirt
["mail_shirt", "Mail Shirt", [("mail_shirt_a",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 2280, weight(24)|abundance(80)|head_armor(4)|body_armor(38)|leg_armor(10)|difficulty(12), imodbits_armor ],

["mail_hauberk", "Mail Hauberk", [("hauberk_a_new",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 1445, weight(17)|abundance(120)|head_armor(0)|body_armor(34)|leg_armor(6)|difficulty(10), imodbits_armor ],

["mail_with_surcoat", "Mail with Surcoat", [("mail_long_surcoat_new",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 2280, weight(24)|abundance(80)|head_armor(4)|body_armor(38)|leg_armor(10)|difficulty(12), imodbits_armor ],
["mail_with_surcoat_a", "Mail with Surcoat", [("mail_long_surcoat_new",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 2280, weight(24)|abundance(80)|head_armor(4)|body_armor(38)|leg_armor(10)|difficulty(12), imodbits_armor ],
["surcoat_over_mail", "Surcoat over Mail", [("surcoat_over_mail_new",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 2280, weight(24)|abundance(80)|head_armor(4)|body_armor(38)|leg_armor(10)|difficulty(12), imodbits_armor ],
#["lamellar_cuirass", "Lamellar Cuirass", [("lamellar_armor",0)], itp_type_body_armor  |itp_covers_legs,0, 1020 , weight(25)|abundance(100)|head_armor(0)|body_armor(43)|leg_armor(15)|difficulty(8) ,imodbits_armor ],
#NEW: was "brigandine_a"
["brigandine_red", "Brigandine", [("brigandine_b",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 1445, weight(17)|abundance(100)|head_armor(0)|body_armor(34)|leg_armor(6)|difficulty(10), imodbits_armor ],
["lamellar_armor", "Lamellar Armor", [("lamellar_armor_b",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 1280, weight(16)|abundance(120)|head_armor(0)|body_armor(32)|leg_armor(4)|difficulty(8), imodbits_armor ],
["scale_armor", "Scale Armor", [("lamellar_armor_e",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 2600, weight(26)|abundance(80)|head_armor(6)|body_armor(40)|leg_armor(12)|difficulty(12), imodbits_armor ],
#NEW: was "reinf_jerkin"
["banded_armor", "Banded Armor", [("banded_armor_a",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 2600, weight(26)|abundance(80)|head_armor(6)|body_armor(40)|leg_armor(12)|difficulty(12), imodbits_armor ],
#NEW: was hard_lthr_a
["cuir_bouilli", "Cuir Bouilli", [("cuir_bouilli_a",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 650, weight(10.5)|abundance(140)|head_armor(0)|body_armor(26)|leg_armor(0)|difficulty(6), imodbits_cloth ],
["coat_of_plates", "Coat of Plates", [("coat_of_plates_a",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 3600, weight(30)|abundance(60)|head_armor(8)|body_armor(48)|leg_armor(16)|difficulty(12), imodbits_armor ],
["coat_of_plates_a", "Coat of Plates", [("coat_of_plates_a",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 3600, weight(30)|abundance(60)|head_armor(8)|body_armor(48)|leg_armor(16)|difficulty(12), imodbits_armor ],
["coat_of_plates_red", "Coat of Plates", [("coat_of_plates_red",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 3600, weight(30)|abundance(60)|head_armor(8)|body_armor(48)|leg_armor(16)|difficulty(12), imodbits_armor ],
["plate_armor", "Plate Armor", [("full_plate_armor",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 4940, weight(38)|abundance(40)|head_armor(8)|body_armor(52)|leg_armor(20)|difficulty(15), imodbits_armor ],
# ["black_armor", "Black Armor", [("black_armor",0)], itp_type_body_armor  |itp_covers_legs ,0,
# 9496 , weight(28)|abundance(100)|head_armor(0)|body_armor(57)|leg_armor(18)|difficulty(10) ,imodbits_plate ],

##armors_d
["pelt_coat", "Pelt Coat", [("thick_coat_a",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 200, weight(6.5)|abundance(100)|head_armor(0)|body_armor(12)|leg_armor(0)|difficulty(0), imodbits_cloth ],
##armors_e
["khergit_elite_armor", "Khergit Elite Armor", [("lamellar_armor_d",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 1280, weight(16)|abundance(120)|head_armor(0)|body_armor(32)|leg_armor(4)|difficulty(8), imodbits_armor ],
["vaegir_elite_armor", "Vaegir Elite Armor", [("lamellar_armor_c",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 1280, weight(16)|abundance(100)|head_armor(0)|body_armor(32)|leg_armor(4)|difficulty(8), imodbits_armor ],
["sarranid_elite_armor", "Sarranid Elite Armor", [("tunic_armor_a",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 1620, weight(18)|abundance(100)|head_armor(0)|body_armor(36)|leg_armor(8)|difficulty(10), imodbits_armor ],


["sarranid_dress_a", "Dress", [("woolen_dress",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 240, weight(6)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["sarranid_dress_b", "Dress", [("woolen_dress",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 240, weight(6)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["sarranid_cloth_robe", "Worn Robe", [("sar_robe",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 300, weight(6)|abundance(100)|head_armor(0)|body_armor(10)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["sarranid_cloth_robe_b", "Worn Robe", [("sar_robe_b",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 300, weight(6)|abundance(100)|head_armor(0)|body_armor(10)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["skirmisher_armor", "Skirmisher Armor", [("skirmisher_armor",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 200, weight(6.5)|abundance(100)|head_armor(0)|body_armor(12)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["archers_vest", "Archer's Padded Vest", [("archers_vest",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 280, weight(7.5)|abundance(100)|head_armor(0)|body_armor(16)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["sarranid_leather_armor", "Sarranid Leather Armor", [("sarranid_leather_armor",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 650, weight(10)|abundance(140)|head_armor(0)|body_armor(26)|leg_armor(0)|difficulty(6), imodbits_cloth ],
["sarranid_cavalry_robe", "Cavalry Robe", [("arabian_armor_a",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 280, weight(7.5)|abundance(100)|head_armor(0)|body_armor(16)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["arabian_armor_b", "Sarranid Guard Armor", [("arabian_armor_b",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 1620, weight(18)|abundance(100)|head_armor(0)|body_armor(36)|leg_armor(8)|difficulty(10), imodbits_armor ],
["sarranid_mail_shirt", "Sarranid Mail Shirt", [("sarranian_mail_shirt",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 2280, weight(24)|abundance(80)|head_armor(4)|body_armor(38)|leg_armor(10)|difficulty(12), imodbits_armor ],
["mamluke_mail", "Mamluke Mail", [("sarranid_elite_cavalary",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 2970, weight(27)|abundance(60)|head_armor(6)|body_armor(44)|leg_armor(14)|difficulty(12), imodbits_armor ],

# New Pellagus Sarranid clothing
["archers_vest_P1", "Sarranid Padded Vest", [("archers_vest_P1",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 280, weight(7.5)|abundance(100)|body_armor(16)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["archers_vest_P2", "Archer's Padded Vest", [("archers_vest_P2",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 280, weight(7.5)|abundance(100)|body_armor(16)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["archers_vest_P3", "Royal Archer's Padded Vest", [("archers_vest_P3",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 280, weight(7.5)|abundance(100)|body_armor(16)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["sarranian_mail_shirt_P1", "Sarranian Noble Mail Shirt", [("sarranian_mail_shirt_P1",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs|itp_civilian, 0, 2280, weight(24)|abundance(80)|body_armor(38)|leg_armor(10)|difficulty(12), imodbits_armor ],
["sarranian_mail_shirt_P2", "Sarranian Royal Mail Shirt", [("sarranian_mail_shirt_P2",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs|itp_civilian, 0, 2280, weight(24)|abundance(80)|body_armor(38)|leg_armor(10)|difficulty(12), imodbits_armor ],
["sarranid_cloth_robe_P1", "Sarranid Black Striped Robe", [("sarranid_cloth_robe_P1",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 300, weight(6)|body_armor(10)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["sarranid_cloth_robe_P2", "Sarranid Yellow Striped Robe", [("sarranid_cloth_robe_P2",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 300, weight(6)|body_armor(10)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["sarranid_cloth_robe_P3", "Sarranid Red Striped Robe", [("sarranid_cloth_robe_P3",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 300, weight(6)|body_armor(10)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["arabian_armor_P3", "Sarranid Infantry Armour", [("arabian_armor_P3",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 980, weight(14)|body_armor(28)|leg_armor(2)|difficulty(8), imodbits_armor ],
["arabian_armor_P4", "Sarranid Master Armour", [("arabian_armor_P4",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 1280, weight(16)|abundance(120)|body_armor(32)|leg_armor(4)|difficulty(8), imodbits_armor ],
["arabian_armor_P1", "Sarranid Horseman's Armour", [("arabian_armor_P1",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs|itp_civilian, 0, 1445, weight(17)|abundance(100)|body_armor(34)|leg_armor(6)|difficulty(10), imodbits_armor ],
["arabian_armor_P2", "Sarranid Lancers Robe", [("arabian_armor_P2",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs|itp_civilian, 0, 1445, weight(17)|abundance(100)|body_armor(34)|leg_armor(6)|difficulty(10), imodbits_armor ],


# New Pellagus Plate
["full_plate_armor_nord", "Nord Full Plate Armour", [("full_plate_armor_nord",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 6090, weight(42)|head_armor(10)|body_armor(58)|leg_armor(20)|difficulty(18), imodbits_armor ],
["full_plate_RHODOK", "Rhodok Full Plate", [("full_plate_armor_rhodok",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 6090, weight(42)|head_armor(10)|body_armor(58)|leg_armor(20)|difficulty(18), imodbits_armor ],
["full_plate_armor_swadian", "Swadian Full Plate Armour", [("full_plate_armor_swadian",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 6090, weight(42)|head_armor(10)|body_armor(58)|leg_armor(20)|difficulty(18), imodbits_armor ],
["full_plate_armor_rhodok2", "Rhodok Sergeant Plate", [("full_plate_armor_rhodok2",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 4000, weight(37)|head_armor(8)|body_armor(50)|leg_armor(18)|difficulty(15), imodbits_armor|imodbit_cracked|imodbit_heavy|imodbit_strong|imodbit_hardened ],
["wb_ni_helm", "Nihelm", [("WB_NI_Helm",0)], itp_type_head_armor, 0, 900, weight(5)|head_armor(36)|difficulty(10), imodbits_armor|imodbit_crude|imodbit_lordly ],
["wb_dk_helm", "Dark Knight Helm", [("WB_DK_helm",0)], itp_type_head_armor, 0, 1430, weight(6.5)|abundance(20)|head_armor(44)|difficulty(12), imodbits_armor|imodbit_crude|imodbit_lordly ],
["wb_dk_helm_b", "Dark Knight Helm", [("WB_DK_helm",0)], itp_type_head_armor, 0, 1430, weight(6.5)|head_armor(44)|difficulty(12), imodbits_armor|imodbit_crude|imodbit_lordly ],


["dk_full_plate", "Dark Knight Plate Armour", [("DK_full_plate",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 6600, weight(44)|head_armor(10)|body_armor(60)|leg_armor(20)|difficulty(20), imodbits_armor ],
["dk_full_plate_2", "Dark Knight Lord Plate Armour", [("DK_full_plate_2",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 8750, weight(50)|head_armor(20)|body_armor(70)|leg_armor(30)|difficulty(20), imodbits_armor ],


["wb_dk_plate_boots", "Dark Knight Plate Boots", [("WB_DK_plate_boots",0)], itp_type_foot_armor|itp_attach_armature|itp_civilian, 0, 1710, weight(9)|leg_armor(38)|difficulty(15), imodbits_armor ],



#Quest-specific - perhaps can be used for prisoners,
["burlap_tunic", "Burlap Tunic", [("shirt",0)], itp_type_body_armor|itp_covers_legs, 0, 240, weight(6)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(0)|difficulty(0), imodbits_armor ],


["heraldic_mail_with_surcoat", "Heraldic Mail with Surcoat", [("heraldic_armor_new_a",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 2970, weight(27)|abundance(80)|head_armor(6)|body_armor(44)|leg_armor(12)|difficulty(12), imodbits_armor, [(ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_shield_item_set_banner","tableau_heraldic_armor_a",":agent_no",":troop_no")])] ],
["heraldic_mail_with_tunic", "Heraldic Mail", [("heraldic_armor_new_b",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 2970, weight(27)|abundance(80)|head_armor(6)|body_armor(44)|leg_armor(12)|difficulty(12), imodbits_armor, [(ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_shield_item_set_banner","tableau_heraldic_armor_b",":agent_no",":troop_no")])] ],
["heraldic_mail_with_tunic_b", "Heraldic Mail", [("heraldic_armor_new_c",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 2970, weight(27)|abundance(80)|head_armor(6)|body_armor(44)|leg_armor(12)|difficulty(12), imodbits_armor, [(ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_shield_item_set_banner","tableau_heraldic_armor_c",":agent_no",":troop_no")])] ],
["heraldic_mail_with_tabard", "Heraldic Mail with Tabard", [("heraldic_armor_new_d",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 2970, weight(27)|abundance(80)|head_armor(6)|body_armor(44)|leg_armor(12)|difficulty(12), imodbits_armor, [(ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_shield_item_set_banner","tableau_heraldic_armor_d",":agent_no",":troop_no")])] ],
["turret_hat_ruby", "Turret Hat", [("turret_hat_r",0)], itp_type_head_armor|itp_civilian|itp_fit_to_head, 0, 20, weight(1)|abundance(100)|head_armor(4)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["turret_hat_blue", "Turret Hat", [("turret_hat_b",0)], itp_type_head_armor|itp_civilian|itp_fit_to_head, 0, 20, weight(1)|abundance(100)|head_armor(4)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["turret_hat_green", "Barbette", [("barbette_new",0)], itp_type_head_armor|itp_merchandise|itp_civilian|itp_fit_to_head, 0, 20, weight(1)|abundance(100)|head_armor(4)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["head_wrappings", "Head Wrapping", [("head_wrapping",0)], itp_type_head_armor|itp_fit_to_head, 0, 20, weight(1)|head_armor(4), imodbits_cloth ],
["court_hat", "Turret Hat", [("court_hat",0)], itp_type_head_armor|itp_civilian|itp_fit_to_head, 0, 20, weight(1)|abundance(100)|head_armor(4)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["wimple_a", "Wimple", [("wimple_a_new",0)], itp_type_head_armor|itp_merchandise|itp_civilian|itp_fit_to_head, 0, 20, weight(1)|abundance(100)|head_armor(4)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["wimple_with_veil", "Wimple with Veil", [("wimple_b_new",0)], itp_type_head_armor|itp_merchandise|itp_civilian|itp_fit_to_head, 0, 20, weight(1)|abundance(100)|head_armor(4)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["straw_hat", "Straw Hat", [("straw_hat_new",0)], itp_type_head_armor|itp_merchandise|itp_civilian, 0, 20, weight(1)|abundance(100)|head_armor(4)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["common_hood", "Hood", [("hood_new",0)], itp_type_head_armor|itp_merchandise|itp_civilian, 0, 20, weight(1)|abundance(100)|head_armor(4)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["hood_b", "Hood", [("hood_b",0)], itp_type_head_armor|itp_merchandise|itp_civilian, 0, 20, weight(1)|abundance(100)|head_armor(4)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["hood_c", "Hood", [("hood_c",0)], itp_type_head_armor|itp_merchandise|itp_civilian, 0, 20, weight(1)|abundance(100)|head_armor(4)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["hood_d", "Hood", [("hood_d",0)], itp_type_head_armor|itp_merchandise|itp_civilian, 0, 20, weight(1)|abundance(100)|head_armor(4)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["headcloth", "Headcloth", [("headcloth_a_new",0)], itp_merchandise| itp_type_head_armor  |itp_civilian ,0, 1 , weight(0.5)|abundance(100)|head_armor(4)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["woolen_hood", "Woolen Hood", [("woolen_hood",0)], itp_type_head_armor|itp_merchandise|itp_civilian, 0, 20, weight(1)|abundance(100)|head_armor(4)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["arming_cap", "Arming Cap", [("arming_cap_a_new",0)], itp_type_head_armor|itp_merchandise|itp_civilian, 0, 100, weight(2)|abundance(100)|head_armor(10)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["fur_hat", "Fur Hat", [("fur_hat_a_new",0)], itp_type_head_armor|itp_merchandise|itp_civilian, 0, 20, weight(1)|abundance(100)|head_armor(4)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["nomad_cap", "Nomad Cap", [("nomad_cap_a_new",0)], itp_type_head_armor|itp_merchandise|itp_civilian, 0, 100, weight(2)|abundance(100)|head_armor(10)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["nomad_cap_b", "Nomad Cap", [("nomad_cap_b_new",0)], itp_type_head_armor|itp_merchandise|itp_civilian, 0, 100, weight(2)|abundance(100)|head_armor(10)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["steppe_cap", "Steppe Cap", [("steppe_cap_a_new",0)], itp_type_head_armor|itp_merchandise|itp_civilian, 0, 100, weight(2)|abundance(100)|head_armor(10)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["padded_coif", "Padded Coif", [("padded_coif_a_new",0)], itp_type_head_armor|itp_merchandise, 0, 100, weight(2)|abundance(100)|head_armor(10)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["woolen_cap", "Woolen Cap", [("woolen_cap_new",0)], itp_type_head_armor|itp_merchandise|itp_civilian, 0, 100, weight(2)|abundance(100)|head_armor(10)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["felt_hat", "Felt Hat", [("felt_hat_a_new",0)], itp_type_head_armor|itp_merchandise|itp_civilian, 0, 20, weight(1)|abundance(100)|head_armor(4)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["felt_hat_b", "Felt Hat", [("felt_hat_b_new",0)], itp_type_head_armor|itp_merchandise|itp_civilian, 0, 20, weight(1)|abundance(100)|head_armor(4)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["leather_cap", "Leather Cap", [("leather_cap_a_new",0)], itp_type_head_armor|itp_merchandise|itp_civilian, 0, 180, weight(2.5)|abundance(140)|head_armor(12)|body_armor(0)|leg_armor(0)|difficulty(6), imodbits_cloth ],
["female_hood", "Lady's Hood", [("ladys_hood_new",0)], itp_type_head_armor|itp_merchandise|itp_civilian, 0, 20, weight(1)|abundance(100)|head_armor(4)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["leather_steppe_cap_a", "Steppe Cap", [("leather_steppe_cap_a_new",0)], itp_type_head_armor|itp_merchandise, 0, 100, weight(2)|abundance(100)|head_armor(10)|body_armor(0)|leg_armor(0), imodbits_cloth ],
["leather_steppe_cap_b", "Steppe Cap ", [("tattered_steppe_cap_b_new",0)], itp_type_head_armor|itp_merchandise, 0, 100, weight(2)|abundance(100)|head_armor(10)|body_armor(0)|leg_armor(0), imodbits_cloth ],
["leather_steppe_cap_c", "Steppe Cap", [("steppe_cap_a_new",0)], itp_type_head_armor|itp_merchandise, 0, 100, weight(2)|abundance(100)|head_armor(10)|body_armor(0)|leg_armor(0), imodbits_cloth ],
["leather_warrior_cap", "Leather Warrior Cap", [("skull_cap_new_b",0)], itp_type_head_armor|itp_merchandise|itp_civilian, 0, 210, weight(2.5)|abundance(140)|head_armor(14)|body_armor(0)|leg_armor(0)|difficulty(6), imodbits_cloth ],
["skullcap", "Skullcap", [("skull_cap_new_a",0)], itp_type_head_armor|itp_merchandise, 0, 300, weight(3)|abundance(120)|head_armor(20)|body_armor(0)|leg_armor(0)|difficulty(8), imodbits_armor ],
["mail_coif", "Mail Coif", [("mail_coif_new",0)], itp_type_head_armor|itp_merchandise, 0, 640, weight(4)|abundance(80)|head_armor(32)|body_armor(0)|leg_armor(0)|difficulty(10), imodbits_armor ],
["footman_helmet", "Footman's Helmet", [("skull_cap_new",0)], itp_type_head_armor|itp_merchandise, 0, 600, weight(4)|abundance(100)|head_armor(30)|body_armor(0)|leg_armor(0)|difficulty(8), imodbits_armor ],
#missing...
["nasal_helmet", "Nasal Helmet", [("nasal_helmet_b",0)], itp_type_head_armor|itp_merchandise, 0, 640, weight(4)|abundance(80)|head_armor(32)|body_armor(0)|leg_armor(0)|difficulty(10), imodbits_armor ],
["norman_helmet", "Helmet with Cap", [("norman_helmet_a",0)], itp_type_head_armor|itp_merchandise|itp_fit_to_head, 0, 640, weight(4)|abundance(80)|head_armor(32)|body_armor(0)|leg_armor(0)|difficulty(10), imodbits_armor ],
["segmented_helmet", "Segmented Helmet", [("segmented_helm_new",0)], itp_type_head_armor|itp_merchandise, 0, 640, weight(4)|abundance(80)|head_armor(32)|body_armor(0)|leg_armor(0)|difficulty(10), imodbits_armor ],
["helmet_with_neckguard", "Helmet with Neckguard", [("neckguard_helm_new",0)], itp_type_head_armor|itp_merchandise, 0, 640, weight(4)|abundance(80)|head_armor(32)|body_armor(0)|leg_armor(0)|difficulty(10), imodbits_armor ],
["flat_topped_helmet", "Flat Topped Helmet", [("flattop_helmet_new",0)], itp_type_head_armor|itp_merchandise, 0, 640, weight(4)|abundance(80)|head_armor(32)|body_armor(0)|leg_armor(0)|difficulty(10), imodbits_armor ],
["kettle_hat", "Kettle Hat", [("kettle_hat_new",0)], itp_type_head_armor|itp_merchandise, 0, 765, weight(4.5)|abundance(80)|head_armor(34)|body_armor(0)|leg_armor(0)|difficulty(10), imodbits_armor ],
["spiked_helmet", "Spiked Helmet", [("spiked_helmet_new",0)], itp_type_head_armor|itp_merchandise, 0, 765, weight(4.5)|abundance(80)|head_armor(34)|body_armor(0)|leg_armor(0)|difficulty(10), imodbits_armor ],
["nordic_helmet", "Nordic Helmet", [("helmet_w_eyeguard_new",0)], itp_type_head_armor|itp_merchandise, 0, 765, weight(4.5)|abundance(80)|head_armor(34)|body_armor(0)|leg_armor(0)|difficulty(10), imodbits_armor ],
["khergit_lady_hat", "Khergit Lady Hat", [("khergit_lady_hat",0)], itp_type_head_armor|itp_doesnt_cover_hair|itp_civilian|itp_fit_to_head, 0, 20, weight(1)|abundance(100)|head_armor(4)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["khergit_lady_hat_b", "Khergit Lady Leather Hat", [("khergit_lady_hat_b",0)], itp_type_head_armor|itp_doesnt_cover_hair|itp_civilian|itp_fit_to_head, 0, 20, weight(1)|abundance(100)|head_armor(4)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["sarranid_felt_hat", "Sarranid Felt Hat", [("sar_helmet3",0)], itp_type_head_armor|itp_merchandise, 0, 20, weight(1)|abundance(100)|head_armor(4)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["turban", "Turban", [("tuareg_open",0)], itp_type_head_armor|itp_merchandise, 0, 100, weight(2)|abundance(100)|head_armor(10)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["desert_turban", "Desert Turban", [("tuareg",0)], itp_type_head_armor|itp_merchandise|itp_covers_beard, 0, 100, weight(2)|abundance(100)|head_armor(10)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["sarranid_warrior_cap", "Sarranid Warrior Cap", [("tuareg_helmet",0)], itp_type_head_armor|itp_merchandise|itp_covers_beard, 0, 330, weight(3)|abundance(120)|head_armor(22)|body_armor(0)|leg_armor(0)|difficulty(8), imodbits_armor ],
["sarranid_horseman_helmet", "Sarranid Horseman Helmet", [("sar_helmet2",0)], itp_type_head_armor|itp_merchandise, 0, 360, weight(3)|abundance(120)|head_armor(24)|body_armor(0)|leg_armor(0)|difficulty(8), imodbits_armor ],
["sarranid_helmet1", "Sarranid Keffiyeh Helmet", [("sar_helmet1",0)], itp_type_head_armor|itp_merchandise, 0, 360, weight(3)|abundance(120)|head_armor(24)|body_armor(0)|leg_armor(0)|difficulty(8), imodbits_armor ],
["sarranid_mail_coif", "Sarranid Mail Coif", [("tuareg_helmet2",0)], itp_type_head_armor|itp_merchandise, 0, 640, weight(4)|abundance(80)|head_armor(32)|body_armor(0)|leg_armor(0)|difficulty(10), imodbits_armor ],
["sarranid_veiled_helmet", "Sarranid Veiled Helmet", [("sar_helmet4",0)], itp_type_head_armor|itp_merchandise|itp_covers_beard, 0, 600, weight(4)|abundance(100)|head_armor(30)|body_armor(0)|leg_armor(0)|difficulty(8), imodbits_armor ],
["nordic_archer_helmet", "Nordic Archer Helmet", [("Helmet_A_vs2",0)], itp_type_head_armor|itp_merchandise, 0, 300, weight(3)|abundance(120)|head_armor(20)|body_armor(0)|leg_armor(0)|difficulty(8), imodbits_armor ],
["nordic_veteran_archer_helmet", "Nordic Leather Helmet", [("Helmet_A",0)], itp_type_head_armor|itp_merchandise, 0, 300, weight(3)|abundance(120)|head_armor(20)|body_armor(0)|leg_armor(0)|difficulty(8), imodbits_armor ],
["nordic_footman_helmet", "Nordic Footman Helmet", [("Helmet_B_vs2",0)], itp_type_head_armor|itp_merchandise|itp_fit_to_head, 0, 765, weight(4.5)|abundance(60)|head_armor(34)|body_armor(0)|leg_armor(0)|difficulty(10), imodbits_armor ],
["nordic_fighter_helmet", "Nordic Fighter Helmet", [("Helmet_B",0)], itp_type_head_armor|itp_merchandise|itp_fit_to_head, 0, 765, weight(4.5)|abundance(60)|head_armor(34)|body_armor(0)|leg_armor(0)|difficulty(10), imodbits_armor ],
["nordic_huscarl_helmet", "Nordic Huscarl's Helmet", [("Helmet_C_vs2",0)], itp_type_head_armor|itp_merchandise, 0, 1260, weight(6)|abundance(40)|head_armor(42)|body_armor(0)|leg_armor(0)|difficulty(12), imodbits_armor ],
["nordic_warlord_helmet", "Nordic Warlord Helmet", [("Helmet_C",0)], itp_type_head_armor|itp_merchandise, 0, 1260, weight(6)|abundance(40)|head_armor(42)|body_armor(0)|leg_armor(0)|difficulty(12), imodbits_armor ],

["vaegir_fur_cap", "Vaegir Fur Cap", [("vaeg_helmet3",0)], itp_type_head_armor|itp_merchandise, 0, 120, weight(2)|abundance(100)|head_armor(12)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["vaegir_fur_helmet", "Vaegir Fur Helmet", [("vaeg_helmet2",0)], itp_type_head_armor|itp_merchandise, 0, 140, weight(2)|abundance(100)|head_armor(14)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_armor ],
["vaegir_spiked_helmet", "Vaegir Spiked Cap", [("vaeg_helmet1",0)], itp_type_head_armor|itp_merchandise, 0, 360, weight(3)|abundance(120)|head_armor(24)|body_armor(0)|leg_armor(0)|difficulty(8), imodbits_armor ],
["vaegir_lamellar_helmet", "Vaegir Lamellar Helmet", [("vaeg_helmet4",0)], itp_type_head_armor|itp_merchandise, 0, 455, weight(3.5)|abundance(120)|head_armor(26)|body_armor(0)|leg_armor(0)|difficulty(8), imodbits_armor ],
["vaegir_noble_helmet", "Vaegir Nobleman Helmet", [("vaeg_helmet7",0)], itp_type_head_armor|itp_merchandise, 0, 900, weight(5)|abundance(60)|head_armor(36)|body_armor(0)|leg_armor(0)|difficulty(10), imodbits_armor ],
["vaegir_war_helmet", "Vaegir War Helmet", [("vaeg_helmet6",0)], itp_type_head_armor|itp_merchandise, 0, 900, weight(5)|abundance(60)|head_armor(36)|body_armor(0)|leg_armor(0)|difficulty(10), imodbits_armor ],
["vaegir_mask", "Vaegir War Mask", [("vaeg_helmet9",0)], itp_type_head_armor|itp_merchandise|itp_covers_beard, 0, 1260, weight(6)|abundance(40)|head_armor(42)|body_armor(0)|leg_armor(0)|difficulty(12), imodbits_armor ],

#TODO:
#["skullcap_b", "Skullcap_b", [("skull_cap_new_b",0)], itp_merchandise| itp_type_head_armor   ,0, 71 , weight(1.5)|abundance(100)|head_armor(20)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate ],

["bascinet", "Bascinet", [("bascinet_avt_new",0)], itp_type_head_armor|itp_merchandise, 0, 900, weight(5)|abundance(60)|head_armor(36)|body_armor(0)|leg_armor(0)|difficulty(10), imodbits_armor ],
["bascinet_2", "Bascinet with Aventail", [("bascinet_new_a",0)], itp_type_head_armor|itp_merchandise, 0, 900, weight(5)|abundance(60)|head_armor(36)|body_armor(0)|leg_armor(0)|difficulty(10), imodbits_armor ],
["bascinet_3", "Bascinet with Nose Guard", [("bascinet_new_b",0)], itp_type_head_armor|itp_merchandise, 0, 900, weight(5)|abundance(60)|head_armor(36)|body_armor(0)|leg_armor(0)|difficulty(10), imodbits_armor ],
["guard_helmet", "Guard Helmet", [("reinf_helmet_new",0)], itp_type_head_armor|itp_merchandise, 0, 950, weight(5)|abundance(60)|head_armor(38)|body_armor(0)|leg_armor(0)|difficulty(10), imodbits_armor ],
#["black_helmet", "Black Helmet", [("black_helm",0)], itp_type_head_armor,0, 638 , weight(2.75)|abundance(100)|head_armor(50)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],
["full_helm", "Full Helm", [("great_helmet_new_b",0)], itp_type_head_armor|itp_merchandise|itp_covers_head, 0, 1200, weight(6)|abundance(40)|head_armor(40)|body_armor(0)|leg_armor(0)|difficulty(12), imodbits_armor ],
["great_helmet", "Great Helmet", [("great_helmet_new",0)], itp_type_head_armor|itp_merchandise|itp_covers_head, 0, 1430, weight(6.5)|abundance(20)|head_armor(48)|body_armor(0)|leg_armor(0)|difficulty(12), imodbits_armor ],
["winged_great_helmet", "Winged Great Helmet", [("maciejowski_helmet_new",0)], itp_type_head_armor|itp_merchandise|itp_covers_head, 0, 1430, weight(6.5)|abundance(20)|head_armor(48)|body_armor(0)|leg_armor(0)|difficulty(12), imodbits_armor ],


#WEAPONS
["wooden_stick",         "Wooden Stick", [("wooden_stick",0)], itp_type_one_handed_wpn|itp_merchandise| itp_primary|itp_wooden_parry|itp_wooden_attack, itc_scimitar,4 , weight(2.5)|difficulty(0)|spd_rtng(99) | weapon_length(63)|swing_damage(13 , blunt) | thrust_damage(0 ,  pierce),imodbits_none ],
["cudgel",         "Cudgel", [("club",0)], itp_type_one_handed_wpn|itp_merchandise| itp_primary|itp_wooden_parry|itp_wooden_attack, itc_scimitar,4 , weight(2.5)|difficulty(0)|spd_rtng(99) | weapon_length(70)|swing_damage(13 , blunt) | thrust_damage(0 ,  pierce),imodbits_none ],
["hammer",         "Hammer", [("iron_hammer_new",0)], itp_type_one_handed_wpn|itp_can_knock_down|itp_merchandise| itp_primary|itp_wooden_parry, itc_scimitar,7 , weight(2)|difficulty(0)|spd_rtng(100) | weapon_length(55)|swing_damage(24 , blunt) | thrust_damage(0 ,  pierce),imodbits_mace ],
["club",         "Club", [("club",0)], itp_type_one_handed_wpn|itp_merchandise| itp_can_knock_down|itp_primary|itp_wooden_parry|itp_wooden_attack, itc_scimitar,11 , weight(2.5)|difficulty(0)|spd_rtng(98) | weapon_length(70)|swing_damage(20 , blunt) | thrust_damage(0 ,  pierce),imodbits_none ],
["winged_mace",         "Flanged Mace", [("flanged_mace",0)], itp_type_one_handed_wpn|itp_can_knock_down|itp_merchandise| itp_primary|itp_wooden_parry, itc_scimitar|itcf_carry_mace_left_hip,122 , weight(3.5)|difficulty(0)|spd_rtng(103) | weapon_length(70)|swing_damage(24 , blunt) | thrust_damage(0 ,  pierce),imodbits_mace ],
["spiked_mace",         "Spiked Mace", [("spiked_mace_new",0)], itp_type_one_handed_wpn|itp_can_knock_down|itp_merchandise| itp_primary|itp_wooden_parry, itc_scimitar|itcf_carry_mace_left_hip,180 , weight(3.5)|difficulty(0)|spd_rtng(98) | weapon_length(70)|swing_damage(28 , blunt) | thrust_damage(0 ,  pierce),imodbits_pick ],
["military_hammer", "Military Hammer", [("military_hammer",0)], itp_type_one_handed_wpn|itp_can_knock_down|itp_merchandise| itp_primary|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip,317 , weight(2)|difficulty(0)|spd_rtng(95) | weapon_length(70)|swing_damage(31 , blunt) | thrust_damage(0 ,  pierce),imodbits_mace ],
["maul",         "Maul", [("maul_b",0)], itp_crush_through|itp_type_two_handed_wpn|itp_merchandise|itp_can_knock_down |itp_primary|itp_two_handed|itp_wooden_parry|itp_wooden_attack|itp_unbalanced, itc_nodachi|itcf_carry_spear,97 , weight(6)|difficulty(11)|spd_rtng(83) | weapon_length(79)|swing_damage(36 , blunt) | thrust_damage(0 ,  pierce),imodbits_mace ],
["sledgehammer", "Sledgehammer", [("maul_c",0)], itp_crush_through|itp_type_two_handed_wpn|itp_merchandise|itp_can_knock_down|itp_primary|itp_two_handed|itp_wooden_parry|itp_wooden_attack|itp_unbalanced, itc_nodachi|itcf_carry_spear,101 , weight(7)|difficulty(10)|spd_rtng(81) | weapon_length(82)|swing_damage(39, blunt) | thrust_damage(0 ,  pierce),imodbits_mace ],
["warhammer",         "Great Hammer", [("maul_d",0)], itp_crush_through|itp_type_two_handed_wpn|itp_merchandise|itp_can_knock_down|itp_primary|itp_two_handed|itp_wooden_parry|itp_wooden_attack|itp_unbalanced, itc_nodachi|itcf_carry_spear,290 , weight(9)|difficulty(14)|spd_rtng(79) | weapon_length(75)|swing_damage(45 , blunt) | thrust_damage(0 ,  pierce),imodbits_mace ],
["pickaxe",         "Pickaxe", [("fighting_pick_new",0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip,27 , weight(3)|difficulty(0)|spd_rtng(99) | weapon_length(70)|swing_damage(19 , pierce) | thrust_damage(0 ,  pierce),imodbits_pick ],
["spiked_club",         "Spiked Club", [("spiked_club",0)], itp_type_one_handed_wpn|itp_primary|itp_wooden_parry, itc_scimitar|itcf_carry_mace_left_hip,83 , weight(3)|difficulty(0)|spd_rtng(97) | weapon_length(70)|swing_damage(21 , pierce) | thrust_damage(0 ,  pierce),imodbits_mace ],
["fighting_pick", "Fighting Pick", [("fighting_pick_new",0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip,108 , weight(1.0)|difficulty(0)|spd_rtng(98) | weapon_length(70)|swing_damage(22 , pierce) | thrust_damage(0 ,  pierce),imodbits_pick ],
["military_pick", "Military Pick", [("steel_pick_new",0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip,280 , weight(1.5)|difficulty(0)|spd_rtng(97) | weapon_length(70)|swing_damage(31 , pierce) | thrust_damage(0 ,  pierce),imodbits_pick ],
["morningstar",         "Morningstar", [("mace_morningstar_new",0)], itp_crush_through|itp_type_two_handed_wpn|itp_merchandise|itp_primary|itp_wooden_parry|itp_unbalanced, itc_morningstar|itcf_carry_axe_left_hip,305 , weight(4.5)|difficulty(13)|spd_rtng(95) | weapon_length(85)|swing_damage(38 , pierce) | thrust_damage(0 ,  pierce),imodbits_mace ],


["sickle",         "Sickle", [("sickle",0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_secondary|itp_no_parry|itp_wooden_parry, itc_cleaver,9 , weight(1.5)|difficulty(0)|spd_rtng(99) | weapon_length(40)|swing_damage(20 , cut) | thrust_damage(0 ,  pierce),imodbits_none ],
["cleaver",         "Cleaver", [("cleaver_new",0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_secondary|itp_no_parry|itp_wooden_parry, itc_cleaver,14 , weight(1.5)|difficulty(0)|spd_rtng(103) | weapon_length(35)|swing_damage(24 , cut) | thrust_damage(0 ,  pierce),imodbits_none ],
["knife",         "Knife", [("peasant_knife_new",0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_secondary|itp_no_parry, itc_dagger|itcf_carry_dagger_front_left,18 , weight(0.5)|difficulty(0)|spd_rtng(110) | weapon_length(40)|swing_damage(21 , cut) | thrust_damage(13 ,  pierce),imodbits_sword ],
["butchering_knife", "Butchering Knife", [("khyber_knife_new",0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_secondary|itp_no_parry, itc_dagger|itcf_carry_dagger_front_right,23 , weight(0.75)|difficulty(0)|spd_rtng(108) | weapon_length(60)|swing_damage(24 , cut) | thrust_damage(17 ,  pierce),imodbits_sword ],
["dagger",         "Dagger", [("dagger_b",0),("dagger_b_scabbard",ixmesh_carry),("dagger_b",imodbits_good),("dagger_b_scabbard",ixmesh_carry|imodbits_good)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_secondary|itp_no_parry, itc_dagger|itcf_carry_dagger_front_left|itcf_show_holster_when_drawn,37 , weight(0.75)|difficulty(0)|spd_rtng(109) | weapon_length(47)|swing_damage(22 , cut) | thrust_damage(19 ,  pierce),imodbits_sword_high ],
#["nordic_sword", "Nordic Sword", [("viking_sword",0),("scab_vikingsw", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 142 , weight(1.5)|difficulty(0)|spd_rtng(99) | weapon_length(98)|swing_damage(27 , cut) | thrust_damage(19 ,  pierce),imodbits_sword ],
#["arming_sword", "Arming Sword", [("b_long_sword",0),("scab_longsw_b", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 156 , weight(1.5)|difficulty(0)|spd_rtng(101) | weapon_length(100)|swing_damage(25 , cut) | thrust_damage(22 ,  pierce),imodbits_sword ],
#["sword",         "Sword", [("long_sword",0),("scab_longsw_a", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 148 , weight(1.5)|difficulty(0)|spd_rtng(100) | weapon_length(102)|swing_damage(26 , cut) | thrust_damage(23 ,  pierce),imodbits_sword ],
["falchion",         "Falchion", [("falchion_new",0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_scimitar|itcf_carry_sword_left_hip,105 , weight(2.5)|difficulty(8)|spd_rtng(96) | weapon_length(73)|swing_damage(30 , cut) | thrust_damage(0 ,  pierce),imodbits_sword ],
#["broadsword",         "Broadsword", [("broadsword",0),("scab_broadsword", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_scimitar|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 122 , weight(2.5)|difficulty(8)|spd_rtng(91) | weapon_length(101)|swing_damage(27 , cut) | thrust_damage(0 ,  pierce),imodbits_sword ],
#["scimitar",         "Scimitar", [("scimeter",0),("scab_scimeter", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_scimitar|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
#108 , weight(1.5)|difficulty(0)|spd_rtng(100) | weapon_length(97)|swing_damage(29 , cut) | thrust_damage(0 ,  pierce),imodbits_sword_high ],

["scimitar",         "Scimitar", [("scimitar_a",0),("scab_scimeter_a", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_scimitar|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,210 , weight(1.5)|difficulty(0)|spd_rtng(101) | weapon_length(97)|swing_damage(30 , cut) | thrust_damage(0 ,  pierce),imodbits_sword_high ],
["scimitar_b",         "Elite Scimitar", [("scimitar_b",0),("scab_scimeter_b", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_scimitar|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,290 , weight(1.5)|difficulty(0)|spd_rtng(100) | weapon_length(103)|swing_damage(32 , cut) | thrust_damage(0 ,  pierce),imodbits_sword_high ],

["arabian_sword_a",         "Sarranid Sword", [("arabian_sword_a",0),("scab_arabian_sword_a", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,108 , weight(1.5)|difficulty(0)|spd_rtng(99) | weapon_length(97)|swing_damage(26 , cut) | thrust_damage(19 ,  pierce),imodbits_sword_high ],
["arabian_sword_b",         "Sarranid Arming Sword", [("arabian_sword_b",0),("scab_arabian_sword_b", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,218 , weight(1.5)|difficulty(0)|spd_rtng(99) | weapon_length(97)|swing_damage(28 , cut) | thrust_damage(19 ,  pierce),imodbits_sword_high ],
["sarranid_cavalry_sword",         "Sarranid Cavalry Sword", [("arabian_sword_c",0),("scab_arabian_sword_c", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,310 , weight(1.5)|difficulty(0)|spd_rtng(98) | weapon_length(105)|swing_damage(28 , cut) | thrust_damage(19 ,  pierce),imodbits_sword_high ],
["arabian_sword_d",         "Sarranid Guard Sword", [("arabian_sword_d",0),("scab_arabian_sword_d", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,420 , weight(1.5)|difficulty(0)|spd_rtng(99) | weapon_length(97)|swing_damage(30 , cut) | thrust_damage(20 ,  pierce),imodbits_sword_high ],


#["nomad_sabre",         "Nomad Sabre", [("shashqa",0),("scab_shashqa", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_scimitar|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 115 , weight(1.75)|difficulty(0)|spd_rtng(101) | weapon_length(100)|swing_damage(27 , cut) | thrust_damage(0 ,  pierce),imodbits_sword ],
#["bastard_sword", "Bastard Sword", [("bastard_sword",0),("scab_bastardsw", ixmesh_carry)], itp_type_two_handed_wpn|itp_merchandise| itp_primary, itc_bastardsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 279 , weight(2.25)|difficulty(8)|spd_rtng(102) | weapon_length(120)|swing_damage(33 , cut) | thrust_damage(27 ,  pierce),imodbits_sword ],
["great_sword",         "Great Sword", [("b_bastard_sword",0),("scab_bastardsw_b", ixmesh_carry)], itp_type_two_handed_wpn|itp_merchandise| itp_two_handed|itp_primary, itc_greatsword|itcf_carry_sword_back|itcf_show_holster_when_drawn, 423 , weight(2.75)|difficulty(10)|spd_rtng(95) | weapon_length(125)|swing_damage(39 , cut) | thrust_damage(31 ,  pierce),imodbits_sword_high ],
["sword_of_war", "Sword of War", [("b_bastard_sword",0),("scab_bastardsw_b", ixmesh_carry)], itp_type_two_handed_wpn|itp_merchandise| itp_two_handed|itp_primary, itc_greatsword|itcf_carry_sword_back|itcf_show_holster_when_drawn, 524 , weight(3)|difficulty(11)|spd_rtng(94) | weapon_length(130)|swing_damage(40 , cut) | thrust_damage(31 ,  pierce),imodbits_sword_high ],
["hatchet",         "Hatchet", [("hatchet",0)], itp_type_one_handed_wpn|itp_merchandise| itp_primary|itp_secondary|itp_bonus_against_shield|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip,13 , weight(2)|difficulty(0)|spd_rtng(97) | weapon_length(60)|swing_damage(23 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["hand_axe",         "Hand Axe", [("hatchet",0)], itp_type_one_handed_wpn|itp_merchandise| itp_primary|itp_secondary|itp_bonus_against_shield|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip,24 , weight(2)|difficulty(7)|spd_rtng(95) | weapon_length(75)|swing_damage(27 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["fighting_axe", "Fighting Axe", [("fighting_ax",0)], itp_type_one_handed_wpn|itp_merchandise| itp_primary|itp_secondary|itp_bonus_against_shield|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip,77 , weight(2.5)|difficulty(8)|spd_rtng(92) | weapon_length(90)|swing_damage(31 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["axe",                 "Axe", [("iron_ax",0)], itp_type_two_handed_wpn|itp_merchandise| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry, itc_nodachi|itcf_carry_axe_back,65 , weight(4)|difficulty(8)|spd_rtng(91) | weapon_length(108)|swing_damage(32 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["voulge", "Voulge", [("voulge",0)], itp_type_two_handed_wpn|itp_merchandise|itp_wooden_parry|itp_two_handed|itp_primary|itp_bonus_against_shield, itc_nodachi|itcf_carry_axe_back, 140, weight(5)|difficulty(6)|spd_rtng(90)|weapon_length(120)|swing_damage(40,cut)|thrust_damage(20,pierce), imodbits_axe ],
["battle_axe",         "Battle Axe", [("battle_ax",0)], itp_type_two_handed_wpn|itp_merchandise| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry, itc_nodachi|itcf_carry_axe_back,240 , weight(5)|difficulty(8)|spd_rtng(88) | weapon_length(108)|swing_damage(41 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["war_axe",         "War Axe", [("war_ax",0)], itp_type_two_handed_wpn|itp_merchandise| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry, itc_nodachi|itcf_carry_axe_back,264 , weight(5)|difficulty(10)|spd_rtng(86) | weapon_length(110)|swing_damage(43 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
#["double_axe",         "Double Axe", [("dblhead_ax",0)], itp_type_two_handed_wpn|itp_merchandise| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry, itc_nodachi|itcf_carry_axe_back, 359 , weight(6.5)|difficulty(10)|spd_rtng(85) | weapon_length(95)|swing_damage(43 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
#["great_axe",         "Great Axe", [("great_ax",0)], itp_type_two_handed_wpn|itp_merchandise| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry, itc_nodachi|itcf_carry_axe_back, 415 , weight(7)|difficulty(13)|spd_rtng(82) | weapon_length(120)|swing_damage(45 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],

["sword_two_handed_b",         "Two Handed Sword", [("sword_two_handed_b",0)], itp_type_two_handed_wpn|itp_merchandise| itp_two_handed|itp_primary, itc_greatsword|itcf_carry_sword_back, 670 , weight(2.75)|difficulty(10)|spd_rtng(97) | weapon_length(110)|swing_damage(40 , cut) | thrust_damage(28 ,  pierce),imodbits_sword_high ],
["sword_two_handed_a",         "Great Sword", [("sword_two_handed_a",0)], itp_type_two_handed_wpn|itp_merchandise|itp_two_handed|itp_primary, itc_greatsword|itcf_carry_sword_back, 1123 , weight(2.75)|difficulty(10)|spd_rtng(96) | weapon_length(120)|swing_damage(42 , cut) | thrust_damage(29 ,  pierce),imodbits_sword_high ],


["khergit_sword_two_handed_a",         "Two Handed Sabre", [("khergit_sword_two_handed_a",0)], itp_type_two_handed_wpn|itp_merchandise|itp_two_handed|itp_primary, itc_nodachi|itcf_carry_sword_back, 523 , weight(2.75)|difficulty(10)|spd_rtng(96) | weapon_length(120)|swing_damage(40 , cut) | thrust_damage(0 ,  pierce),imodbits_sword_high ],
["khergit_sword_two_handed_b",         "Two Handed Sabre", [("khergit_sword_two_handed_b",0)], itp_type_two_handed_wpn|itp_merchandise|itp_two_handed|itp_primary, itc_nodachi|itcf_carry_sword_back, 920 , weight(2.75)|difficulty(10)|spd_rtng(96) | weapon_length(120)|swing_damage(44 , cut) | thrust_damage(0 ,  pierce),imodbits_sword_high ],

["two_handed_cleaver", "War Cleaver", [("military_cleaver_a",0)], itp_type_two_handed_wpn|itp_merchandise|itp_two_handed|itp_primary, itc_nodachi|itcf_carry_sword_back, 640 , weight(2.75)|difficulty(10)|spd_rtng(93) | weapon_length(120)|swing_damage(45 , cut) | thrust_damage(0 ,  cut),imodbits_sword_high ],
["military_cleaver_b", "Soldier's Cleaver", [("military_cleaver_b",0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_scimitar|itcf_carry_sword_left_hip, 193 , weight(1.5)|difficulty(0)|spd_rtng(96) | weapon_length(95)|swing_damage(31 , cut) | thrust_damage(0 ,  pierce),imodbits_sword_high ],
["military_cleaver_c", "Military Cleaver", [("military_cleaver_c",0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_scimitar|itcf_carry_sword_left_hip, 263 , weight(1.5)|difficulty(0)|spd_rtng(96) | weapon_length(95)|swing_damage(35 , cut) | thrust_damage(0 ,  pierce),imodbits_sword_high ],

["military_sickle_a", "Military Sickle", [("military_sickle_a",0)], itp_type_one_handed_wpn|itp_merchandise| itp_primary|itp_secondary|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip, 220 , weight(1.0)|difficulty(8)|spd_rtng(100) | weapon_length(75)|swing_damage(26 , pierce) | thrust_damage(0 ,  pierce),imodbits_axe ],


["bastard_sword_a", "Bastard Sword", [("bastard_sword_a",0),("bastard_sword_a_scabbard", ixmesh_carry)], itp_type_two_handed_wpn|itp_merchandise| itp_primary, itc_bastardsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 294 , weight(2.0)|difficulty(8)|spd_rtng(98) | weapon_length(101)|swing_damage(35 , cut) | thrust_damage(26 ,  pierce),imodbits_sword_high ],
["bastard_sword_b", "Heavy Bastard Sword", [("bastard_sword_b",0),("bastard_sword_b_scabbard", ixmesh_carry)], itp_type_two_handed_wpn|itp_merchandise| itp_primary, itc_bastardsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 526 , weight(2.25)|difficulty(8)|spd_rtng(97) | weapon_length(105)|swing_damage(37 , cut) | thrust_damage(27 ,  pierce),imodbits_sword_high ],

["one_handed_war_axe_a", "One Handed Axe", [("one_handed_war_axe_a",0)], itp_type_one_handed_wpn|itp_merchandise| itp_primary|itp_secondary|itp_bonus_against_shield|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip, 87 , weight(1.5)|difficulty(8)|spd_rtng(98) | weapon_length(71)|swing_damage(32 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["one_handed_battle_axe_a", "One Handed Battle Axe", [("one_handed_battle_axe_a",0)], itp_type_one_handed_wpn|itp_merchandise| itp_primary|itp_secondary|itp_bonus_against_shield|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip, 142 , weight(1.5)|difficulty(8)|spd_rtng(98) | weapon_length(73)|swing_damage(34 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["one_handed_war_axe_b", "One Handed War Axe", [("one_handed_war_axe_b",0)], itp_type_one_handed_wpn|itp_merchandise| itp_primary|itp_secondary|itp_bonus_against_shield|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip, 190 , weight(1.5)|difficulty(8)|spd_rtng(98) | weapon_length(76)|swing_damage(34 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["one_handed_battle_axe_b", "One Handed Battle Axe", [("one_handed_battle_axe_b",0)], itp_type_one_handed_wpn|itp_merchandise| itp_primary|itp_secondary|itp_bonus_against_shield|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip, 230 , weight(1.75)|difficulty(8)|spd_rtng(98) | weapon_length(72)|swing_damage(36 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["one_handed_battle_axe_c", "One Handed Battle Axe", [("one_handed_battle_axe_c",0)], itp_type_one_handed_wpn|itp_merchandise| itp_primary|itp_secondary|itp_bonus_against_shield|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip, 550 , weight(2.0)|difficulty(8)|spd_rtng(98) | weapon_length(76)|swing_damage(37 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],


["two_handed_axe",         "Two Handed Axe", [("two_handed_battle_axe_a",0)], itp_type_two_handed_wpn|itp_merchandise| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_unbalanced, itc_nodachi|itcf_carry_axe_back, 90 , weight(4.5)|difficulty(10)|spd_rtng(96) | weapon_length(90)|swing_damage(38 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["two_handed_battle_axe_2",         "Two Handed War Axe", [("two_handed_battle_axe_b",0)], itp_type_two_handed_wpn|itp_merchandise| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_unbalanced, itc_nodachi|itcf_carry_axe_back, 152 , weight(4.5)|difficulty(10)|spd_rtng(96) | weapon_length(92)|swing_damage(44 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["shortened_voulge",         "Shortened Voulge", [("two_handed_battle_axe_c",0)], itp_type_two_handed_wpn|itp_merchandise| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_unbalanced, itc_nodachi|itcf_carry_axe_back, 228 , weight(4.5)|difficulty(10)|spd_rtng(92) | weapon_length(100)|swing_damage(45 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["great_axe",         "Great Axe", [("two_handed_battle_axe_e",0)], itp_type_two_handed_wpn|itp_merchandise| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_unbalanced, itc_nodachi|itcf_carry_axe_back, 316 , weight(4.5)|difficulty(10)|spd_rtng(94) | weapon_length(96)|swing_damage(47 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["long_axe", "Long Axe", [("long_axe_a",0)], itp_type_polearm|itp_merchandise|itp_wooden_parry|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_next_item_as_melee, itc_staff|itcf_carry_axe_back, 240, weight(8)|difficulty(10)|spd_rtng(90)|weapon_length(120)|swing_damage(40,cut)|thrust_damage(14,blunt), imodbits_axe ],
["long_axe_alt", "Long Axe", [("long_axe_a",0)], itp_type_two_handed_wpn|itp_wooden_parry|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_unbalanced, itc_nodachi|itcf_carry_axe_back, 240, weight(8)|difficulty(10)|spd_rtng(90)|weapon_length(120)|swing_damage(40,cut)|thrust_damage(14,blunt), imodbits_axe ],
["long_axe_b", "Long War Axe", [("long_axe_b",0)], itp_type_polearm|itp_merchandise|itp_wooden_parry|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_next_item_as_melee, itc_staff|itcf_carry_axe_back, 300, weight(10)|difficulty(12)|spd_rtng(80)|weapon_length(120)|swing_damage(44,cut)|thrust_damage(16,blunt), imodbits_axe ],
["long_axe_b_alt", "Long War Axe", [("long_axe_b",0)], itp_type_two_handed_wpn|itp_wooden_parry|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_unbalanced, itc_nodachi|itcf_carry_axe_back, 300, weight(10)|difficulty(12)|spd_rtng(80)|weapon_length(120)|swing_damage(44,cut)|thrust_damage(16,blunt), imodbits_axe ],
["long_axe_c", "Great Long Axe", [("long_axe_c",0)], itp_type_polearm|itp_merchandise|itp_wooden_parry|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_next_item_as_melee, itc_staff|itcf_carry_axe_back, 480, weight(12)|difficulty(15)|spd_rtng(70)|weapon_length(130)|swing_damage(50,cut)|thrust_damage(18,blunt), imodbits_axe ],
["long_axe_c_alt", "Great Long Axe", [("long_axe_c",0)], itp_type_two_handed_wpn|itp_wooden_parry|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_unbalanced, itc_nodachi|itcf_carry_axe_back, 480, weight(12)|difficulty(15)|spd_rtng(70)|weapon_length(130)|swing_damage(50,cut)|thrust_damage(18,blunt), imodbits_axe ],

["bardiche", "Bardiche", [("two_handed_battle_axe_d",0)], itp_type_two_handed_wpn|itp_merchandise|itp_wooden_parry|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_unbalanced, itc_nodachi|itcf_carry_axe_back, 160, weight(6)|difficulty(10)|spd_rtng(100)|weapon_length(110)|swing_damage(40,cut)|thrust_damage(6,blunt), imodbits_axe ],
["great_bardiche",         "Great Bardiche", [("two_handed_battle_axe_f",0)], itp_type_two_handed_wpn|itp_merchandise|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_unbalanced, itc_nodachi|itcf_carry_axe_back, 617 , weight(4.5)|difficulty(10)|spd_rtng(89) | weapon_length(116)|swing_damage(50 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],




["voulge", "Voulge", [("two_handed_battle_long_axe_a",0)], itp_type_polearm|itp_merchandise|itp_wooden_parry|itp_two_handed|itp_primary|itp_bonus_against_shield, itc_staff, 140, weight(5)|difficulty(12)|spd_rtng(90)|weapon_length(120)|swing_damage(40,cut)|thrust_damage(20,pierce), imodbits_axe ],
["long_bardiche", "Long Bardiche", [("two_handed_battle_long_axe_b",0)], itp_type_polearm|itp_merchandise|itp_wooden_parry|itp_two_handed|itp_primary|itp_bonus_against_shield, itc_staff, 240, weight(8)|difficulty(12)|spd_rtng(90)|weapon_length(140)|swing_damage(44,cut)|thrust_damage(10,blunt), imodbits_axe ],
["great_long_bardiche", "Great Long Bardiche", [("two_handed_battle_long_axe_c",0)], itp_type_polearm|itp_merchandise|itp_wooden_parry|itp_two_handed|itp_primary|itp_bonus_against_shield, itc_staff, 400, weight(12)|difficulty(15)|spd_rtng(80)|weapon_length(160)|swing_damage(50,cut)|thrust_damage(14,blunt), imodbits_axe ],

["hafted_blade_b", "Hafted Blade", [("khergit_pike_b",0)], itp_type_polearm|itp_merchandise|itp_wooden_parry|itp_two_handed|itp_primary|itp_penalty_with_shield, itc_guandao|itcf_carry_spear, 80, weight(3)|difficulty(0)|spd_rtng(80)|weapon_length(120)|swing_damage(32,cut)|thrust_damage(18,pierce), imodbits_polearm ],
["hafted_blade_a", "Hafted Blade", [("khergit_pike_a",0)], itp_type_polearm|itp_merchandise|itp_wooden_parry|itp_two_handed|itp_primary|itp_penalty_with_shield, itc_guandao|itcf_carry_spear, 80, weight(3)|difficulty(0)|spd_rtng(80)|weapon_length(120)|swing_damage(32,cut)|thrust_damage(18,pierce), imodbits_polearm ],

["shortened_military_scythe",         "Shortened Military Scythe", [("two_handed_battle_scythe_a",0)], itp_type_two_handed_wpn|itp_merchandise| itp_two_handed|itp_primary, itc_nodachi|itcf_carry_sword_back, 264 , weight(3.0)|difficulty(10)|spd_rtng(90) | weapon_length(112)|swing_damage(45 , cut) | thrust_damage(0 ,  pierce),imodbits_sword_high ],

["sword_medieval_a", "Sword", [("sword_medieval_a",0),("sword_medieval_a_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 163 , weight(1.5)|difficulty(0)|spd_rtng(99) | weapon_length(95)|swing_damage(27 , cut) | thrust_damage(22 ,  pierce),imodbits_sword_high ],
#["sword_medieval_a_long", "Sword", [("sword_medieval_a_long",0),("sword_medieval_a_long_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 156 , weight(1.5)|difficulty(0)|spd_rtng(97) | weapon_length(105)|swing_damage(25 , cut) | thrust_damage(22 ,  pierce),imodbits_sword ],
["sword_medieval_b", "Sword", [("sword_medieval_b",0),("sword_medieval_b_scabbard", ixmesh_carry),("sword_rusty_a",imodbit_rusty),("sword_rusty_a_scabbard", ixmesh_carry|imodbit_rusty)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 243 , weight(1.5)|difficulty(0)|spd_rtng(99) | weapon_length(95)|swing_damage(28 , cut) | thrust_damage(23 ,  pierce),imodbits_sword_high ],
["sword_medieval_b_small", "Short Sword", [("sword_medieval_b_small",0),("sword_medieval_b_small_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 152 , weight(1.5)|difficulty(0)|spd_rtng(102) | weapon_length(85)|swing_damage(26, cut) | thrust_damage(24, pierce),imodbits_sword_high ],
["sword_medieval_c", "Arming Sword", [("sword_medieval_c",0),("sword_medieval_c_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 410 , weight(1.5)|difficulty(0)|spd_rtng(99) | weapon_length(95)|swing_damage(29 , cut) | thrust_damage(24 ,  pierce),imodbits_sword_high ],
["sword_medieval_c_small", "Short Arming Sword", [("sword_medieval_c_small",0),("sword_medieval_c_small_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 243 , weight(1.5)|difficulty(0)|spd_rtng(103) | weapon_length(86)|swing_damage(26, cut) | thrust_damage(24 ,  pierce),imodbits_sword_high ],
["sword_medieval_c_long", "Long Arming Sword", [("sword_medieval_c_long",0),("sword_medieval_c_long_scabbard",ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 480, weight(1.5)|difficulty(0)|spd_rtng(99)|weapon_length(100)|swing_damage(29,cut)|thrust_damage(28,pierce), imodbits_sword_high ],
["sword_medieval_d_long", "Long Arming Sword", [("sword_medieval_d_long",0),("sword_medieval_d_long_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 550 , weight(1.5)|difficulty(0)|spd_rtng(96) | weapon_length(105)|swing_damage(33 , cut) | thrust_damage(28 ,  pierce),imodbits_sword ],
#["sword_medieval_d", "sword_medieval_d", [("sword_medieval_d",0),("sword_medieval_d_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
# 131 , weight(1.5)|difficulty(0)|spd_rtng(99) | weapon_length(95)|swing_damage(24 , cut) | thrust_damage(21 ,  pierce),imodbits_sword ],
#["sword_medieval_e", "sword_medieval_e", [("sword_medieval_e",0),("sword_medieval_e_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
# 131 , weight(1.5)|difficulty(0)|spd_rtng(99) | weapon_length(95)|swing_damage(24 , cut) | thrust_damage(21 ,  pierce),imodbits_sword ],

["sword_viking_1", "Nordic Sword", [("sword_viking_c",0),("sword_viking_c_scabbard ", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 147 , weight(1.5)|difficulty(0)|spd_rtng(99) | weapon_length(94)|swing_damage(28 , cut) | thrust_damage(20 ,  pierce),imodbits_sword_high ] ,
["sword_viking_2", "Nordic Sword", [("sword_viking_b",0),("sword_viking_b_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 276 , weight(1.5)|difficulty(0)|spd_rtng(99) | weapon_length(95)|swing_damage(29 , cut) | thrust_damage(21 ,  pierce),imodbits_sword_high ],
["sword_viking_2_small", "Nordic Short Sword", [("sword_viking_b_small",0),("sword_viking_b_small_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 162 , weight(1.25)|difficulty(0)|spd_rtng(103) | weapon_length(85)|swing_damage(28 , cut) | thrust_damage(21 ,  pierce),imodbits_sword_high ],
["sword_viking_3", "Nordic War Sword", [("sword_viking_a",0),("sword_viking_a_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 394 , weight(1.5)|difficulty(0)|spd_rtng(99) | weapon_length(95)|swing_damage(30 , cut) | thrust_damage(21 ,  pierce),imodbits_sword_high ],
#["sword_viking_a_long", "sword_viking_a_long", [("sword_viking_a_long",0),("sword_viking_a_long_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
# 142 , weight(1.5)|difficulty(0)|spd_rtng(97) | weapon_length(105)|swing_damage(27 , cut) | thrust_damage(19 ,  pierce),imodbits_sword ],
["sword_viking_3_small", "Nordic Short War Sword", [("sword_viking_a_small",0),("sword_viking_a_small_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 280 , weight(1.25)|difficulty(0)|spd_rtng(103) | weapon_length(86)|swing_damage(29 , cut) | thrust_damage(21 ,  pierce),imodbits_sword_high ],
#["sword_viking_c_long", "sword_viking_c_long", [("sword_viking_c_long",0),("sword_viking_c_long_scabbard ", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
# 142 , weight(1.5)|difficulty(0)|spd_rtng(95) | weapon_length(105)|swing_damage(27 , cut) | thrust_damage(19 ,  pierce),imodbits_sword ] ,

["sword_khergit_1", "Nomad Sabre", [("khergit_sword_b",0),("khergit_sword_b_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_scimitar|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 105 , weight(1.25)|difficulty(0)|spd_rtng(100) | weapon_length(97)|swing_damage(29 , cut),imodbits_sword_high ],
["sword_khergit_2", "Sabre", [("khergit_sword_c",0),("khergit_sword_c_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_scimitar|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 191 , weight(1.5)|difficulty(0)|spd_rtng(99) | weapon_length(97)|swing_damage(30 , cut),imodbits_sword_high ],
["sword_khergit_3", "Sabre", [("khergit_sword_a",0),("khergit_sword_a_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_scimitar|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 294 , weight(1.5)|difficulty(0)|spd_rtng(99) | weapon_length(98)|swing_damage(31 , cut),imodbits_sword_high ],
["sword_khergit_4", "Heavy Sabre", [("khergit_sword_d",0),("khergit_sword_d_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_scimitar|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 384 , weight(1.75)|difficulty(0)|spd_rtng(98) | weapon_length(96)|swing_damage(33 , cut),imodbits_sword_high ],



["mace_1",         "Spiked Club", [("mace_d",0)], itp_type_one_handed_wpn|itp_can_knock_down|itp_merchandise| itp_primary|itp_wooden_parry, itc_scimitar|itcf_carry_mace_left_hip, 45 , weight(1.5)|difficulty(0)|spd_rtng(99) | weapon_length(70)|swing_damage(19 , pierce) | thrust_damage(0 ,  pierce),imodbits_mace ],
["mace_2",         "Knobbed_Mace", [("mace_a",0)], itp_type_one_handed_wpn|itp_can_knock_down|itp_merchandise| itp_primary|itp_wooden_parry, itc_scimitar|itcf_carry_mace_left_hip, 98 , weight(2.5)|difficulty(0)|spd_rtng(98) | weapon_length(70)|swing_damage(21 , blunt) | thrust_damage(0 ,  pierce),imodbits_mace ],
["mace_3",         "Spiked Mace", [("mace_c",0)], itp_type_one_handed_wpn|itp_can_knock_down|itp_merchandise| itp_primary|itp_wooden_parry, itc_scimitar|itcf_carry_mace_left_hip, 152 , weight(2.5)|difficulty(0)|spd_rtng(98) | weapon_length(70)|swing_damage(23 , blunt) | thrust_damage(0 ,  pierce),imodbits_mace ],
["mace_4",         "Winged_Mace", [("mace_b",0)], itp_type_one_handed_wpn|itp_can_knock_down|itp_merchandise| itp_primary|itp_wooden_parry, itc_scimitar|itcf_carry_mace_left_hip, 212 , weight(2.5)|difficulty(0)|spd_rtng(98) | weapon_length(70)|swing_damage(24 , blunt) | thrust_damage(0 ,  pierce),imodbits_mace ],
# Goedendag
["club_with_spike_head",  "Spiked Staff", [("mace_e",0)],  itp_type_two_handed_wpn|itp_merchandise|itp_can_knock_down|itp_primary|itp_wooden_parry, itc_bastardsword|itcf_carry_axe_back, 200 , weight(2.5)|difficulty(8)|spd_rtng(95) | weapon_length(117)|swing_damage(24 , blunt) | thrust_damage(20 ,  pierce),imodbits_mace ],

["long_spiked_club", "Long Spiked Club", [("mace_long_c",0)], itp_type_polearm|itp_merchandise|itp_wooden_parry|itp_primary|itp_can_knock_down, itc_staff|itcf_carry_axe_back, 160, weight(3)|difficulty(8)|spd_rtng(80)|weapon_length(100)|swing_damage(22,pierce)|thrust_damage(14,blunt), imodbits_mace ],
["long_hafted_knobbed_mace", "Long Hafted Knobbed Mace", [("mace_long_a",0)], itp_type_polearm|itp_wooden_parry|itp_primary|itp_can_knock_down, itc_staff|itcf_carry_axe_back, 300, weight(5)|difficulty(12)|spd_rtng(90)|weapon_length(130)|swing_damage(30,blunt)|thrust_damage(18,blunt), imodbits_mace ],
["long_hafted_spiked_mace", "Long Hafted Spiked Mace", [("mace_long_b",0)], itp_type_polearm|itp_merchandise|itp_wooden_parry|itp_primary|itp_can_knock_down, itc_staff|itcf_carry_axe_back, 220, weight(4)|difficulty(10)|spd_rtng(90)|weapon_length(120)|swing_damage(28,pierce)|thrust_damage(16,blunt), imodbits_mace ],

["sarranid_two_handed_mace_1",         "Iron Mace", [("mace_long_d",0)], itp_type_two_handed_wpn|itp_can_knock_down|itp_two_handed|itp_merchandise| itp_primary|itp_crush_through|itp_unbalanced, itc_greatsword|itcf_carry_axe_back,470 , weight(4.5)|difficulty(0)|spd_rtng(90) | weapon_length(95)|swing_damage(35 , blunt) | thrust_damage(22 ,  blunt),imodbits_mace ],


["sarranid_mace_1",         "Iron Mace", [("mace_small_d",0)], itp_type_one_handed_wpn|itp_merchandise|itp_can_knock_down |itp_primary|itp_wooden_parry, itc_scimitar|itcf_carry_mace_left_hip, 45 , weight(2.0)|difficulty(0)|spd_rtng(99) | weapon_length(73)|swing_damage(22 , blunt) | thrust_damage(0 ,  pierce),imodbits_mace ],
["sarranid_axe_a", "Iron Battle Axe", [("one_handed_battle_axe_g",0)], itp_type_one_handed_wpn|itp_merchandise| itp_primary|itp_secondary|itp_bonus_against_shield|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip, 250 , weight(1.75)|difficulty(8)|spd_rtng(97) | weapon_length(71)|swing_damage(35 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["sarranid_axe_b", "Iron War Axe", [("one_handed_battle_axe_h",0)], itp_type_one_handed_wpn|itp_merchandise| itp_primary|itp_secondary|itp_bonus_against_shield|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip, 360 , weight(1.75)|difficulty(8)|spd_rtng(97) | weapon_length(71)|swing_damage(38 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],

["sarranid_two_handed_axe_a",         "Sarranid Battle Axe", [("two_handed_battle_axe_g",0)], itp_type_two_handed_wpn|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_unbalanced, itc_nodachi|itcf_carry_axe_back, 350 , weight(3.0)|difficulty(10)|spd_rtng(89) | weapon_length(95)|swing_damage(49 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["sarranid_two_handed_axe_b",         "Sarranid War Axe", [("two_handed_battle_axe_h",0)], itp_type_two_handed_wpn|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_unbalanced, itc_nodachi|itcf_carry_axe_back, 280 , weight(3.0)|difficulty(10)|spd_rtng(90) | weapon_length(90)|swing_damage(46 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],




["scythe", "Scythe", [("scythe",0)], itp_type_polearm|itp_merchandise|itp_wooden_parry|itp_primary|itp_penalty_with_shield|itp_offset_lance, itc_staff|itcf_carry_spear, 20, weight(4)|difficulty(12)|spd_rtng(90)|weapon_length(160)|swing_damage(26,cut)|thrust_damage(12,pierce), imodbits_polearm ],
["pitch_fork", "Pitch Fork", [("pitch_fork",0)], itp_type_polearm|itp_merchandise|itp_wooden_parry|itp_primary|itp_penalty_with_shield|itp_offset_lance, itc_spear, 40, weight(5)|difficulty(0)|spd_rtng(110)|weapon_length(120)|swing_damage(12,blunt)|thrust_damage(20,pierce), imodbits_polearm ],
["military_fork", "Military Fork", [("military_fork",0)], itp_type_polearm|itp_merchandise|itp_wooden_parry|itp_primary|itp_offset_lance, itc_spear, 120, weight(6.5)|difficulty(6)|spd_rtng(100)|weapon_length(120)|swing_damage(16,blunt)|thrust_damage(32,pierce), imodbits_polearm ],
["battle_fork", "Battle Fork", [("battle_fork",0)], itp_type_polearm|itp_merchandise|itp_wooden_parry|itp_primary|itp_offset_lance, itc_spear, 220, weight(8)|difficulty(8)|spd_rtng(90)|weapon_length(120)|swing_damage(18,blunt)|thrust_damage(40,pierce), imodbits_polearm ],
["boar_spear", "Boar Spear", [("spear",0)], itp_type_polearm|itp_merchandise|itp_wooden_parry|itp_primary|itp_penalty_with_shield, itc_spear|itcf_carry_spear, 75, weight(2)|difficulty(0)|spd_rtng(80)|weapon_length(160)|swing_damage(14,blunt)|thrust_damage(20,pierce), imodbits_polearm ],
#["spear",         "Spear", [("spear",0)], itp_type_polearm|itp_merchandise| itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_cutting_spear|itcf_carry_spear, 173 , weight(4.5)|difficulty(0)|spd_rtng(80) | weapon_length(158)|swing_damage(17 , blunt) | thrust_damage(23 ,  pierce),imodbits_polearm ],


["jousting_lance", "Jousting Lance", [("joust_of_peace",0)], itp_type_polearm|itp_merchandise|itp_wooden_parry|itp_primary|itp_penalty_with_shield|itp_couchable|itp_can_knock_down, itc_greatlance, 60, weight(10)|difficulty(0)|spd_rtng(50)|weapon_length(260)|swing_damage(4,blunt)|thrust_damage(20,blunt), imodbits_polearm ],
#["lance",         "Lance", [("pike",0)], itp_type_polearm|itp_merchandise| itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_spear, 196 , weight(5)|difficulty(0)|spd_rtng(72) | weapon_length(170)|swing_damage(0 , cut) | thrust_damage(20 ,  pierce),imodbits_polearm ],
["double_sided_lance", "Double Sided Lance", [("lance_dblhead",0)], itp_type_polearm|itp_merchandise|itp_wooden_parry|itp_primary|itp_offset_lance|itp_couchable, itc_staff, 160, weight(4)|difficulty(10)|spd_rtng(80)|weapon_length(130)|swing_damage(22,cut)|thrust_damage(24,pierce), imodbits_polearm ],
#["pike",         "Pike", [("pike",0)], itp_type_polearm|itp_merchandise| itp_primary|itp_two_handed|itp_wooden_parry, itc_spear,
# 212 , weight(6)|difficulty(0)|spd_rtng(77) | weapon_length(167)|swing_damage(0 , blunt) | thrust_damage(23 ,  pierce),imodbits_polearm ],
["glaive", "Glaive", [("glaive_b",0)], itp_type_polearm|itp_merchandise|itp_wooden_parry|itp_two_handed|itp_primary|itp_offset_lance, itc_staff|itcf_carry_spear, 240, weight(6)|difficulty(15)|spd_rtng(90)|weapon_length(160)|swing_damage(46,cut)|thrust_damage(24,pierce), imodbits_polearm ],
["poleaxe", "Poleaxe", [("pole_ax",0)], itp_type_polearm|itp_merchandise|itp_wooden_parry|itp_two_handed|itp_primary|itp_offset_lance, itc_staff, 300, weight(8)|difficulty(15)|spd_rtng(80)|weapon_length(150)|swing_damage(48,cut)|thrust_damage(12,blunt), imodbits_polearm ],
["polehammer", "Polehammer", [("pole_hammer",0)], itp_type_polearm|itp_wooden_parry|itp_two_handed|itp_primary|itp_offset_lance, itc_staff, 90, weight(6)|difficulty(12)|spd_rtng(60)|weapon_length(120)|swing_damage(46,blunt)|thrust_damage(26,blunt), imodbits_polearm ],
["staff", "Staff", [("wooden_staff",0)], itp_type_polearm|itp_merchandise|itp_wooden_attack|itp_wooden_parry|itp_primary|itp_penalty_with_shield|itp_offset_lance, itc_staff|itcf_carry_sword_back, 10, weight(2)|difficulty(0)|spd_rtng(100)|weapon_length(129)|swing_damage(16,blunt)|thrust_damage(16,blunt), imodbits_polearm ],
["quarter_staff", "Quarter Staff", [("quarter_staff",0)], itp_type_polearm|itp_merchandise|itp_wooden_attack|itp_wooden_parry|itp_primary|itp_offset_lance, itc_staff|itcf_carry_sword_back, 20, weight(2.5)|difficulty(6)|spd_rtng(90)|weapon_length(120)|swing_damage(18,blunt)|thrust_damage(18,blunt), imodbits_polearm ],
["iron_staff", "Iron Staff", [("iron_staff",0)], itp_type_polearm|itp_merchandise|itp_primary|itp_offset_lance, itc_staff|itcf_carry_sword_back, 80, weight(4)|difficulty(8)|spd_rtng(80)|weapon_length(120)|swing_damage(24,blunt)|thrust_damage(24,blunt), imodbits_polearm ],

#["glaive_b",         "Glaive_b", [("glaive_b",0)], itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary|itp_two_handed|itp_wooden_parry, itc_staff|itcf_carry_spear,
# 352 , weight(4.5)|difficulty(0)|spd_rtng(83) | weapon_length(157)|swing_damage(38 , cut) | thrust_damage(21 ,  pierce),imodbits_polearm ],


["shortened_spear", "Shortened Spear", [("spear_g_1-9m",0)], itp_type_polearm|itp_merchandise|itp_wooden_parry|itp_primary|itp_offset_lance|itp_couchable, itc_staff|itcf_carry_spear, 20, weight(1.5)|difficulty(0)|spd_rtng(120)|weapon_length(110)|swing_damage(12,blunt)|thrust_damage(16,pierce), imodbits_polearm ],
["spear", "Spear", [("spear_h_2-15m",0)], itp_type_polearm|itp_merchandise|itp_wooden_parry|itp_primary|itp_offset_lance|itp_couchable, itc_staff|itcf_carry_spear, 90, weight(2.5)|difficulty(0)|spd_rtng(100)|weapon_length(140)|swing_damage(16,blunt)|thrust_damage(24,pierce), imodbits_polearm ],

["bamboo_spear", "Bamboo Spear", [("arabian_spear_a_3m",0)], itp_type_polearm|itp_merchandise|itp_wooden_parry|itp_primary|itp_penalty_with_shield|itp_offset_lance|itp_couchable, itc_staff|itcf_carry_spear, 40, weight(2)|difficulty(0)|spd_rtng(80)|weapon_length(220)|swing_damage(14,blunt)|thrust_damage(18,pierce), imodbits_polearm ],




["war_spear", "War Spear", [("spear_i_2-3m",0)], itp_type_polearm|itp_merchandise|itp_wooden_parry|itp_primary|itp_offset_lance, itc_staff|itcf_carry_spear, 110, weight(3)|difficulty(6)|spd_rtng(90)|weapon_length(150)|swing_damage(20,blunt)|thrust_damage(28,pierce), imodbits_polearm ],
#TODO:["shortened_spear",         "shortened_spear", [("spear_e_2-1m",0)], itp_type_polearm|itp_merchandise| itp_primary|itp_wooden_parry, itc_staff|itcf_carry_spear,
# 65 , weight(2.0)|difficulty(0)|spd_rtng(98) | weapon_length(110)|swing_damage(17 , blunt) | thrust_damage(23 ,  pierce),imodbits_polearm ],
#TODO:["spear_2-4m",         "spear", [("spear_e_2-25m",0)], itp_type_polearm|itp_merchandise| itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_staff|itcf_carry_spear,
# 67 , weight(2.0)|difficulty(0)|spd_rtng(95) | weapon_length(125)|swing_damage(17 , blunt) | thrust_damage(23 ,  pierce),imodbits_polearm ],
["military_scythe", "Military Scythe", [("spear_e_2-5m",0),("spear_c_2-5m",imodbits_bad)], itp_type_polearm|itp_merchandise|itp_wooden_parry|itp_primary|itp_penalty_with_shield|itp_offset_lance, itc_staff|itcf_carry_spear, 80, weight(5)|difficulty(15)|spd_rtng(80)|weapon_length(160)|swing_damage(32,cut)|thrust_damage(14,pierce), imodbits_polearm ],
["light_lance", "Light Lance", [("spear_b_2-75m",0)], itp_type_polearm|itp_merchandise|itp_wooden_parry|itp_primary|itp_penalty_with_shield|itp_offset_lance|itp_couchable, itc_cutting_spear, 100, weight(2)|difficulty(0)|spd_rtng(110)|weapon_length(160)|swing_damage(12,blunt)|thrust_damage(22,pierce), imodbits_polearm ],
["lance", "Lance", [("spear_d_2-8m",0)], itp_type_polearm|itp_merchandise|itp_wooden_parry|itp_primary|itp_penalty_with_shield|itp_offset_lance|itp_couchable, itc_cutting_spear, 140, weight(2.5)|difficulty(6)|spd_rtng(90)|weapon_length(190)|swing_damage(14,blunt)|thrust_damage(24,pierce), imodbits_polearm ],
["heavy_lance", "Heavy Lance", [("spear_f_2-9m",0)], itp_type_polearm|itp_merchandise|itp_wooden_parry|itp_primary|itp_penalty_with_shield|itp_offset_lance|itp_couchable|itp_can_knock_down, itc_greatlance, 200, weight(3)|difficulty(8)|spd_rtng(70)|weapon_length(190)|swing_damage(16,blunt)|thrust_damage(28,pierce), imodbits_polearm ],
["great_lance", "Great Lance", [("heavy_lance",0)], itp_type_polearm|itp_merchandise|itp_wooden_parry|itp_primary|itp_penalty_with_shield|itp_couchable, itc_greatlance, 320, weight(8)|difficulty(10)|spd_rtng(60)|weapon_length(260)|swing_damage(20,blunt)|thrust_damage(22,pierce), imodbits_polearm ],
["pike", "Pike", [("spear_a_3m",0)], itp_type_polearm|itp_merchandise|itp_wooden_parry|itp_two_handed|itp_primary|itp_penalty_with_shield|itp_cant_use_on_horseback, itc_cutting_spear, 135, weight(4)|difficulty(6)|spd_rtng(70)|weapon_length(200)|swing_damage(14,blunt)|thrust_damage(28,pierce), imodbits_polearm ],
##["spear_e_3-25m",         "Spear_3-25m", [("spear_e_3-25m",0)], itp_type_polearm|itp_merchandise| itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_cutting_spear|itcf_carry_spear,
## 150 , weight(4.5)|difficulty(0)|spd_rtng(81) | weapon_length(225)|swing_damage(19 , blunt) | thrust_damage(23 ,  pierce),imodbits_polearm ],
["ashwood_pike", "Ashwood Pike", [("pike",0)], itp_type_polearm|itp_merchandise|itp_wooden_parry|itp_two_handed|itp_primary|itp_offset_lance, itc_cutting_spear, 240, weight(5)|difficulty(8)|spd_rtng(75)|weapon_length(180)|swing_damage(16,blunt)|thrust_damage(32,pierce), imodbits_polearm ],
["awlpike", "Awlpike", [("awl_pike_b",0)], itp_type_polearm|itp_merchandise|itp_wooden_parry|itp_primary|itp_penalty_with_shield|itp_offset_lance, itc_cutting_spear|itcf_carry_spear, 280, weight(5.5)|difficulty(10)|spd_rtng(80)|weapon_length(180)|swing_damage(18,blunt)|thrust_damage(34,pierce), imodbits_polearm ],
["awlpike_long", "Long Awlpike", [("awl_pike_a",0)], itp_type_polearm|itp_merchandise|itp_wooden_parry|itp_primary|itp_penalty_with_shield|itp_offset_lance, itc_cutting_spear|itcf_carry_spear, 320, weight(6)|difficulty(12)|spd_rtng(80)|weapon_length(240)|swing_damage(20,blunt)|thrust_damage(30,pierce), imodbits_polearm ],
#["awlpike",         "Awlpike", [("pike",0)], itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary|itp_two_handed|itp_wooden_parry, itc_cutting_spear|itcf_carry_spear,
# 378 , weight(3.5)|difficulty(10)|spd_rtng(92) | weapon_length(160)|swing_damage(20 ,blunt) | thrust_damage(31 ,  pierce),imodbits_polearm ],

["bec_de_corbin_a", "War Hammer", [("bec_de_corbin_a",0)], itp_type_polearm|itp_merchandise|itp_wooden_parry|itp_two_handed|itp_primary|itp_penalty_with_shield|itp_cant_use_on_horseback, itc_cutting_spear|itcf_carry_spear, 60, weight(4)|difficulty(6)|spd_rtng(80)|weapon_length(100)|swing_damage(38,blunt)|thrust_damage(20,blunt), imodbits_polearm ],



# SHIELDS

["wooden_shield", "Wooden Shield", [("shield_round_a",0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_round_shield, 25, weight(1.5)|difficulty(0)|hit_points(240)|body_armor(1)|spd_rtng(120)|shield_width(40), imodbits_shield ],
##["wooden_shield", "Wooden Shield", [("shield_round_a",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_round_shield,  42 , weight(2)|hit_points(360)|body_armor(1)|spd_rtng(100)|shield_width(50),imodbits_shield,




#["round_shield", "Round Shield", [("shield_round_c",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_round_shield,  64 , weight(2)|hit_points(400)|body_armor(1)|spd_rtng(100)|shield_width(50),imodbits_shield ],
#["nordic_shield", "Nordic Shield", [("shield_round_b",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_round_shield,  95 , weight(2)|hit_points(440)|body_armor(1)|spd_rtng(100)|shield_width(50),imodbits_shield ],
#["kite_shield",         "Kite Shield", [("shield_kite_a",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  118 , weight(2.5)|hit_points(480)|body_armor(1)|spd_rtng(82)|shield_width(90),imodbits_shield ],
#["kite_shield_", "Kite Shield", [("shield_kite_b",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  118 , weight(2.5)|hit_points(480)|body_armor(1)|spd_rtng(82)|shield_width(90),imodbits_shield ],
#["large_shield", "Large Shield", [("shield_kite_c",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  165 , weight(2.5)|hit_points(520)|body_armor(1)|spd_rtng(80)|shield_width(92),imodbits_shield ],
#["battle_shield", "Battle Shield", [("shield_kite_d",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  196 , weight(3)|hit_points(560)|body_armor(1)|spd_rtng(78)|shield_width(94),imodbits_shield ],
["fur_covered_shield", "Fur Covered Shield", [("shield_kite_m",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield, 15, weight(1)|difficulty(0)|hit_points(160)|body_armor(1)|spd_rtng(130)|shield_width(40), imodbits_shield ],
#["heraldric_shield", "Heraldric Shield", [("shield_heraldic",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  301 , weight(3.5)|hit_points(640)|body_armor(1)|spd_rtng(83)|shield_width(65),imodbits_shield ],
#["heater_shield", "Heater Shield", [("shield_heater_a",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  477 , weight(3.5)|hit_points(710)|body_armor(4)|spd_rtng(80)|shield_width(60),imodbits_shield ],
["steel_shield", "Steel Shield", [("shield_dragon",0)], itp_type_shield|itp_merchandise, itcf_carry_round_shield, 850, weight(10)|difficulty(2)|hit_points(1200)|body_armor(20)|spd_rtng(70)|shield_width(30)|shield_height(75), imodbits_shield ],
#["nomad_shield", "Nomad Shield", [("shield_wood_b",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_round_shield,  12 , weight(2)|hit_points(260)|body_armor(6)|spd_rtng(110)|shield_width(30),imodbits_shield ],

["plate_covered_round_shield", "Plate Covered Round Shield", [("shield_round_e",0)], itp_type_shield, itcf_carry_round_shield, 300, weight(5)|difficulty(3)|hit_points(580)|body_armor(16)|spd_rtng(80)|shield_width(40), imodbits_shield ],
["leather_covered_round_shield", "Leather Covered Round Shield", [("shield_round_d",0)], itp_type_shield|itp_wooden_parry, itcf_carry_round_shield, 110, weight(2.5)|difficulty(1)|hit_points(300)|body_armor(6)|spd_rtng(105)|shield_width(40), imodbits_shield ],
["hide_covered_round_shield", "Hide Covered Round Shield", [("shield_round_f",0)], itp_type_shield|itp_wooden_parry, itcf_carry_round_shield, 90, weight(2.5)|difficulty(0)|hit_points(240)|body_armor(6)|spd_rtng(110)|shield_width(40), imodbits_shield ],

["shield_heater_c", "Heater Shield", [("shield_heater_c",0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_kite_shield, 240, weight(6)|difficulty(2)|hit_points(360)|body_armor(14)|spd_rtng(80)|shield_width(35), imodbits_shield ],
#["shield_heater_d", "Heater Shield", [("shield_heater_d",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  477 , weight(3.5)|hit_points(710)|body_armor(4)|spd_rtng(80)|shield_width(60),imodbits_shield ],

#["shield_kite_g",         "Kite Shield g", [("shield_kite_g",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  118 , weight(2.5)|hit_points(480)|body_armor(1)|spd_rtng(82)|shield_width(90),imodbits_shield ],
#["shield_kite_h",         "Kite Shield h", [("shield_kite_h",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  118 , weight(2.5)|hit_points(480)|body_armor(1)|spd_rtng(82)|shield_width(90),imodbits_shield ],
#["shield_kite_i",         "Kite Shield i ", [("shield_kite_i",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  118 , weight(2.5)|hit_points(480)|body_armor(1)|spd_rtng(82)|shield_width(90),imodbits_shield ],
#["shield_kite_k",         "Kite Shield k", [("shield_kite_k",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  118 , weight(2.5)|hit_points(480)|body_armor(1)|spd_rtng(82)|shield_width(90),imodbits_shield ],

["norman_shield_1", "Kite Shield", [("norman_shield_1",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield, 140, weight(5)|difficulty(1)|hit_points(540)|body_armor(6)|spd_rtng(90)|shield_width(35)|shield_height(60), imodbits_shield ],
["norman_shield_2", "Kite Shield", [("norman_shield_2",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield, 140, weight(5)|difficulty(1)|hit_points(540)|body_armor(6)|spd_rtng(90)|shield_width(35)|shield_height(60), imodbits_shield ],
["norman_shield_3", "Kite Shield", [("norman_shield_3",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield, 140, weight(5)|difficulty(1)|hit_points(540)|body_armor(6)|spd_rtng(90)|shield_width(35)|shield_height(60), imodbits_shield ],
["norman_shield_4", "Kite Shield", [("norman_shield_4",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield, 140, weight(5)|difficulty(1)|hit_points(540)|body_armor(6)|spd_rtng(90)|shield_width(35)|shield_height(60), imodbits_shield ],
["norman_shield_5", "Kite Shield", [("norman_shield_5",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield, 140, weight(5)|difficulty(1)|hit_points(540)|body_armor(6)|spd_rtng(90)|shield_width(35)|shield_height(60), imodbits_shield ],
["norman_shield_6", "Kite Shield", [("norman_shield_6",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield, 140, weight(5)|difficulty(1)|hit_points(540)|body_armor(6)|spd_rtng(90)|shield_width(35)|shield_height(60), imodbits_shield ],
["norman_shield_7", "Kite Shield", [("norman_shield_7",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield, 140, weight(5)|difficulty(1)|hit_points(540)|body_armor(6)|spd_rtng(90)|shield_width(35)|shield_height(60), imodbits_shield ],
["norman_shield_8", "Kite Shield", [("norman_shield_8",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield, 140, weight(5)|difficulty(1)|hit_points(540)|body_armor(6)|spd_rtng(90)|shield_width(35)|shield_height(60), imodbits_shield ],

["tab_shield_round_a", "Old Round Shield", [("tableau_shield_round_5",0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_round_shield, 40, weight(2)|difficulty(0)|hit_points(140)|body_armor(2)|spd_rtng(110)|shield_width(40), imodbits_shield, [(ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_shield_item_set_banner","tableau_round_shield_5",":agent_no",":troop_no")])] ],
["tab_shield_round_b", "Plain Round Shield", [("tableau_shield_round_3",0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_round_shield, 75, weight(2)|difficulty(0)|hit_points(160)|body_armor(6)|spd_rtng(110)|shield_width(40), imodbits_shield, [(ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_shield_item_set_banner","tableau_round_shield_3",":agent_no",":troop_no")])] ],
["tab_shield_round_c", "Round Shield", [("tableau_shield_round_2",0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_round_shield, 140, weight(3)|difficulty(1)|hit_points(300)|body_armor(8)|spd_rtng(105)|shield_width(40), imodbits_shield, [(ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_shield_item_set_banner","tableau_round_shield_2",":agent_no",":troop_no")])] ],
["tab_shield_round_d", "Heavy Round Shield", [("tableau_shield_round_1",0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_round_shield, 190, weight(3.5)|difficulty(2)|hit_points(420)|body_armor(12)|spd_rtng(95)|shield_width(40), imodbits_shield, [(ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_shield_item_set_banner","tableau_round_shield_1",":agent_no",":troop_no")])] ],
["tab_shield_round_e", "Huscarl's Round Shield", [("tableau_shield_round_4",0)], itp_type_shield|itp_merchandise, itcf_carry_round_shield, 460, weight(3)|difficulty(2)|hit_points(360)|body_armor(18)|spd_rtng(80)|shield_width(50), imodbits_shield, [(ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_shield_item_set_banner","tableau_round_shield_4",":agent_no",":troop_no")])] ],

["tab_shield_kite_a", "Old Kite Shield", [("tableau_shield_kite_1",0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_kite_shield, 20, weight(5)|difficulty(0)|hit_points(180)|body_armor(4)|spd_rtng(80)|shield_width(35)|shield_height(60), imodbits_shield, [(ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_shield_item_set_banner","tableau_kite_shield_1",":agent_no",":troop_no")])] ],
["tab_shield_kite_b", "Plain Kite Shield", [("tableau_shield_kite_3",0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_kite_shield, 80, weight(5)|difficulty(0)|hit_points(240)|body_armor(6)|spd_rtng(90)|shield_width(35)|shield_height(60), imodbits_shield, [(ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_shield_item_set_banner","tableau_kite_shield_3",":agent_no",":troop_no")])] ],
["tab_shield_kite_c", "Kite Shield", [("tableau_shield_kite_2",0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_kite_shield, 140, weight(5)|difficulty(1)|hit_points(540)|body_armor(6)|spd_rtng(90)|shield_width(35)|shield_height(60), imodbits_shield, [(ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_shield_item_set_banner","tableau_kite_shield_2",":agent_no",":troop_no")])] ],
["tab_shield_kite_d", "Heavy Kite Shield", [("tableau_shield_kite_2",0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_kite_shield, 400, weight(6)|difficulty(2)|hit_points(700)|body_armor(10)|spd_rtng(80)|shield_width(35)|shield_height(60), imodbits_shield, [(ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_shield_item_set_banner","tableau_kite_shield_2",":agent_no",":troop_no")])] ],
["tab_shield_kite_cav_a", "Horseman's Kite Shield", [("tableau_shield_kite_4",0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_kite_shield, 220, weight(5)|difficulty(1)|hit_points(180)|body_armor(8)|spd_rtng(110)|shield_width(35)|shield_height(50), imodbits_shield, [(ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_shield_item_set_banner","tableau_kite_shield_4",":agent_no",":troop_no")])] ],
["tab_shield_kite_cav_b", "Knightly Kite Shield", [("tableau_shield_kite_4",0)], itp_type_shield|itp_merchandise, itcf_carry_kite_shield, 480, weight(8)|difficulty(3)|hit_points(350)|body_armor(16)|spd_rtng(80)|shield_width(35)|shield_height(60), imodbits_shield, [(ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_shield_item_set_banner","tableau_kite_shield_4",":agent_no",":troop_no")])] ],

["tab_shield_heater_a", "Old Heater Shield", [("tableau_shield_heater_1",0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_kite_shield, 20, weight(6)|difficulty(1)|hit_points(140)|body_armor(8)|spd_rtng(80)|shield_width(35)|shield_height(75), imodbits_shield, [(ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_shield_item_set_banner","tableau_heater_shield_1",":agent_no",":troop_no")])] ],
["tab_shield_heater_b", "Plain Heater Shield", [("tableau_shield_heater_1",0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_kite_shield, 60, weight(6)|difficulty(1)|hit_points(240)|body_armor(12)|spd_rtng(80)|shield_width(35)|shield_height(75), imodbits_shield, [(ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_shield_item_set_banner","tableau_heater_shield_1",":agent_no",":troop_no")])] ],
["tab_shield_heater_c", "Heater Shield", [("tableau_shield_heater_1",0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_kite_shield, 240, weight(6)|difficulty(2)|hit_points(360)|body_armor(14)|spd_rtng(80)|shield_width(35)|shield_height(75), imodbits_shield, [(ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_shield_item_set_banner","tableau_heater_shield_1",":agent_no",":troop_no")])] ],
["tab_shield_heater_d", "Heavy Heater Shield", [("tableau_shield_heater_1",0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_kite_shield, 340, weight(8)|difficulty(3)|hit_points(640)|body_armor(18)|spd_rtng(65)|shield_width(35)|shield_height(75), imodbits_shield, [(ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_shield_item_set_banner","tableau_heater_shield_1",":agent_no",":troop_no")])] ],
["tab_shield_heater_cav_a", "Horseman's Heater Shield", [("tableau_shield_heater_2",0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_kite_shield, 140, weight(6)|difficulty(2)|hit_points(180)|body_armor(14)|spd_rtng(90)|shield_width(35)|shield_height(75), imodbits_shield, [(ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_shield_item_set_banner","tableau_heater_shield_2",":agent_no",":troop_no")])] ],
["tab_shield_heater_cav_b", "Knight Heater Shield", [("tableau_shield_heater_2",0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_kite_shield, 720, weight(10)|difficulty(4)|hit_points(580)|body_armor(20)|spd_rtng(70)|shield_width(35)|shield_height(75), imodbits_shield, [(ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_shield_item_set_banner","tableau_heater_shield_2",":agent_no",":troop_no")])] ],

["tab_shield_pavise_a", "Old Board Shield", [("tableau_shield_pavise_2",0)], itp_type_shield|itp_merchandise|itp_wooden_parry|itp_cant_use_on_horseback, itcf_carry_board_shield, 40, weight(8)|difficulty(0)|hit_points(320)|body_armor(8)|spd_rtng(70)|shield_width(50)|shield_height(100), imodbits_shield, [(ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_shield_item_set_banner","tableau_pavise_shield_2",":agent_no",":troop_no")])] ],
["tab_shield_pavise_b", "Plain Board Shield", [("tableau_shield_pavise_2",0)], itp_type_shield|itp_merchandise|itp_wooden_parry|itp_cant_use_on_horseback, itcf_carry_board_shield, 120, weight(10)|difficulty(1)|hit_points(510)|body_armor(10)|spd_rtng(65)|shield_width(50)|shield_height(100), imodbits_shield, [(ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_shield_item_set_banner","tableau_pavise_shield_2",":agent_no",":troop_no")])] ],
["tab_shield_pavise_c", "Board Shield", [("tableau_shield_pavise_1",0)], itp_type_shield|itp_merchandise|itp_wooden_parry|itp_cant_use_on_horseback, itcf_carry_board_shield, 180, weight(12)|difficulty(2)|hit_points(740)|body_armor(14)|spd_rtng(60)|shield_width(50)|shield_height(100), imodbits_shield, [(ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_shield_item_set_banner","tableau_pavise_shield_1",":agent_no",":troop_no")])] ],
["tab_shield_pavise_d", "Heavy Board Shield", [("tableau_shield_pavise_1",0)], itp_type_shield|itp_merchandise|itp_wooden_parry|itp_cant_use_on_horseback, itcf_carry_board_shield, 480, weight(16)|difficulty(3)|hit_points(1100)|body_armor(18)|spd_rtng(55)|shield_width(50)|shield_height(100), imodbits_shield, [(ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_shield_item_set_banner","tableau_pavise_shield_1",":agent_no",":troop_no")])] ],

["tab_shield_small_round_a", "Plain Cavalry Shield", [("tableau_shield_small_round_3",0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_round_shield, 60, weight(2)|difficulty(0)|hit_points(180)|body_armor(6)|spd_rtng(110)|shield_width(30), imodbits_shield, [(ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_shield_item_set_banner","tableau_small_round_shield_3",":agent_no",":troop_no")])] ],
["tab_shield_small_round_b", "Round Cavalry Shield", [("tableau_shield_small_round_1",0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_round_shield, 140, weight(2.5)|difficulty(1)|hit_points(200)|body_armor(10)|spd_rtng(110)|shield_width(30), imodbits_shield, [(ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_shield_item_set_banner","tableau_small_round_shield_1",":agent_no",":troop_no")])] ],
["tab_shield_small_round_c", "Elite Cavalry Shield", [("tableau_shield_small_round_2",0)], itp_type_shield|itp_merchandise, itcf_carry_round_shield, 280, weight(2.5)|difficulty(2)|hit_points(240)|body_armor(14)|spd_rtng(100)|shield_width(30), imodbits_shield, [(ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_shield_item_set_banner","tableau_small_round_shield_2",":agent_no",":troop_no")])] ],


#RANGED


#TODO:
["darts", "Darts", [("dart_b",0),("dart_b_bag",ixmesh_carry)], itp_type_thrown|itp_merchandise|itp_primary, itcf_throw_javelin|itcf_carry_quiver_right_vertical|itcf_show_holster_when_drawn, 180, weight(6)|difficulty(1)|spd_rtng(90)|shoot_speed(24)|thrust_damage(20,pierce)|max_ammo(14)|weapon_length(32), imodbits_bolts],
["war_darts", "War Darts", [("dart_a",0),("dart_a_bag",ixmesh_carry)], itp_type_thrown|itp_merchandise|itp_primary, itcf_throw_javelin|itcf_carry_quiver_back|itcf_show_holster_when_drawn, 400, weight(12)|difficulty(3)|spd_rtng(95)|shoot_speed(27)|thrust_damage(26,pierce)|max_ammo(20)|weapon_length(45), imodbits_iron_bolts],

["javelin", "Javelins", [("javelin",0),("javelins_quiver_new",ixmesh_carry)], itp_type_thrown|itp_merchandise|itp_primary|itp_next_item_as_melee, itcf_throw_javelin|itcf_carry_quiver_back|itcf_show_holster_when_drawn, 240, weight(6)|difficulty(1)|spd_rtng(95)|shoot_speed(27)|thrust_damage(32,pierce)|max_ammo(16)|weapon_length(75), imodbits_iron_bolts],
["javelin_melee", "Javelin", [("javelin",0)], itp_type_polearm|itp_wooden_parry|itp_primary, itc_staff, 400, weight(4)|difficulty(6)|spd_rtng(100)|swing_damage(10,cut)|thrust_damage(16,pierce)|weapon_length(75), imodbits_polearm ],

["throwing_spears", "Throwing Spears", [("jarid_new_b",0),("jarid_new_b_bag",ixmesh_carry)], itp_type_thrown|itp_merchandise|itp_primary|itp_next_item_as_melee, itcf_throw_javelin|itcf_carry_quiver_back|itcf_show_holster_when_drawn, 700, weight(10)|difficulty(3)|spd_rtng(85)|shoot_speed(23)|thrust_damage(56,pierce)|max_ammo(12)|weapon_length(65), imodbits_iron_bolts],
["throwing_spear_melee", "Throwing Spears", [("jarid_new_b",0),("javelins_quiver",ixmesh_carry)], itp_type_polearm|itp_wooden_parry|itp_primary, itc_staff, 580, weight(6)|difficulty(10)|spd_rtng(80)|swing_damage(18,cut)|thrust_damage(28,pierce)|weapon_length(75), imodbits_thrown ],

["jarid", "Jarids", [("jarid_new",0),("jarid_quiver",ixmesh_carry)], itp_type_thrown|itp_merchandise|itp_primary|itp_next_item_as_melee, itcf_throw_javelin|itcf_carry_quiver_back|itcf_show_holster_when_drawn, 460, weight(8)|difficulty(2)|spd_rtng(90)|shoot_speed(25)|thrust_damage(38,pierce)|max_ammo(16)|weapon_length(65), imodbits_iron_bolts],
["jarid_melee", "Jarids", [("jarid_new",0),("jarid_quiver",ixmesh_carry)], itp_type_polearm|itp_wooden_parry|itp_primary, itc_staff, 460, weight(5)|difficulty(8)|spd_rtng(90)|swing_damage(14,cut)|thrust_damage(20,pierce)|weapon_length(75), imodbits_thrown ],


#TODO:
#TODO: Heavy throwing Spear
["stones",         "Stones", [("throwing_stone",0)], itp_type_thrown |itp_merchandise|itp_primary ,itcf_throw_stone, 1 , weight(4)|difficulty(0)|spd_rtng(97) | shoot_speed(30) | thrust_damage(11 ,  blunt)|max_ammo(18)|weapon_length(8),imodbits_bolts],

#["throwing_knives", "Throwing Knives", [("throwing_knife",0)], itp_type_thrown |itp_merchandise|itp_primary ,itcf_throw_knife, 76 , weight(3.5)|difficulty(0)|spd_rtng(121) | shoot_speed(25) | thrust_damage(19 ,  cut)|max_ammo(14)|weapon_length(0),imodbits_thrown ],
#["throwing_daggers", "Throwing Daggers", [("throwing_dagger",0)], itp_type_thrown |itp_merchandise|itp_primary ,itcf_throw_knife, 193 , weight(3.5)|difficulty(0)|spd_rtng(110) | shoot_speed(24) | thrust_damage(25 ,  cut)|max_ammo(13)|weapon_length(0),imodbits_thrown ],
#TODO: Light Trowing axe, Heavy Throwing Axe
["light_throwing_axes", "Light Throwing Axes", [("francisca",0)], itp_type_thrown|itp_merchandise|itp_primary|itp_next_item_as_melee, itcf_throw_axe, 360, weight(6)|difficulty(2)|spd_rtng(100)|shoot_speed(18)|thrust_damage(32,cut)|max_ammo(16)|weapon_length(53), imodbits_iron_bolts - imodbit_heavy],
["light_throwing_axes_melee", "Light Throwing Axe", [("francisca",0)], itp_type_one_handed_wpn |itp_primary|itp_bonus_against_shield,itc_scimitar,360, weight(1)|difficulty(2)|spd_rtng(99)|weapon_length(53)| swing_damage(26,cut),imodbits_thrown_minus_heavy ],
["throwing_axes", "Throwing Axes", [("throwing_axe_a",0)], itp_type_thrown|itp_merchandise|itp_primary|itp_next_item_as_melee, itcf_throw_axe, 490, weight(8)|difficulty(3)|spd_rtng(90)|shoot_speed(18)|thrust_damage(38,cut)|max_ammo(14)|weapon_length(53), imodbits_iron_bolts - imodbit_heavy],
["throwing_axes_melee", "Throwing Axe", [("throwing_axe_a",0)], itp_type_one_handed_wpn |itp_primary|itp_bonus_against_shield,itc_scimitar,490, weight(1)|difficulty(3)|spd_rtng(98) | swing_damage(29,cut)|weapon_length(53),imodbits_thrown_minus_heavy ],
["heavy_throwing_axes", "Heavy Throwing Axes", [("throwing_axe_b",0)], itp_type_thrown|itp_merchandise|itp_primary|itp_next_item_as_melee, itcf_throw_axe, 620, weight(10)|difficulty(4)|spd_rtng(80)|shoot_speed(18)|thrust_damage(46,cut)|max_ammo(12)|weapon_length(53), imodbits_iron_bolts - imodbit_heavy],
["heavy_throwing_axes_melee", "Heavy Throwing Axe", [("throwing_axe_b",0)], itp_type_one_handed_wpn |itp_primary|itp_bonus_against_shield,itc_scimitar,620, weight(1)|difficulty(4)|spd_rtng(97) | swing_damage(32,cut)|weapon_length(53),imodbits_thrown_minus_heavy ],



# ["hunting_bow",         "Hunting Bow", [("hunting_bow",0),("hunting_bow_carry",ixmesh_carry)],itp_type_bow |itp_merchandise|itp_primary|itp_two_handed,itcf_shoot_bow|itcf_carry_bow_back,
# 17 , weight(1)|difficulty(0)|spd_rtng(100) | shoot_speed(52) | thrust_damage(15 ,  pierce),imodbits_bow ],
# ["short_bow",         "Short Bow", [("short_bow",0),("short_bow_carry",ixmesh_carry)], itp_type_bow |itp_merchandise|itp_primary|itp_two_handed ,itcf_shoot_bow|itcf_carry_bow_back,
# 58 , weight(1)|difficulty(1)|spd_rtng(97) | shoot_speed(55) | thrust_damage(18 ,  pierce  ),imodbits_bow ],
# ["nomad_bow",         "Nomad Bow", [("nomad_bow",0),("nomad_bow_case", ixmesh_carry)], itp_type_bow |itp_merchandise|itp_primary|itp_two_handed ,itcf_shoot_bow|itcf_carry_bowcase_left|itcf_show_holster_when_drawn,
# 164 , weight(1.25)|difficulty(2)|spd_rtng(94) | shoot_speed(56) | thrust_damage(20 ,  pierce),imodbits_bow ],
# ["long_bow",         "Long Bow", [("long_bow",0),("long_bow_carry",ixmesh_carry)], itp_type_bow |itp_merchandise|itp_primary|itp_two_handed ,itcf_shoot_bow|itcf_carry_bow_back,
# 145 , weight(1.75)|difficulty(3)|spd_rtng(79) | shoot_speed(56) | thrust_damage(22 ,  pierce),imodbits_bow ],
# ["khergit_bow",         "Khergit Bow", [("khergit_bow",0),("khergit_bow_case", ixmesh_carry)], itp_type_bow |itp_merchandise|itp_primary|itp_two_handed,itcf_shoot_bow|itcf_carry_bowcase_left|itcf_show_holster_when_drawn,
# 269 , weight(1.25)|difficulty(3)|spd_rtng(90) | shoot_speed(57) | thrust_damage(21 ,pierce),imodbits_bow ],
# ["strong_bow",         "Strong Bow", [("strong_bow",0),("strong_bow_case", ixmesh_carry)], itp_type_bow |itp_merchandise|itp_primary|itp_two_handed ,itcf_shoot_bow|itcf_carry_bowcase_left|itcf_show_holster_when_drawn,
# 437 , weight(1.25)|difficulty(3)|spd_rtng(88) | shoot_speed(58) | thrust_damage(23 ,pierce),imodbit_cracked | imodbit_bent | imodbit_masterwork ],
# ["war_bow",         "War Bow", [("war_bow",0),("war_bow_carry",ixmesh_carry)],itp_type_bow|itp_merchandise|itp_primary|itp_two_handed ,itcf_shoot_bow|itcf_carry_bow_back,
# 728 , weight(1.5)|difficulty(4)|spd_rtng(84) | shoot_speed(59) | thrust_damage(25 ,pierce),imodbits_bow ],
# ["hunting_crossbow", "Hunting Crossbow", [("crossbow_a",0)], itp_type_crossbow |itp_merchandise|itp_primary|itp_two_handed ,itcf_shoot_crossbow|itcf_carry_crossbow_back,
# 22 , weight(2.25)|difficulty(0)|spd_rtng(47) | shoot_speed(50) | thrust_damage(37 ,  pierce)|max_ammo(1),imodbits_crossbow ],
# ["light_crossbow", "Light Crossbow", [("crossbow_b",0)], itp_type_crossbow |itp_merchandise|itp_primary|itp_two_handed ,itcf_shoot_crossbow|itcf_carry_crossbow_back,
# 67 , weight(2.5)|difficulty(8)|spd_rtng(45) | shoot_speed(59) | thrust_damage(44 ,  pierce)|max_ammo(1),imodbits_crossbow ],
# ["crossbow",         "Crossbow",         [("crossbow_a",0)], itp_type_crossbow |itp_merchandise|itp_primary|itp_two_handed|itp_cant_reload_on_horseback ,itcf_shoot_crossbow|itcf_carry_crossbow_back,
# 182 , weight(3)|difficulty(8)|spd_rtng(43) | shoot_speed(66) | thrust_damage(49,pierce)|max_ammo(1),imodbits_crossbow ],
# ["heavy_crossbow", "Heavy Crossbow", [("crossbow_c",0)], itp_type_crossbow |itp_merchandise|itp_primary|itp_two_handed|itp_cant_reload_on_horseback ,itcf_shoot_crossbow|itcf_carry_crossbow_back,
# 349 , weight(3.5)|difficulty(8)|spd_rtng(41) | shoot_speed(68) | thrust_damage(58 ,pierce)|max_ammo(1),imodbits_crossbow ],
# ["sniper_crossbow", "Siege Crossbow", [("crossbow_c",0)], itp_type_crossbow |itp_merchandise|itp_primary|itp_two_handed|itp_cant_reload_on_horseback ,itcf_shoot_crossbow|itcf_carry_crossbow_back,
# 683 , weight(3.75)|difficulty(10)|spd_rtng(37) | shoot_speed(70) | thrust_damage(63 ,pierce)|max_ammo(1),imodbits_crossbow ],
["flintlock_pistol", "Flintlock Pistol", [("flintlock_pistol",0)], itp_type_pistol |itp_merchandise|itp_primary ,itcf_shoot_pistol|itcf_reload_pistol, 230 , weight(1.5)|difficulty(0)|spd_rtng(38) | shoot_speed(160) | thrust_damage(45 ,pierce)|max_ammo(1)|accuracy(65),imodbits_none, [(ti_on_weapon_attack, [(play_sound,"snd_pistol_shot"),(position_move_x, pos1,27),(position_move_y, pos1,36),(particle_system_burst, "psys_pistol_smoke", pos1, 15)])]],
["torch",         "Torch", [("club",0)], itp_type_one_handed_wpn|itp_primary, itc_scimitar, 11 , weight(2.5)|difficulty(0)|spd_rtng(95) | weapon_length(95)|swing_damage(11 , blunt) | thrust_damage(0 ,  pierce),imodbits_none, [(ti_on_init_item, [(set_position_delta,0,60,0),(particle_system_add_new, "psys_torch_fire"),(particle_system_add_new, "psys_torch_smoke"),(set_current_color,150, 130, 70),(add_point_light, 10, 30),])]],

["lyre",         "Lyre", [("lyre",0)], itp_type_shield|itp_wooden_parry|itp_civilian, itcf_carry_bow_back,  118 , weight(2.5)|hit_points(480)|body_armor(1)|spd_rtng(82)|shield_width(90),0 ],
["lute",         "Lute", [("lute",0)], itp_type_shield|itp_wooden_parry|itp_civilian, itcf_carry_bow_back,  118 , weight(2.5)|hit_points(480)|body_armor(1)|spd_rtng(82)|shield_width(90),0 ],

##["short_sword", "Short Sword",
## [("sword_norman",0),("sword_norman_scabbard", ixmesh_carry),("sword_norman_rusty",imodbit_rusty),("sword_norman_rusty_scabbard", ixmesh_carry|imodbit_rusty)],
## itp_type_one_handed_wpn|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 183 , weight(1.25)|difficulty(0)|spd_rtng(103) | weapon_length(75)|swing_damage(25 , cut) | thrust_damage(19 ,  pierce),imodbits_sword ],

["strange_armor", "Strange Armor", [("samurai_armor",0)], itp_type_body_armor|itp_covers_legs, 0, 1620, weight(18)|abundance(0)|head_armor(0)|body_armor(36)|leg_armor(8)|difficulty(10), imodbits_armor ],
["strange_boots", "Strange Boots", [("samurai_boots",0)], itp_type_foot_armor|itp_attach_armature, 0, 840, weight(7)|abundance(50)|head_armor(0)|body_armor(0)|leg_armor(24)|difficulty(8), imodbits_armor ],
["strange_helmet", "Strange Helmet", [("samurai_helmet",0)], itp_type_head_armor, 0, 455, weight(3.5)|abundance(100)|head_armor(26)|body_armor(0)|leg_armor(0)|difficulty(8), imodbits_armor ],
["strange_sword", "Strange Sword", [("katana",0),("katana_scabbard",ixmesh_carry)], itp_type_two_handed_wpn| itp_primary, itc_bastardsword|itcf_carry_katana|itcf_show_holster_when_drawn, 679 , weight(2.0)|difficulty(8)|spd_rtng(108) | weapon_length(95)|swing_damage(32 , cut) | thrust_damage(18 ,  pierce),imodbits_sword ],
["strange_great_sword",  "Strange Great Sword", [("no_dachi",0),("no_dachi_scabbard",ixmesh_carry)], itp_type_two_handed_wpn|itp_two_handed|itp_primary, itc_nodachi|itcf_carry_sword_back|itcf_show_holster_when_drawn, 920 , weight(3.5)|difficulty(11)|spd_rtng(92) | weapon_length(125)|swing_damage(38 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["strange_short_sword", "Strange Short Sword", [("wakizashi",0),("wakizashi_scabbard",ixmesh_carry)], itp_type_one_handed_wpn|itp_primary, itc_longsword|itcf_carry_wakizashi|itcf_show_holster_when_drawn, 321 , weight(1.25)|difficulty(0)|spd_rtng(108) | weapon_length(65)|swing_damage(25 , cut) | thrust_damage(19 ,  pierce),imodbits_sword ],
["court_dress", "Court Dress", [("court_dress",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 240, weight(6)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["rich_outfit", "Rich Outfit", [("merchant_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 240, weight(6)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["khergit_guard_armor", "Khergit Guard Armor", [("lamellar_armor_a",0)], itp_type_body_armor|itp_covers_legs, 0, 1620, weight(18)|abundance(100)|head_armor(0)|body_armor(36)|leg_armor(8)|difficulty(10), imodbits_armor ],
#["leather_steppe_cap_c", "Leather Steppe Cap", [("leather_steppe_cap_c",0)], itp_type_head_armor   ,0, 51 , weight(2)|abundance(100)|head_armor(18)|body_armor(0)|leg_armor(0) ,imodbits_cloth ],
["felt_steppe_cap", "Felt Steppe Cap", [("felt_steppe_cap",0)], itp_type_head_armor, 0, 100, weight(2)|abundance(100)|head_armor(10)|body_armor(0)|leg_armor(0), imodbits_cloth ],
["khergit_war_helmet", "Khergit War Helmet", [("tattered_steppe_cap_a_new",0)], itp_type_head_armor|itp_merchandise, 0, 270, weight(2.5)|abundance(120)|head_armor(18)|body_armor(0)|leg_armor(0)|difficulty(6), imodbits_armor ],
["khergit_helmet", "Khergit Helmet", [("khergit_guard_helmet",0)], itp_type_head_armor, 0, 240, weight(2.5)|abundance(140)|head_armor(16)|body_armor(0)|leg_armor(0)|difficulty(6), imodbits_cloth ],
#["khergit_sword", "Khergit Sword", [("khergit_sword",0),("khergit_sword_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 183 , weight(1.25)|difficulty(0)|spd_rtng(100) | weapon_length(97)|swing_damage(23 , cut) | thrust_damage(14 ,  pierce),imodbits_sword ],
["khergit_guard_boots", "Khergit Guard Boots", [("lamellar_boots_a",0)], itp_type_foot_armor|itp_attach_armature, 0, 910, weight(7)|abundance(50)|head_armor(0)|body_armor(0)|leg_armor(26)|difficulty(8), imodbits_armor ],
["khergit_guard_helmet", "Khergit Guard Helmet", [("lamellar_helmet_a",0)], itp_type_head_armor|itp_merchandise, 0, 490, weight(3.5)|abundance(100)|head_armor(28)|body_armor(0)|leg_armor(0)|difficulty(8), imodbits_armor ],
["khergit_cavalry_helmet", "Khergit Cavalry Helmet", [("lamellar_helmet_b",0)], itp_type_head_armor|itp_merchandise, 0, 490, weight(3.5)|abundance(100)|head_armor(28)|body_armor(0)|leg_armor(0)|difficulty(8), imodbits_armor ],

["black_hood", "Black Hood", [("hood_black",0)], itp_type_head_armor|itp_merchandise, 0, 20, weight(1)|abundance(100)|head_armor(4)|body_armor(0)|leg_armor(0), imodbits_cloth ],
["light_leather", "Light Leather", [("light_leather",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 400, weight(9)|abundance(100)|head_armor(0)|body_armor(18)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["light_leather_boots", "Light Leather Boots", [("light_leather_boots",0)], itp_type_foot_armor|itp_attach_armature|itp_merchandise, 0, 240, weight(4)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(12)|difficulty(0), imodbits_cloth ],
["light_leather_a", "Light Leather", [("light_leather",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 400, weight(9)|abundance(100)|head_armor(0)|body_armor(18)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["light_leather_boots_a", "Light Leather Boots", [("light_leather_boots",0)], itp_type_foot_armor|itp_attach_armature|itp_merchandise, 0, 240, weight(4)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(12)|difficulty(0), imodbits_cloth ],
#["mail_and_plate", "Mail and Plate", [("mail_and_plate",0)], itp_type_body_armor|itp_covers_legs   ,0, 593 , weight(16)|abundance(100)|head_armor(0)|body_armor(34)|leg_armor(12)|difficulty(0) ,imodbits_armor ],
#["light_mail_and_plate", "Light Mail and Plate", [("light_mail_and_plate",0)], itp_type_body_armor|itp_covers_legs   ,0, 532 , weight(10)|abundance(100)|head_armor(0)|body_armor(32)|leg_armor(12)|difficulty(0) ,imodbits_armor ],

["byzantion_helmet_a", "Byzantion Helmet", [("byzantion_helmet_a",0)], itp_type_head_armor, 0, 240, weight(2.5)|abundance(140)|head_armor(16)|body_armor(0)|leg_armor(0)|difficulty(6), imodbits_cloth ],
["magyar_helmet_a", "Magyar Helmet", [("magyar_helmet_a",0)], itp_type_head_armor, 0, 240, weight(2.5)|abundance(140)|head_armor(16)|body_armor(0)|leg_armor(0)|difficulty(6), imodbits_cloth ],
["rus_helmet_a", "Rus Helmet", [("rus_helmet_a",0)], itp_type_head_armor, 0, 240, weight(2.5)|abundance(140)|head_armor(16)|body_armor(0)|leg_armor(0)|difficulty(6), imodbits_cloth ],
["sipahi_helmet_a", "Sipahi Helmet", [("sipahi_helmet_a",0)], itp_type_head_armor, 0, 240, weight(2.5)|abundance(140)|head_armor(16)|body_armor(0)|leg_armor(0)|difficulty(6), imodbits_cloth ],
["shahi", "Shahi", [("shahi",0)], itp_type_head_armor, 0, 240, weight(2.5)|abundance(140)|head_armor(16)|body_armor(0)|leg_armor(0)|difficulty(6), imodbits_cloth ],
["rabati", "Rabati", [("rabati",0)], itp_type_head_armor, 0, 240, weight(2.5)|abundance(140)|head_armor(16)|body_armor(0)|leg_armor(0)|difficulty(6), imodbits_cloth ],

["tunic_with_green_cape", "Tunic with Green Cape", [("peasant_man_a",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs|itp_civilian, 0, 240, weight(6)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["tunic_with_green_cape_a", "Tunic with Green Cape", [("peasant_man_a",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs|itp_civilian, 0, 240, weight(6)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["keys", "Ring of Keys", [("throwing_axe_a",0)], itp_type_one_handed_wpn |itp_primary|itp_bonus_against_shield,itc_scimitar,240, weight(5)|spd_rtng(98) | swing_damage(29,cut)|max_ammo(5)|weapon_length(53),imodbits_thrown ],
["bride_dress", "Bride Dress", [("bride_dress",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 240, weight(6)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["bride_crown", "Crown of Flowers", [("bride_crown",0)], itp_type_head_armor|itp_attach_armature|itp_doesnt_cover_hair|itp_civilian, 0, 20, weight(1)|abundance(100)|head_armor(4)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["bride_shoes", "Bride Shoes", [("bride_shoes",0)], itp_type_foot_armor|itp_attach_armature|itp_civilian, 0, 20, weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(2)|difficulty(0), imodbits_cloth ],

["practice_bow_2","Practice Bow", [("hunting_bow",0), ("hunting_bow_carry",ixmesh_carry)], itp_type_bow |itp_primary|itp_two_handed,itcf_shoot_bow|itcf_carry_bow_back, 0, weight(1.5)|spd_rtng(90) | shoot_speed(40) | thrust_damage(21, blunt),imodbits_bow ],
["practice_arrows_2","Practice Arrows", [("arena_arrow",0),("flying_missile",ixmesh_flying_ammo),("quiver", ixmesh_carry)], itp_type_arrows, itcf_carry_quiver_back, 0,weight(1.5)|weapon_length(95)|max_ammo(80),imodbits_none],


["plate_boots", "Plate Boots", [("plate_boots",0)], itp_type_foot_armor|itp_attach_armature|itp_merchandise, 0, 1050, weight(7.5)|abundance(50)|head_armor(0)|body_armor(0)|leg_armor(28)|difficulty(10), imodbits_armor ],

["heraldic_mail_with_surcoat_for_tableau", "{!}Heraldic Mail with Surcoat", [("heraldic_armor_new_a",0)], itp_type_body_armor |itp_covers_legs ,0, 1, weight(22)|abundance(100)|head_armor(0)|body_armor(1)|leg_armor(1),imodbits_armor, [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_heraldic_armor_a", ":agent_no", ":troop_no")])]],
["mail_boots_for_tableau", "Mail Boots", [("mail_boots_a",0)], itp_type_foot_armor | itp_attach_armature  ,0, 1, weight(3)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(1) ,imodbits_armor ],

# Quest and Other Special Items

["strange_portrait", "Strange Portrait", [("strange_portrait", 0)], itp_type_goods|itp_unique,      0, 7215, weight( 0.70),                                           imodbits_none],
["whiskey",          "Whiskey",          [("whiskey", 0)],          itp_type_goods|itp_consumable,  0, 3910, weight(30.00)|abundance(10)|max_ammo(30), imodbits_none],
["gems",             "Gems",             [("gems", 0)],             itp_type_goods,                 0, 7780, weight( 0.50)|abundance(10),                             imodbits_none],
["silver_item",      "Gold",             [("gold_ingot", 0)],       itp_type_goods,                 0, 6000, weight(60   )|abundance(60),                             imodbits_none],
################################################################################
# NATIVE EXPANSION COMMENTS
# Horses
################################################################################

# Horses

# ["sumpter_horse",   "Farm Horse",             [("sumpter_horse", 0)],                                         itp_merchandise|itp_type_horse, 0,  164, abundance(90)|hit_points( 60)|body_armor( 17)|difficulty(1)|horse_speed(35)|horse_maneuver(35)|horse_charge(10), imodbits_horse_basic],
# ["saddle_horse",    "Saddle Horse",           [("saddle_horse", 0), ("horse_c", imodbits_horse_good)],        itp_merchandise|itp_type_horse, 0,  312, abundance(90)|hit_points( 60)|body_armor( 14)|difficulty(1)|horse_speed(45)|horse_maneuver(45)|horse_charge(14), imodbits_horse_basic],
# ["steppe_horse",    "Steppe Horse",           [("steppe_horse", 0)],                                          itp_merchandise|itp_type_horse, 0,  492, abundance(80)|hit_points( 65)|body_armor( 15)|difficulty(2)|horse_speed(48)|horse_maneuver(48)|horse_charge(17), imodbits_horse_basic],
# ["courser",         "Courser",                [("courser", 0)],                                               itp_merchandise|itp_type_horse, 0,  723, abundance(70)|hit_points( 65)|body_armor( 16)|difficulty(2)|horse_speed(52)|horse_maneuver(48)|horse_charge(20), imodbits_horse_basic|imodbit_champion],
# ["hunter",          "Hunter",                 [("hunting_horse", 0), ("hunting_horse", imodbits_horse_good)], itp_merchandise|itp_type_horse, 0,  734, abundance(60)|hit_points( 70)|body_armor( 29)|difficulty(3)|horse_speed(43)|horse_maneuver(43)|horse_charge(26), imodbits_horse_basic|imodbit_champion],
# ["warhorse",        "Warhorse",               [("warhorse", 0)],                                              itp_merchandise|itp_type_horse, 0, 1224, abundance(50)|hit_points( 60)|body_armor( 52)|difficulty(4)|horse_speed(40)|horse_maneuver(40)|horse_charge(45), imodbits_horse_basic|imodbit_champion],
# ["charger",         "Charger",                [("charger", 0)],                                               itp_merchandise|itp_type_horse, 0, 2411, abundance(40)|hit_points( 80)|body_armor( 65)|difficulty(4)|horse_speed(42)|horse_maneuver(42)|horse_charge(55), imodbits_horse_basic|imodbit_champion],
# ["warhorse_nord",   "Nord Warhorse",          [("warhorse", 0)],                                              itp_merchandise|itp_type_horse, 0,  700, abundance(50)|hit_points( 90)|body_armor( 52)|difficulty(0)|horse_speed(32)|horse_maneuver(37)|horse_charge(22), imodbits_horse_basic|imodbit_champion],
# ["charger_nord",    "Nord Charger",           [("charger", 0)],                                               itp_merchandise|itp_type_horse, 0,  800, abundance(40)|hit_points(100)|body_armor( 65)|difficulty(0)|horse_speed(34)|horse_maneuver(36)|horse_charge(30), imodbits_horse_basic|imodbit_champion],
##  ["larktin_charger", "Lady Larktin's Charger", [("charger", 0)],                                              itp_unique     |itp_type_horse, 0, 2411, abundance(40)|hit_points(320)|body_armor(110)|difficulty(4)|horse_speed(51)|horse_maneuver(51)|horse_charge(70), imodbits_horse_basic|imodbit_champion],
# ["larktin_charger", "Lady Larktin's Charger", [("DK_armour_charger", 0)],                                     itp_type_horse|itp_unique, 0, 2411, 0|abundance(40)|body_armor(110)|difficulty(4)|hit_points(320)|horse_maneuver(51)|horse_speed(51)|horse_charge(70), imodbits_horse_basic|imodbit_champion ],
# ["jarls_warhorse", "Mail Armour Warhorse",    [("mail_armour_charger1", 0)], itp_type_horse|itp_merchandise, 0, 3411, 0|abundance(40)|body_armor(73)|difficulty(3)|hit_points(85)|horse_maneuver(40)|horse_speed(35)|horse_charge(48), imodbits_horse_basic|imodbit_champion ],
	# ["jarls_charger", "Mail Armour Charger", [("mail_armour_charger2", 0)], itp_type_horse|itp_merchandise, 0, 4321, 0|abundance(40)|body_armor(78)|difficulty(4)|hit_points(90)|horse_maneuver(42)|horse_speed(38)|horse_charge(55), imodbits_horse_basic|imodbit_champion ],
	# ["knights_steed", "Knight's Steed", [("barded_armour_charger2", 0)], itp_type_horse|itp_merchandise, 0, 3921, 0|abundance(30)|body_armor(70)|difficulty(4)|hit_points(80)|horse_maneuver(44)|horse_speed(42)|horse_charge(39), imodbits_horse_good|imodbit_stubborn|imodbit_lame|imodbit_champion ],
	# ["knights_charger", "Knight's Charger", [("barded_armour_charger1", 0)], itp_type_horse|itp_merchandise, 0, 4521, 0|abundance(40)|body_armor(65)|difficulty(5)|hit_points(80)|horse_maneuver(42)|horse_speed(44)|horse_charge(40), imodbits_horse_basic|imodbit_champion ],
	# ["dukes_charger", "Duke's Charger", [("plate_armour_charger1", 0)], itp_type_horse|itp_merchandise, 0, 5899, 0|abundance(20)|body_armor(70)|difficulty(6)|hit_points(80)|horse_maneuver(43)|horse_speed(49)|horse_charge(45), imodbits_horse_basic|imodbit_champion ],
	# ["leather_armoured_warhorse", "Leather Armoured Warhorse", [("leather_armour_charger1", 0)], itp_type_horse|itp_merchandise, 0, 2511, 0|abundance(40)|body_armor(60)|difficulty(3)|hit_points(70)|horse_maneuver(40)|horse_speed(45)|horse_charge(30), imodbits_horse_basic|imodbit_champion ],
	# ["rhodok_warhorse", "Rhodok Warhorse", [("rhodok_plate_armour_charger1", 0)], itp_type_horse|itp_merchandise, 0, 3411, 0|abundance(40)|body_armor(80)|difficulty(3)|hit_points(85)|horse_maneuver(38)|horse_speed(33)|horse_charge(45), imodbits_horse_basic|imodbit_champion ],
	# ["heavy_plated_armour_charger", "Plated Armour Warhorse", [("heavy_plated_armour_charger", 0)], itp_type_horse|itp_merchandise, 0, 3831, 0|abundance(30)|body_armor(90)|difficulty(4)|hit_points(90)|horse_maneuver(33)|horse_speed(35)|horse_charge(55), imodbits_horse_basic|imodbit_champion ],
	# ["Vaegir_Warhorse", "Vaegir Warhorse", [("scale_armour_charger_silver", 0)], itp_type_horse|itp_merchandise, 0, 3631, 0|abundance(50)|body_armor(70)|difficulty(3)|hit_points(75)|horse_maneuver(46)|horse_speed(42)|horse_charge(45), imodbits_horse_basic|imodbit_champion ],
	# ["Vaegir_Charger", "Vaegir Charger", [("scale_armour_charger_gold", 0)], itp_type_horse|itp_merchandise, 0, 4431, 0|abundance(30)|body_armor(72)|difficulty(5)|hit_points(80)|horse_maneuver(48)|horse_speed(45)|horse_charge(50), imodbits_horse_basic|imodbit_champion ],
	# ["armoured_courser", "Armoured Courser", [("new_courser_1", 0)], itp_type_horse|itp_merchandise, 0, 799, 0|abundance(60)|body_armor(20)|difficulty(2)|hit_points(65)|horse_maneuver(47)|horse_speed(52)|horse_charge(20), imodbits_horse_basic|imodbit_champion ],
	# ["armoured_hunter", "Armoured Hunter", [("new_hunting_horse_1", 0), ("new_hunting_horse_1", imodbits_none)], itp_type_horse|itp_merchandise, 0, 794, 0|abundance(50)|body_armor(32)|difficulty(3)|hit_points(70)|horse_maneuver(42)|horse_speed(43)|horse_charge(27), imodbits_horse_basic|imodbit_champion ],
	# ["dk_charger", "Dark Knight Charger", [("DK_armour_charger", 0)], itp_type_horse, 0, 2811, 0|abundance(90)|body_armor(65)|difficulty(4)|hit_points(80)|horse_maneuver(42)|horse_speed(42)|horse_charge(55), imodbits_horse_basic|imodbit_champion ],
# ["hospitaller_charger", "Hospitaller Charger", [("hospitaller_charger", 0)], itp_type_horse|itp_merchandise, 0, 2811, 0|abundance(1)|body_armor(65)|difficulty(4)|hit_points(80)|horse_maneuver(42)|horse_speed(42)|horse_charge(55), imodbits_horse_basic|imodbit_champion ],
	# ["templar_charger", "Templar Charger", [("templar_charger", 0)], itp_type_horse|itp_merchandise, 0, 2811, 0|abundance(1)|body_armor(65)|difficulty(4)|hit_points(80)|horse_maneuver(42)|horse_speed(42)|horse_charge(55), imodbits_horse_basic|imodbit_champion ],

	
################################################################################
# NATIVE EXPANSION COMMENTS
# Missile Weapons and Ammo
################################################################################

# Throwing Weapons

["throwing_knives", "Throwing Knives", [("throwing_knife",0)], itp_type_thrown|itp_merchandise|itp_primary, itcf_throw_knife, 120, weight(3)|difficulty(0)|accuracy(75)|spd_rtng(120)|shoot_speed(36)|thrust_damage(18,cut)|max_ammo(32)|weapon_length(0), imodbits_iron_bolts],
["throwing_daggers", "Throwing Daggers", [("throwing_dagger",0)], itp_type_thrown|itp_merchandise|itp_primary, itcf_throw_knife, 220, weight(5)|difficulty(1)|accuracy(85)|spd_rtng(110)|shoot_speed(32)|thrust_damage(24,cut)|max_ammo(28)|weapon_length(0), imodbits_iron_bolts],
# BUG: WTF leg_armor(99) rating is for? FIXED.
["winged_throwing_knives", "Winged_Throwing_Knives", [("throwing_dagger_winged",0)], itp_type_thrown|itp_merchandise|itp_primary, itcf_throw_knife, 900, weight(7)|abundance(20)|difficulty(5)|spd_rtng(100)|shoot_speed(40)|max_ammo(20)|thrust_damage(34,cut), imodbits_iron_bolts],
# ["throwing_axes",          "Throwing Axes",          [("francisca", 0)],              itp_type_thrown|itp_merchandise|itp_primary|itp_bonus_against_shield, itcf_throw_axe,   241, weight(5.00)|difficulty(1)|accuracy(70)|spd_rtng( 95)|shoot_speed(26)|thrust_damage(52, cut)  |max_ammo(14)|weapon_length(53),           imodbits_thrown],
# ["jarid",                  "Jarids",                  [("jarid_new", 0), ("jarid_quiver", ixmesh_carry)],    itp_type_thrown|itp_merchandise|itp_primary|itp_bonus_against_shield, itcf_throw_javelin|itcf_carry_quiver_back|itcf_show_holster_when_drawn, 209, weight(4)|difficulty(2)|accuracy(75)|spd_rtng(100)|shoot_speed(36)|thrust_damage(44, pierce)|max_ammo(14)|weapon_length( 65),               imodbits_thrown],
# ["javelin",                "Javelins",                [("javelin", 0),   ("javelins_quiver", ixmesh_carry)], itp_type_thrown|itp_merchandise|itp_primary|itp_bonus_against_shield, itcf_throw_javelin|itcf_carry_quiver_back|itcf_show_holster_when_drawn,  75, weight(5)|difficulty(1)|accuracy(75)|spd_rtng(110)|shoot_speed(38)|thrust_damage(33, pierce)|max_ammo(20)|weapon_length( 75),               imodbits_thrown],
#  ["throwing_spear",         "Throwing Spears",         [("jarid_new_b", 0), ("jarid_new_b", ixmesh_carry)],       itp_type_thrown|itp_merchandise|itp_primary|itp_bonus_against_shield, itcf_throw_javelin|itcf_carry_quiver_back|itcf_show_holster_when_drawn, 750, weight(4)|difficulty(6)|accuracy(84)|spd_rtng(100)|shoot_speed(40)|thrust_damage(60, pierce)|max_ammo(12)|weapon_length(109)|abundance(15), imodbits_thrown],

# Bows

# Difficulty rating 0
["hunting_bow", "Hunting Bow", [("hunting_bow",0),("hunting_bow_carry",ixmesh_carry)], itp_type_bow|itp_merchandise|itp_two_handed|itp_primary, itcf_shoot_bow|itcf_carry_bow_back, 40, weight(2)|difficulty(0)|accuracy(85)|spd_rtng(110)|shoot_speed(55)|thrust_damage(2,pierce), imodbits_bow ],
# Difficulty rating 1
["short_bow", "Short Bow", [("short_bow",0),("short_bow_carry",ixmesh_carry)], itp_type_bow|itp_merchandise|itp_two_handed|itp_primary, itcf_shoot_bow|itcf_carry_bow_back, 70, weight(2.5)|difficulty(1)|accuracy(80)|spd_rtng(100)|shoot_speed(60)|thrust_damage(4,pierce), imodbits_bow ],
["ash_hunting_bow", "Ashwood Hunting Bow", [("hunting_bow",0),("hunting_bow_carry",ixmesh_carry)], itp_type_bow|itp_merchandise|itp_two_handed|itp_primary, itcf_shoot_bow|itcf_carry_bow_back, 160, weight(3)|difficulty(1)|accuracy(80)|spd_rtng(100)|shoot_speed(60)|thrust_damage(4,pierce), imodbits_bow ],
# Difficulty rating 2
["nomad_bow", "Nomad Bow", [("nomad_bow",0),("nomad_bow_case",ixmesh_carry)], itp_type_bow|itp_merchandise|itp_two_handed|itp_primary, itcf_shoot_bow|itcf_carry_bowcase_left|itcf_show_holster_when_drawn, 600, weight(3)|difficulty(3)|accuracy(85)|spd_rtng(80)|shoot_speed(55)|thrust_damage(10,pierce), imodbits_bow ],
["ash_short_bow", "Ashwood Short Bow", [("short_bow",0),("short_bow_carry",ixmesh_carry)], itp_type_bow|itp_merchandise|itp_two_handed|itp_primary, itcf_shoot_bow|itcf_carry_bow_back, 420, weight(3.5)|difficulty(2)|accuracy(75)|spd_rtng(90)|shoot_speed(65)|thrust_damage(6,pierce), imodbits_bow ],
# Difficulty rating 3
["yew_hunting_bow", "Yew Hunting Bow", [("hunting_bow",0),("hunting_bow_carry",ixmesh_carry)], itp_type_bow|itp_merchandise|itp_two_handed|itp_primary, itcf_shoot_bow|itcf_carry_bow_back, 160, weight(3)|difficulty(1)|accuracy(100)|spd_rtng(120)|shoot_speed(80)|thrust_damage(2,pierce), imodbits_bow ],
["strong_bow", "Strong Bow", [("strong_bow",0),("strong_bow_case",ixmesh_carry)], itp_type_bow|itp_merchandise|itp_two_handed|itp_primary, itcf_shoot_bow|itcf_carry_bowcase_left|itcf_show_holster_when_drawn, 420, weight(3.5)|difficulty(3)|accuracy(80)|spd_rtng(80)|shoot_speed(70)|thrust_damage(10,pierce), imodbits_bow ],
["elm_nomad_bow", "Elmwood Nomad Bow", [("nomad_bow",0),("nomad_bow_case",ixmesh_carry)], itp_type_bow|itp_merchandise|itp_two_handed|itp_primary, itcf_shoot_bow|itcf_carry_bowcase_left|itcf_show_holster_when_drawn, 720, weight(3.5)|difficulty(4)|accuracy(85)|spd_rtng(80)|shoot_speed(55)|thrust_damage(12,pierce), imodbits_bow ],
["khergit_bow", "Khergit Bow", [("khergit_bow",0),("khergit_bow_case",ixmesh_carry)], itp_type_bow|itp_merchandise|itp_two_handed|itp_primary, itcf_shoot_bow|itcf_carry_bowcase_left|itcf_show_holster_when_drawn, 600, weight(3)|difficulty(3)|accuracy(85)|spd_rtng(100)|shoot_speed(75)|thrust_damage(7,pierce), imodbits_bow ],
["long_bow", "Long Bow", [("long_bow",0),("long_bow_carry",ixmesh_carry)], itp_type_bow|itp_merchandise|itp_two_handed|itp_primary, itcf_shoot_bow|itcf_carry_bow_back, 160, weight(3)|difficulty(2)|accuracy(75)|spd_rtng(90)|shoot_speed(65)|thrust_damage(8,pierce), imodbits_bow ],
# Difficulty rating 4
["yew_short_bow", "Yew Short Bow", [("short_bow",0),("short_bow_carry",ixmesh_carry)], itp_type_bow|itp_merchandise|itp_two_handed|itp_primary, itcf_shoot_bow|itcf_carry_bow_back, 420, weight(3.5)|difficulty(2)|accuracy(95)|spd_rtng(110)|shoot_speed(65)|thrust_damage(4,pierce), imodbits_bow ],
["teak_nomad_bow", "Teak Nomad Bow", [("nomad_bow",0),("nomad_bow_case",ixmesh_carry)], itp_type_bow|itp_merchandise|itp_two_handed|itp_primary, itcf_shoot_bow|itcf_carry_bowcase_left|itcf_show_holster_when_drawn, 600, weight(4)|difficulty(4)|accuracy(85)|spd_rtng(90)|shoot_speed(65)|thrust_damage(10,pierce), imodbits_bow ],
["ash_strong_bow", "Ashwood Strong Bow", [("strong_bow",0),("strong_bow_case",ixmesh_carry)], itp_type_bow|itp_merchandise|itp_two_handed|itp_primary, itcf_shoot_bow|itcf_carry_bowcase_left|itcf_show_holster_when_drawn, 720, weight(4.5)|difficulty(4)|accuracy(75)|spd_rtng(75)|shoot_speed(75)|thrust_damage(12,pierce), imodbits_bow ],
["elm_khergit_bow", "Elmwood Khergit Bow", [("khergit_bow",0),("khergit_bow_case",ixmesh_carry)], itp_type_bow|itp_merchandise|itp_two_handed|itp_primary, itcf_shoot_bow|itcf_carry_bowcase_left|itcf_show_holster_when_drawn, 720, weight(3.5)|difficulty(4)|accuracy(85)|spd_rtng(100)|shoot_speed(75)|thrust_damage(9,pierce), imodbits_bow ],
["ash_long_bow", "Ashwood Long Bow", [("long_bow",0),("long_bow_carry",ixmesh_carry)], itp_type_bow|itp_merchandise|itp_two_handed|itp_primary, itcf_shoot_bow|itcf_carry_bow_back, 600, weight(4)|difficulty(3)|accuracy(70)|spd_rtng(80)|shoot_speed(70)|thrust_damage(10,pierce), imodbits_bow ],
#["ivory_bow", "Ivory Bow", [("guizugong",0),("guizugong2",ixmesh_carry)], itp_type_bow|itp_two_handed|itp_primary, itcf_shoot_bow|itcf_carry_bow_back, 2250, weight(4.5)|difficulty(4)|accuracy(100)|spd_rtng(90)|shoot_speed(80)|thrust_damage(14,pierce), imodbits_bow ],
["ivory_bow", "Ivory Bow", [("ivorybow",0),("ivorybow_carry",ixmesh_carry)], itp_type_bow|itp_two_handed|itp_primary, itcf_shoot_bow|itcf_carry_bow_back, 2250, weight(4.5)|difficulty(4)|accuracy(100)|spd_rtng(90)|shoot_speed(80)|thrust_damage(14,pierce), imodbits_bow ],
["swadian_long_bow", "Royal Ashwood Longbow", [("long_bow",0),("long_bow_carry",ixmesh_carry)], itp_type_bow|itp_merchandise|itp_two_handed|itp_primary, itcf_shoot_bow|itcf_carry_bow_back, 2250, weight(5.5)|difficulty(6)|accuracy(85)|spd_rtng(65)|shoot_speed(80)|thrust_damage(16,pierce), imodbits_bow ],
# Difficulty rating 5
["yew_strong_bow", "Yew Strong Bow", [("strong_bow",0),("strong_bow_case",ixmesh_carry)], itp_type_bow|itp_merchandise|itp_two_handed|itp_primary, itcf_shoot_bow|itcf_carry_bowcase_left|itcf_show_holster_when_drawn, 720, weight(4.5)|difficulty(4)|accuracy(95)|spd_rtng(90)|shoot_speed(75)|thrust_damage(10,pierce), imodbits_bow ],
["teak_khergit_bow", "Teak Khergit Bow", [("khergit_bow",0),("khergit_bow_case",ixmesh_carry)], itp_type_bow|itp_merchandise|itp_two_handed|itp_primary, itcf_shoot_bow|itcf_carry_bowcase_left|itcf_show_holster_when_drawn, 600, weight(4)|difficulty(4)|accuracy(85)|spd_rtng(110)|shoot_speed(85)|thrust_damage(7,pierce), imodbits_bow ],
["war_bow", "Composite Longbow", [("war_bow",0),("war_bow_carry",ixmesh_carry)], itp_type_bow|itp_merchandise|itp_two_handed|itp_primary, itcf_shoot_bow|itcf_carry_bow_back, 600, weight(4)|difficulty(4)|accuracy(85)|spd_rtng(70)|shoot_speed(75)|thrust_damage(12,pierce), imodbits_bow ],
["ash_war_bow", "Composite Ash Longbow", [("war_bow",0),("war_bow_carry",ixmesh_carry)], itp_type_bow|itp_merchandise|itp_two_handed|itp_primary, itcf_shoot_bow|itcf_carry_bow_back, 1350, weight(5)|difficulty(5)|accuracy(80)|spd_rtng(70)|shoot_speed(80)|thrust_damage(15,pierce), imodbits_bow ],
["yew_long_bow", "Yew Longbow", [("long_bow",0),("long_bow_carry",ixmesh_carry)], itp_type_bow|itp_merchandise|itp_two_handed|itp_primary, itcf_shoot_bow|itcf_carry_bow_back, 600, weight(4)|difficulty(3)|accuracy(90)|spd_rtng(100)|shoot_speed(70)|thrust_damage(8,pierce), imodbits_bow ],
# Difficulty rating 6
["yew_war_bow", "Composite Yew Bow", [("war_bow",0),("war_bow_carry",ixmesh_carry)], itp_type_bow|itp_merchandise|itp_two_handed|itp_primary, itcf_shoot_bow|itcf_carry_bow_back, 1350, weight(5)|difficulty(5)|accuracy(100)|spd_rtng(90)|shoot_speed(90)|thrust_damage(12,pierce), imodbits_bow ],
# Difficulty rating 7
# BUG: leg_armor(100) rating set for some reason! FIXED.
["tulgan_bow", "Tulgan Bow", [("tulgan_bow_1",0),("tulgan_bow_holster_full",ixmesh_carry)], itp_type_bow|itp_two_handed|itp_primary, itcf_shoot_bow|itcf_carry_bowcase_left|itcf_show_holster_when_drawn, 1350, weight(5)|difficulty(7)|accuracy(95)|spd_rtng(80)|shoot_speed(85)|thrust_damage(25,pierce), imodbits_bow ],

# Crossbows

["hunting_crossbow", "Hunting Crossbow", [("crossbow_a",0)], itp_type_crossbow|itp_merchandise|itp_two_handed|itp_primary, itcf_shoot_crossbow|itcf_carry_crossbow_back, 140, weight(3)|difficulty(0)|accuracy(80)|spd_rtng(40)|shoot_speed(60)|thrust_damage(35,pierce)|max_ammo(1), imodbits_crossbow ],
["light_crossbow", "Light Crossbow", [("crossbow_b",0)], itp_type_crossbow|itp_merchandise|itp_two_handed|itp_primary, itcf_shoot_crossbow|itcf_carry_crossbow_back, 260, weight(4)|difficulty(6)|accuracy(80)|spd_rtng(40)|shoot_speed(60)|thrust_damage(40,pierce)|max_ammo(1), imodbits_crossbow ],
["crossbow", "Crossbow", [("crossbow_a",0)], itp_type_crossbow|itp_merchandise|itp_two_handed|itp_primary|itp_cant_use_on_horseback, itcf_shoot_crossbow|itcf_carry_crossbow_back, 400, weight(5)|difficulty(8)|accuracy(80)|spd_rtng(40)|shoot_speed(65)|thrust_damage(45,pierce)|max_ammo(1), imodbits_crossbow ],
["heavy_crossbow", "Heavy Crossbow", [("crossbow_c",0)], itp_type_crossbow|itp_merchandise|itp_two_handed|itp_primary|itp_cant_use_on_horseback, itcf_shoot_crossbow|itcf_carry_crossbow_back, 520, weight(8)|difficulty(12)|accuracy(90)|spd_rtng(35)|shoot_speed(75)|thrust_damage(65,pierce)|max_ammo(1), imodbits_crossbow ],
["rhodes_crossbow", "Rhodok Crossbow", [("crossbow_b",0)], itp_type_crossbow|itp_merchandise|itp_two_handed|itp_primary, itcf_shoot_crossbow|itcf_carry_crossbow_back, 620, weight(4)|difficulty(10)|accuracy(90)|spd_rtng(50)|shoot_speed(70)|thrust_damage(50,pierce)|max_ammo(1)|abundance(100), imodbits_crossbow ],
["sniper_crossbow", "Siege Crossbow", [("crossbow_c",0)], itp_type_crossbow|itp_merchandise|itp_two_handed|itp_primary|itp_cant_use_on_horseback, itcf_shoot_crossbow|itcf_carry_crossbow_back, 700, weight(9)|difficulty(15)|accuracy(95)|spd_rtng(30)|shoot_speed(75)|thrust_damage(70,pierce)|max_ammo(1), imodbits_crossbow ],
["scout_crossbow", "Scout's Crossbow", [("crossbow_b",0)], itp_type_crossbow|itp_merchandise|itp_two_handed|itp_primary, itcf_shoot_crossbow|itcf_carry_crossbow_back, 800, weight(6)|difficulty(12)|accuracy(90)|spd_rtng(50)|shoot_speed(70)|thrust_damage(55,pierce)|max_ammo(1), imodbits_crossbow ],
# BUG: flag itp_cant_use_on_horseback is in the wrong section. FIXED.
["skirmisher_crossbow", "Skirmish Crossbow", [("crossbow",0)], itp_type_crossbow|itp_merchandise|itp_two_handed|itp_primary|itp_cant_use_on_horseback, itcf_shoot_crossbow|itcf_carry_crossbow_back, 400, weight(7)|difficulty(10)|accuracy(85)|spd_rtng(40)|shoot_speed(75)|thrust_damage(60,pierce)|max_ammo(1), imodbits_crossbow ],
# BUG: flag itp_cant_use_on_horseback is in the wrong section. FIXED.
["battle_crossbow", "Battle Crossbow", [("crossbow_c",0)], itp_type_crossbow|itp_merchandise|itp_two_handed|itp_primary|itp_cant_use_on_horseback, itcf_shoot_crossbow|itcf_carry_crossbow_back, 900, weight(10)|difficulty(18)|accuracy(100)|spd_rtng(30)|shoot_speed(80)|thrust_damage(75,pierce)|max_ammo(1), imodbits_crossbow ],
["repeating_crossbow", "Repeating Crossbow", [("crossbow_c",0)], itp_type_crossbow|itp_merchandise|itp_two_handed|itp_primary|itp_cant_use_on_horseback, itcf_shoot_crossbow|itcf_carry_crossbow_back, 2250, weight(22)|difficulty(18)|accuracy(90)|spd_rtng(10)|shoot_speed(60)|thrust_damage(55,pierce)|max_ammo(9), imodbits_crossbow ],
["war_crossbow", "War Crossbow", [("crossbow_c",0)], itp_type_crossbow|itp_merchandise|itp_two_handed|itp_primary|itp_cant_use_on_horseback, itcf_shoot_crossbow|itcf_carry_crossbow_back, 1850, weight(12)|difficulty(21)|accuracy(100)|spd_rtng(25)|shoot_speed(85)|thrust_damage(80,pierce)|max_ammo(1), imodbits_crossbow ],
["arbalest", "Arbalest", [("pop_arbalest",0)], itp_type_crossbow|itp_merchandise|itp_two_handed|itp_primary|itp_cant_use_on_horseback, itcf_shoot_crossbow|itcf_carry_crossbow_back, 2750, weight(14)|difficulty(21)|accuracy(100)|spd_rtng(25)|shoot_speed(85)|thrust_damage(80,pierce)|max_ammo(1), imodbits_crossbow, [(ti_on_weapon_attack,[(play_sound,"snd_pistol_shot"),(position_move_x,pos1,27),(position_move_y,pos1,36),(particle_system_burst,"psys_pistol_smoke",pos1,15)])] ],
# What is this da_veidar item?
["da_veidar_mark_4", "Da Veidar Mk 4", [("da_veidar_mark_4",0)], itp_type_crossbow|itp_unique|itp_two_handed|itp_primary|itp_cant_use_on_horseback, itcf_shoot_crossbow|itcf_carry_crossbow_back, 3600, weight(14)|difficulty(15)|spd_rtng(30)|shoot_speed(90)|max_ammo(1)|thrust_damage(60,pierce), imodbits_none ],


# Firearms & Ammo

# ["flintlock_pistol", "Flintlock Pistol", [("flintlock_pistol", 0)], itp_type_pistol|itp_merchandise|itp_primary, itcf_shoot_pistol|itcf_reload_pistol, 230, weight(1.5)|difficulty(0)|spd_rtng(38)|shoot_speed(160)|thrust_damage(41, pierce)|max_ammo(1)|accuracy(65), imodbits_none,
# [(ti_on_weapon_attack, [(play_sound, "snd_pistol_shot"), (position_move_x, pos1, 27), (position_move_y, pos1, 36), (particle_system_burst, "psys_pistol_smoke", pos1, 15)])]],

# Arrows

["arrows", "Broadhead Arrows", [("arrow",0),("flying_missile",ixmesh_flying_ammo),("quiver",ixmesh_carry)], itp_type_arrows|itp_merchandise, itcf_carry_quiver_back, 80, weight(2.5)|abundance(80)|weapon_length(95)|thrust_damage(14,cut)|max_ammo(50), imodbits_iron_bolts],
#["ivory_arrows", "Ivory Arrows", [("yinjian",0),("yinjian",ixmesh_flying_ammo),("guizubao",ixmesh_carry)], itp_type_arrows|itp_merchandise, itcf_carry_quiver_back, 2200, weight(3)|abundance(10)|weapon_length(95)|thrust_damage(38,pierce)|max_ammo(36), imodbits_iron_bolts],
["ivory_arrows", "Ivory Arrows", [("ivorygoldquiver_arrow",0),("flying_missile_white",ixmesh_flying_ammo),("ivorygoldquiver",ixmesh_carry)], itp_type_arrows|itp_merchandise, itcf_carry_quiver_back, 2200, weight(3)|abundance(10)|weapon_length(95)|thrust_damage(38,pierce)|max_ammo(36), imodbits_iron_bolts],
["stone_arrows", "Stone-tipped Arrows", [("arrow",0),("flying_missile",ixmesh_flying_ammo),("quiver",ixmesh_carry)], itp_type_arrows|itp_merchandise, itcf_carry_quiver_back, 80, weight(5)|abundance(100)|weapon_length(95)|thrust_damage(14,blunt)|max_ammo(50), imodbits_bolts],
["bone_arrows", "Bone-tipped Arrows", [("arrow",0),("flying_missile",ixmesh_flying_ammo),("quiver",ixmesh_carry)], itp_type_arrows|itp_merchandise, itcf_carry_quiver_back, 40, weight(2)|abundance(40)|weapon_length(95)|thrust_damage(12,cut)|max_ammo(50), imodbits_bolts],
["copper_arrows", "Copper-tipped Arrows", [("arrow",0),("flying_missile",ixmesh_flying_ammo),("quiver",ixmesh_carry)], itp_type_arrows|itp_merchandise, itcf_carry_quiver_back, 120, weight(3)|abundance(100)|weapon_length(95)|thrust_damage(16,cut)|max_ammo(44), imodbits_bolts],
["iron_arrows", "Iron-tipped Arrows", [("barbed_arrow",0),("flying_missile",ixmesh_flying_ammo),("quiver_d",ixmesh_carry)], itp_type_arrows|itp_merchandise, itcf_carry_quiver_back, 160, weight(3.5)|abundance(100)|weapon_length(95)|thrust_damage(18,cut)|max_ammo(44), imodbits_iron_bolts],
["steel_arrows", "Steel-tipped Arrows", [("piercing_arrow",0),("flying_missile",ixmesh_flying_ammo),("quiver_c",ixmesh_carry)], itp_type_arrows|itp_merchandise, itcf_carry_quiver_back, 200, weight(2.5)|abundance(80)|weapon_length(95)|thrust_damage(20,cut)|max_ammo(44), imodbits_iron_bolts],
["heavy_steel_arrows", "Heavy Steel Arrows", [("barbed_arrow",0),("flying_missile",ixmesh_flying_ammo),("quiver_d",ixmesh_carry)], itp_type_arrows|itp_merchandise, itcf_carry_quiver_back, 340, weight(3)|abundance(40)|weapon_length(95)|thrust_damage(22,cut)|max_ammo(36), imodbits_iron_bolts - imodbit_heavy],
["tempered_steel_arrows", "Tempered Steel Arrows", [("piercing_arrow",0),("flying_missile",ixmesh_flying_ammo),("quiver_c",ixmesh_carry)], itp_type_arrows|itp_merchandise, itcf_carry_quiver_back, 500, weight(3)|abundance(20)|weapon_length(95)|thrust_damage(26,cut)|max_ammo(36), imodbits_iron_bolts - imodbit_tempered],
["barbed_arrows", "Barbed Arrows", [("barbed_arrow",0),("flying_missile",ixmesh_flying_ammo),("quiver_d",ixmesh_carry)], itp_type_arrows|itp_merchandise, itcf_carry_quiver_back, 120, weight(2)|abundance(40)|weapon_length(95)|thrust_damage(14,pierce)|max_ammo(50), imodbits_iron_bolts],
["copper_bodkin_arrows", "Copper Bodkin Arrows", [("piercing_arrow",0),("flying_missile",ixmesh_flying_ammo),("quiver_c",ixmesh_carry)], itp_type_arrows|itp_merchandise, itcf_carry_quiver_back, 560, weight(3.5)|abundance(100)|weapon_length(95)|thrust_damage(10,pierce)|max_ammo(44), imodbits_bolts],
["iron_bodkin_arrows", "Iron Bodkin Arrows", [("piercing_arrow",0),("flying_missile",ixmesh_flying_ammo),("quiver_c",ixmesh_carry)], itp_type_arrows|itp_merchandise, itcf_carry_quiver_back, 680, weight(4)|abundance(80)|weapon_length(95)|thrust_damage(16,pierce)|max_ammo(44), imodbits_iron_bolts],
["steel_bodkin_arrows", "Steel Bodkin Arrows", [("piercing_arrow",0),("flying_missile",ixmesh_flying_ammo),("quiver_c",ixmesh_carry)], itp_type_arrows|itp_merchandise, itcf_carry_quiver_back, 800, weight(3)|abundance(40)|weapon_length(95)|thrust_damage(22,pierce)|max_ammo(44), imodbits_iron_bolts],
["masterwork_bodkin_arrows", "Masterwork Bodkin Arrows", [("piercing_arrow",0),("flying_missile",ixmesh_flying_ammo),("quiver_c",ixmesh_carry)], itp_type_arrows|itp_merchandise, itcf_carry_quiver_back, 1400, weight(3)|abundance(20)|weapon_length(95)|thrust_damage(28,pierce)|max_ammo(36), imodbits_iron_bolts - imodbit_masterwork],

# Bolts

["bolts", "Wooden Bolts", [("bolt",0),("flying_missile",ixmesh_flying_ammo),("bolt_bag",ixmesh_carry),("bolt_bag_b",ixmesh_carry|imodbit_large_bag)], itp_type_bolts|itp_merchandise, itcf_carry_quiver_right_vertical, 20, weight(4)|abundance(80)|weapon_length(55)|thrust_damage(8,pierce)|max_ammo(34), imodbits_bolts ],
["stone_bolts", "Stone-Tipped Bolts", [("bolt",0),("flying_missile",ixmesh_flying_ammo),("bolt_bag",ixmesh_carry),("bolt_bag_b",ixmesh_carry|imodbit_large_bag)], itp_type_bolts|itp_merchandise, itcf_carry_quiver_right_vertical, 60, weight(4)|abundance(80)|weapon_length(55)|thrust_damage(12,pierce)|max_ammo(34), imodbits_bolts ],
["copper_bolts", "Copper Bolts", [("bolt",0),("flying_missile",ixmesh_flying_ammo),("bolt_bag_c",ixmesh_carry)], itp_type_bolts|itp_merchandise, itcf_carry_quiver_right_vertical, 100, weight(5)|abundance(100)|weapon_length(55)|thrust_damage(16,pierce)|max_ammo(34), imodbits_bolts ],
["bronze_bolts", "Bronze Bolts", [("bolt",0),("flying_missile",ixmesh_flying_ammo),("bolt_bag_c",ixmesh_carry)], itp_type_bolts|itp_merchandise, itcf_carry_quiver_right_vertical, 140, weight(5)|abundance(100)|weapon_length(55)|thrust_damage(20,pierce)|max_ammo(34), imodbits_bolts ],
["iron_bolts", "Iron Bolts", [("bolt",0),("flying_missile",ixmesh_flying_ammo),("bolt_bag",ixmesh_carry),("bolt_bag_b",ixmesh_carry|imodbit_large_bag)], itp_type_bolts|itp_merchandise, itcf_carry_quiver_right_vertical, 180, weight(6)|abundance(80)|weapon_length(55)|thrust_damage(24,pierce)|max_ammo(34), imodbits_iron_bolts ],
["steel_bolts", "Steel Bolts", [("bolt",0),("flying_missile",ixmesh_flying_ammo),("bolt_bag_c",ixmesh_carry)], itp_type_bolts|itp_merchandise, itcf_carry_quiver_right_vertical, 250, weight(4)|abundance(60)|weapon_length(55)|thrust_damage(28,pierce)|max_ammo(34), imodbits_iron_bolts ],
["rhodok_bolts", "Rhodok Bolts", [("bolt",0),("flying_missile",ixmesh_flying_ammo),("bolt_bag_c",ixmesh_carry)], itp_type_bolts|itp_merchandise, itcf_carry_quiver_right_vertical, 440, weight(4)|abundance(20)|weapon_length(55)|thrust_damage(30,pierce)|max_ammo(34), imodbits_iron_bolts ],

# Firearms Ammo

["cartridges", "Cartridges", [("cartridge_a", 0)], itp_type_bullets|itp_merchandise, 0, 41, weight(2.25)|abundance(90)|weapon_length(3)|thrust_damage(1, pierce)|max_ammo(40), imodbits_missile],

################################################################################
# NATIVE EXPANSION COMMENTS
# Civilian Clothing
################################################################################

# Common Bodywear

# ["leather_jacket", "Leather Jacket", [("leather_jacket", 0)], itp_merchandise|itp_type_body_armor|itp_civilian, 0, 50, weight(3)|abundance(100)|head_armor(0)|body_armor(15)|leg_armor(0)|difficulty(0), imodbits_cloth],
# ["rawhide_coat", "Rawhide Coat", [("coat_of_plates_b", 0)], itp_merchandise|itp_type_body_armor|itp_civilian|itp_covers_legs, 0, 12, weight(5)|abundance(100)|head_armor(0)|body_armor(10)|leg_armor(0)|difficulty(0), imodbits_cloth],
# ["fur_coat", "Fur Coat", [("fur_coat", 0)], itp_merchandise|itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 115, weight(6)|abundance(100)|head_armor(0)|body_armor(13)|leg_armor(6)|difficulty(0), imodbits_armor],
# ["dress", "Dress", [("dress", 0)], itp_merchandise|itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 6, weight(1)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(2)|difficulty(0), imodbits_cloth],
#  ["blue_dress", "Blue Dress", [("blue_dress", 0)], itp_merchandise|itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 6, weight(1)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(2)|difficulty(0), imodbits_cloth],
# ["peasant_dress", "Peasant Dress", [("peasant_dress_b", 0)], itp_merchandise|itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 6, weight(1)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(2)|difficulty(0), imodbits_cloth],
# ["woolen_dress", "Woolen Dress", [("woolen_dress", 0)], itp_merchandise|itp_type_body_armor|itp_civilian|itp_covers_legs, 0, 10, weight(1.75)|abundance(100)|head_armor(0)|body_armor(8)|leg_armor(2)|difficulty(0), imodbits_cloth],
# ["shirt", "Shirt", [("shirt", 0)], itp_merchandise|itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth],
# ["linen_tunic", "Linen Tunic", [("shirt_a", 0)], itp_merchandise|itp_type_body_armor|itp_civilian|itp_covers_legs, 0, 6, weight(1)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(1)|difficulty(0), imodbits_cloth],
# ["short_tunic", "Rich Tunic", [("cvl_costume_a", 0)], itp_merchandise|itp_type_body_armor|itp_civilian|itp_covers_legs, 0, 10, weight(1)|abundance(100)|head_armor(0)|body_armor(7)|leg_armor(1)|difficulty(0), imodbits_cloth],
#  ["robe", "Robe", [("robe", 0)], itp_merchandise|itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 31, weight(1.5)|abundance(100)|head_armor(0)|body_armor(8)|leg_armor(6)|difficulty(0), imodbits_cloth],
#["coarse_tunic", "Coarse Tunic", [("coarse_tunic", 0)], itp_merchandise|itp_type_body_armor|itp_civilian|itp_covers_legs, 0, 47, weight(2)|abundance(100)|head_armor(0)|body_armor(11)|leg_armor(6)|difficulty(0), imodbits_cloth],
#  ["leather_apron", "Leather Apron", [("leather_apron", 0)], itp_merchandise|itp_type_body_armor|itp_civilian|itp_covers_legs, 0, 61, weight(3)|abundance(100)|head_armor(0)|body_armor(12)|leg_armor(7)|difficulty(0), imodbits_cloth],
# ["tabard", "Tabard", [("tabard_b", 0)], itp_merchandise|itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 107, weight(3)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(6)|difficulty(0), imodbits_cloth],
#["leather_vest", "Leather Vest", [("leather_vest", 0)], itp_merchandise|itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 146, weight(4)|abundance(100)|head_armor(0)|body_armor(15)|leg_armor(7)|difficulty(0), imodbits_cloth],
["common_linen_a", "Linen Cloth", [("tabard_b",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs|itp_civilian, 0, 240, weight(6)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["common_linen_b", "Linen Cloth", [("fattiglinenskjortir",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs|itp_civilian, 0, 240, weight(6)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["common_linen_c", "Linen Cloth", [("ad_viking_costumes_03",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs|itp_civilian, 0, 240, weight(6)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["common_linen_d", "Linen Cloth", [("ad_viking_costumes_06",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs|itp_civilian, 0, 240, weight(6)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(0)|difficulty(0), imodbits_cloth ],

# ["gambeson", "Gambeson", [("white_gambeson", 0)], itp_merchandise|itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 260, weight(5)|abundance(100)|head_armor(0)|body_armor(20)|leg_armor(5)|difficulty(0), imodbits_cloth],
# ["blue_gambeson", "Blue Gambeson", [("blue_gambeson", 0)], itp_merchandise|itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 270, weight(5)|abundance(100)|head_armor(0)|body_armor(21)|leg_armor(5)|difficulty(0), imodbits_cloth],
# ["red_gambeson", "Red Gambeson", [("red_gambeson", 0)], itp_merchandise|itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 276, weight(5)|abundance(100)|head_armor(0)|body_armor(21)|leg_armor(5)|difficulty(0), imodbits_cloth],
# ["leather_jerkin", "Leather Jerkin", [("ragged_leather_jerkin", 0)], itp_merchandise|itp_type_body_armor|itp_civilian|itp_covers_legs, 0, 321, weight(6)|abundance(100)|head_armor(0)|body_armor(23)|leg_armor(6)|difficulty(0), imodbits_cloth],
# ["nomad_vest", "Nomad Vest", [("nomad_vest_new", 0)], itp_merchandise|itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 360, weight(7)|abundance(50)|head_armor(0)|body_armor(22)|leg_armor(8)|difficulty(0), imodbits_cloth],
# ["ragged_outfit", "Ragged Outfit", [("ragged_outfit_a", 0)], itp_merchandise|itp_type_body_armor|itp_civilian|itp_covers_legs, 0, 390, weight(7)|abundance(100)|head_armor(0)|body_armor(23)|leg_armor(9)|difficulty(0), imodbits_cloth],
#  ["padded_leather", "Padded Leather", [("leather_armor_b", 0)], itp_merchandise|itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 454, weight(12)|abundance(100)|head_armor(0)|body_armor(27)|leg_armor(10)|difficulty(0), imodbits_cloth],
# ["tribal_warrior_outfit", "Tribal Warrior Outfit", [("tribal_warrior_outfit_a", 0)], itp_merchandise|itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 520, weight(14)|abundance(100)|head_armor(0)|body_armor(30)|leg_armor(10)|difficulty(0), imodbits_cloth],
# ["nomad_robe", "Nomad Robe", [("nomad_robe_a", 0)], itp_merchandise|itp_type_body_armor|itp_civilian|itp_covers_legs|itp_civilian, 0, 610, weight(15)|abundance(100)|head_armor(0)|body_armor(32)|leg_armor(10)|difficulty(0), imodbits_cloth],
# note this one
#["lamellar_vest", "Lamellar Vest", [("nmd_warrior_a", 0)], itp_merchandise|itp_type_body_armor|itp_civilian|itp_covers_legs, 0, 1370, weight(18)|abundance(100)|head_armor(0)|body_armor(46)|leg_armor(8)|difficulty(7), imodbits_cloth],
["tribal_warrior_outfit_b", "Heavy Tribal Warrior Outfit", [("dan_chainfur",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs|itp_civilian, 0, 650, weight(10.5)|abundance(140)|head_armor(0)|body_armor(26)|leg_armor(0)|difficulty(6), imodbits_cloth ],
["lamellar_vest_b", "Lamellar Vest", [("vij_pike",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs|itp_civilian, 0, 1125, weight(15)|abundance(120)|head_armor(0)|body_armor(30)|leg_armor(4)|difficulty(8), imodbits_armor ],
["lamellar_vest_c", "Lamellar Vest", [("lamellar_armor_b_ll",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs|itp_civilian, 0, 1125, weight(15)|abundance(120)|head_armor(0)|body_armor(30)|leg_armor(4)|difficulty(8), imodbits_armor ],
["lamellar_vest_d", "Lamellar Vest", [("nmd_warrior_a_b",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs|itp_civilian, 0, 1125, weight(15)|abundance(120)|head_armor(0)|body_armor(30)|leg_armor(4)|difficulty(8), imodbits_armor ],

# Noble Bodywear

# ["coat", "Coat", [("nobleman_outf", 0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 348, weight(4)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(10)|difficulty(0), imodbits_cloth],
# ["leather_coat", "Leather Coat", [("nobleman_outf", 0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 348, weight(4)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(10)|difficulty(0), imodbits_cloth],
# ["lady_dress_ruby", "Lady Dress", [("lady_dress_r", 0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 500, weight(3)|abundance(100)|head_armor(0)|body_armor(10)|leg_armor(10)|difficulty(0), imodbits_cloth],
# ["lady_dress_green", "Lady Dress", [("lady_dress_g", 0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 500, weight(3)|abundance(100)|head_armor(0)|body_armor(10)|leg_armor(10)|difficulty(0), imodbits_cloth],
# ["lady_dress_blue", "Lady Dress", [("lady_dress_b", 0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 500, weight(3)|abundance(100)|head_armor(0)|body_armor(10)|leg_armor(10)|difficulty(0), imodbits_cloth],
# ["courtly_outfit", "Courtly Outfit", [("nobleman_outf", 0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 348, weight(4)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(10)|difficulty(0), imodbits_cloth],
# ["nobleman_outfit", "Nobleman Outfit", [("nobleman_outfit_b", 0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 348, weight(4)|abundance(100)|head_armor(0)|body_armor(15)|leg_armor(12)|difficulty(0), imodbits_cloth],
# ["court_dress", "Court Dress", [("court_dress", 0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 348, weight(4)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(4)|difficulty(0), imodbits_cloth],
# ["rich_outfit", "Rich Outfit", [("merchant_outf", 0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 348, weight(4)|abundance(100)|head_armor(0)|body_armor(16)|leg_armor(4)|difficulty(0), imodbits_cloth],
["wedding_dress", "Wedding Dress", [("wedding_dress_test",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 1250, weight(6)|body_armor(6), imodbits_none ],

# Common Headwear

# ["turret_hat_green", "Barbette", [("barbette_new", 0)], itp_merchandise|itp_type_head_armor|itp_civilian|itp_fit_to_head, 0, 70, weight(0.5)|abundance(100)|head_armor(6)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth],
#["wimple_a", "Wimple", [("wimple_a", 0)], itp_merchandise|itp_type_head_armor|itp_civilian|itp_fit_to_head, 0, 10, weight(0.5)|abundance(100)|head_armor(4)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth],
#  ["wimple_with_veil", "Wimple with Veil", [("wimple_b", 0)], itp_merchandise|itp_type_head_armor|itp_civilian|itp_fit_to_head, 0, 10, weight(0.5)|abundance(100)|head_armor(4)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth],
# ["straw_hat", "Straw Hat", [("straw_hat", 0)], itp_merchandise|itp_type_head_armor|itp_civilian, 0, 9, weight(1)|abundance(100)|head_armor(2)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth],
# ["common_hood", "Hood", [("hood_a", 0), ("hood_b", 0), ("hood_c", 0), ("hood_d", 0)], itp_merchandise|itp_type_head_armor|itp_civilian, 0, 9, weight(1)|abundance(100)|head_armor(10)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth],
# ["headcloth", "Headcloth", [("headcloth", 0)], itp_merchandise|itp_type_head_armor|itp_civilian, 0, 1, weight(0.5)|abundance(100)|head_armor(4)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth],
# ["woolen_hood", "Woolen Hood", [("woolen_hood", 0)], itp_merchandise|itp_type_head_armor|itp_civilian, 0, 4, weight(1)|abundance(100)|head_armor(8)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth],
#  ["fur_hat", "Fur Hat", [("fur_hat_a_new", 0)], itp_merchandise|itp_type_head_armor|itp_civilian, 0, 4, weight(0.5)|abundance(100)|head_armor(8)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth],
# ["nomad_cap", "Nomad Cap", [("nomad_cap_a_new", 0), ("helmet_fur_a", imodbits_good)], itp_merchandise|itp_type_head_armor|itp_civilian, 0, 6, weight(0.75)|abundance(100)|head_armor(10)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth],
# ["steppe_cap", "Steppe Cap", [("helmet_fur_b", 0)], itp_merchandise|itp_type_head_armor|itp_civilian, 0, 14, weight(1)|abundance(100)|head_armor(12)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth],
# ["woolen_cap", "Woolen Cap", [("woolen_cap", 0)], itp_merchandise|itp_type_head_armor|itp_civilian, 0, 2, weight(1)|abundance(100)|head_armor(6)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth],
# ["felt_hat", "Felt Hat", [("felt_hat_a", 0), ("felt_hat_b", imodbits_good)], itp_merchandise|itp_type_head_armor|itp_civilian, 0, 4, weight(1)|abundance(100)|head_armor(8)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth],
# ["leather_cap", "Leather Cap", [("leather_cap", 0)], itp_merchandise|itp_type_head_armor|itp_civilian, 0, 6, weight(1)|abundance(100)|head_armor(10)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth],
# ["arming_cap", "Arming Cap", [("arming_cap_a_new", 0)], itp_merchandise|itp_type_head_armor|itp_civilian, 0, 6, weight(1)|abundance(100)|head_armor(10)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth],
# ["female_hood", "Lady's Hood", [("woolen_hood", 0)], itp_merchandise|itp_type_head_armor|itp_civilian, 0, 9, weight(1)|abundance(100)|head_armor(10)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth],
#  ["leather_warrior_cap", "Leather Warrior Cap", [("skull_cap_new_b", 0)], itp_merchandise|itp_type_head_armor|itp_civilian, 0, 14, weight(1)|abundance(100)|head_armor(18)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth],

# Noble Headwear

# ["turret_hat_ruby", "Turret Hat", [("turret_hat_r", 0)], itp_type_head_armor|itp_civilian|itp_fit_to_head, 0, 70, weight(0.5)|abundance(100)|head_armor(8)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth],
# ["turret_hat_blue", "Turret Hat", [("turret_hat_b", 0)], itp_type_head_armor|itp_civilian|itp_fit_to_head, 0, 80, weight(0.5)|abundance(100)|head_armor(8)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth],
# ["court_hat", "Turret Hat", [("court_hat", 0)], itp_type_head_armor|itp_civilian|itp_fit_to_head, 0, 80, weight(0.5)|abundance(100)|head_armor(8)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth],

# Legwear

# ["wrapping_boots", "Wrapping Boots", [("wrapping_boots_a", 0)], itp_merchandise|itp_type_foot_armor|itp_civilian|itp_attach_armature, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(3)|difficulty(0), imodbits_cloth],
# ["woolen_hose", "Woolen Hose", [("woolen_hose", 0)], itp_merchandise|itp_type_foot_armor|itp_civilian|itp_attach_armature, 0, 6, weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(4)|difficulty(0), imodbits_cloth],
#  ["blue_hose", "Blue Hose", [("blue_hose_a", 0)], itp_merchandise|itp_type_foot_armor|itp_civilian|itp_attach_armature, 0, 11, weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(5)|difficulty(0), imodbits_cloth],
# ["hunter_boots", "Hunter Boots", [("hunter_boots_a", 0)], itp_merchandise|itp_type_foot_armor|itp_civilian|itp_attach_armature, 0, 19, weight(1.25)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(9)|difficulty(0), imodbits_cloth],
# ["hide_boots", "Hide Boots", [("boot_nomad_a", 0)], itp_merchandise|itp_type_foot_armor|itp_civilian|itp_attach_armature, 0, 34, weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(10)|difficulty(0), imodbits_cloth],
# ["ankle_boots", "Ankle Boots", [("ankle_boots_a", 0)], itp_merchandise|itp_type_foot_armor|itp_civilian|itp_attach_armature, 0, 76, weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(12)|difficulty(0), imodbits_cloth],
#  ["nomad_boots", "Nomad Boots", [("nomad_boots_a", 0)], itp_merchandise|itp_type_foot_armor|itp_civilian|itp_attach_armature, 0, 116, weight(1.25)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(14)|difficulty(0), imodbits_cloth],
# ["leather_boots", "Leather Boots", [("leather_boots_a", 0)], itp_merchandise|itp_type_foot_armor|itp_civilian|itp_attach_armature, 0, 174, weight(1.25)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(16)|difficulty(0), imodbits_cloth],

# Disguise Clothing

# ["pilgrim_disguise", "Pilgrim Disguise", [("pilgrim_outfit", 0)], 0|itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 26, weight(2)|abundance(100)|head_armor(0)|body_armor(19)|leg_armor(8)|difficulty(0), imodbits_cloth],
# ["pilgrim_hood", "Pilgrim Hood", [("pilgrim_hood", 0)], 0|itp_type_head_armor|itp_civilian, 0, 36, weight(1.25)|abundance(100)|head_armor(14)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth],


################################################################################
# NATIVE EXPANSION COMMENTS
# Armor and Protective Equipment
################################################################################

# Merchandise Body Armors


# Armor 10+
#["khergit_armor", "Khergit Armor", [("khergit_armor_new", 0)], itp_merchandise|itp_type_body_armor, 0, 38, weight(2)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(0)|difficulty(0), imodbits_cloth],
#["steppe_armor", "Steppe Armor", [("lamellar_leather", 0)], itp_merchandise|itp_type_body_armor|itp_covers_legs, 0, 196, weight(5)|abundance(100)|head_armor(0)|body_armor(16)|leg_armor(8)|difficulty(0), imodbits_cloth],
#["leather_armor", "Leather Armor", [("tattered_leather_armor_a", 0)], itp_merchandise|itp_type_body_armor|itp_covers_legs, 0, 66, weight(7)|abundance(100)|head_armor(0)|body_armor(18)|leg_armor(0)|difficulty(0), imodbits_cloth],
# Armor 20+
#["padded_cloth", "Padded Cloth", [("padded_cloth_b", 0)], itp_merchandise|itp_type_body_armor|itp_covers_legs, 0, 297, weight(11)|abundance(100)|head_armor(0)|body_armor(22)|leg_armor(6)|difficulty(0), imodbits_cloth],
# ["nomad_armor", "Nomad Armor", [("nomad_armor_new", 0)], itp_merchandise|itp_type_body_armor, 0, 26, weight(2)|abundance(100)|head_armor(0)|body_armor(24)|leg_armor(0)|difficulty(0), imodbits_cloth],
# Armor 30+
#["studded_leather_coat", "Studded Leather Coat", [("leather_armor_a", 0)], itp_merchandise|itp_type_body_armor|itp_covers_legs, 0, 695, weight(14)|abundance(100)|head_armor(0)|body_armor(34)|leg_armor(10)|difficulty(7), imodbits_armor],
#["khergit_guard_armor", "Khergit Guard Armor", [("lamellar_armor_a", 0)], itp_merchandise|itp_type_body_armor|itp_covers_legs, 0, 345, weight(14)|abundance(100)|head_armor(0)|body_armor(34)|leg_armor(8)|difficulty(0), imodbits_armor],
#["mail_shirt", "Mail Shirt", [("mail_shirt_a", 0)], itp_merchandise|itp_type_body_armor|itp_covers_legs, 0, 925, weight(19)|abundance(100)|head_armor(0)|body_armor(37)|leg_armor(12)|difficulty(7), imodbits_armor],
["mail_shirt_b", "Mail Shirt", [("dan_mail",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 2280, weight(24)|abundance(80)|head_armor(4)|body_armor(38)|leg_armor(10)|difficulty(12), imodbits_armor ],
#["byrnie", "Byrnie", [("ad_viking_byrnie_02", 0)], itp_merchandise|itp_type_body_armor|itp_covers_legs, 0, 795, weight(17)|abundance(100)|head_armor(0)|body_armor(39)|leg_armor(6)|difficulty(7), imodbits_armor],
["byrnie_b", "Byrnie", [("ad_viking_byrnie_03",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 1445, weight(17)|abundance(120)|head_armor(0)|body_armor(34)|leg_armor(6)|difficulty(10), imodbits_armor ],
# Armor 40+
#["mail_hauberk", "Mail Hauberk", [("hauberk_a", 0)], itp_merchandise|itp_type_body_armor|itp_covers_legs, 0, 1195, weight(19)|abundance(100)|head_armor(0)|body_armor(40)|leg_armor(12)|difficulty(7), imodbits_armor],
#["haubergeon", "Haubergeon", [("haubergeon_c", 0), ("haubergeon_b", imodbits_good)], itp_merchandise|itp_type_body_armor|itp_covers_legs, 0, 865, weight(18)|abundance(100)|head_armor(0)|body_armor(41)|leg_armor(6)|difficulty(6), imodbits_armor],
# ["mail_with_surcoat", "Mail with Surcoat", [("mail_long_surcoat", 0)], itp_merchandise|itp_type_body_armor|itp_covers_legs, 0, 1545, weight(22)|abundance(100)|head_armor(0)|body_armor(42)|leg_armor(14)|difficulty(7), imodbits_armor],
["byrnie_c", "Byrnie", [("ad_viking_byrnie_06",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 1445, weight(17)|abundance(120)|head_armor(0)|body_armor(34)|leg_armor(6)|difficulty(10), imodbits_armor ],
# ["surcoat_over_mail", "Surcoat over Mail", [("surcoat_over_mail", 0)], itp_merchandise|itp_type_body_armor|itp_covers_legs, 0, 1725, weight(22)|abundance(100)|head_armor(0)|body_armor(43)|leg_armor(14)|difficulty(7), imodbits_armor],
["brigandine_a", "Brigandine", [("brigandine_b",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 1445, weight(17)|abundance(100)|head_armor(0)|body_armor(34)|leg_armor(6)|difficulty(10), imodbits_armor ],
# ["lamellar_armor", "Lamellar Armor", [("lamellar_armor_b", 0)], itp_merchandise|itp_type_body_armor|itp_covers_legs, 0, 2415, weight(25)|abundance(100)|head_armor(0)|body_armor(48)|leg_armor(13)|difficulty(0), imodbits_armor],
["lamellar_armor_b", "Lamellar Armor", [("lamellar_armor",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 120, weight(16)|abundance(120)|head_armor(0)|body_armor(32)|leg_armor(4)|difficulty(8), imodbits_armor ],
["lamellar_armor_c", "Heavy Steppe Scale", [("kushan_new_scaled_armour",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 2730, weight(26)|abundance(80)|head_armor(6)|body_armor(42)|leg_armor(12)|difficulty(12), imodbits_armor ],
["lamellar_armor_d", "Heavy Steppe Scale", [("lamellar_armor_b_l",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 2730, weight(26)|abundance(80)|head_armor(6)|body_armor(42)|leg_armor(12)|difficulty(12), imodbits_armor ],
["lamellar_armor_e", "Hardened Leather", [("kushan_armour",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 650, weight(10.5)|abundance(140)|head_armor(0)|body_armor(26)|leg_armor(0)|difficulty(6), imodbits_cloth ],
["steppe_heavy", "Hardened Leather", [("lamellar_armor_a_b_1",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 650, weight(10.5)|abundance(140)|head_armor(0)|body_armor(26)|leg_armor(0)|difficulty(6), imodbits_cloth ],
# ["banded_armor", "Banded Armor", [("banded_armor_a", 0)], itp_merchandise|itp_type_body_armor|itp_covers_legs, 0, 2715, weight(23)|abundance(100)|head_armor(0)|body_armor(49)|leg_armor(14)|difficulty(8), imodbits_armor],
# ["heraldic_mail_with_surcoat", "Heraldic Mail with Surcoat", [("heraldic_armor_new_a", 0)], itp_merchandise|itp_type_body_armor|itp_covers_legs, 0, 3455, weight(22)|abundance(100)|head_armor(0)|body_armor(49)|leg_armor(17)|difficulty(7), imodbits_armor,
# [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"), (store_trigger_param_2, ":troop_no"), (call_script, "script_shield_item_set_banner", "tableau_heraldic_armor_a", ":agent_no", ":troop_no")])]],
##Armor 50+
#["cuir_bouilli", "Cuir Bouilli", [("cuir_bouilli_a", 0)], itp_merchandise|itp_type_body_armor|itp_covers_legs, 0, 3105, weight(24)|abundance(100)|head_armor(0)|body_armor(50)|leg_armor(15)|difficulty(8), imodbits_armor],
["plate_cuirass", "Plated Cuirass", [("plate_ssm_10042",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 3600, weight(30)|abundance(60)|head_armor(8)|body_armor(48)|leg_armor(16)|difficulty(12), imodbits_armor ],
["allmail", "Heavy Mail", [("dan_allchain",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 2970, weight(27)|abundance(80)|head_armor(6)|body_armor(44)|leg_armor(14)|difficulty(12), imodbits_armor ],
["plate_armor_old", "Old Plate Armor", [("plate_bur_1374",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 4000, weight(37)|abundance(60)|head_armor(8)|body_armor(50)|leg_armor(18)|difficulty(15), imodbits_armor ],
# ["heraldic_mail_with_tabard", "Heraldic_Mail_with_Tabard", [("heraldic_armor_d", 0)], itp_merchandise|itp_type_body_armor|itp_covers_legs, 0, 3655, weight(21)|abundance(100)|head_armor(0)|body_armor(51)|leg_armor(15)|difficulty(7), imodbits_armor,
#  [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"), (store_trigger_param_2, ":troop_no"), (call_script, "script_shield_item_set_banner", "tableau_heraldic_armor_d", ":agent_no", ":troop_no")])]],
#  ["coat_of_plates", "Coat of Plates", [("coat_of_plates", 0)], itp_merchandise|itp_type_body_armor|itp_covers_legs, 0, 3825, weight(25)|abundance(100)|head_armor(0)|body_armor(52)|leg_armor(16)|difficulty(8), imodbits_armor],
# ["plate_armor", "Plate Armor", [("full_plate_armor", 0)], itp_merchandise|itp_type_body_armor|itp_covers_legs, 0, 6555, weight(27)|abundance(100)|head_armor(0)|body_armor(55)|leg_armor(17)|difficulty(8), imodbits_plate],
["brightscale", "Bright Scale Armor", [("dan_brightscale",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 2600, weight(26)|abundance(80)|head_armor(6)|body_armor(40)|leg_armor(12)|difficulty(12), imodbits_armor ],
# Armor 60+
["scalefur", "Scale Armor", [("dan_scalefur",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 2600, weight(26)|abundance(80)|head_armor(6)|body_armor(40)|leg_armor(12)|difficulty(12), imodbits_armor ],
["tarkhan_lamellar", "Tarkhan Armor", [("lamellar_armor_a",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 4000, weight(32)|abundance(60)|head_armor(8)|difficulty(15)|body_armor(50)|leg_armor(18), imodbits_armor ],
["huskarl_armor_a", "Huscarl's Armor", [("dan_hard_lthr",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 5600, weight(40)|abundance(40)|head_armor(10)|body_armor(56)|leg_armor(20)|difficulty(15), imodbits_armor ],
# Armor 70+
["crested_cuirass", "Plated Cuirass", [("plate_bur243",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 3600, weight(30)|abundance(60)|head_armor(8)|body_armor(48)|leg_armor(16)|difficulty(12), imodbits_armor ],
# Armor 80+
["crested_armourplate", "Crested Full Plate", [("plate_ssm_10043",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 6600, weight(44)|abundance(10)|head_armor(10)|body_armor(60)|leg_armor(20)|difficulty(20), imodbits_armor ],

# Special Body Armors

["black_armor", "Black Armor", [("DK_black_armor",0)], itp_type_body_armor|itp_covers_legs, 0, 5265, weight(39)|abundance(40)|head_armor(8)|body_armor(54)|leg_armor(20)|difficulty(15), imodbits_armor ],
#["black_armor", "Black Armor", [("black_armor", 0)], itp_type_body_armor|itp_covers_legs, 0, 9495, weight(28)|abundance(100)|head_armor(30)|body_armor(80)|leg_armor(40)|difficulty(20), imodbits_plate],
# ["strange_armor", "Strange Armor", [("samurai_armor", 0)], itp_type_body_armor|itp_covers_legs, 0, 1255, weight(18)|abundance(100)|head_armor(0)|body_armor(38)|leg_armor(19)|difficulty(7), imodbits_armor],
# ["light_leather", "Ranger Jerkin", [("light_leather", 0)], itp_type_body_armor|itp_covers_legs, 0, 355, weight(5)|abundance(100)|head_armor(0)|body_armor(50)|leg_armor(15)|difficulty(0), imodbits_armor],
["mail_and_plate", "Mail and Plate", [("mail_and_plate",0)], itp_type_body_armor|itp_covers_legs, 0, 2970, weight(27)|abundance(60)|head_armor(6)|body_armor(44)|leg_armor(14)|difficulty(12), imodbits_armor ],
["light_mail_and_plate", "Light Mail and Plate", [("light_mail_and_plate",0)], itp_type_body_armor|itp_covers_legs, 0, 1980, weight(22)|abundance(80)|head_armor(2)|body_armor(36)|leg_armor(8)|difficulty(12), imodbits_armor ],
["knight_armor", "Knight Light Armor", [("mail_and_plate",0)], itp_type_body_armor|itp_covers_legs, 0, 3200, weight(28)|abundance(60)|head_armor(6)|body_armor(46)|leg_armor(14)|difficulty(12), imodbits_armor ],
["knight_armor3", "Knight Light Armor", [("mail_and_plate",0)], itp_type_body_armor|itp_covers_legs, 0, 3220, weight(28)|abundance(60)|head_armor(6)|body_armor(46)|leg_armor(14)|difficulty(12), imodbits_armor ],
["heavy_plate_armor", "Heavy Plate Armor", [("full_plate_armor",0)], itp_type_body_armor|itp_covers_legs, 0, 5265, weight(39)|abundance(40)|head_armor(8)|body_armor(54)|leg_armor(20)|difficulty(15), imodbits_armor ],
["larktin_black_armor", "Lady Larktin's Armor", [("DK_full_plate_2",0)], itp_type_body_armor|itp_unique|itp_civilian|itp_covers_legs, 0, 12500, weight(50)|abundance(0)|head_armor(40)|body_armor(100)|leg_armor(80)|difficulty(20), imodbits_plate ],
["dk_plate_armor", "Dark Plate Armour", [("dark_plate_armor",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 4940, weight(38)|head_armor(8)|body_armor(52)|leg_armor(20)|difficulty(15), imodbits_armor ],
["ssin_low_armour", "Novice Assassin's Armour", [("assassin_low_armour",0)], itp_type_body_armor|itp_covers_legs, 0, 280, weight(7.5)|body_armor(16)|leg_armor(0)|difficulty(0), imodbits_cloth ],
["ssin_med_armour", "Assassin's Armour", [("assassin_med_armour",0)], itp_type_body_armor|itp_covers_legs, 0, 650, weight(10.5)|body_armor(26)|leg_armor(0)|difficulty(6), imodbits_armor ],
["ssin_high_armour", "Royal Assassin's Armour", [("assassin_high_armour",0)], itp_type_body_armor|itp_covers_legs, 0, 1620, weight(18)|head_armor(0)|body_armor(36)|leg_armor(8)|difficulty(10), imodbits_armor ],

	
# Merchandise Headwear

# Armor 0+
# Armor 10+
# ["padded_coif", "Padded Coif", [("padded_coif", 0)], itp_merchandise|itp_type_head_armor, 0, 6, weight(1)|abundance(100)|head_armor(11)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth],
#["leather_steppe_cap_a", "Leather Steppe Cap", [("leather_steppe_cap_a_new", 0)], itp_merchandise|itp_type_head_armor, 0, 24, weight(1)|abundance(100)|head_armor(12)|body_armor(0)|leg_armor(0), imodbits_cloth],
#["leather_steppe_cap_b", "Leather Steppe Cap", [("leather_steppe_cap_a", 0)], itp_merchandise|itp_type_head_armor, 0, 36, weight(1)|abundance(100)|head_armor(14)|body_armor(0)|leg_armor(0), imodbits_cloth],
# ["leather_steppe_cap_c", "Leather Steppe Cap", [("leather_steppe_cap_b", 0)], itp_merchandise|itp_type_head_armor, 0, 51, weight(1)|abundance(100)|head_armor(16)|body_armor(0)|leg_armor(0), imodbits_cloth],
# ["felt_steppe_cap", "Felt Steppe Cap", [("felt_steppe_cap", 0)], itp_merchandise|itp_type_head_armor, 0, 237, weight(2)|abundance(100)|head_armor(16)|body_armor(0)|leg_armor(0), imodbits_cloth],
# Armor 20+
#  ["skullcap", "Skullcap", [("skull_cap_new_a", 0)], itp_merchandise|itp_type_head_armor, 0, 65, weight(1.0)|abundance(100)|head_armor(20)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_plate],
#["mail_coif", "Mail Coif", [("mail_coif", 0)], itp_merchandise|itp_type_head_armor, 0, 75, weight(1.25)|abundance(100)|head_armor(22)|body_armor(0)|leg_armor(0)|difficulty(7), imodbits_armor],
# ["footman_helmet", "Footman's_Helmet", [("skull_cap_new", 0)], itp_merchandise|itp_type_head_armor, 0, 95, weight(1.5)|abundance(100)|head_armor(24)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_plate],
#["nasal_helmet", "Nasal Helmet", [("nasal_helmet_b", 0)], itp_merchandise|itp_type_head_armor, 0, 125, weight(1.25)|abundance(100)|head_armor(26)|body_armor(0)|leg_armor(0)|difficulty(7), imodbits_plate],
# ["norman_helmet", "Helmet with Cap", [("norman_helmet_a", 0)], itp_merchandise|itp_type_head_armor|itp_fit_to_head, 0, 145, weight(1.25)|abundance(100)|head_armor(28)|body_armor(0)|leg_armor(0)|difficulty(7), imodbits_plate],
["footman_helmet_b", "Footman's_Helmet", [("skullcap_coif",0)], itp_type_head_armor|itp_merchandise, 0, 600, weight(4)|abundance(100)|head_armor(30)|body_armor(0)|leg_armor(0)|difficulty(8), imodbits_armor ],
# Armor 30+
#["segmented_helmet", "Segmented Helmet", [("segmented_helm_new", 0)], itp_merchandise|itp_type_head_armor, 0, 175, weight(1.25)|abundance(100)|head_armor(31)|body_armor(0)|leg_armor(0)|difficulty(7), imodbits_plate],
# ["kettle_hat", "Kettle Hat", [("kettle_hat_new", 0)], itp_merchandise|itp_type_head_armor, 0, 195, weight(2)|abundance(100)|head_armor(33)|body_armor(0)|leg_armor(0)|difficulty(7), imodbits_plate],
#["helmet_with_neckguard", "Helmet with Neckguard", [("neckguard_helm_new", 0)], itp_merchandise|itp_type_head_armor, 0, 235, weight(1.75)|abundance(100)|head_armor(35)|body_armor(0)|leg_armor(0)|difficulty(7), imodbits_plate],
# ["spiked_helmet", "Spiked Helmet", [("spiked_helmet_new", 0)], itp_merchandise|itp_type_head_armor, 0, 275, weight(2)|abundance(100)|head_armor(38)|body_armor(0)|leg_armor(0)|difficulty(7), imodbits_plate],
# ["khergit_war_helmet", "Khergit War Helmet", [("tattered_steppe_cap_a_new", 0)], itp_merchandise|itp_type_head_armor, 0, 322, weight(2)|abundance(100)|head_armor(31)|body_armor(0)|leg_armor(0), imodbits_cloth],
# ["khergit_helmet", "Khergit Helmet", [("khergit_guard_helmet", 0)], itp_merchandise|itp_type_head_armor, 0, 361, weight(2)|abundance(100)|head_armor(33)|body_armor(0)|leg_armor(0), imodbits_cloth],
#["khergit_guard_helmet", "Khergit Guard Helmet", [("lamellar_helmet_a", 0)], itp_merchandise|itp_type_head_armor, 0, 433, weight(2)|abundance(100)|head_armor(36)|body_armor(0)|leg_armor(0), imodbits_cloth],
# ["khergit_cavalry_helmet", "Khergit Cavalry Helmet", [("lamellar_helmet_b", 0)], itp_merchandise|itp_type_head_armor, 0, 433, weight(2)|abundance(100)|head_armor(36)|body_armor(0)|leg_armor(0), imodbits_cloth],
["kettle_hat_b", "Light Kettle Hat", [("kettlehat_paddedcoif",0)], itp_type_head_armor|itp_merchandise, 0, 600, weight(4)|abundance(100)|head_armor(30)|body_armor(0)|leg_armor(0)|difficulty(8), imodbits_armor ],
["segmented_helmet_b", "Segmented Helmet", [("segmented_helm_new",0)], itp_type_head_armor|itp_merchandise, 0, 640, weight(4)|abundance(80)|head_armor(32)|body_armor(0)|leg_armor(0)|difficulty(10), imodbits_armor ],
["barbuta2", "Barbuta", [("barbuta2",0)], itp_type_head_armor|itp_merchandise, 0, 600, weight(4)|abundance(100)|head_armor(30)|body_armor(0)|leg_armor(0)|difficulty(8), imodbits_armor ],
# Armor 40+
# ["nordic_helmet", "Nordic Helmet", [("helmet_w_eyeguard_new", 0)], itp_merchandise|itp_type_head_armor, 0, 345, weight(2)|abundance(100)|head_armor(40)|body_armor(0)|leg_armor(0)|difficulty(7), imodbits_plate],
# ["flat_topped_helmet", "Flat Topped Helmet", [("flattop_helmet_new", 0)], itp_merchandise|itp_type_head_armor, 0, 415, weight(2)|abundance(100)|head_armor(42)|body_armor(0)|leg_armor(0)|difficulty(8), imodbits_plate],
# ["bascinet", "Bascinet", [("bascinet_avt_new", 0), ("bascinet_avt_new1", imodbit_crude|imodbit_rusty), ("bascinet_avt_new2", imodbits_good)], itp_merchandise|itp_type_head_armor, 0, 475, weight(2.25)|abundance(100)|head_armor(45)|body_armor(0)|leg_armor(0)|difficulty(8), imodbits_plate],
#  ["guard_helmet", "Guard Helmet", [("reinf_helmet_new", 0)], itp_merchandise|itp_type_head_armor, 0, 555, weight(2.5)|abundance(100)|head_armor(47)|body_armor(0)|leg_armor(0)|difficulty(8), imodbits_plate],
["nordic_helmet_b", "Nordic Helmet", [("helmet_w_eyeguard_new",0)], itp_type_head_armor|itp_merchandise, 0, 765, weight(4.5)|abundance(80)|head_armor(34)|body_armor(0)|leg_armor(0)|difficulty(10), imodbits_armor ],
["vaegirhelmet", "Vaegirhelmet", [("vaegirhelm",0)], itp_type_head_armor|itp_merchandise, 0, 455, weight(3.5)|abundance(120)|head_armor(26)|body_armor(0)|leg_armor(0)|difficulty(8), imodbits_armor ],
# Armor 50+
# ["great_helmet", "Great Helmet", [("great_helmet_new", 0)], itp_merchandise|itp_type_head_armor|itp_covers_head, 0, 985, weight(2.75)|abundance(100)|head_armor(53)|body_armor(0)|leg_armor(0)|difficulty(10), imodbits_plate],
# ["winged_great_helmet", "Winged Great Helmet", [("maciejowski_helmet_new", 0)], itp_merchandise|itp_type_head_armor|itp_covers_head, 0, 1245, weight(2.75)|abundance(100)|head_armor(55)|body_armor(0)|leg_armor(0)|difficulty(10), imodbits_plate],
["sallete", "Sallet Helm", [("sallete_new",0)], itp_type_head_armor|itp_merchandise, 0, 950, weight(5)|abundance(60)|head_armor(38)|difficulty(10), imodbits_armor ],
["sallet", "Sallet Helm", [("sallete_new",0)], itp_type_head_armor|itp_merchandise, 0, 950, weight(5)|abundance(60)|head_armor(38)|difficulty(10), imodbits_armor ],
["tarkhan_helm", "Tarkhan Helmet", [("lamellar_helmet_a",0)], itp_type_head_armor|itp_merchandise, 0, 600, weight(4)|abundance(100)|difficulty(8)|head_armor(30), imodbits_armor ],
["greatbascinet", "Bascinet", [("bascinet_avt_new",0)], itp_type_head_armor|itp_merchandise, 0, 1200, weight(6)|abundance(40)|head_armor(40)|body_armor(0)|leg_armor(0)|difficulty(12), imodbits_armor ],
["klappvisier", "Klapptvisier", [("klappvisier",0)], itp_type_head_armor|itp_merchandise|itp_covers_head, 0, 1200, weight(6)|abundance(40)|head_armor(40)|body_armor(0)|leg_armor(0)|difficulty(12), imodbits_armor ],
# Armor 60+
["salletpigface", "Knight Helm", [("pigface_new",0)], itp_type_head_armor|itp_merchandise|itp_covers_head, 0, 1750, weight(7)|head_armor(50)|difficulty(15), imodbits_armor ],

# Special Headwear

# ["head_wrappings", "head_wrapping", [("head_wrapping", 0)], itp_type_head_armor|itp_fit_to_head, 0, 16, weight(0.25)|head_armor(3), imodbit_tattered|imodbit_ragged|imodbit_sturdy|imodbit_thick],
["black_helmet", "Black Helmet", [("DK_black_helm",0)], itp_type_head_armor, 0, 1750, weight(7)|abundance(20)|head_armor(50)|body_armor(0)|leg_armor(0)|difficulty(15), imodbits_armor ],
#["black_helmet", "Black Helmet", [("black_helm", 0)], itp_type_head_armor, 0, 635, weight(2.75)|abundance(100)|head_armor(60)|body_armor(0)|leg_armor(0)|difficulty(8), imodbits_plate],
# ["strange_helmet", "Strange Helmet", [("samurai_helmet", 0)], itp_type_head_armor, 0, 825, weight(2)|abundance(100)|head_armor(44)|body_armor(0)|leg_armor(0)|difficulty(7), imodbits_plate],
# ["black_hood", "Ranger Hood", [("hood_black", 0)], itp_type_head_armor, 0, 193, weight(2)|abundance(100)|head_armor(35)|body_armor(0)|leg_armor(0), imodbits_cloth],
["face_fe", "Rounded Great Helm", [("talak_sugarloaf_helm",0)], itp_type_head_armor|itp_covers_head, 0, 1430, weight(6.5)|abundance(20)|head_armor(48)|difficulty(12), imodbits_armor ],
["face_helm", "Face Helm", [("talak_sutton_hoo",0)], itp_type_head_armor, 0, 1200, weight(6)|head_armor(40)|difficulty(12), imodbits_armor ],
["crown", "Crown", [("talak_crown_ornate",0)], itp_type_head_armor|itp_covers_head, 0, 2400, weight(8)|head_armor(70)|difficulty(8), imodbits_armor ],
["crown_non_armour", "Royal Crown", [("crown_non_armour",0)], itp_type_head_armor|itp_doesnt_cover_hair, 0, 600, weight(4)|head_armor(20), imodbits_none ],
["black_helmet_b", "Black Helmet", [("Helm_Horned01",0)], itp_type_head_armor, 0, 1750, weight(7)|abundance(20)|head_armor(50)|body_armor(0)|leg_armor(0)|difficulty(15), imodbits_armor ],
["black_helmet_c", "Black Helmet", [("Helm_horned03",0)], itp_type_head_armor, 0, 1750, weight(7)|abundance(20)|head_armor(50)|body_armor(0)|leg_armor(0)|difficulty(15), imodbits_armor ],
["ragnar_helmet", "Ragnar's Helmet", [("VALSGARDE8",0)], itp_type_head_armor|itp_covers_head, 0, 950, weight(5)|abundance(60)|head_armor(38)|body_armor(0)|leg_armor(0)|difficulty(10), imodbits_armor ],
["dark_great_helm", "Dark Great_Helm", [("dark_great_helm_a",0)], itp_type_head_armor, 0, 1950, weight(7.5)|head_armor(52)|difficulty(15), imodbits_armor ],
["dark_winged_helmetB", "Dark Winged Helm", [("dark_winged_helmetB", 0)], itp_type_head_armor, 0, 4505, weight(1.60)|head_armor(62)|difficulty(12), imodbits_armor ],

	
# Merchandise Legwear

# Armor 20+
# ["khergit_guard_boots", "Khergit Guard Boots", [("lamellar_boots_a", 0)], itp_merchandise|itp_type_foot_armor|itp_attach_armature, 0, 254, weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(20)|difficulty(0), imodbits_cloth],
# ["mail_chausses", "Mail Chausses", [("mail_chausses_a", 0)], itp_merchandise|itp_type_foot_armor|itp_attach_armature, 0, 415, weight(3)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(21)|difficulty(0), imodbits_armor],
# ["splinted_leather_greaves", "Splinted Leather Greaves", [("leather_greaves_a", 0)], itp_merchandise|itp_type_foot_armor|itp_attach_armature, 0, 765, weight(3)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(24)|difficulty(0), imodbits_armor],
# ["splinted_greaves", "Splinted Greaves", [("splinted_greaves_a", 0)], itp_merchandise|itp_type_foot_armor|itp_attach_armature, 0, 1153, weight(3.5)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(28)|difficulty(7), imodbits_armor],
# Armor 30+
# ["mail_boots", "Mail Boots", [("mail_boots_a", 0)], itp_merchandise|itp_type_foot_armor|itp_attach_armature, 0, 1746, weight(3)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(31)|difficulty(8), imodbits_armor],
# ["iron_greaves", "Iron Greaves", [("iron_greaves_a", 0)], itp_merchandise|itp_type_foot_armor|itp_attach_armature, 0, 2375, weight(3.5)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(8), imodbits_armor],
# Armor 40+
["tarkhan_boots", "Tarkhan Boots", [("lamellar_boots_a",0)], itp_type_foot_armor|itp_attach_armature|itp_merchandise, 0, 910, weight(7)|difficulty(8)|leg_armor(24)|abundance(50), imodbits_armor ],

# Special Legwear

["black_greaves", "Black Greaves", [("DK_greaves",0)], itp_type_foot_armor|itp_attach_armature, 0, 1710, weight(9)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(38)|difficulty(15), imodbits_armor ],
#  ["black_greaves", "Black Greaves", [("black_greaves", 0)], itp_type_foot_armor|itp_attach_armature, 0, 3565, weight(3.5)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(48)|difficulty(0), imodbits_armor],
# ["strange_boots", "Strange Boots", [("samurai_boots", 0)], itp_type_foot_armor|itp_attach_armature, 0, 466, weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(21)|difficulty(0), imodbits_cloth],
# ["light_leather_boots", "Ranger Boots", [("light_leather_boots", 0)], itp_type_foot_armor|itp_attach_armature, 0, 91, weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(30)|difficulty(0), imodbits_cloth],
["plate_greaves", "Plated Greaves", [("iron_greaves_a",0)], itp_type_foot_armor|itp_attach_armature, 0, 1620, weight(9)|abundance(40)|leg_armor(36)|difficulty(15), imodbits_armor ],
["plate_greaves2", "Plated Greaves", [("iron_greaves_a",0)], itp_type_foot_armor|itp_attach_armature, 0, 1620, weight(9)|abundance(40)|leg_armor(36)|difficulty(15), imodbits_armor ],
["larktin_black_greaves", "Lady Larktin's Greaves", [("WB_DK_plate_boots",0)], itp_type_foot_armor|itp_attach_armature|itp_unique, 0, 2700, weight(9)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(60)|difficulty(15), imodbits_armor ],
["assassin_boots", "Assassin Boots", [("assassin_boots",0)], itp_type_foot_armor|itp_attach_armature|itp_civilian, 0, 240, weight(4)|leg_armor(12), imodbits_cloth ],
["dark_greaves", "Dark Greaves", [("dark_greaves",0)], itp_type_foot_armor|itp_attach_armature|itp_civilian, 0, 1710, weight(9)|leg_armor(38)|difficulty(15), imodbits_armor ],

	
# Merchandise Gauntlets

["leather_gloves", "Leather Gloves", [("leather_gloves_L", 0)], itp_merchandise|itp_type_hand_armor, 0, 130, weight(0.25)|abundance(120)|body_armor(2)|difficulty(0), imodbits_cloth],
["mail_mittens", "Mail Mittens", [("mail_mittens_L",0)], itp_type_hand_armor|itp_merchandise, 0, 200, weight(1)|abundance(120)|body_armor(4)|difficulty(6), imodbits_armor ],
["scale_gauntlets", "Scale Gauntlets", [("new_scale_gaunt_L",0)], itp_type_hand_armor|itp_merchandise, 0, 450, weight(1.5)|abundance(100)|body_armor(6)|difficulty(6), imodbits_armor ],
["gauntlets", "Gauntlets", [("gauntlets_L",0),("gauntlet_b_L",imodbit_reinforced)], itp_type_hand_armor|itp_merchandise, 0, 1250, weight(2.5)|abundance(60)|body_armor(10)|difficulty(10), imodbits_armor ],
["new_scale_gaunt_L", "Vaegir Scale Gauntlets", [("new_scale_gaunt_L",0),("new_scale_gaunt_Lx",imodbits_none)], itp_type_hand_armor|itp_merchandise, 0, 1250, weight(2.5)|abundance(40)|body_armor(10)|difficulty(12), imodbits_armor|imodbit_crude|imodbit_thick|imodbit_hardened|imodbit_reinforced|imodbit_lordly ],

# Special Gauntlets

["plate_gauntlets", "Plated Gauntlets", [("gauntlets_L",0),("gauntlet_b_L",imodbit_reinforced)], itp_type_hand_armor, 0, 2100, weight(3.5)|abundance(20)|body_armor(12)|difficulty(15), imodbits_armor ],
["plate_gauntlets2", "Plated Gauntlets", [("gauntlets_L",0),("gauntlet_b_L",imodbit_reinforced)], itp_type_hand_armor, 0, 2100, weight(3.5)|abundance(20)|body_armor(12)|difficulty(15), imodbits_armor ],
["larktin_gauntlets", "Lady Larktin's Gauntlets", [("DK_gauntlet_a_L", 0), ("gauntlet_b_L", imodbit_reinforced)], itp_type_hand_armor|itp_unique, 0, 5005, weight(1.0)|abundance(20)|body_armor(20)|difficulty(10), imodbits_armor],
["dk_gauntlet_a_L", "Dark Knight Gauntlets", [("DK_gauntlet_a_L",0),("DK_gauntlet_a_Lx",imodbits_none)], itp_type_hand_armor, 0, 2100, weight(3.5)|body_armor(12)|difficulty(15), imodbits_armor|imodbit_rusty|imodbit_battered|imodbit_crude|imodbit_thick|imodbit_reinforced ],
["dark_gauntlet_b_L", "Dark Gauntlets", [("dark_gauntlet_b_L",0),("dark_gauntlet_b_Lx",imodbits_none)], itp_type_hand_armor, 0, 1500, weight(2.5)|body_armor(12)|difficulty(15), imodbits_armor ],
["assassin_glove_L", "Assassin Gloves", [("assassin_glove_L",0),("assassin_glove_L",imodbits_none)], itp_type_hand_armor, 0, 1500, weight(2)|body_armor(6)|difficulty(6), imodbits_cloth|imodbit_tattered|imodbit_ragged|imodbit_thick|imodbit_reinforced ],

# Merchandise Shields

# ["wooden_shield", "Wooden Shield", [("shield_round_a", 0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_round_shield, 42, weight(2)|hit_points(360)|body_armor(1)|spd_rtng(100)|weapon_length(50), imodbits_shield],
#  ["steel_shield", "Steel Shield", [("shield_dragon", 0)], itp_merchandise|itp_type_shield, itcf_carry_round_shield, 697, weight(4)|hit_points(700)|body_armor(17)|spd_rtng(61)|weapon_length(40), imodbits_shield],
# ["shield_heater_c", "Heater Shield", [("shield_heater_c", 0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield, 277, weight(3.5)|hit_points(410)|body_armor(2)|spd_rtng(80)|weapon_length(50), imodbits_shield],
# ["tab_shield_round_a", "Old Round Shield", [("tableau_shield_round_5", 0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_round_shield, 26, weight(2.5)|hit_points(350)|body_armor(0)|spd_rtng(93)|weapon_length(50), imodbits_shield,
# [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"), (store_trigger_param_2, ":troop_no"), (call_script, "script_shield_item_set_banner", "tableau_round_shield_5", ":agent_no", ":troop_no")])]],
# ["tab_shield_round_b", "Plain Round Shield", [("tableau_shield_round_3", 0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_round_shield, 65, weight(3)|hit_points(460)|body_armor(2)|spd_rtng(90)|weapon_length(50), imodbits_shield,
# [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"), (store_trigger_param_2, ":troop_no"), (call_script, "script_shield_item_set_banner", "tableau_round_shield_3", ":agent_no", ":troop_no")])]],
# ["tab_shield_round_c", "Round_Shield", [("tableau_shield_round_2", 0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_round_shield, 105, weight(3.5)|hit_points(540)|body_armor(4)|spd_rtng(87)|weapon_length(50), imodbits_shield,
# [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"), (store_trigger_param_2, ":troop_no"), (call_script, "script_shield_item_set_banner", "tableau_round_shield_2", ":agent_no", ":troop_no")])]],
# ["tab_shield_round_d", "Heavy Round_Shield", [("tableau_shield_round_1", 0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_round_shield, 210, weight(4)|hit_points(600)|body_armor(6)|spd_rtng(84)|weapon_length(50), imodbits_shield,
# [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"), (store_trigger_param_2, ":troop_no"), (call_script, "script_shield_item_set_banner", "tableau_round_shield_1", ":agent_no", ":troop_no")])]],
# ["tab_shield_round_e", "Huskarl's Round_Shield", [("tableau_shield_round_4", 0)], itp_merchandise|itp_type_shield, itcf_carry_round_shield, 460, weight(4.5)|hit_points(700)|body_armor(8)|spd_rtng(81)|weapon_length(65), imodbits_shield,
# [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"), (store_trigger_param_2, ":troop_no"), (call_script, "script_shield_item_set_banner", "tableau_round_shield_4", ":agent_no", ":troop_no")])]],
# ["tab_shield_kite_a", "Old Kite Shield", [("tableau_shield_kite_1", 0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield, 33, weight(2)|hit_points(285)|body_armor(0)|spd_rtng(96)|weapon_length(60), imodbits_shield,
# [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"), (store_trigger_param_2, ":troop_no"), (call_script, "script_shield_item_set_banner", "tableau_kite_shield_1", ":agent_no", ":troop_no")])]],
# ["tab_shield_kite_b", "Plain Kite Shield", [("tableau_shield_kite_3", 0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield, 70, weight(2.5)|hit_points(365)|body_armor(2)|spd_rtng(93)|weapon_length(60), imodbits_shield,
# [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"), (store_trigger_param_2, ":troop_no"), (call_script, "script_shield_item_set_banner", "tableau_kite_shield_3", ":agent_no", ":troop_no")])]],
# ["tab_shield_kite_c", "Kite Shield", [("tableau_shield_kite_2", 0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield, 156, weight(3)|hit_points(435)|body_armor(5)|spd_rtng(90)|weapon_length(60), imodbits_shield,
# [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"), (store_trigger_param_2, ":troop_no"), (call_script, "script_shield_item_set_banner", "tableau_kite_shield_2", ":agent_no", ":troop_no")])]],
# ["tab_shield_kite_d", "Heavy Kite Shield", [("tableau_shield_kite_2", 0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield, 320, weight(3.5)|hit_points(515)|body_armor(8)|spd_rtng(87)|weapon_length(60), imodbits_shield,
# [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"), (store_trigger_param_2, ":troop_no"), (call_script, "script_shield_item_set_banner", "tableau_kite_shield_2", ":agent_no", ":troop_no")])]],
# ["tab_shield_kite_cav_a", "Horseman's Kite Shield", [("tableau_shield_kite_4", 0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield, 205, weight(2)|hit_points(310)|body_armor(10)|spd_rtng(103)|weapon_length(40), imodbits_shield,
# [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"), (store_trigger_param_2, ":troop_no"), (call_script, "script_shield_item_set_banner", "tableau_kite_shield_4", ":agent_no", ":troop_no")])]],
# ["tab_shield_kite_cav_b", "Knightly Kite Shield", [("tableau_shield_kite_4", 0)], itp_merchandise|itp_type_shield, itcf_carry_kite_shield, 360, weight(2.5)|hit_points(370)|body_armor(16)|spd_rtng(100)|weapon_length(40), imodbits_shield,
# [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"), (store_trigger_param_2, ":troop_no"), (call_script, "script_shield_item_set_banner", "tableau_kite_shield_4", ":agent_no", ":troop_no")])]],
# ["tab_shield_heater_a", "Old Heater Shield", [("tableau_shield_heater_1", 0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield, 36, weight(2)|hit_points(280)|body_armor(1)|spd_rtng(96)|weapon_length(60), imodbits_shield,
# [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"), (store_trigger_param_2, ":troop_no"), (call_script, "script_shield_item_set_banner", "tableau_heater_shield_1", ":agent_no", ":troop_no")])]],
# ["tab_shield_heater_b", "Plain Heater Shield", [("tableau_shield_heater_1", 0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield, 74, weight(2.5)|hit_points(360)|body_armor(3)|spd_rtng(93)|weapon_length(60), imodbits_shield,
# [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"), (store_trigger_param_2, ":troop_no"), (call_script, "script_shield_item_set_banner", "tableau_heater_shield_1", ":agent_no", ":troop_no")])]],
# ["tab_shield_heater_c", "Heater Shield", [("tableau_shield_heater_1", 0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield, 160, weight(3)|hit_points(430)|body_armor(6)|spd_rtng(90)|weapon_length(60), imodbits_shield,
# [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"), (store_trigger_param_2, ":troop_no"), (call_script, "script_shield_item_set_banner", "tableau_heater_shield_1", ":agent_no", ":troop_no")])]],
# ["tab_shield_heater_d", "Heavy Heater Shield", [("tableau_shield_heater_1", 0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield, 332, weight(3.5)|hit_points(510)|body_armor(9)|spd_rtng(87)|weapon_length(60), imodbits_shield,
# [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"), (store_trigger_param_2, ":troop_no"), (call_script, "script_shield_item_set_banner", "tableau_heater_shield_1", ":agent_no", ":troop_no")])]],
# ["tab_shield_heater_cav_a", "Horseman's Heater Shield", [("tableau_shield_heater_2", 0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield, 229, weight(2)|hit_points(300)|body_armor(12)|spd_rtng(103)|weapon_length(40), imodbits_shield,
# [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"), (store_trigger_param_2, ":troop_no"), (call_script, "script_shield_item_set_banner", "tableau_heater_shield_2", ":agent_no", ":troop_no")])]],
# ["tab_shield_heater_cav_b", "Knightly Heater Shield", [("tableau_shield_heater_2", 0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield, 390, weight(2.5)|hit_points(360)|body_armor(18)|spd_rtng(100)|weapon_length(40), imodbits_shield,
# [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"), (store_trigger_param_2, ":troop_no"), (call_script, "script_shield_item_set_banner", "tableau_heater_shield_2", ":agent_no", ":troop_no")])]],
# ["tab_shield_pavise_a", "Old Board Shield", [("tableau_shield_pavise_2", 0)], itp_merchandise|itp_type_shield|itp_cant_use_on_horseback|itp_wooden_parry, itcf_carry_board_shield, 60, weight(3.5)|hit_points(510)|body_armor(0)|spd_rtng(89)|weapon_length(84), imodbits_shield,
# [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"), (store_trigger_param_2, ":troop_no"), (call_script, "script_shield_item_set_banner", "tableau_pavise_shield_2", ":agent_no", ":troop_no")])]],
# ["tab_shield_pavise_b", "Plain Board Shield", [("tableau_shield_pavise_2", 0)], itp_merchandise|itp_type_shield|itp_cant_use_on_horseback|itp_wooden_parry, itcf_carry_board_shield, 114, weight(4)|hit_points(640)|body_armor(1)|spd_rtng(85)|weapon_length(84), imodbits_shield,
# [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"), (store_trigger_param_2, ":troop_no"), (call_script, "script_shield_item_set_banner", "tableau_pavise_shield_2", ":agent_no", ":troop_no")])]],
# ["tab_shield_pavise_c", "Board Shield", [("tableau_shield_pavise_1", 0)], itp_merchandise|itp_type_shield|itp_cant_use_on_horseback|itp_wooden_parry, itcf_carry_board_shield, 210, weight(4.5)|hit_points(760)|body_armor(2)|spd_rtng(81)|weapon_length(84), imodbits_shield,
# [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"), (store_trigger_param_2, ":troop_no"), (call_script, "script_shield_item_set_banner", "tableau_pavise_shield_1", ":agent_no", ":troop_no")])]],
# ["tab_shield_pavise_d", "Heavy Board Shield", [("tableau_shield_pavise_1", 0)], itp_merchandise|itp_type_shield|itp_cant_use_on_horseback|itp_wooden_parry, itcf_carry_board_shield, 370, weight(5)|hit_points(980)|body_armor(3)|spd_rtng(78)|weapon_length(84), imodbits_shield,
# [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"), (store_trigger_param_2, ":troop_no"), (call_script, "script_shield_item_set_banner", "tableau_pavise_shield_1", ":agent_no", ":troop_no")])]],
# ["tab_shield_small_round_a", "Plain Cavalry Shield", [("tableau_shield_small_round_3", 0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_round_shield, 96, weight(2)|hit_points(310)|body_armor(3)|spd_rtng(105)|weapon_length(40), imodbits_shield,
# [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"), (store_trigger_param_2, ":troop_no"), (call_script, "script_shield_item_set_banner", "tableau_small_round_shield_3", ":agent_no", ":troop_no")])]],
# ["tab_shield_small_round_b", "Round Cavalry Shield", [("tableau_shield_small_round_1", 0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_round_shield, 195, weight(2.5)|hit_points(370)|body_armor(9)|spd_rtng(103)|weapon_length(40), imodbits_shield,
# [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"), (store_trigger_param_2, ":troop_no"), (call_script, "script_shield_item_set_banner", "tableau_small_round_shield_1", ":agent_no", ":troop_no")])]],
# ["tab_shield_small_round_c", "Elite Cavalry Shield", [("tableau_shield_small_round_2", 0)], itp_merchandise|itp_type_shield, itcf_carry_round_shield, 370, weight(3)|hit_points(420)|body_armor(14)|spd_rtng(100)|weapon_length(40), imodbits_shield,
# [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"), (store_trigger_param_2, ":troop_no"), (call_script, "script_shield_item_set_banner", "tableau_small_round_shield_2", ":agent_no", ":troop_no")])]],
["larktin_shield", "Lady Larktin's Shield", [("tableau_shield_heater_1", 0)], itp_type_shield|itp_wooden_parry|itp_unique, itcf_carry_kite_shield, 332, weight(3.5)|hit_points(8400)|body_armor(15)|spd_rtng(112)|shield_width(60), imodbits_shield,    [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"), (store_trigger_param_2, ":troop_no"), (call_script, "script_shield_item_set_banner", "tableau_heater_shield_1", ":agent_no", ":troop_no")])]],
["nordic_shield", "Nordic Shield", [("ad_viking_shield_round_15",0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_round_shield, 80, weight(2.5)|hit_points(400)|body_armor(2)|spd_rtng(120)|shield_width(30), imodbits_shield ],
["nordic_shield_b", "Nordic Shield", [("ad_viking_shield_round_14",0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_round_shield, 80, weight(2.5)|hit_points(400)|body_armor(2)|spd_rtng(120)|shield_width(30), imodbits_shield ],
["nordic_shield_c", "Nordic Shield", [("ad_viking_shield_round_13",0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_round_shield, 80, weight(2.5)|hit_points(400)|body_armor(2)|spd_rtng(120)|shield_width(30), imodbits_shield ],
["nordic_shield_d", "Nordic Shield", [("ad_viking_shield_round_12",0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_round_shield, 80, weight(2.5)|hit_points(400)|body_armor(2)|spd_rtng(120)|shield_width(30), imodbits_shield ],
["nordic_shield_e", "Nordic Shield", [("ad_viking_shield_round_10",0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_round_shield, 80, weight(2.5)|hit_points(400)|body_armor(2)|spd_rtng(120)|shield_width(30), imodbits_shield ],
["nordic_shield_f", "Nordic Shield", [("ad_viking_shield_round_02",0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_round_shield, 80, weight(2.5)|hit_points(400)|body_armor(2)|spd_rtng(120)|shield_width(30), imodbits_shield ],

# Special Shields

# ["fur_covered_shield", "Fur Covered Shield", [("shield_kite_m", 0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield, 227, weight(3.5)|hit_points(600)|body_armor(1)|spd_rtng(76)|weapon_length(81), imodbits_shield],
# ["leather_covered_round_shield", "Leather_Covered_Round_Shield", [("shield_round_d", 0)], itp_type_shield|itp_wooden_parry, itcf_carry_round_shield, 80, weight(2.5)|hit_points(310)|body_armor(8)|spd_rtng(96)|weapon_length(40), imodbits_shield],
# ["hide_covered_round_shield", "Hide_Covered_Round_Shield", [("shield_round_f", 0)], itp_type_shield|itp_wooden_parry, itcf_carry_round_shield, 40, weight(2)|hit_points(260)|body_armor(3)|spd_rtng(100)|weapon_length(40), imodbits_shield],
# ["hide_covered_round_shield", "Hide_Covered_Round_Shield", [("shield_round_f", 0)], itp_type_shield|itp_wooden_parry, itcf_carry_round_shield, 40, weight(2)|hit_points(260)|body_armor(3)|spd_rtng(100)|weapon_length(40), imodbits_shield],
# ["norman_shield_1", "Kite Shield", [("norman_shield_1", 0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield, 118, weight(2.5)|hit_points(480)|body_armor(1)|spd_rtng(82)|weapon_length(90), imodbits_shield],
# ["norman_shield_2", "Kite Shield", [("norman_shield_2", 0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield, 118, weight(2.5)|hit_points(480)|body_armor(1)|spd_rtng(82)|weapon_length(90), imodbits_shield],
# ["norman_shield_3", "Kite Shield", [("norman_shield_3", 0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield, 118, weight(2.5)|hit_points(480)|body_armor(1)|spd_rtng(82)|weapon_length(90), imodbits_shield],
# ["norman_shield_4", "Kite Shield", [("norman_shield_4", 0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield, 118, weight(2.5)|hit_points(480)|body_armor(1)|spd_rtng(82)|weapon_length(90), imodbits_shield],
# ["norman_shield_5", "Kite Shield", [("norman_shield_5", 0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield, 118, weight(2.5)|hit_points(480)|body_armor(1)|spd_rtng(82)|weapon_length(90), imodbits_shield],
# ["norman_shield_6", "Kite Shield", [("norman_shield_6", 0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield, 118, weight(2.5)|hit_points(480)|body_armor(1)|spd_rtng(82)|weapon_length(90), imodbits_shield],
# ["norman_shield_7", "Kite Shield", [("norman_shield_7", 0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield, 118, weight(2.5)|hit_points(480)|body_armor(1)|spd_rtng(82)|weapon_length(90), imodbits_shield],
# ["norman_shield_8", "Kite Shield", [("norman_shield_8", 0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield, 118, weight(2.5)|hit_points(480)|body_armor(1)|spd_rtng(82)|weapon_length(90), imodbits_shield],
["plated32_shield", "Knight Heater Shield", [("tableau_shield_heater_2",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield, 720, weight(10)|difficulty(4)|hit_points(580)|body_armor(20)|spd_rtng(70)|shield_width(35)|shield_height(75), imodbits_shield, [(ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_shield_item_set_banner","tableau_heater_shield_2",":agent_no",":troop_no")])] ],
["tarkhan_shield", "Tarkhan Shield", [("newshieldf",0)], itp_type_shield|itp_merchandise, itcf_carry_round_shield, 360, weight(4)|hit_points(620)|body_armor(6)|spd_rtng(80)|shield_width(40), imodbits_shield ],
#["royal_kite", "Royal Kite Shield", [("Rivendell_long_shield",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield, 480, weight(8)|hit_points(700)|body_armor(12)|spd_rtng(70)|shield_width(35)|shield_height(60), imodbits_shield ],
["royal_kite", "Royal Kite Shield", [("shield_kite_o",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield, 480, weight(8)|hit_points(700)|body_armor(12)|spd_rtng(70)|shield_width(35)|shield_height(60), imodbits_shield ],
# BUG: not set as itp_type_shield! FIXED.
# ["plate_covered_round_shield", "Plate_Covered_Round_Shield", [("shield_round_e", 0)], itp_type_shield, itcf_carry_round_shield, 140, weight(4)|hit_points(330)|body_armor(16)|spd_rtng(90)|weapon_length(40), imodbits_shield],

################################################################################
# NATIVE EXPANSION COMMENTS
# Melee Weapons
################################################################################

# Improvised Melee Weapons

#["wooden_stick", "Wooden Stick", [("wooden_stick", 0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_wooden_parry|itp_wooden_attack, itc_scimitar, 5, weight(2.5)|difficulty(0)|spd_rtng(99)|weapon_length(90)|swing_damage(13, blunt)|thrust_damage(0, pierce), imodbits_none],
# ["cudgel", "Cudgel", [("club", 0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_wooden_parry|itp_wooden_attack, itc_scimitar, 5, weight(2.5)|difficulty(0)|spd_rtng(99)|weapon_length(90)|swing_damage(13, blunt)|thrust_damage(0, pierce), imodbits_none],
# ["club", "Club", [("club", 0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_wooden_parry|itp_wooden_attack, itc_scimitar, 15, weight(2.5)|difficulty(0)|spd_rtng(95)|weapon_length(95)|swing_damage(15, blunt)|thrust_damage(0, pierce), imodbits_none],
#  ["sickle", "Sickle", [("sickle", 0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_secondary|itp_no_parry|itp_wooden_parry, itc_cleaver, 1, weight(1.5)|difficulty(0)|spd_rtng(99)|weapon_length(40)|swing_damage(20, cut)|thrust_damage(0, pierce), imodbits_none],
# ["cleaver", "Cleaver", [("cleaver", 0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_secondary|itp_no_parry|itp_wooden_parry, itc_cleaver, 3, weight(1.5)|difficulty(0)|spd_rtng(103)|weapon_length(30)|swing_damage(24, cut)|thrust_damage(0, pierce), imodbits_none],
# ["knife", "Knife", [("peasant_knife_new", 0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_secondary|itp_no_parry, itc_dagger|itcf_carry_dagger_front_left, 4, weight(0.5)|difficulty(0)|spd_rtng(110)|weapon_length(40)|swing_damage(21, cut)|thrust_damage(13, pierce), imodbits_sword],
["knife1", "Cheese Knife", [("peasant_knife_new", 0)], itp_type_one_handed_wpn|itp_primary|itp_secondary|itp_no_parry, itc_dagger|itcf_carry_dagger_front_left, 4, weight(0.5)|difficulty(0)|abundance(1000)|spd_rtng(110)|weapon_length(40)|swing_damage(21, cut)|thrust_damage(13, pierce), imodbits_sword],
#["butchering_knife", "Butchering Knife", [("khyber_knife", 0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_secondary|itp_no_parry, itc_dagger|itcf_carry_dagger_front_right, 13, weight(0.75)|difficulty(0)|spd_rtng(108)|weapon_length(60)|swing_damage(24, cut)|thrust_damage(17, pierce), imodbits_sword],
#  ["hammer", "Hammer", [("iron_hammer", 0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_wooden_parry, itc_scimitar, 5, weight(2)|difficulty(0)|spd_rtng(100)|weapon_length(55)|swing_damage(14, blunt)|thrust_damage(0, pierce), imodbits_mace],
# ["hatchet", "Hatchet", [("hatchet", 0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_secondary|itp_bonus_against_shield|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip, 5, weight(2)|difficulty(0)|spd_rtng(97)|weapon_length(60)|swing_damage(23, cut)|thrust_damage(0, pierce), imodbits_axe],
# ["pitch_fork", "Pitch Fork", [("pitch_fork", 0)], itp_type_polearm|itp_merchandise|itp_type_polearm|itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_spear, 19, weight(3.5)|difficulty(0)|spd_rtng(83)|weapon_length(154)|swing_damage(0, blunt)|thrust_damage(18, pierce), imodbits_polearm],
["broken_bottle", "Broken Bottle", [("broken_bottle", 0)], itp_type_one_handed_wpn|itp_no_parry|itp_secondary|itp_primary, itcf_carry_dagger_front_left|itc_dagger, 1, weight(0.50)|spd_rtng(120)|weapon_length(20)|thrust_damage(10, pierce)|swing_damage(28, cut), imodbits_none],

# One-Handed Swords

# ["dagger", "Dagger", [("dagger", 0), ("scab_dagger", ixmesh_carry), ("dagger_b", imodbits_good), ("dagger_b_scabbard", ixmesh_carry|imodbits_good)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_secondary|itp_no_parry, itc_dagger|itcf_carry_dagger_front_left|itcf_show_holster_when_drawn, 17, weight(0.75)|difficulty(0)|spd_rtng(112)|weapon_length(47)|swing_damage(22, cut)|thrust_damage(19, pierce), imodbits_sword_high],
# ["falchion", "Falchion", [("falchion", 0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_scimitar|itcf_carry_sword_left_hip, 104, weight(2.5)|difficulty(8)|spd_rtng(96)|weapon_length(73)|swing_damage(30, cut)|thrust_damage(0, pierce), imodbits_sword],
# ["scimitar", "Scimitar", [("scimeter", 0), ("scab_scimeter", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_scimitar|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 108, weight(1.5)|difficulty(0)|spd_rtng(105)|weapon_length(97)|swing_damage(29, cut)|thrust_damage(0, pierce), imodbits_sword_high],
# ["sword_medieval_a", "Sword", [("sword_medieval_a", 0), ("sword_medieval_a_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 163, weight(1.5)|difficulty(0)|spd_rtng(99)|weapon_length(95)|swing_damage(27, cut)|thrust_damage(22, pierce), imodbits_sword_high],
# ["sword_medieval_b", "Mercenary Sword", [("sword_medieval_b", 0), ("sword_medieval_b_scabbard", ixmesh_carry), ("sword_rusty_a", imodbit_rusty), ("sword_rusty_a_scabbard", ixmesh_carry|imodbit_rusty)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 243, weight(1.5)|difficulty(0)|spd_rtng(99)|weapon_length(95)|swing_damage(28, cut)|thrust_damage(23, pierce), imodbits_sword_high],
# ["sword_medieval_b_small", "Short Sword", [("sword_medieval_b_small", 0), ("sword_medieval_b_small_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 152, weight(1.5)|difficulty(0)|spd_rtng(102)|weapon_length(85)|swing_damage(26, cut)|thrust_damage(24, pierce), imodbits_sword_high],
# ["sword_medieval_c", "Arming Sword", [("sword_medieval_c", 0), ("sword_medieval_c_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 410, weight(1.5)|difficulty(0)|spd_rtng(99)|weapon_length(95)|swing_damage(29, cut)|thrust_damage(24, pierce), imodbits_sword_high],
# ["sword_medieval_c_small", "Short Arming Sword", [("sword_medieval_c_small", 0), ("sword_medieval_c_small_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 243, weight(1.5)|difficulty(0)|spd_rtng(103)|weapon_length(86)|swing_damage(26, cut)|thrust_damage(24, pierce), imodbits_sword_high],
# ["sword_viking_1", "Nord Sword", [("rrr_clontarf_steel.1", 0), ("sword_viking_b_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 147, weight(1.5)|difficulty(0)|spd_rtng(99)|weapon_length(94)|swing_damage(28, cut)|thrust_damage(20, pierce), imodbits_sword_high],
# ["sword_viking_2", "Nord Raider Sword", [("rrr_hersir_steel.1", 0), ("sword_viking_b_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 276, weight(1.5)|difficulty(0)|spd_rtng(99)|weapon_length(95)|swing_damage(29, cut)|thrust_damage(21, pierce), imodbits_sword_high],
# ["sword_viking_2_small", "Nord Short Sword", [("rrr_gotland_steel.1", 0), ("sword_viking_b_small_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 162, weight(1.25)|difficulty(0)|spd_rtng(103)|weapon_length(85)|swing_damage(28, cut)|thrust_damage(21, pierce), imodbits_sword_high],
# ["sword_viking_3", "Nord Warrior Sword", [("rrr_huskarl_steel.1", 0), ("sword_viking_b_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 400, weight(1.5)|difficulty(0)|spd_rtng(99)|weapon_length(99)|swing_damage(33, cut)|thrust_damage(21, pierce), imodbits_sword_high],
# ["sword_viking_3_small", "Nord Small Sword", [("sword_viking_a_small", 0), ("sword_viking_a_small_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 280, weight(1.25)|difficulty(0)|spd_rtng(103)|weapon_length(86)|swing_damage(29, cut)|thrust_damage(21, pierce), imodbits_sword_high],
["nord_sword", "Nord Sword", [("nord_sword", 0), ("sword_viking_c_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itcf_carry_sword_left_hip|itc_longsword, 1470, weight(1.50)|abundance(40)|spd_rtng(99)|weapon_length(80)|thrust_damage(25, pierce)|swing_damage(27, cut), imodbit_tempered|imodbit_chipped|imodbit_masterwork|imodbit_crude|imodbit_balanced],
# ["sword_khergit_1", "Nomad Sabre", [("khergit_sword_b", 0), ("khergit_sword_b_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_scimitar|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 104, weight(1.25)|difficulty(0)|spd_rtng(100)|weapon_length(97)|swing_damage(29, cut), imodbits_sword_high],
# ["sword_khergit_2", "Sabre", [("khergit_sword_c", 0), ("khergit_sword_c_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_scimitar|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 191, weight(1.5)|difficulty(0)|spd_rtng(99)|weapon_length(97)|swing_damage(30, cut), imodbits_sword_high],
# ["sword_khergit_3", "Khergit Sabre", [("khergit_sword_a", 0), ("khergit_sword_a_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_scimitar|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 294, weight(1.5)|difficulty(0)|spd_rtng(99)|weapon_length(98)|swing_damage(31, cut), imodbits_sword_high],
# ["sword_khergit_4", "Heavy Sabre", [("khergit_sword_d", 0), ("khergit_sword_d_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_scimitar|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 384, weight(1.75)|difficulty(0)|spd_rtng(96)|weapon_length(96)|swing_damage(33, cut), imodbits_sword_high],
# ["strange_short_sword", "Strange Short Sword", [("wakizashi", 0), ("wakizashi_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_primary, itc_longsword|itcf_carry_wakizashi|itcf_show_holster_when_drawn, 321, weight(1.25)|difficulty(0)|spd_rtng(108)|weapon_length(65)|swing_damage(25, cut)|thrust_damage(19, pierce), imodbits_sword],
["machete", "Heavy Cleaver", [("truhs", 0)], itp_primary|itp_type_one_handed_wpn, itc_scimitar|itcf_carry_sword_left_hip, 1500, weight(2.75)|abundance(20)|difficulty(15)|hit_points(72000)|spd_rtng(120)|weapon_length(120)|swing_damage(60, cut), imodbits_sword_high],
["estoc", "Estoc", [("talak_estoc", 0), ("talak_scab_estoc", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itcf_thrust_onehanded|itcf_horseback_thrust_onehanded|itcf_carry_sword_left_hip|itc_parry_onehanded, 2500, weight(2.75)|abundance(5)|difficulty(15)|hit_points(72000)|spd_rtng(120)|weapon_length(120)|thrust_damage(40, pierce), imodbits_sword_high],

# Resource Patch: replacing model with vanilla Warband one:
["brokensworda", "Golden Company Short Sword", [("sword_medieval_c_long", 0)], itp_type_one_handed_wpn|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_horseback_thrust_onehanded, 1500, weight(1.75)|difficulty(10)|abundance(0)|hit_points(22000)|spd_rtng(133)|weapon_length(100)|thrust_damage(25, pierce)|swing_damage(25, cut), imodbits_sword_high],
# Resource Patch end.

["brokenswordb", "Broken Sword", [("broken_sword", 0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip, 1000, weight(1.75)|abundance(50)|difficulty(10)|hit_points(22000)|spd_rtng(110)|weapon_length(72)|thrust_damage(11, pierce)|swing_damage(5, cut), imodbits_sword_high],
["longsword_gc", "Golden Company Longsword", [("bastard_sword2", 0)], itp_type_one_handed_wpn|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_horseback_thrust_onehanded, 5000, weight(1.75)|difficulty(10)|abundance(0)|hit_points(22000)|spd_rtng(100)|weapon_length(130)|thrust_damage(27, pierce)|swing_damage(27, cut), imodbits_sword_high],
["rapier_gc", "Golden Company Rapier", [("talak_foil", 0), ("talak_scab_foil", ixmesh_carry)], itp_type_one_handed_wpn|itp_primary, itc_longsword, 6000, weight(1.75)|difficulty(10)|abundance(0)|hit_points(22000)|spd_rtng(150)|weapon_length(122)|thrust_damage(30, pierce)|swing_damage(15, cut), imodbits_sword_high],
["seax_knife", "Seax Knife", [("talak_seax", 0), ("talak_scab_seax", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_secondary, itcf_carry_quiver_front_right|itcf_show_holster_when_drawn|itc_dagger, 400, weight(0.75)|abundance(120)|difficulty(0)|spd_rtng(136)|weapon_length(70)|swing_damage(40, cut)|thrust_damage(28, pierce), imodbits_sword_high],
["kopis", "Kopis", [("kopisfaradon", 0), ("khergit_sword_d_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_scimitar|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 384, weight(1.75)|difficulty(0)|spd_rtng(96)|weapon_length(96)|swing_damage(33, cut), imodbits_sword_high],
["sword_nordic_long", "Huskarl Sword", [("talak_nordic_sword", 0), ("talak_scab_nordic_sword", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 360, weight(2.5)|difficulty(0)|spd_rtng(97)|weapon_length(105)|swing_damage(36, cut)|thrust_damage(23, pierce), imodbits_sword],
["vara_sword", "Varangian Sword", [("dan_spatha", 0), ("dan_spatha_scab", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 263, weight(2.5)|difficulty(0)|spd_rtng(93)|weapon_length(100)|swing_damage(32, cut)|thrust_damage(19, pierce), imodbits_sword],

# Two-Handed Swords

# ["great_sword", "Heavy Military Sword", [("b_bastard_sword", 0), ("scab_bastardsw_b", ixmesh_carry)], itp_type_two_handed_wpn|itp_merchandise|itp_two_handed|itp_primary, itc_greatsword|itcf_carry_sword_back|itcf_show_holster_when_drawn, 423, weight(2.75)|difficulty(10)|spd_rtng(95)|weapon_length(125)|swing_damage(39, cut)|thrust_damage(31, pierce), imodbits_sword_high],
# ["sword_of_war", "Sword of War", [("b_bastard_sword", 0), ("scab_bastardsw_b", ixmesh_carry)], itp_type_two_handed_wpn|itp_merchandise|itp_two_handed|itp_primary, itc_greatsword|itcf_carry_sword_back|itcf_show_holster_when_drawn, 524, weight(3)|difficulty(11)|spd_rtng(93)|weapon_length(130)|swing_damage(40, cut)|thrust_damage(31, pierce), imodbits_sword_high],
#  ["sword_two_handed_b", "Two Handed Sword", [("sword_two_handed_b", 0)], itp_type_two_handed_wpn|itp_merchandise|itp_two_handed|itp_primary, itc_greatsword|itcf_carry_sword_back, 670, weight(2.75)|difficulty(10)|spd_rtng(93)|weapon_length(110)|swing_damage(40, cut)|thrust_damage(27, pierce), imodbits_sword_high],
# ["sword_two_handed_a", "Great Sword", [("sword_two_handed_a", 0)], itp_type_two_handed_wpn|itp_merchandise|itp_two_handed|itp_primary, itc_greatsword|itcf_carry_sword_back, 1123, weight(2.75)|difficulty(10)|spd_rtng(89)|weapon_length(120)|swing_damage(42, cut)|thrust_damage(28, pierce), imodbits_sword_high],
["bastard_sword_a", "Bastard Sword", [("bastard_sword_a", 0), ("bastard_sword_a_scabbard", ixmesh_carry)], itp_type_two_handed_wpn|itp_merchandise|itp_primary, itc_bastardsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 294, weight(2.25)|difficulty(8)|spd_rtng(98)|weapon_length(101)|swing_damage(37, cut)|thrust_damage(26, pierce), imodbits_sword_high],
["bastard_sword_b", "Heavy Bastard Sword", [("bastard_sword_b", 0), ("bastard_sword_b_scabbard", ixmesh_carry)], itp_type_two_handed_wpn|itp_merchandise|itp_primary, itc_bastardsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 526, weight(2.25)|difficulty(8)|spd_rtng(96)|weapon_length(105)|swing_damage(37, cut)|thrust_damage(28, pierce), imodbits_sword_high],
#  ["strange_sword", "Strange Sword", [("katana", 0), ("katana_scabbard", ixmesh_carry)], itp_type_two_handed_wpn|itp_primary, itc_bastardsword|itcf_carry_katana|itcf_show_holster_when_drawn, 679, weight(2.0)|difficulty(8)|spd_rtng(108)|weapon_length(95)|swing_damage(32, cut)|thrust_damage(18, pierce), imodbits_sword],
#  ["strange_great_sword", "Strange Great Sword", [("no_dachi", 0), ("no_dachi_scabbard", ixmesh_carry)], itp_type_two_handed_wpn|itp_two_handed|itp_primary, itc_nodachi|itcf_carry_sword_back|itcf_show_holster_when_drawn, 925, weight(3.5)|difficulty(11)|spd_rtng(92)|weapon_length(125)|swing_damage(38, cut)|thrust_damage(0, pierce), imodbits_axe],
["longsword", "Long Sword", [("talak_bastard_sword", 0), ("talak_scab_bastard_sword", ixmesh_carry)], itp_primary|itp_type_two_handed_wpn, itc_dagger|itc_greatsword|itcf_carry_sword_back|itcf_show_holster_when_drawn, 7000, weight(2.75)|abundance(5)|difficulty(15)|hit_points(72000)|spd_rtng(120)|weapon_length(120)|swing_damage(49, cut)|thrust_damage(38, pierce), imodbits_sword_high],
["sword_long", "Hand and a Half Sword", [("anduril", 0)], itp_primary|itp_type_two_handed_wpn, itc_dagger|itc_greatsword|itcf_carry_sword_back, 7000, weight(2.75)|abundance(5)|difficulty(15)|hit_points(72000)|spd_rtng(120)|weapon_length(110)|swing_damage(52, cut)|thrust_damage(30, pierce), imodbits_sword_high],
["longsword_claymore", "Claymore", [("trscs", 0), ("trscsscab", ixmesh_carry)], itp_primary|itp_type_two_handed_wpn, itc_dagger|itc_greatsword|itcf_show_holster_when_drawn|itcf_carry_sword_left_hip, 2000, weight(2.75)|abundance(20)|difficulty(15)|hit_points(72000)|spd_rtng(120)|weapon_length(120)|swing_damage(56, cut)|thrust_damage(32, pierce), imodbits_sword_high],
#["dao", "Dao", [("subaodao", 0)], itp_primary|itp_type_two_handed_wpn, itc_dagger|itc_greatsword|itcf_carry_sword_left_hip, 2000, weight(2.75)|abundance(20)|difficulty(15)|hit_points(72000)|spd_rtng(120)|weapon_length(120)|swing_damage(50, cut)|thrust_damage(25, pierce), imodbits_sword_high],
["dao", "Dao", [("greatfalchion", 0),("greatfalchion_scabbard", ixmesh_carry)], itp_primary|itp_type_two_handed_wpn, itc_dagger|itc_greatsword|itcf_carry_sword_left_hip, 2000, weight(2.75)|abundance(20)|difficulty(15)|hit_points(72000)|spd_rtng(120)|weapon_length(120)|swing_damage(50, cut)|thrust_damage(25, pierce), imodbits_sword_high],
#["dadao", "Jian", [("sultancimitar", 0)], itp_primary|itp_type_two_handed_wpn, itc_dagger|itc_greatsword|itcf_carry_sword_left_hip, 2000, weight(2.75)|abundance(20)|difficulty(15)|hit_points(72000)|spd_rtng(120)|weapon_length(120)|swing_damage(54, cut)|thrust_damage(22, pierce), imodbits_sword_high],
["dadao", "Jian", [("falchion2hand", 0),("falchion2hand_scabbard", ixmesh_carry)], itp_primary|itp_type_two_handed_wpn, itc_dagger|itc_greatsword|itcf_carry_sword_left_hip, 2000, weight(2.75)|abundance(20)|difficulty(15)|hit_points(72000)|spd_rtng(120)|weapon_length(120)|swing_damage(54, cut)|thrust_damage(22, pierce), imodbits_sword_high],
["Harlaus_sword", "Swadian Royal Blade", [("anduril", 0)], itp_primary|itp_always_loot|itp_type_two_handed_wpn, itc_dagger|itc_greatsword|itcf_carry_sword_back, 30000, weight(2.75)|difficulty(15)|hit_points(72000)|spd_rtng(120)|weapon_length(120)|swing_damage(58, cut)|thrust_damage(45, pierce), imodbits_sword_high],
["larktin_sword", "Lady Larktin's Blade", [("anduril", 0)], itp_primary|itp_always_loot|itp_type_two_handed_wpn, itc_dagger|itc_greatsword|itcf_carry_sword_back, 30000, weight(2.75)|difficulty(36)|hit_points(72000)|spd_rtng(130)|weapon_length(140)|swing_damage(72, cut)|thrust_damage(56, pierce), imodbits_sword_high],
["kaltzbalger", "Katzbalger", [("talak_katzbalger", 0), ("talak_scab_katzbalger", ixmesh_carry)], itp_primary|itp_type_two_handed_wpn, itc_dagger|itc_greatsword|itcf_carry_sword_back, 3000, weight(2.75)|abundance(0)|difficulty(15)|hit_points(72000)|spd_rtng(130)|weapon_length(100)|swing_damage(42, cut)|thrust_damage(30, pierce), imodbits_sword_high],
#["knightblade_sword", "Knightly Blade", [("knight's_sword", 0)], itp_type_two_handed_wpn|itp_merchandise|itp_primary, itc_dagger|itc_greatsword|itcf_carry_sword_left_hip, 3000, weight(2.75)|abundance(75)|difficulty(10)|hit_points(42000)|spd_rtng(130)|weapon_length(135)|swing_damage(40, cut)|thrust_damage(25, pierce), imodbits_sword_high],
["knightblade_sword", "Knightly Blade", [("Highlander_Claymore", 0)], itp_type_two_handed_wpn|itp_merchandise|itp_primary, itc_dagger|itc_greatsword|itcf_carry_sword_back, 3000, weight(2.75)|abundance(75)|difficulty(10)|hit_points(42000)|spd_rtng(130)|weapon_length(135)|swing_damage(40, cut)|thrust_damage(25, pierce), imodbits_sword_high],
["championsword", "Champion Sword", [("viktor_sword", 0), ("viktor_sword", ixmesh_carry)], itp_type_two_handed_wpn|itp_merchandise|itp_primary, itc_dagger|itc_greatsword|itcf_carry_sword_back, 3800, weight(2.75)|abundance(75)|difficulty(10)|hit_points(42000)|spd_rtng(124)|weapon_length(132)|swing_damage(39, cut)|thrust_damage(26, pierce), imodbits_sword_high],
["cruelsword", "Cruel Sword", [("Flamberge", 0)], itp_type_two_handed_wpn|itp_merchandise|itp_primary, itc_greatsword|itcf_carry_sword_back, 4000, weight(2.75)|abundance(75)|difficulty(10)|hit_points(42000)|spd_rtng(110)|weapon_length(135)|swing_damage(42, cut)|thrust_damage(36, pierce), imodbits_sword_high],
["brokensword", "Broken Longsword", [("broken_longbladed_sword", 0)], itp_type_two_handed_wpn|itp_merchandise|itp_primary, itc_dagger|itc_greatsword|itcf_carry_sword_left_hip, 200, weight(1.75)|abundance(50)|difficulty(10)|hit_points(22000)|spd_rtng(110)|weapon_length(72)|thrust_damage(9, pierce)|swing_damage(5, cut), imodbits_sword_high],
["knightblade_knight", "Two Handed Knight Sword", [("Royal_Claymore2", 0)], itp_type_two_handed_wpn|itp_merchandise|itp_two_handed|itp_primary, itc_greatsword|itcf_carry_sword_back, 4000, weight(2.75)|abundance(75)|difficulty(10)|hit_points(42000)|spd_rtng(100)|weapon_length(136)|swing_damage(40, cut)|thrust_damage(20, pierce), imodbits_sword_high],
["knightblade", "Heavy Longsword", [("Bastard_sword3", 0)], itp_type_two_handed_wpn|itp_merchandise|itp_primary, itc_dagger|itc_greatsword|itcf_carry_sword_back, 3500, weight(2.75)|abundance(75)|difficulty(10)|hit_points(42000)|spd_rtng(130)|weapon_length(125)|swing_damage(36, cut)|thrust_damage(26, pierce), imodbits_sword_high],
["knightblade_m", "Masterful Longsword", [("mastercrafted_sword", 0)], itp_type_two_handed_wpn|itp_merchandise|itp_primary, itc_dagger|itc_greatsword|itcf_carry_sword_back, 3500, weight(2.75)|abundance(75)|difficulty(10)|hit_points(42000)|spd_rtng(125)|weapon_length(130)|swing_damage(40, cut)|thrust_damage(30, pierce), imodbits_sword_high],
["jewelsword", "Jeweled Sword", [("freak_sword", 0)], itp_type_two_handed_wpn|itp_merchandise|itp_primary, itc_dagger|itc_greatsword|itcf_carry_sword_back, 9000, weight(3.75)|abundance(50)|difficulty(10)|hit_points(22000)|spd_rtng(100)|weapon_length(125)|swing_damage(42, cut)|thrust_damage(27, pierce), imodbits_sword_high],
#["sanjar_sword", "Khan's Supremacy", [("sultancimitar", 0)], itp_primary|itp_always_loot|itp_type_two_handed_wpn, itc_dagger|itc_greatsword|itcf_carry_sword_left_hip, 30000, weight(2.75)|difficulty(15)|hit_points(72000)|spd_rtng(135)|weapon_length(120)|swing_damage(60, cut)|thrust_damage(22, pierce), imodbits_sword_high],
["sanjar_sword", "Khan's Supremacy", [("snake_gold", 0),("snake_gold_scabbard", ixmesh_carry)], itp_primary|itp_always_loot|itp_type_two_handed_wpn, itc_dagger|itc_greatsword|itcf_carry_sword_left_hip, 30000, weight(2.75)|difficulty(15)|hit_points(72000)|spd_rtng(135)|weapon_length(120)|swing_damage(60, cut)|thrust_damage(22, pierce), imodbits_sword_high],
["brokenswordc", "Broken Bastard Sword", [("broken_sword", 0)], itp_type_two_handed_wpn|itp_two_handed|itp_merchandise|itp_primary, itc_greatsword|itcf_carry_sword_left_hip, 200, weight(1.75)|abundance(50)|difficulty(10)|hit_points(22000)|spd_rtng(110)|weapon_length(72)|thrust_damage(9, pierce)|swing_damage(4, cut), imodbits_sword_high],
["2hand_dk_sword", "Geroian Sword", [("wb_geroian_sword", 0), ("wb_geroian_scabbard", ixmesh_carry)], itp_type_two_handed_wpn|itp_primary, itcf_carry_sword_back|itc_nodachi|itcf_thrust_onehanded|itcf_overswing_onehanded|itcf_show_holster_when_drawn|itcf_slashright_onehanded|itcf_thrust_twohanded|itcf_slashleft_onehanded, 5000, weight(2.25)|difficulty(12)|spd_rtng(120)|weapon_length(98)|thrust_damage(50, pierce)|swing_damage(62, cut), imodbit_tempered|imodbit_masterwork|imodbit_balanced|imodbit_rusty],
["2hand_swadian_regent", "Swadian Regent Sword", [("wb_swadian_regent", 0), ("wb_swadian_regent_scabbard", ixmesh_carry)], itp_type_two_handed_wpn|itp_two_handed|itp_primary|itp_unique, itcf_carry_sword_back|itc_nodachi|itcf_show_holster_when_drawn|itcf_thrust_twohanded, 12232, weight(3.00)|difficulty(24)|spd_rtng(120)|weapon_length(125)|thrust_damage(52, pierce)|swing_damage(67, cut), imodbit_tempered|imodbit_masterwork|imodbit_balanced],
["kurgan_sword", "Kurgan", [("kurgan", 0), ("kurgan_in_scabbard", ixmesh_carry)], itp_type_two_handed_wpn|itp_primary|itp_unique, itcf_carry_sword_back|itc_nodachi|itcf_thrust_onehanded|itcf_overswing_onehanded|itcf_show_holster_when_drawn|itcf_slashright_onehanded|itcf_thrust_twohanded|itcf_slashleft_onehanded, 4599, weight(2.25)|difficulty(10)|spd_rtng(110)|weapon_length(105)|thrust_damage(50, pierce)|swing_damage(55, cut), imodbit_tempered|imodbit_masterwork|imodbit_balanced|imodbit_rusty],
["sultans_wrath",         "Sultan's Wrath", [("arabian_sword_c",0),("scab_arabian_sword_c", ixmesh_carry)], itp_type_one_handed_wpn|itp_primary|itp_always_loot, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 31000 , weight(1.5)|difficulty(15)|spd_rtng(130) | weapon_length(105)|swing_damage(55 , cut) | thrust_damage(34 ,  pierce),imodbits_sword_high ],

# One-Handed Axes

# ["pickaxe", "Pickaxe", [("fighting_pick_new", 0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip, 25, weight(3)|difficulty(0)|spd_rtng(96)|weapon_length(80)|swing_damage(19, pierce)|thrust_damage(0, pierce), imodbits_pick],
# ["fighting_pick", "Fighting Pick", [("fighting_pick_new", 0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip, 105, weight(3.5)|difficulty(0)|spd_rtng(94)|weapon_length(90)|swing_damage(25, pierce)|thrust_damage(0, pierce), imodbits_pick],
# ["military_pick", "Military Pick", [("steel_pick", 0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip, 145, weight(4)|difficulty(0)|spd_rtng(90)|weapon_length(90)|swing_damage(27, pierce)|thrust_damage(0, pierce), imodbits_pick],
# ["hand_axe", "Hand Axe", [("hatchet", 0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_secondary|itp_bonus_against_shield|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip, 25, weight(2)|difficulty(7)|spd_rtng(95)|weapon_length(75)|swing_damage(27, cut)|thrust_damage(0, pierce), imodbits_axe],
#  ["fighting_axe", "Fighting Axe", [("fighting_ax", 0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_secondary|itp_bonus_against_shield|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip, 75, weight(2.5)|difficulty(8)|spd_rtng(92)|weapon_length(90)|swing_damage(31, cut)|thrust_damage(0, pierce), imodbits_axe],
#["one_handed_war_axe_a", "Light Bearded Axe", [("one_handed_war_axe_a", 0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_secondary|itp_bonus_against_shield|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip, 85, weight(1.5)|difficulty(8)|spd_rtng(100)|weapon_length(60)|swing_damage(32, cut)|thrust_damage(0, pierce), imodbits_axe],
#["one_handed_war_axe_b", "One Handed Bearded Axe", [("one_handed_war_axe_b", 0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_secondary|itp_bonus_against_shield|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip, 135, weight(1.5)|difficulty(8)|spd_rtng(98)|weapon_length(61)|swing_damage(34, cut)|thrust_damage(0, pierce), imodbits_axe],
# ["one_handed_battle_axe_a", "Light Battle Axe", [("one_handed_battle_axe_a", 0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_secondary|itp_bonus_against_shield|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip, 105, weight(1.5)|difficulty(8)|spd_rtng(97)|weapon_length(69)|swing_damage(34, cut)|thrust_damage(0, pierce), imodbits_axe],
# ["one_handed_battle_axe_b", "One Handed Battle Axe", [("one_handed_battle_axe_b", 0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_secondary|itp_bonus_against_shield|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip, 175, weight(1.75)|difficulty(8)|spd_rtng(96)|weapon_length(70)|swing_damage(36, cut)|thrust_damage(0, pierce), imodbits_axe],
# ["one_handed_battle_axe_c", "One Handed Spiked Battle Axe", [("one_handed_battle_axe_c", 0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_secondary|itp_bonus_against_shield|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip, 295, weight(2.0)|difficulty(8)|spd_rtng(95)|weapon_length(72)|swing_damage(38, cut)|thrust_damage(0, pierce), imodbits_axe],

# Two-Handed Axes

#["axe", "Axe", [("iron_ax", 0)], itp_type_two_handed_wpn|itp_merchandise|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry, itc_nodachi|itcf_carry_axe_back, 65, weight(4)|difficulty(8)|spd_rtng(91)|weapon_length(108)|swing_damage(32, cut)|thrust_damage(0, pierce), imodbits_axe],
# BUG: shouldn't this be a polearm? WILL NOT FIX, NOT SURE.
["voulge", "Voulge", [("voulge",0)], itp_type_two_handed_wpn|itp_merchandise|itp_wooden_parry|itp_two_handed|itp_primary|itp_bonus_against_shield, itc_nodachi|itcf_carry_axe_back, 140, weight(5)|difficulty(12)|spd_rtng(90)|weapon_length(120)|swing_damage(40,cut)|thrust_damage(20,pierce), imodbits_axe ],
#["battle_axe", "Battle Axe", [("battle_ax", 0)], itp_type_two_handed_wpn|itp_merchandise|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry, itc_nodachi|itcf_carry_axe_back, 245, weight(5)|difficulty(8)|spd_rtng(88)|weapon_length(108)|swing_damage(41, cut)|thrust_damage(0, pierce), imodbits_axe],
#  ["war_axe", "War Axe", [("war_ax", 0)], itp_type_two_handed_wpn|itp_merchandise|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry, itc_nodachi|itcf_carry_axe_back, 265, weight(5)|difficulty(10)|spd_rtng(86)|weapon_length(110)|swing_damage(43, cut)|thrust_damage(0, pierce), imodbits_axe],
#["two_handed_axe", "Two_Handed_Axe", [("two_handed_battle_axe_a", 0)], itp_type_two_handed_wpn|itp_merchandise|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry, itc_nodachi|itcf_carry_axe_back, 115, weight(4.5)|difficulty(10)|spd_rtng(90)|weapon_length(90)|swing_damage(40, cut)|thrust_damage(0, pierce), imodbits_axe],
# ["two_handed_battle_axe_2", "Two_Handed_War_Axe", [("two_handed_battle_axe_b", 0)], itp_type_two_handed_wpn|itp_merchandise|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry, itc_nodachi|itcf_carry_axe_back, 205, weight(4.5)|difficulty(10)|spd_rtng(92)|weapon_length(92)|swing_damage(47, cut)|thrust_damage(0, pierce), imodbits_axe],
# BUG: should be renamed. FIXED: renamed to Heavy Voulge
["two_handed_battle_axe_3", "Heavy Voulge", [("two_handed_battle_axe_c", 0)], itp_type_two_handed_wpn|itp_merchandise|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry, itc_nodachi|itcf_carry_axe_back, 255, weight(4.5)|difficulty(10)|spd_rtng(87)|weapon_length(100)|swing_damage(48, cut)|thrust_damage(0, pierce), imodbits_axe],
#["bardiche", "Bardiche", [("two_handed_battle_axe_d", 0)], itp_type_two_handed_wpn|itp_merchandise|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry, itc_nodachi|itcf_carry_axe_back, 315, weight(4.5)|difficulty(10)|spd_rtng(87)|weapon_length(102)|swing_damage(50, cut)|thrust_damage(0, pierce), imodbits_axe],
# ["great_axe", "Great_Axe", [("two_handed_battle_axe_e", 0)], itp_type_two_handed_wpn|itp_merchandise|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry, itc_nodachi|itcf_carry_axe_back, 445, weight(4.5)|difficulty(10)|spd_rtng(90)|weapon_length(96)|swing_damage(51, cut)|thrust_damage(0, pierce), imodbits_axe],
# ["great_bardiche", "Great_Bardiche", [("two_handed_battle_axe_f", 0)], itp_type_two_handed_wpn|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry, itc_nodachi|itcf_carry_axe_back, 615, weight(4.5)|difficulty(10)|spd_rtng(90)|weapon_length(116)|swing_damage(47, cut)|thrust_damage(0, pierce), imodbits_axe],
# BUG: shouldn't this be a polearm? WILL NOT FIX, NOT SURE.
#  ["shortened_military_scythe", "Shortened Military Scythe", [("two_handed_battle_scythe_a", 0)], itp_type_two_handed_wpn|itp_merchandise|itp_two_handed|itp_primary, itc_nodachi|itcf_carry_sword_back, 265, weight(3.0)|difficulty(10)|spd_rtng(90)|weapon_length(112)|swing_damage(44, cut)|thrust_damage(0, pierce), imodbits_axe],
["dwarf_axe", "Double Headed Axe", [("trgba", 0)], itp_type_two_handed_wpn|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry, itc_nodachi|itcf_thrust_twohanded|itcf_carry_axe_back, 2005, weight(4.5)|difficulty(10)|hit_points(48128)|spd_rtng(92)|weapon_length(104)|swing_damage(62, cut)|thrust_damage(33, blunt), imodbits_axe],
["ragnar's_axe", "King Ragnar's Axe", [("talak_nordic_axe", 0)], itp_type_two_handed_wpn|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry, itc_nodachi|itcf_thrust_twohanded|itcf_carry_axe_back, 30005, weight(4.5)|difficulty(10)|hit_points(48128)|spd_rtng(92)|weapon_length(112)|swing_damage(58, cut)|thrust_damage(12, blunt), imodbits_axe],
["dwarfaxe2", "Long Axe", [("long_axe_a", 0)], itp_type_two_handed_wpn|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry, itc_nodachi|itcf_carry_axe_back, 4005, weight(4.5)|difficulty(10)|hit_points(48128)|spd_rtng(92)|weapon_length(122)|swing_damage(61, cut), imodbits_axe],
["varangian_axe", "Varangian Axe", [("varangian_axe_slim", 0)], itp_type_two_handed_wpn|itp_two_handed|itp_wooden_parry|itp_bonus_against_shield|itp_primary|itp_unique, itcf_carry_sword_back|itc_nodachi, 4005, weight(4.50)|difficulty(12)|spd_rtng(85)|weapon_length(120)|swing_damage(80, cut), imodbit_balanced|imodbit_heavy ],	
["fighting_axe_b", "Ornamented Axe", [("threadaxe1", 0)], itp_type_two_handed_wpn|itp_two_handed|itp_primary|itp_merchandise|itp_bonus_against_shield|itp_wooden_parry, itc_nodachi|itcf_carry_axe_back, 269, weight(3.00)|difficulty(10)|spd_rtng(92)|weapon_length(120)|swing_damage(39, cut), imodbits_axe],


# One-Handed Clubs, Maces and Hammers

# ["winged_mace", "Winged Mace", [("winged_mace", 0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_wooden_parry, itc_scimitar|itcf_carry_mace_left_hip, 125, weight(3.5)|difficulty(0)|spd_rtng(99)|weapon_length(80)|swing_damage(21, blunt)|thrust_damage(0, pierce), imodbits_mace],
# ["spiked_mace", "Iron Spiked Mace", [("spiked_mace", 0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_wooden_parry, itc_scimitar|itcf_carry_mace_left_hip, 185, weight(3.5)|difficulty(0)|spd_rtng(95)|weapon_length(90)|swing_damage(22, blunt)|thrust_damage(0, pierce), imodbits_pick],
# ["military_hammer", "Military Hammer", [("iron_hammer", 0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip, 315, weight(4)|difficulty(0)|spd_rtng(92)|weapon_length(90)|swing_damage(25, blunt)|thrust_damage(0, pierce), imodbits_mace],
# ["spiked_club", "Spiked Club", [("spiked_club", 0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_wooden_parry, itc_scimitar|itcf_carry_mace_left_hip, 85, weight(3)|difficulty(0)|spd_rtng(97)|weapon_length(97)|swing_damage(21, pierce)|thrust_damage(0, pierce), imodbits_mace],
# ["morningstar", "Morningstar", [("mace_morningstar_new", 0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip, 455, weight(5.5)|difficulty(13)|spd_rtng(80)|weapon_length(102)|swing_damage(36, pierce)|thrust_damage(0, pierce), imodbits_mace],
# ["club_with_spike_head", "Head-Spiked Club", [("mace_e", 0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_wooden_parry|itp_wooden_attack, itc_bastardsword|itcf_carry_axe_back, 75, weight(3.5)|difficulty(8)|spd_rtng(103)|weapon_length(80)|swing_damage(26, blunt)|thrust_damage(25, pierce), imodbits_mace],
# ["mace_1", "Nailed Club", [("mace_d", 0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_wooden_parry, itc_scimitar|itcf_carry_mace_left_hip, 45, weight(1.5)|difficulty(0)|spd_rtng(99)|weapon_length(62)|swing_damage(19, pierce)|thrust_damage(0, pierce), imodbits_mace],
# ["mace_2", "Knobbed_Mace", [("mace_a", 0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_wooden_parry, itc_scimitar|itcf_carry_mace_left_hip, 95, weight(2.5)|difficulty(0)|spd_rtng(98)|weapon_length(60)|swing_damage(21, blunt)|thrust_damage(0, pierce), imodbits_mace],
# ["mace_3", "Steel Spiked Mace", [("mace_c", 0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_wooden_parry, itc_scimitar|itcf_carry_mace_left_hip, 155, weight(2.5)|difficulty(0)|spd_rtng(98)|weapon_length(62)|swing_damage(23, blunt)|thrust_damage(0, pierce), imodbits_mace],
# ["mace_4", "Flanged Mace", [("mace_b", 0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_wooden_parry, itc_scimitar|itcf_carry_mace_left_hip, 215, weight(2.5)|difficulty(0)|spd_rtng(98)|weapon_length(60)|swing_damage(24, blunt)|thrust_damage(0, pierce), imodbits_mace],
["mace_lev_1", "Footmans Mace", [("mace_lev_1", 0)], itp_type_one_handed_wpn|itp_wooden_parry|itp_merchandise|itp_primary, itcf_carry_axe_back|itc_scimitar, 1050, weight(2.00)|difficulty(5)|spd_rtng(90)|weapon_length(80)|swing_damage(30, blunt), imodbit_masterwork|imodbit_balanced|imodbit_heavy|imodbit_rusty ],
["mace_lev_2", "Infantrymans Mace", [("mace_lev_2", 0)], itp_type_one_handed_wpn|itp_wooden_parry|itp_merchandise|itp_primary, itcf_carry_axe_back|itc_scimitar, 1650, weight(3.00)|difficulty(8)|spd_rtng(93)|weapon_length(90)|swing_damage(32, blunt), imodbit_masterwork|imodbit_balanced|imodbit_heavy|imodbit_rusty ],
["mace_lev_3", "Sergeants Mace", [("mace_lev_3", 0)], itp_type_one_handed_wpn|itp_wooden_parry|itp_merchandise|itp_primary, itcf_carry_axe_back|itc_scimitar, 1850, weight(3.00)|abundance(70)|difficulty(10)|spd_rtng(98)|weapon_length(93)|swing_damage(34, blunt), imodbit_masterwork|imodbit_balanced|imodbit_heavy|imodbit_rusty ],
["mace_lev_4", "Knight's Mace", [("mace_lev_4",0)], itp_type_one_handed_wpn|itp_merchandise|itp_wooden_parry|itp_primary, itc_morningstar|itcf_carry_axe_back, 2750, weight(3.50)|abundance(50)|difficulty(12)|spd_rtng(100)|weapon_length(98)|swing_damage(36,blunt), imodbit_rusty|imodbit_balanced|imodbit_masterwork|imodbit_heavy ],
["star_lev_1", "Eye-Plucker Morningstar", [("star_lev_1", 0)], itp_type_one_handed_wpn|itp_wooden_parry|itp_merchandise|itp_primary, itcf_carry_axe_back|itc_scimitar, 1010, weight(3.00)|difficulty(10)|spd_rtng(90)|weapon_length(85)|swing_damage(34, pierce), imodbit_masterwork|imodbit_balanced|imodbit_heavy|imodbit_rusty ],
["star_lev_2", "Knight's Morningstar", [("star_lev_2", 0)], itp_type_one_handed_wpn|itp_wooden_parry|itp_merchandise|itp_primary, itcf_carry_axe_back|itc_scimitar, 1310, weight(3.00)|abundance(80)|difficulty(14)|spd_rtng(100)|weapon_length(95)|swing_damage(36, pierce), imodbit_masterwork|imodbit_balanced|imodbit_heavy|imodbit_rusty ],
["star_lev_3", "Knight Captain's Morningstar", [("star_lev_3", 0)], itp_type_one_handed_wpn|itp_wooden_parry|itp_merchandise|itp_primary, itcf_carry_axe_back|itc_scimitar, 2510, weight(3.50)|abundance(40)|difficulty(15)|spd_rtng(110)|weapon_length(100)|swing_damage(38, pierce), imodbit_masterwork|imodbit_balanced|imodbit_heavy|imodbit_rusty ],

# Two-Handed Clubs, Maces and Hammers

# ["maul", "Banded Wooden Maul", [("maul_b", 0)], itp_type_two_handed_wpn|itp_merchandise|itp_primary|itp_two_handed|itp_wooden_parry|itp_wooden_attack, itc_nodachi|itcf_carry_spear, 95, weight(6)|difficulty(11)|spd_rtng(84)|weapon_length(79)|swing_damage(33, blunt)|thrust_damage(0, pierce), imodbits_mace],
# ["sledgehammer", "Wooden Sledgehammer", [("maul_c", 0)], itp_type_two_handed_wpn|itp_merchandise|itp_primary|itp_two_handed|itp_wooden_parry|itp_wooden_attack, itc_nodachi|itcf_carry_spear, 105, weight(7)|difficulty(10)|spd_rtng(82)|weapon_length(82)|swing_damage(35, blunt)|thrust_damage(0, pierce), imodbits_mace],
#["warhammer", "Sledgehammer", [("maul_d", 0)], itp_type_two_handed_wpn|itp_merchandise|itp_primary|itp_two_handed|itp_wooden_parry|itp_wooden_attack, itc_nodachi|itcf_carry_spear, 305, weight(9)|difficulty(14)|spd_rtng(85)|weapon_length(75)|swing_damage(38, blunt)|thrust_damage(0, pierce), imodbits_mace],
["warhammer_talak", "Warhammer", [("bec_de_corbin_a", 0)], itp_type_two_handed_wpn|itp_merchandise|itp_primary|itp_two_handed|itp_wooden_parry|itp_wooden_attack, itc_nodachi|itcf_carry_spear, 1005, weight(6)|difficulty(11)|spd_rtng(110)|weapon_length(90)|swing_damage(42, blunt), imodbits_mace],
["yaroglek_sword", "Skullcrusher", [("mace_morningstar_new", 0)], itp_type_two_handed_wpn|itp_primary|itp_always_loot, itc_dagger|itc_greatsword|itcf_carry_sword_back, 30000, weight(3.75)|difficulty(10)|hit_points(22000)|spd_rtng(120)|weapon_length(125)|swing_damage(56, pierce)|thrust_damage(0, cut), imodbits_sword_high],

# One-Handed Polearms

# ["scythe", "Scythe", [("scythe", 0)], itp_type_polearm|itp_merchandise|itp_type_polearm|itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_staff|itcf_carry_spear, 43, weight(3)|difficulty(0)|spd_rtng(79)|weapon_length(182)|swing_damage(19, cut)|thrust_damage(14, pierce), imodbits_polearm],
["spear_e_2-5m", "War Scythe", [("scythe_simple",0)], itp_type_polearm|itp_merchandise|itp_wooden_parry|itp_primary|itp_penalty_with_shield|itp_cant_use_on_horseback, itc_staff|itcf_carry_spear, 120, weight(6)|difficulty(18)|spd_rtng(70)|weapon_length(160)|swing_damage(36,cut)|thrust_damage(16,pierce), imodbits_polearm ],
# ["staff", "Staff", [("wooden_staff", 0)], itp_type_polearm|itp_merchandise|itp_type_polearm|itp_primary|itp_penalty_with_shield|itp_wooden_parry|itp_wooden_attack, itc_staff|itcf_carry_sword_back, 36, weight(1.5)|difficulty(0)|spd_rtng(100)|weapon_length(130)|swing_damage(18, blunt)|thrust_damage(19, blunt), imodbits_polearm],
# ["quarter_staff", "Quarter Staff", [("quarter_staff", 0)], itp_type_polearm|itp_merchandise|itp_type_polearm|itp_primary|itp_penalty_with_shield|itp_wooden_parry|itp_wooden_attack, itc_staff|itcf_carry_sword_back, 60, weight(2)|difficulty(0)|spd_rtng(104)|weapon_length(140)|swing_damage(20, blunt)|thrust_damage(20, blunt), imodbits_polearm],
# ["iron_staff", "Iron Staff", [("iron_staff", 0)], itp_type_polearm|itp_merchandise|itp_type_polearm|itp_primary|itp_penalty_with_shield, itc_staff|itcf_carry_sword_back, 202, weight(2)|difficulty(0)|spd_rtng(97)|weapon_length(140)|swing_damage(25, blunt)|thrust_damage(26, blunt), imodbits_polearm],
# ["military_fork", "Military Fork", [("military_fork", 0)], itp_type_polearm|itp_merchandise|itp_type_polearm|itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_spear, 153, weight(4.5)|difficulty(0)|spd_rtng(88)|weapon_length(135)|swing_damage(0, blunt)|thrust_damage(23, pierce), imodbits_polearm],
# ["battle_fork", "Battle Fork", [("battle_fork", 0)], itp_type_polearm|itp_merchandise|itp_type_polearm|itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_spear, 282, weight(4.5)|difficulty(0)|spd_rtng(87)|weapon_length(142)|swing_damage(0, blunt)|thrust_damage(24, pierce), imodbits_polearm],
# ["boar_spear", "Boar Spear", [("spear", 0)], itp_type_polearm|itp_merchandise|itp_type_polearm|itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_spear|itcf_carry_spear, 76, weight(4)|difficulty(0)|spd_rtng(81)|weapon_length(157)|swing_damage(0, cut)|thrust_damage(23, pierce), imodbits_polearm],
# ["shortened_spear", "Half Spear", [("spear_g_1-9m", 0)], itp_type_polearm|itp_merchandise|itp_type_polearm|itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_staff|itcf_carry_spear, 53, weight(2.0)|difficulty(0)|spd_rtng(102)|weapon_length(120)|swing_damage(19, blunt)|thrust_damage(25, pierce), imodbits_polearm],
# ["spear", "Spear", [("spear_h_2-15m", 0)], itp_type_polearm|itp_merchandise|itp_type_polearm|itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_staff|itcf_carry_spear, 75, weight(2.25)|difficulty(0)|spd_rtng(98)|weapon_length(135)|swing_damage(20, blunt)|thrust_damage(26, pierce), imodbits_polearm],
# ["war_spear", "War_Spear", [("spear_i_2-3m", 0)], itp_type_polearm|itp_merchandise|itp_type_polearm|itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_staff|itcf_carry_spear, 90, weight(2.5)|difficulty(0)|spd_rtng(96)|weapon_length(150)|swing_damage(20, blunt)|thrust_damage(27, pierce), imodbits_polearm],
# ["pike", "Pike", [("spear_a_3m", 0)], itp_type_polearm|itp_merchandise|itp_cant_use_on_horseback|itp_type_polearm|itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_cutting_spear, 125, weight(3.0)|difficulty(0)|spd_rtng(81)|weapon_length(245)|swing_damage(16, blunt)|thrust_damage(26, pierce), imodbits_polearm],
# ["jousting_lance", "Jousting Lance", [("joust_of_peace", 0)], itp_type_polearm|itp_merchandise|itp_type_polearm|itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_greatlance, 158, weight(5)|difficulty(0)|spd_rtng(61)|weapon_length(218)|swing_damage(0, cut)|thrust_damage(17, blunt), imodbits_polearm],
# ["double_sided_lance", "Double Sided Lance", [("lance_dblhead", 0)], itp_type_polearm|itp_merchandise|itp_type_polearm|itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_staff, 261, weight(5.5)|difficulty(0)|spd_rtng(80)|weapon_length(130)|swing_damage(0, cut)|thrust_damage(27, pierce), imodbits_polearm],
# ["light_lance", "Light Lance", [("spear_b_2-75m", 0)], itp_type_polearm|itp_merchandise|itp_type_polearm|itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_cutting_spear, 89, weight(2.5)|difficulty(0)|spd_rtng(90)|weapon_length(175)|swing_damage(16, blunt)|thrust_damage(27, pierce), imodbits_polearm],
# ["lance", "Lance", [("spear_d_2-8m", 0)], itp_type_polearm|itp_merchandise|itp_type_polearm|itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_cutting_spear, 110, weight(2.5)|difficulty(0)|spd_rtng(88)|weapon_length(180)|swing_damage(16, blunt)|thrust_damage(26, pierce), imodbits_polearm],
# ["heavy_lance", "Heavy Lance", [("spear_f_2-9m", 0)], itp_type_polearm|itp_merchandise|itp_type_polearm|itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_cutting_spear, 130, weight(2.75)|difficulty(10)|spd_rtng(85)|weapon_length(190)|swing_damage(16, blunt)|thrust_damage(26, pierce), imodbits_polearm],
 ["klance", "Knight Lance", [("talak_lance",0)], itp_type_polearm|itp_wooden_parry|itp_primary|itp_couchable, itc_greatlance, 1600, weight(6)|difficulty(18)|hit_points(40000)|spd_rtng(95)|weapon_length(190)|swing_damage(22,blunt)|thrust_damage(34,pierce), imodbits_polearm ],

# Two-Handed Polearms

# ["glaive", "Glaive", [("glaive", 0)], itp_type_polearm|itp_merchandise|itp_type_polearm|itp_primary|itp_two_handed|itp_wooden_parry, itc_staff|itcf_carry_spear, 352, weight(4.5)|difficulty(0)|spd_rtng(83)|weapon_length(157)|swing_damage(38, cut)|thrust_damage(21, pierce), imodbits_polearm],
# ["poleaxe", "Poleaxe", [("pole_ax", 0)], itp_type_polearm|itp_merchandise|itp_type_polearm|itp_primary|itp_two_handed|itp_wooden_parry, itc_staff, 384, weight(6.5)|difficulty(0)|spd_rtng(77)|weapon_length(180)|swing_damage(37, cut)|thrust_damage(21, pierce), imodbits_polearm],
#  ["polehammer", "Polehammer", [("pole_hammer", 0)], itp_type_polearm|itp_type_polearm|itp_primary|itp_two_handed|itp_wooden_parry, itc_staff, 169, weight(7)|difficulty(14)|spd_rtng(73)|weapon_length(130)|swing_damage(29, blunt)|thrust_damage(25, blunt), imodbits_polearm],
["halberd", "Halberd", [("rrr_halberd6",0)], itp_type_polearm|itp_merchandise|itp_wooden_parry|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_cant_use_on_horseback, itc_staff|itcf_carry_spear, 1800, weight(12)|difficulty(18)|spd_rtng(80)|weapon_length(210)|swing_damage(54,cut)|thrust_damage(20,blunt), imodbits_polearm ],
["ancient_halbard", "Ancient Council Blade", [("talak_halberd",0)], itp_type_polearm|itp_always_loot|itp_wooden_parry|itp_two_handed|itp_primary|itp_bonus_against_shield, itc_staff|itcf_carry_spear, 4600, weight(14)|difficulty(21)|hit_points(500000)|spd_rtng(100)|weapon_length(190)|swing_damage(56,cut)|thrust_damage(22,blunt), imodbits_polearm ],
# BUG: should not be usable on horseback. FIXED.
#  ["ashwood_pike", "Ashwood_Pike", [("winged_pike", 0)], itp_type_polearm|itp_two_handed|itp_cant_use_on_horseback|itp_wooden_parry|itp_merchandise|itp_primary|itp_type_polearm, itc_cutting_spear, 205, weight(3.50)|difficulty(11)|spd_rtng(90)|weapon_length(245)|thrust_damage(31, pierce)|swing_damage(19, cut), imodbits_polearm ],
# ["awlpike", "Awlpike", [("pike", 0)], itp_type_polearm|itp_merchandise|itp_type_polearm|itp_primary|itp_two_handed|itp_wooden_parry, itc_cutting_spear|itcf_carry_spear, 378, weight(3.5)|difficulty(10)|spd_rtng(92)|weapon_length(160)|swing_damage(30, cut)|thrust_damage(31, pierce), imodbits_polearm],
 ["lance_lev1", "Page's Lance", [("lance_lev1",0)], itp_type_polearm|itp_merchandise|itp_wooden_parry|itp_primary|itp_couchable, itc_cutting_spear, 480, weight(5)|difficulty(12)|spd_rtng(70)|weapon_length(170)|thrust_damage(22,pierce)|swing_damage(18,blunt), imodbits_polearm|imodbit_balanced|imodbit_heavy ],
 ["lance_lev2", "Senior Page's Lance", [("lance_lev2",0)], itp_type_polearm|itp_merchandise|itp_wooden_parry|itp_primary|itp_couchable, itc_cutting_spear, 760, weight(5)|difficulty(12)|spd_rtng(80)|weapon_length(170)|thrust_damage(26,pierce)|swing_damage(20,blunt), imodbits_polearm|imodbit_balanced ],
 ["lance_lev3", "Squire's Lance", [("lance_lev3",0)], itp_type_polearm|itp_merchandise|itp_wooden_parry|itp_primary|itp_couchable, itc_cutting_spear, 1200, weight(5)|abundance(80)|difficulty(15)|spd_rtng(90)|weapon_length(170)|thrust_damage(30,pierce)|swing_damage(22,blunt), imodbits_polearm|imodbit_balanced ],
 ["lance_lev4", "Knight's Lance", [("lance_lev4",0)], itp_type_polearm|itp_merchandise|itp_wooden_parry|itp_primary|itp_couchable|itp_crush_through|itp_can_knock_down, itc_cutting_spear, 1600, weight(6)|abundance(45)|difficulty(18)|spd_rtng(95)|weapon_length(190)|thrust_damage(34,pierce)|swing_damage(22,blunt), imodbits_polearm|imodbit_balanced|imodbit_heavy ],
 ["lance_lev5", "Knight Lord's Lance", [("lance_lev5",0)], itp_type_polearm|itp_merchandise|itp_wooden_parry|itp_primary|itp_couchable|itp_crush_through|itp_can_knock_down, itc_cutting_spear, 1750, weight(8)|abundance(45)|difficulty(21)|spd_rtng(100)|weapon_length(210)|thrust_damage(36,pierce)|swing_damage(24,blunt), imodbits_polearm|imodbit_balanced|imodbit_heavy ],
 ["lance_lev6", "Cavalier's Lance", [("lance_lev6",0)], itp_type_polearm|itp_merchandise|itp_wooden_parry|itp_primary|itp_couchable|itp_crush_through|itp_can_knock_down, itc_cutting_spear, 1900, weight(8)|abundance(30)|difficulty(24)|spd_rtng(110)|weapon_length(220)|thrust_damage(40,pierce)|swing_damage(26,blunt), imodbits_polearm|imodbit_balanced|imodbit_heavy ],


# Special Objects

# ["torch", "Torch", [("club", 0)], itp_type_one_handed_wpn|itp_primary, itc_scimitar, 11, weight(2.5)|difficulty(0)|spd_rtng(95)|weapon_length(95)|swing_damage(15, blunt)|thrust_damage(0, pierce), imodbits_none,
# [(ti_on_init_item, [(set_position_delta, 0, 60, 0), (particle_system_add_new, "psys_torch_fire"), (particle_system_add_new, "psys_torch_smoke"), (set_current_color, 150, 130, 70), (add_point_light, 10, 30),])]],



["1archers_vest", "Archer's Padded Vest", [("lamellar_ssm_012",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 1125, weight(15)|abundance(120)|head_armor(0)|body_armor(30)|leg_armor(4)|difficulty(8), imodbits_armor ],
["2archers_vest", "Archer's Padded Vest", [("lamellar_orn_182",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 1125, weight(15)|abundance(120)|head_armor(0)|body_armor(30)|leg_armor(4)|difficulty(8), imodbits_armor ],
["3archers_vest", "Archer's Padded Vest", [("maille_orn_102",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 1125, weight(15)|abundance(120)|head_armor(0)|body_armor(30)|leg_armor(4)|difficulty(8), imodbits_armor ],
["4archers_vest", "Archer's Padded Vest", [("maille_orn_142",0)], itp_type_body_armor|itp_merchandise|itp_covers_legs, 0, 1125, weight(15)|abundance(120)|head_armor(0)|body_armor(30)|leg_armor(4)|difficulty(8), imodbits_armor ],
# Unique Items
# Elite quests
["nordhammerb", "Nordhammer", [("nordhammerb",0)], itp_type_two_handed_wpn|itp_unique|itp_wooden_attack|itp_wooden_parry|itp_two_handed|itp_primary|itp_can_penetrate_shield|itp_bonus_against_shield|itp_crush_through, itc_nodachi|itcf_carry_spear, 5050, weight(6.00)|difficulty(24)|spd_rtng(85)|weapon_length(100)|swing_damage(85,blunt), imodbits_none ],
["jewelled_kilij", "Jewelled_Kilij", [("wb_jewelled_kilij", 0), ("wb_jewelled_kilij_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_primary, itcf_carry_sword_left_hip|itc_scimitar|itcf_show_holster_when_drawn, 1204, weight(1.75)|difficulty(12)|spd_rtng(100)|weapon_length(98)|swing_damage(42, cut), imodbit_tempered|imodbit_masterwork|imodbit_balanced],


["mandolin", "Mandolin", [("mandolin", 0)], itp_type_two_handed_wpn|itp_two_handed|itp_primary, itcf_carry_sword_back|itc_nodachi|itcf_thrust_twohanded, 2000, weight(1.50)|difficulty(5)|spd_rtng(110)|weapon_length(75)|thrust_damage(6, blunt)|swing_damage(15, blunt), imodbit_battered ],


################################################################################
# FISH&CHIP MODIFICATIONS START
################################################################################

  # HERALDIC ITEMS

  ["hera_shirt_a", "Heraldic Linen Tunic", [("shirt_a",0)], itp_merchandise|itp_type_body_armor|itp_civilian|itp_covers_legs,0,6,weight(1)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(1)|difficulty(0),imodbits_cloth,
    [(ti_on_init_item, [(store_trigger_param_1, ":agent_id"),(store_trigger_param_2, ":troop_id"),(call_script, "script_shield_item_set_banner", "tableau_heraldic_shirt_a", ":agent_id", ":troop_id")])],[]],
  ["hera_arena_tunicW", "Heraldic Arena Tunic", [("arena_tunicW_new",0)], itp_type_body_armor|itp_civilian|itp_covers_legs ,0, 47 , weight(2)|abundance(100)|head_armor(0)|body_armor(16)|leg_armor(6), imodbits_cloth,
    [(ti_on_init_item, [(store_trigger_param_1, ":agent_id"),(store_trigger_param_2, ":troop_id"),(call_script, "script_shield_item_set_banner", "tableau_heraldic_arena_tunicW", ":agent_id", ":troop_id")])],[]],
  ["hera_rich_tunic_a", "Heraldic Tunic", [("rich_tunic_a",0)], itp_type_body_armor|itp_civilian|itp_covers_legs ,0, 10 , weight(1)|abundance(100)|head_armor(0)|body_armor(7)|leg_armor(1)|difficulty(0) ,imodbits_cloth,
    [(ti_on_init_item, [(store_trigger_param_1, ":agent_id"),(store_trigger_param_2, ":troop_id"),(call_script, "script_shield_item_set_banner", "tableau_heraldic_rich_tunic_a", ":agent_id", ":troop_id")])],[]],
  ["hera_tabard_b", "Heraldic Tabard", [("tabard_b",0)],     itp_type_body_armor  |itp_covers_legs |itp_civilian,0, 107 , weight(3)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(6)|difficulty(0) ,imodbits_cloth,
    [(ti_on_init_item, [(store_trigger_param_1, ":agent_id"),(store_trigger_param_2, ":troop_id"),(call_script, "script_shield_item_set_banner", "tableau_heraldic_tabard_b", ":agent_id", ":troop_id")])], []],
  ["hera_padded_cloth_a", "Heraldic Aketon", [("padded_cloth_a",0)], itp_civilian| itp_type_body_armor  |itp_covers_legs ,0, 297 , weight(11)|abundance(100)|head_armor(0)|body_armor(22)|leg_armor(6)|difficulty(0) ,imodbits_cloth,
    [(ti_on_init_item, [(store_trigger_param_1, ":agent_id"),(store_trigger_param_2, ":troop_id"),(call_script, "script_shield_item_set_banner", "tableau_heraldic_padded_cloth_a", ":agent_id", ":troop_id")])],[]],
  ["hera_padded_cloth_b", "Heraldic Padded Cloth", [("padded_cloth_b",0)], itp_civilian| itp_type_body_armor  |itp_covers_legs ,0, 297 , weight(11)|abundance(100)|head_armor(0)|body_armor(22)|leg_armor(6)|difficulty(0) ,imodbits_cloth,
    [(ti_on_init_item, [(store_trigger_param_1, ":agent_id"),(store_trigger_param_2, ":troop_id"),(call_script, "script_shield_item_set_banner", "tableau_heraldic_padded_cloth_b", ":agent_id", ":troop_id")])],[]],
  ["hera_archers_vest", "Heraldic Archer's Padded Vest", [("archers_vest",0)], itp_civilian| itp_type_body_armor  |itp_covers_legs ,0, 260 , weight(6)|abundance(100)|head_armor(0)|body_armor(23)|leg_armor(12)|difficulty(0) ,imodbits_cloth,
    [(ti_on_init_item, [(store_trigger_param_1, ":agent_id"),(store_trigger_param_2, ":troop_id"),(call_script, "script_shield_item_set_banner", "tableau_heraldic_archers_vest", ":agent_id", ":troop_id")])],[]],
  ["hera_leather_vest", "Heraldic Leather Vest", [("leather_vest_a",0)], itp_type_body_armor  |itp_covers_legs|itp_civilian ,0, 146 , weight(4)|abundance(100)|head_armor(0)|body_armor(15)|leg_armor(7)|difficulty(0) ,imodbits_cloth,
    [(ti_on_init_item, [(store_trigger_param_1, ":agent_id"),(store_trigger_param_2, ":troop_id"),(call_script, "script_shield_item_set_banner", "tableau_heraldic_leather_vest_a", ":agent_id", ":troop_id")])],[]],
  ["hera_sarranid_leather_armor", "Heraldic Sarranid Leather Armor", [("sarranid_leather_armor",0)], itp_civilian| itp_type_body_armor  |itp_covers_legs ,0, 650 , weight(9)|abundance(100)|head_armor(0)|body_armor(32)|leg_armor(12)|difficulty(0) ,imodbits_armor,
    [(ti_on_init_item, [(store_trigger_param_1, ":agent_id"),(store_trigger_param_2, ":troop_id"),(call_script, "script_shield_item_set_banner", "tableau_heraldic_sarranid_leather_armor", ":agent_id", ":troop_id")])],[]],
  ["hera_tribal_warrior_outfit_a_new", "Heraldic Tribal Warrior Outfit", [("tribal_warrior_outfit_a_new",0)], itp_type_body_armor  |itp_covers_legs|itp_civilian ,0, 520 , weight(14)|abundance(100)|head_armor(0)|body_armor(30)|leg_armor(10)|difficulty(0) ,imodbits_cloth,
    [(ti_on_init_item, [(store_trigger_param_1, ":agent_id"),(store_trigger_param_2, ":troop_id"),(call_script, "script_shield_item_set_banner", "tableau_heraldic_tribal_warrior_outfit_a", ":agent_id", ":troop_id")])],[]],
  ["hera_light_mail_and_plate", "Heraldic Light Mail and Plate", [("light_mail_and_plate",0)], itp_merchandise|itp_type_body_armor|itp_covers_legs   ,0, 532 , weight(10)|abundance(100)|head_armor(0)|body_armor(32)|leg_armor(12)|difficulty(0) ,imodbits_armor,
    [(ti_on_init_item, [(store_trigger_param_1, ":agent_id"),(store_trigger_param_2, ":troop_id"),(call_script, "script_shield_item_set_banner", "tableau_heraldic_light_mail_and_plate", ":agent_id", ":troop_id")])],[]],
  ["hera_mail_and_plate", "Heraldic Mail and Plate", [("mail_and_plate",0)], itp_merchandise|itp_type_body_armor|itp_covers_legs   ,0, 593 , weight(16)|abundance(100)|head_armor(0)|body_armor(34)|leg_armor(12)|difficulty(0) ,imodbits_armor,
    [(ti_on_init_item, [(store_trigger_param_1, ":agent_id"),(store_trigger_param_2, ":troop_id"),(call_script, "script_shield_item_set_banner", "tableau_heraldic_mail_and_plate", ":agent_id", ":troop_id")])],[]],
  ["hera_cuir_bouilli_a", "Heraldic Cuir Bouilli", [("cuir_bouilli_a",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 3100 , weight(24)|abundance(100)|head_armor(0)|body_armor(50)|leg_armor(15)|difficulty(8) ,imodbits_armor,
    [(ti_on_init_item, [(store_trigger_param_1, ":agent_id"),(store_trigger_param_2, ":troop_id"),(call_script, "script_shield_item_set_banner", "tableau_heraldic_cuir_bouilli_a", ":agent_id", ":troop_id")])],[]],
  ["hera_brigandine_b", "Heraldic Brigandine", [("brigandine_b",0)], itp_merchandise| itp_type_body_armor|itp_covers_legs,0, 1830 , weight(19)|abundance(100)|head_armor(0)|body_armor(46)|leg_armor(12)|difficulty(0) ,imodbits_armor,
    [(ti_on_init_item, [(store_trigger_param_1, ":agent_id"),(store_trigger_param_2, ":troop_id"),(call_script, "script_shield_item_set_banner", "tableau_heraldic_brigandine_b", ":agent_id", ":troop_id")])],[]],
  ["hera_arena_armorW", "Heraldic Mail", [("arena_armorW_new",0)], itp_merchandise|itp_type_body_armor  |itp_covers_legs ,0, 650 , weight(16)|abundance(100)|head_armor(0)|body_armor(29)|leg_armor(13), imodbits_armor,
    [(ti_on_init_item, [(store_trigger_param_1, ":agent_id"),(store_trigger_param_2, ":troop_id"),(call_script, "script_shield_item_set_banner", "tableau_heraldic_arena_armorW", ":agent_id", ":troop_id")])], []],
  ["hera_mail_long_surcoat", "Heraldic Mail with Surcoat", [("mail_long_surcoat_new",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 1544 , weight(22)|abundance(100)|head_armor(0)|body_armor(42)|leg_armor(14)|difficulty(7) ,imodbits_armor,
    [(ti_on_init_item, [(store_trigger_param_1, ":agent_id"),(store_trigger_param_2, ":troop_id"),(call_script, "script_shield_item_set_banner", "tableau_heraldic_mail_long_surcoat", ":agent_id", ":troop_id")])],[]],
  ["hera_plate_armor", "Heraldic Plate Armor", [("full_plate_armor",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 6553 , weight(27)|abundance(100)|head_armor(0)|body_armor(55)|leg_armor(17)|difficulty(9) ,imodbits_plate,
    [(ti_on_init_item, [(store_trigger_param_1, ":agent_id"),(store_trigger_param_2, ":troop_id"),(call_script, "script_shield_item_set_banner", "tableau_heraldic_full_plate", ":agent_id", ":troop_id")])], []],

################################################################################
# FISH&CHIP MODIFICATIONS END
################################################################################

["items_end", "Items End", [("shield_round_a",0)], 0, 0, 1, 0, 0],
]
