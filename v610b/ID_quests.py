qst_save_town_from_bandits = 104
qst_duel_for_lady = 29
qst_denounce_lord = 66
qst_deliver_love_letter = 18
qst_capture_prisoners = 15
qst_quests_end = 105
qst_cheese_paladins = 44
qst_screen_army = 25
qst_deliver_cattle_to_army = 23
qst_move_cattle_herd = 35
qst_save_relative_of_merchant = 103
qst_organize_feast = 63
qst_visit_lady = 55
qst_deliver_grain = 46
qst_kill_local_merchant = 7
qst_kill_elites = 19
qst_raid_caravan_to_start_war = 71
qst_report_to_army = 22
qst_blank_quest_4 = 77
qst_scout_waypoints = 26
qst_collect_taxes = 5
qst_kidnapped_girl = 39
qst_hunt_down_fugitive = 6
qst_blank_quest_8 = 81
qst_fight_king = 34
qst_blank_quest_19 = 92
qst_blank_quest_18 = 91
qst_deliver_message = 0
qst_blank_quest_11 = 84
qst_blank_quest_10 = 83
qst_blank_quest_13 = 86
qst_blank_quest_12 = 85
qst_blank_quest_15 = 88
qst_blank_quest_14 = 87
qst_blank_quest_17 = 90
qst_blank_quest_16 = 89
qst_rescue_prisoner = 73
qst_capture_enemy_hero = 10
qst_deliver_letter = 33
qst_deal_with_bandits_at_lords_village = 4
qst_retaliate_for_border_incident = 70
qst_raise_peasants = 16
qst_deliver_message_to_prisoner_lord = 28
qst_rescue_lord_by_replace = 27
qst_bar_brawl = 45
qst_deliver_cattle = 47
qst_wed_betrothed = 58
qst_blank_quest_24 = 97
qst_blank_quest_25 = 98
qst_blank_quest_26 = 99
qst_blank_quest_27 = 100
qst_blank_quest_20 = 93
qst_blank_quest_21 = 94
qst_blank_quest_22 = 95
qst_hunt_down_smugglers = 50
qst_collect_debt = 12
qst_lend_surgeon = 20
qst_hunt_down_girl = 51
qst_formal_marriage_proposal = 56
qst_blank_quest_3 = 76
qst_blank_quest_2 = 75
qst_blank_quest_5 = 78
qst_incriminate_loyal_commander = 13
qst_blank_quest_7 = 80
qst_blank_quest_6 = 79
qst_blank_quest_9 = 82
qst_obtain_liege_blessing = 57
qst_cheese_rustlers = 43
qst_deliver_lumber = 52
qst_track_down_bandits = 68
qst_persuade_lords_to_make_peace = 40
qst_deal_with_night_bandits = 42
qst_offer_gift = 65
qst_resolve_dispute = 64
qst_rebel_against_kingdom = 61
qst_train_peasants_against_bandits = 48
qst_eliminate_bandits_infesting_village = 54
qst_bring_back_runaway_serfs = 8
qst_duel_avenge_insult = 31
qst_meet_spy_in_enemy_town = 14
qst_intrigue_against_lord = 67
qst_escort_merchant_caravan = 36
qst_troublesome_bandits = 38
qst_blank_quest_23 = 96
qst_consult_with_minister = 62
qst_escort_scholars = 49
qst_join_siege_with_army = 24
qst_lend_companion = 11
qst_follow_army = 21
qst_destroy_bandit_lair = 74
qst_raise_troops = 2
qst_deliver_wine = 37
qst_learn_where_merchant_brother_is = 102
qst_escort_lady = 3
qst_party = 17
qst_cause_provocation = 72
qst_follow_spy = 9
qst_deliver_portrait = 32
qst_join_faction = 60
qst_deliver_message_to_enemy_lord = 1
qst_kidnapped_wife = 53
qst_collect_men = 101
qst_track_down_provocateurs = 69
qst_wed_betrothed_female = 59
qst_duel_courtship_rival = 30
qst_deal_with_looters = 41