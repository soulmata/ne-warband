#from header_common import *
#from header_parties import *
from ID_troops import *
from ID_factions import *
from ID_map_icons import *

from compiler import *

####################################################################################################################
#  Each party template record contains the following fields:
#  1) Party-template id: used for referencing party-templates in other files.
#     The prefix pt_ is automatically added before each party-template id.
#  2) Party-template name.
#  3) Party flags. See header_parties.py for a list of available flags
#  4) Menu. ID of the menu to use when this party is met. The value 0 uses the default party encounter system.
#  5) Faction
#  6) Personality. See header_parties.py for an explanation of personality flags.
#  7) List of stacks. Each stack record is a tuple that contains the following fields:
#    7.1) Troop-id. 
#    7.2) Minimum number of troops in the stack. 
#    7.3) Maximum number of troops in the stack. 
#    7.4) Member flags(optional). Use pmf_is_prisoner to note that this member is a prisoner.
#     Note: There can be at most 6 stacks.
####################################################################################################################


party_templates = [
("none","none",icon_gray_knight,0,fac_commoners,merchant_personality,[]),
("rescued_prisoners","Rescued Prisoners",icon_lone_man,0,fac_commoners,merchant_personality,[]),
("enemy","Enemy",icon_gray_knight,0,fac_undeads,merchant_personality,[]),
("hero_party","Hero Party",icon_gray_knight,0,fac_commoners,merchant_personality,[]),
####################################################################################################################
# Party templates before this point are hard-wired into the game and should not be changed.
####################################################################################################################
##  ("old_garrison","Old Garrison",icon_vaegir_knight,0,fac_neutral,merchant_personality,[]),
("village_defenders","Village Defenders",icon_peasant,0,fac_commoners,merchant_personality,[(trp_farmer,10,20),(trp_peasant_woman,0,4)]),

("cattle_herd","Cattle Herd",icon_cattle|carries_goods(10),0,fac_neutral,merchant_personality,[(trp_cattle,80,120)]),

##  ("vaegir_nobleman","Vaegir Nobleman",icon_vaegir_knight|carries_goods(10)|pf_quest_party,0,fac_commoners,merchant_personality,[(trp_nobleman,1,1),(trp_vaegir_knight,2,6),(trp_vaegir_horseman,4,12)]),
##  ("swadian_nobleman","Swadian Nobleman",icon_gray_knight|carries_goods(10)|pf_quest_party,0,fac_commoners,merchant_personality,[(trp_nobleman,1,1),(trp_swadian_knight,2,6),(trp_swadian_man_at_arms,4,12)]),
# Ryan BEGIN
# ("looters","Looters",icon_axeman|carries_goods(8),0,fac_outlaws,bandit_personality,[(trp_looter,3,45)]),
# Ryan END
# ("manhunters","Manhunters",icon_gray_knight,0,fac_manhunters,soldier_personality,[(trp_manhunter,9,40)]),
##  ("peasant","Peasant",icon_peasant,0,fac_commoners,merchant_personality,[(trp_farmer,1,6),(trp_peasant_woman,0,7)]),

# NE
 ("looters", "Mercenary War Band", icon_outlaw_mercenaries|carries_goods(8), 0, fac_outlaws, bandit_personality, [(trp_female_pike_sister,3,12),(trp_female_shield_sister,3,6),(trp_mercenary_swordsman,6,12),(trp_mercenary_crossbowman,3,8),(trp_mercenary_equite,3,6),(trp_female_archer,3,12)] ),
("manhunters","Manhunters",icon_manhunters,0,fac_manhunters,soldier_personality,[(trp_manhunter,24,56),(trp_thief,0,6,pmf_is_prisoner),(trp_brawler,0,4,pmf_is_prisoner),(trp_murderer,0,3,pmf_is_prisoner)]),
("cheese_rustlers","Cheese Rustlers",icon_axeman|carries_goods(2)|pf_quest_party,0,fac_outlaws,bandit_personality,[(trp_cheese_rustler,15,25)]),
("cheese_paladins","Cheese Rustlers",icon_gray_knight|carries_goods(20)|pf_quest_party,0,fac_outlaws,merchant_personality,[(trp_cheese_rustler2,8,12)]),
("scholars","Scholars",icon_lone_man|carries_goods(20)|pf_auto_remove_in_town|pf_quest_party,0,fac_commoners,escorted_merchant_personality,[(trp_scholar,10,10)]),
 ("nemesis", "Hired Blades", icon_gray_knight|carries_goods(2)|pf_quest_party, 0, fac_neutral, merchant_personality, [(trp_female_shield_maiden,6,18),(trp_female_line_maiden,9,27),(trp_female_stalker,9,18),(trp_mercenary_sniper,9,18),(trp_mercenary_zweihander,6,18),(trp_mercenary_cavalry,3,12)] ),
 ("nemesis2", "Hired Blades", icon_gray_knight|carries_goods(0)|pf_quest_party, 0, fac_robber_knights, escorted_merchant_personality, [(trp_mercenary_cavalry,100,150),(trp_female_shield_maiden,50,75)] ),
# v585 Josef elites
("elites_fac1", "Usurpers", icon_swadia_rebels|carries_goods(2)|pf_quest_party, 0, fac_neutral, merchant_personality, [(trp_swadian_paladin,9,27),(trp_swadian_priest,9,27),(trp_swadian_cavalier,27,36)] ),
("elites_fac2", "Usurpers", icon_vaegir_rebels|carries_goods(2)|pf_quest_party, 0, fac_neutral, merchant_personality, [(trp_vaegir_ivory_sentinel,27,36),(trp_vaegir_knyaz,27,36),(trp_vaegir_druzhina,36,45)] ),
("elites_fac3", "Usurpers", icon_khergit_rebels|carries_goods(2)|pf_quest_party, 0, fac_neutral, merchant_personality, [(trp_khergit_guanren, 27, 42), (trp_khergit_keshik,42,90), (trp_khergit_jurtchi,42,72)]),
("elites_fac4", "Usurpers", icon_nordic_rebels|carries_goods(2)|pf_quest_party, 0, fac_neutral, merchant_personality, [(trp_nord_einherjar,27,42),(trp_nord_valkyrie,27,42),(trp_nord_thane,18,36)] ),
("elites_fac5", "Usurpers", icon_rhodok_rebels|carries_goods(2)|pf_quest_party, 0, fac_neutral, merchant_personality, [(trp_rhodok_arbalest,42,60),(trp_rhodok_spear_knight,18,27),(trp_rhodok_councilman,27,36)] ),
("elites_fac6", "Usurpers", icon_sarranid_rebels|carries_goods(2)|pf_quest_party, 0, fac_neutral, merchant_personality, [(trp_sarranid_zhayedan,20,20),(trp_sarranid_spahbod_serden,15,45),(trp_sarranid_spahbod,15,45),(trp_sarranid_dailamite,35,65)] ),

# v585 Josef Kidnapped wife quest
("bandits_awaiting_ransom2","Bandits Awaiting Ransom",icon_axeman|carries_goods(9)|pf_auto_remove_in_town|pf_quest_party,0,fac_neutral,bandit_personality,[(trp_brigand,24,58),(trp_kidnapped_wife,1,1,pmf_is_prisoner)]),
("kidnapped_wife","Miller's Wife",icon_woman|pf_quest_party,0,fac_neutral,merchant_personality,[(trp_kidnapped_wife,1,1)]),
# v585 Josef strange portrait quest
("spymaster","Spymaster",icon_lone_man|carries_goods(9)|pf_auto_remove_in_town|pf_quest_party,0,fac_neutral,bandit_personality,[(trp_spymaster,1,1)]),
# end NE

#  ("black_khergit_raiders","Black Khergit Raiders",icon_khergit_horseman_b|carries_goods(2),0,fac_black_khergits,bandit_personality,[(trp_black_khergit_guard,1,10),(trp_black_khergit_horseman,5,5)]),

# NE bandit numbers - rem change these

("steppe_bandits","Steppe Bandits",icon_khergit|carries_goods(2),0,fac_outlaws,bandit_personality,[(trp_steppe_bandit,15,58),(trp_steppe_raider,9,12)]),
("taiga_bandits","Tundra Bandits",icon_outlaw_tundra_bandits|carries_goods(2),0,fac_outlaws,bandit_personality,[(trp_taiga_bandit,22,58)]),
("desert_bandits","Desert Bandits",icon_outlaw_desert_bandits|carries_goods(2),0,fac_outlaws,bandit_personality,[(trp_desert_bandit,22,58)]),
("forest_bandits","Forest Bandits",icon_axeman|carries_goods(2),0,fac_forest_bandits,bandit_personality,[(trp_scarred_bandit,19,52),(trp_peasant_woman,0,4,pmf_is_prisoner)]),
("mountain_bandits","Mountain Bandits",icon_outlaw_mountain_bandits|carries_goods(2),0,fac_mountain_bandits,bandit_personality,[(trp_ruthless_bandit,22,60),(trp_peasant_woman,0,2,pmf_is_prisoner)]),
("sea_raiders","Sea Raiders",icon_outlaw_sea_raiders|carries_goods(2),0,fac_outlaws,bandit_personality,[(trp_sea_raider,20,50)]),

("deserters","Deserters",icon_outlaw_deserters|carries_goods(3),0,fac_deserters,bandit_personality,[]),

("merchant_caravan","Merchant Caravan",icon_quest_caravan|carries_goods(20)|pf_auto_remove_in_town|pf_quest_party,0,fac_commoners,escorted_merchant_personality,[(trp_caravan_master,1,1),(trp_caravan_guard,5,25)]),
("troublesome_bandits","Troublesome Bandits",icon_axeman|carries_goods(9)|pf_quest_party,0,fac_outlaws,bandit_personality,[(trp_brigand,14,55)]),
("bandits_awaiting_ransom","Bandits Awaiting Ransom",icon_axeman|carries_goods(9)|pf_auto_remove_in_town|pf_quest_party,0,fac_neutral,bandit_personality,[(trp_bandit,24,58),(trp_kidnapped_girl,1,1,pmf_is_prisoner)]),
("kidnapped_girl","Kidnapped Girl",icon_woman|pf_quest_party,0,fac_neutral,merchant_personality,[(trp_kidnapped_girl,1,1)]),

# NE quests
# v585 Josef Cheese Rustlers
# ("cheese_rustlers","Cheese Rustlers",icon_axeman|carries_goods(2)|pf_quest_party,0,fac_outlaws,bandit_personality,[(trp_cheese_rustler,15,25)]), #14,55
#  ("cheese_paladins","Cheese Rustlers",icon_gray_knight|carries_goods(20)|pf_quest_party,0,fac_outlaws,merchant_personality,[(trp_cheese_rustler2,8,12)]), #14,55
##v585 Josef scholars
# ("scholars","Scholars",icon_peasant|carries_goods(20)|pf_auto_remove_in_town|pf_quest_party,0,fac_commoners,escorted_merchant_personality,[(trp_scholar,10,10)]),
# ("nemesis","Hired Blades",icon_axeman|carries_goods(2)|pf_quest_party,0,fac_neutral,merchant_personality,[(trp_female_knight, 6,12),(trp_female_mercenary,6,11),(trp_hired_blade,6,11),(trp_mercenary_crossbowman,6,11),(trp_mercenary_sergeant,2,4),(trp_mercenary_archer,6,11)]),
##v586 Mercenaries to attack other lords with
# ("nemesis2","Hired Blades",icon_axeman|carries_goods(0)|pf_quest_party,0,fac_robber_knights,escorted_merchant_personality,[(trp_female_knight, 75,100),(trp_female_mercenary,50,75)]),


("smugglers","Smugglers",icon_peasant|carries_goods(8)|pf_default_behavior|pf_quest_party,0,fac_neutral,merchant_personality,[(trp_brigand,3,3), (trp_scarred_bandit,3,3), (trp_ruthless_bandit,3,3)]),	
# end v585 Josef

# NE quests end
("village_farmers","Village Farmers",icon_peasant|pf_civilian,0,fac_innocents,merchant_personality,[(trp_farmer,5,10),(trp_peasant_woman,3,8)]),

("spy_partners", "Unremarkable Travellers", icon_outlaw_deserters|carries_goods(10)|pf_default_behavior|pf_quest_party,0,fac_neutral,merchant_personality,[(trp_spy_partner,1,1),(trp_caravan_guard,5,11)]),
("runaway_serfs","Runaway Serfs",icon_peasant|carries_goods(8)|pf_default_behavior|pf_quest_party,0,fac_neutral,merchant_personality,[(trp_farmer,6,7), (trp_peasant_woman,3,3)]),
("spy", "Ordinary Townsman", icon_lone_man|carries_goods(4)|pf_default_behavior|pf_quest_party,0,fac_neutral,merchant_personality,[(trp_spy,1,1)]),
("sacrificed_messenger", "Sacrificed Messenger", icon_gray_knight|carries_goods(3)|pf_default_behavior|pf_quest_party,0,fac_neutral,merchant_personality,[]),
##  ("conspirator", "Conspirators", icon_gray_knight|carries_goods(8)|pf_default_behavior|pf_quest_party,0,fac_neutral,merchant_personality,[(trp_conspirator,3,4)]),
##  ("conspirator_leader", "Conspirator Leader", icon_gray_knight|carries_goods(8)|pf_default_behavior|pf_quest_party,0,fac_neutral,merchant_personality,[(trp_conspirator_leader,1,1)]),
##  ("peasant_rebels", "Peasant Rebels", icon_peasant,0,fac_peasant_rebels,bandit_personality,[(trp_peasant_rebel,33,97)]),
##  ("noble_refugees", "Noble Refugees", icon_gray_knight|carries_goods(12)|pf_quest_party,0,fac_noble_refugees,merchant_personality,[(trp_noble_refugee,3,5),(trp_noble_refugee_woman,5,7)]),

("forager_party","Foraging Party",icon_gray_knight|carries_goods(5)|pf_show_faction,0,fac_commoners,merchant_personality,[]),
("swadian_scouts", "Scouts", icon_swadia_scouts|carries_goods(1)|pf_show_faction, 0, fac_kingdom_1, bandit_personality, [(trp_swadian_veteran_crossbowman,4,8),(trp_swadian_crossbowman,2,4),(trp_swadian_sergeant,1,3)] ),
("vaegir_scouts", "Scouts", icon_vaegir_scouts|carries_goods(1)|pf_show_faction, 0, fac_kingdom_2, bandit_personality, [(trp_vaegir_marksman,6,14),(trp_vaegir_scout,1,3),(trp_vaegir_varangian,1,4)] ),
("khergit_scouts","Scouts",icon_khergit_scouts|carries_goods(1)|pf_show_faction,0,fac_kingdom_3,bandit_personality,[(trp_khergit_horse_archer,2,4),(trp_khergit_master_horse_archer,4,8),(trp_khergit_lancer,3,9)]),
("nord_scouts", "Scouts", icon_nordic_scouts|carries_goods(1)|pf_show_faction, 0, fac_kingdom_4, bandit_personality, [(trp_nord_skald,1,2),(trp_nord_raiding_archer,3,9),(trp_nord_warrior,1,3)] ),
("rhodok_scouts", "Scouts", icon_rhodok_scouts|carries_goods(1)|pf_show_faction, 0, fac_kingdom_5, bandit_personality, [(trp_rhodok_sharpshooter,4,8),(trp_rhodok_master_spearman,2,4),(trp_rhodok_scout,1,1)] ),
("swadian_patrol", "Patrol", icon_swadia_patrol|carries_goods(2)|pf_show_faction, 0, fac_kingdom_1, soldier_personality, [(trp_swadian_man_at_arms,2,4),(trp_swadian_sergeant,6,12),(trp_swadian_infantry,12,18),(trp_swadian_squire,3,9),(trp_swadian_crossbowman,6,18),(trp_swadian_paladin,1,1)] ),
("vaegir_patrol", "Patrol", icon_vaegir_patrol|carries_goods(2)|pf_show_faction, 0, fac_kingdom_2, soldier_personality, [(trp_vaegir_horseman,3,9),(trp_vaegir_varangian_guard,5,9),(trp_vaegir_pikeman,3,12),(trp_vaegir_marksman,3,12),(trp_vaegir_archer,9,27),(trp_vaegir_knyaz,1,1)] ),
("khergit_patrol","Patrol",icon_khergit_patrol|carries_goods(2)|pf_show_faction,0,fac_kingdom_3,soldier_personality,[(trp_khergit_horse_archer,6,18),(trp_khergit_lancer,6,18),(trp_khergit_keshik,1,1),(trp_khergit_tarkhan,2,7),(trp_khergit_kharash,12,27)]),
("nord_patrol", "Patrol", icon_nordic_patrol|carries_goods(2)|pf_show_faction, 0, fac_kingdom_4, soldier_personality, [(trp_nord_merkismathr,3,9),(trp_nord_berserker,3,9),(trp_nord_butsecarl,1,1),(trp_nord_skald,6,12),(trp_nord_warrior,9,18)] ),
("rhodok_patrol", "Patrol", icon_rhodok_patrol|carries_goods(2)|pf_show_faction, 0, fac_kingdom_5, soldier_personality, [(trp_rhodok_spear_knight,1,1),(trp_rhodok_arbalest,5,9),(trp_rhodok_veteran_spearman,9,10),(trp_rhodok_sharpshooter,9,10),(trp_rhodok_swordsman,9,10)] ),
("crusaders", "Holy Crusaders", icon_swadia_elites|carries_goods(2)|pf_show_faction, 0, fac_kingdom_1, soldier_personality, [(trp_swadian_paladin,3,6),(trp_swadian_priest,5,11)] ),
("ivory_guards", "Ivory Guards", icon_vaegir_elites|carries_goods(2)|pf_show_faction, 0, fac_kingdom_2, soldier_personality, [(trp_vaegir_ivory_sentinel,6,12),(trp_vaegir_knyaz,2,7),(trp_vaegir_druzhina,1,1)] ),
("red_horde","Red Horde",icon_khergit_elites1|carries_goods(2)|pf_show_faction,0,fac_kingdom_3,soldier_personality,[(trp_khergit_veteran_horse_archer,27,48),(trp_khergit_lancer,27,48),(trp_khergit_savage_kharash,27,48),(trp_khergit_mangudai,18,36),(trp_khergit_guanren,2,2),(trp_khergit_keshik,2,2)]),
("blue_horde","Blue Horde",icon_khergit_elites2|carries_goods(2)|pf_show_faction,0,fac_kingdom_3,soldier_personality,[(trp_khergit_veteran_horse_archer,27,48),(trp_khergit_lancer,27,48),(trp_khergit_kharash_clansman,27,48),(trp_khergit_jurtchi,9,18),(trp_khergit_guanren,1,4),(trp_khergit_keshik,0,4)]),
("raiders", "Berserkers", icon_nordic_elites|carries_goods(2)|pf_show_faction, 0, fac_kingdom_4, soldier_personality, [(trp_nord_berserker,3,9),(trp_nord_einherjar,2,7),(trp_nord_valkyrie,1,3),(trp_nord_merkismathr,1,3)] ),
("councilmen", "Councilmen", icon_rhodok_elites|carries_goods(2)|pf_show_faction, 0, fac_kingdom_5, soldier_personality, [(trp_rhodok_armsman,3,9),(trp_rhodok_spear_knight,1,3),(trp_rhodok_arbalest,6,18),(trp_rhodok_veteran_scout,3,9),(trp_rhodok_master_swordsman,3,9)] ),
#  ("war_party", "War Party",icon_gray_knight|carries_goods(3),0,fac_commoners,soldier_personality,[]),
("messenger_party","Messenger",icon_gray_knight|pf_show_faction,0,fac_commoners,merchant_personality,[]),
("raider_party","Raiders",icon_gray_knight|carries_goods(16)|pf_quest_party,0,fac_commoners,bandit_personality,[]),
("raider_captives","Raider Captives",0,0,fac_commoners,0,[(trp_peasant_woman,6,30,pmf_is_prisoner)]),
("kingdom_caravan_party","Caravan",icon_mule|carries_goods(25)|pf_show_faction,0,fac_commoners,merchant_personality,[(trp_caravan_master,1,1),(trp_caravan_guard,12,40)]),
("swadian_prisoner_train", "Prisoner Train", icon_gray_knight|carries_goods(5)|pf_show_faction, 0, fac_kingdom_1, merchant_personality, [(trp_swadian_infantry,5,10),(trp_swadian_senior_squire,6,8),(trp_swadian_crossbowman,7,12),(trp_bandit,10,24,pmf_is_prisoner)] ),
("vaegir_prisoner_train", "Prisoner Train", icon_gray_knight|carries_goods(5)|pf_show_faction, 0, fac_kingdom_2, merchant_personality, [(trp_vaegir_sergeant,5,10),(trp_vaegir_horseman,3,5),(trp_vaegir_archer,9,15),(trp_bandit,10,24,pmf_is_prisoner)] ),
("khergit_prisoner_train","Prisoner Train",icon_gray_knight|carries_goods(5)|pf_show_faction,0,fac_kingdom_3,merchant_personality,[(trp_khergit_horse_archer,18,36),(trp_khergit_lancer,18,36),(trp_steppe_bandit,17,95,pmf_is_prisoner),(trp_steppe_raider,3,36,pmf_is_prisoner)] ),
("nord_prisoner_train", "Prisoner Train", icon_gray_knight|carries_goods(5)|pf_show_faction, 0, fac_kingdom_4, merchant_personality, [(trp_nord_warrior,14,36),(trp_sea_raider,5,18,pmf_is_prisoner),(trp_nord_raiding_archer,2,8),(trp_nord_butsecarl,1,1)] ),
("rhodok_prisoner_train", "Prisoner Train", icon_gray_knight|carries_goods(5)|pf_show_faction, 0, fac_kingdom_5, merchant_personality, [(trp_rhodok_veteran_spearman,10,20),(trp_rhodok_crossbowman,10,20),(trp_bandit,10,24,pmf_is_prisoner)] ),
# NE TODO - enter Sarranid prisoner train
("sarranid_scouts", "Sarranid Scouts", icon_sarranid_scouts|carries_goods(1)|pf_show_faction, 0, fac_kingdom_6, bandit_personality, [(trp_sarranid_kamandaran_serden,3,9),(trp_sarranid_kamandaran,6,12),(trp_sarranid_timariot,1,1)] ),
("sarranid_patrol", "Sarranid Patrol", icon_sarranid_patrol|carries_goods(2)|pf_show_faction, 0, fac_kingdom_6, soldier_personality, [(trp_sarranid_dervish,4,8),(trp_sarranid_janissary,5,9),(trp_sarranid_dailamite,9,10),(trp_sarranid_timariot,9,10),(trp_sarranid_kamandaran_serden,9,10),(trp_sarranid_mamluke,1,1)] ),
("sarranid_crusaders", "Jihadists", icon_sarranid_elites|carries_goods(2)|pf_show_faction, 0, fac_kingdom_6, soldier_personality, [(trp_sarranid_bey,2,3),(trp_sarranid_ghazi,4,8),(trp_sarranid_spahbod,4,8)] ),
("sarranid_prisoner_train", "Prisoner Train", icon_gray_knight|carries_goods(5)|pf_show_faction, 0, fac_kingdom_6, merchant_personality, [(trp_sarranid_janissary,5,10),(trp_sarranid_dervish,6,8),(trp_sarranid_akinci,7,12),(trp_bandit,10,24,pmf_is_prisoner)] ),
("mamlukes","Jihadists",icon_sarranid_elites|carries_goods(2)|pf_show_faction,0,fac_kingdom_6,soldier_personality,[(trp_sarranid_mamluke,3,3),(trp_sarranid_siphai,7,10),(trp_sarranid_dervish,10,12)]),





("default_prisoners","Default Prisoners",0,0,fac_commoners,0,[(trp_brigand,5,10,pmf_is_prisoner)]),
# end NE

# ("scout_party","Scouts",icon_gray_knight|carries_goods(1)|pf_show_faction,0,fac_commoners,bandit_personality,[]),
# ("patrol_party","Patrol",icon_gray_knight|carries_goods(2)|pf_show_faction,0,fac_commoners,soldier_personality,[]),
#####("war_party", "War Party",icon_gray_knight|carries_goods(3),0,fac_commoners,soldier_personality,[]),
# ("messenger_party","Messenger",icon_gray_knight|pf_show_faction,0,fac_commoners,merchant_personality,[]),
# ("raider_party","Raiders",icon_gray_knight|carries_goods(16)|pf_quest_party,0,fac_commoners,bandit_personality,[]),
# ("raider_captives","Raider Captives",0,0,fac_commoners,0,[(trp_peasant_woman,6,30,pmf_is_prisoner)]),
# ("kingdom_caravan_party","Caravan",icon_mule|carries_goods(25)|pf_show_faction,0,fac_commoners,merchant_personality,[(trp_caravan_master,1,1),(trp_caravan_guard,12,40)]),
("prisoner_train_party","Prisoner Train",icon_gray_knight|carries_goods(5)|pf_show_faction,0,fac_commoners,merchant_personality,[]),
# ("default_prisoners","Default Prisoners",0,0,fac_commoners,0,[(trp_bandit,5,10,pmf_is_prisoner)]),

("routed_warriors","Routed Enemies",icon_vaegir_knight,0,fac_commoners,soldier_personality,[]),


# Caravans
("center_reinforcements","Reinforcements",icon_axeman|carries_goods(16),0,fac_commoners,soldier_personality,[(trp_townsman,5,30),(trp_watchman,4,20)]),

("kingdom_hero_party","War Party",icon_flagbearer_a|pf_show_faction|pf_default_behavior,0,fac_commoners,soldier_personality,[]),

# Reinforcements
# each faction includes three party templates. One is less-modernised, one is med-modernised and one is high-modernised
# less-modernised templates are generally includes 7-14 troops in total,
# med-modernised templates are generally includes 5-10 troops in total,
# high-modernised templates are generally includes 3-5 troops in total

# ("kingdom_1_reinforcements_a", "{!}kingdom_1_reinforcements_a", 0, 0, fac_commoners, 0, [(trp_swadian_recruit,5,10),(trp_swadian_militia,2,4)]),
# ("kingdom_1_reinforcements_b", "{!}kingdom_1_reinforcements_b", 0, 0, fac_commoners, 0, [(trp_swadian_footman,3,6),(trp_swadian_skirmisher,2,4)]),
# ("kingdom_1_reinforcements_c", "{!}kingdom_1_reinforcements_c", 0, 0, fac_commoners, 0, [(trp_swadian_man_at_arms,2,4),(trp_swadian_crossbowman,1,2)]), #Swadians are a bit less-powered thats why they have a bit more troops in their modernised party template (3-6, others 3-5)

# ("kingdom_2_reinforcements_a", "{!}kingdom_2_reinforcements_a", 0, 0, fac_commoners, 0, [(trp_vaegir_recruit,5,10),(trp_vaegir_footman,2,4)]),
# ("kingdom_2_reinforcements_b", "{!}kingdom_2_reinforcements_b", 0, 0, fac_commoners, 0, [(trp_vaegir_veteran,2,4),(trp_vaegir_skirmisher,2,4),(trp_vaegir_footman,1,2)]),
# ("kingdom_2_reinforcements_c", "{!}kingdom_2_reinforcements_c", 0, 0, fac_commoners, 0, [(trp_vaegir_horseman,2,3),(trp_vaegir_infantry,1,2)]),

# ("kingdom_3_reinforcements_a", "{!}kingdom_3_reinforcements_a", 0, 0, fac_commoners, 0, [(trp_khergit_tribesman,3,5),(trp_khergit_skirmisher,4,9)]), #Khergits are a bit less-powered thats why they have a bit more 2nd upgraded(trp_khergit_skirmisher) than non-upgraded one(trp_khergit_tribesman).
# ("kingdom_3_reinforcements_b", "{!}kingdom_3_reinforcements_b", 0, 0, fac_commoners, 0, [(trp_khergit_horseman,2,4),(trp_khergit_horse_archer,2,4),(trp_khergit_skirmisher,1,2)]),
# ("kingdom_3_reinforcements_c", "{!}kingdom_3_reinforcements_c", 0, 0, fac_commoners, 0, [(trp_khergit_horseman,2,4),(trp_khergit_veteran_horse_archer,2,3)]), #Khergits are a bit less-powered thats why they have a bit more troops in their modernised party template (4-7, others 3-5)

# ("kingdom_4_reinforcements_a", "{!}kingdom_4_reinforcements_a", 0, 0, fac_commoners, 0, [(trp_nord_footman,5,10),(trp_nord_recruit,2,4)]),
# ("kingdom_4_reinforcements_b", "{!}kingdom_4_reinforcements_b", 0, 0, fac_commoners, 0, [(trp_nord_huntsman,2,5),(trp_nord_archer,2,3),(trp_nord_footman,1,2)]),
# ("kingdom_4_reinforcements_c", "{!}kingdom_4_reinforcements_c", 0, 0, fac_commoners, 0, [(trp_nord_warrior,3,5)]),

# ("kingdom_5_reinforcements_a", "{!}kingdom_5_reinforcements_a", 0, 0, fac_commoners, 0, [(trp_rhodok_tribesman,5,10),(trp_rhodok_spearman,2,4)]),
# ("kingdom_5_reinforcements_b", "{!}kingdom_5_reinforcements_b", 0, 0, fac_commoners, 0, [(trp_rhodok_crossbowman,3,6),(trp_rhodok_trained_crossbowman,2,4)]),
# ("kingdom_5_reinforcements_c", "{!}kingdom_5_reinforcements_c", 0, 0, fac_commoners, 0, [(trp_rhodok_veteran_spearman,2,3),(trp_rhodok_veteran_crossbowman,1,2)]),

######NE
# Begin NE 600 series standardized reinforcement packs
("kingdom_1_reinforcements_a", "{!}kingdom_1_reinforcements_a", 0, 0, fac_commoners, 0, [(trp_swadian_recruit,4,9),(trp_swadian_proselyte,3,8),(trp_swadian_page,2,7),(trp_swadian_skirmisher,1,6)] ),
("kingdom_1_reinforcements_b", "{!}kingdom_1_reinforcements_b", 0, 0, fac_commoners, 0, [(trp_swadian_infantry,3,8),(trp_swadian_veteran_crossbowman,2,7), (trp_swadian_pikeman,1,6)] ),
("kingdom_1_reinforcements_c", "{!}kingdom_1_reinforcements_c", 0, 0, fac_commoners, 0, [(trp_swadian_knight,2,7), (trp_swadian_captain,1,6)] ),

("kingdom_2_reinforcements_a", "{!}kingdom_2_reinforcements_a", 0, 0, fac_commoners, 0, [(trp_vaegir_recruit,4,9),(trp_vaegir_footman,3,8),(trp_vaegir_scout,2,7),(trp_vaegir_bowman,1,6)] ),
("kingdom_2_reinforcements_b", "{!}kingdom_2_reinforcements_b", 0, 0, fac_commoners, 0, [(trp_vaegir_sergeant,3,8),(trp_vaegir_pikeman,2,7),(trp_vaegir_archer,1,6)] ),
("kingdom_2_reinforcements_c", "{!}kingdom_2_reinforcements_c", 0, 0, fac_commoners, 0, [(trp_vaegir_master_bowyer,2,7), (trp_vaegir_druzhina,1,6)] ),

("kingdom_3_reinforcements_a", "{!}kingdom_3_reinforcements_a", 0, 0, fac_commoners, 0, [(trp_khergit_tribesman,4,9),(trp_khergit_kharash,3,8),(trp_khergit_rider,2,7), (trp_khergit_kharash_scout,1,6)]),
("kingdom_3_reinforcements_b", "{!}kingdom_3_reinforcements_b", 0, 0, fac_commoners, 0, [(trp_khergit_scarred_kharash,3,8),(trp_khergit_horse_archer,2,7),(trp_khergit_lancer,1,6)]),
("kingdom_3_reinforcements_c", "{!}kingdom_3_reinforcements_c", 0, 0, fac_commoners, 0, [(trp_khergit_tribal_chieftain,2,7), (trp_khergit_mangudai,1,6)]),

("kingdom_4_reinforcements_a", "{!}kingdom_4_reinforcements_a", 0, 0, fac_commoners, 0, [(trp_nord_recruit,4,9),(trp_nord_drengr,3,8),(trp_nord_warrior,2,7),(trp_nord_raiding_hunter,1,6)] ),
("kingdom_4_reinforcements_b", "{!}kingdom_4_reinforcements_b", 0, 0, fac_commoners, 0, [(trp_nord_raiding_marksman,3,8),(trp_nord_shieldmaster,2,7),(trp_nord_veteran_warrior,1,6)] ),
("kingdom_4_reinforcements_c", "{!}kingdom_4_reinforcements_c", 0, 0, fac_commoners, 0, [(trp_nord_housecarl,2,7),(trp_nord_berserker,1,6)] ),

("kingdom_5_reinforcements_a", "{!}kingdom_5_reinforcements_a", 0, 0, fac_commoners, 0, [(trp_rhodok_tribesman,4,9),(trp_rhodok_spearman,3,8),(trp_rhodok_reservist,2,7),(trp_rhodok_skirmisher,1,6)] ),
("kingdom_5_reinforcements_b", "{!}kingdom_5_reinforcements_b", 0, 0, fac_commoners, 0, [(trp_rhodok_crossbowman,3,8),(trp_rhodok_scout,2,7),(trp_rhodok_veteran_spearman,1,6)] ),
("kingdom_5_reinforcements_c", "{!}kingdom_5_reinforcements_c", 0, 0, fac_commoners, 0, [(trp_rhodok_armsman,2,7),(trp_rhodok_councilman,1,6)] ),

("dark_knights_reinforcements_a", "{!}dark_knights_reinforcements_a", 0, 0, fac_commoners, 0, [(trp_dark_page,14,16),(trp_dark_skirmisher,8,10),(trp_dark_squire,8,10)]),
("dark_knights_reinforcements_b", "{!}dark_knights_reinforcements_b", 0, 0, fac_commoners, 0, [(trp_dark_marksman,7,9),(trp_dark_champion,7,9),(trp_dark_knight_master,8,10)]),
("dark_knights_reinforcements_c", "{!}dark_knights_reinforcements_c", 0, 0, fac_commoners, 0, [(trp_blackguard,3,5),(trp_dark_ranger,3,5),(trp_unholy_crusader,5,7)]),

("kingdom_6_reinforcements_a", "{!}kingdom_6_reinforcements_a", 0, 0, fac_commoners, 0, [(trp_sarranid_recruit,4,9),(trp_sarranid_azap,3,8),(trp_sarranid_seyman,2,7),(trp_sarranid_janissary,1,6)]),
("kingdom_6_reinforcements_b", "{!}kingdom_6_reinforcements_b", 0, 0, fac_commoners, 0, [(trp_sarranid_akinci,3,8),(trp_sarranid_kamandaran,2,7),(trp_sarranid_timariot,1,6)]),
("kingdom_6_reinforcements_c", "{!}kingdom_6_reinforcements_c", 0, 0, fac_commoners, 0, [(trp_sarranid_eunuch,2,7),(trp_sarranid_siphai,1,6)]),
# End NE 600 series stanardized reinforcement packs

# NE 2
# ("kingdom_1_reinforcements_a", "{!}kingdom_1_reinforcements_a", 0, 0, fac_commoners, 0, [(trp_swadian_infantry,4,7),(trp_swadian_page,2,4),(trp_swadian_militia,2,6)]),
# ("kingdom_1_reinforcements_b", "{!}kingdom_1_reinforcements_b", 0, 0, fac_commoners, 0, [(trp_swadian_sharpshooter,4,7),(trp_swadian_crossbowman,2,6),(trp_watchman,0,3)]),
# ("kingdom_1_reinforcements_c", "{!}kingdom_1_reinforcements_c", 0, 0, fac_commoners, 0, [(trp_swadian_knight,3,6)]),

# ("kingdom_2_reinforcements_a", "{!}kingdom_2_reinforcements_a", 0, 0, fac_commoners, 0, [(trp_vaegir_footman,2,6),(trp_vaegir_infantry,4,7),(trp_vaegir_clanelder,2,4)]),
# ("kingdom_2_reinforcements_b", "{!}kingdom_2_reinforcements_b", 0, 0, fac_commoners, 0, [(trp_vaegir_archer,2,6),(trp_vaegir_marksman,1,3),(trp_watchman,0,3)]),
# ("kingdom_2_reinforcements_c", "{!}kingdom_2_reinforcements_c", 0, 0, fac_commoners, 0, [(trp_vaegir_knight,3,6)]),

# ("kingdom_3_reinforcements_a", "{!}kingdom_3_reinforcements_a", 0, 0, fac_commoners, 0, [(trp_khergit_skirmisher,2,6),(trp_khergit_lancer,4,7),(trp_khergit_tribe,2,4)]),
# ("kingdom_3_reinforcements_b", "{!}kngdom_3_reinforcements_b", 0, 0, fac_commoners, 0, [(trp_khergit_veteran_horse_archer,2,6),(trp_khergit_keshik,4,7),(trp_watchman,0,3)]),
# ("kingdom_3_reinforcements_c", "{!}kingdom_3_reinforcements_c", 0, 0, fac_commoners, 0, [(trp_khergit_tarkan,3,6)]),

# ("kingdom_4_reinforcements_a", "{!}kingdom_4_reinforcements_a", 0, 0, fac_commoners, 0, [(trp_nord_recruit,4,8),(trp_nord_trained_footman,2,4),(trp_nord_cavalry,2,4)]),
# ("kingdom_4_reinforcements_b", "{!}kingdom_4_reinforcements_b", 0, 0, fac_commoners, 0, [(trp_nord_huntsman,3,5),(trp_nord_raider_archer,2,5),(trp_watchman,0,3)]),
# ("kingdom_4_reinforcements_c", "{!}kingdom_4_reinforcements_c", 0, 0, fac_commoners, 0, [(trp_nord_cavalry,3,6)]),

# ("kingdom_5_reinforcements_a", "{!}kingdom_5_reinforcements_a", 0, 0, fac_commoners, 0, [(trp_rhodok_spearman,3,7),(trp_rhodok_tribesman,3,6),(trp_rhodok_elder,2,4)]),
# ("kingdom_5_reinforcements_b", "{!}kingdom_5_reinforcements_b", 0, 0, fac_commoners, 0, [(trp_rhodok_trained_crossbowman,2,6),(trp_rhodok_sergeant,4,7),(trp_watchman,0,3)]),
# ("kingdom_5_reinforcements_c", "{!}kingdom_5_reinforcements_c", 0, 0, fac_commoners, 0, [(trp_rhodok_crossbowman,3,6)]),

# ("dark_knights_reinforcements_a", "{!}dark_knights_reinforcements_a", 0, 0, fac_commoners, 0, [(trp_dark_page,14,16),(trp_dark_skirmisher,8,10),(trp_dark_squire,8,10)]),
# ("dark_knights_reinforcements_b", "{!}dark_knights_reinforcements_b", 0, 0, fac_commoners, 0, [(trp_dark_marksman,7,9),(trp_dark_champion,7,9),(trp_dark_knight_master,8,10)]),
# ("dark_knights_reinforcements_c", "{!}dark_knights_reinforcements_c", 0, 0, fac_commoners, 0, [(trp_blackguard,3,5),(trp_dark_ranger,3,5),(trp_unholy_crusader,5,7)]),



# ("kingdom_6_reinforcements_a", "{!}kingdom_6_reinforcements_a", 0, 0, fac_commoners, 0, [(trp_sarranid_recruit,5,10),(trp_sarranid_footman,2,4)]),
# ("kingdom_6_reinforcements_b", "{!}kingdom_6_reinforcements_b", 0, 0, fac_commoners, 0, [(trp_sarranid_skirmisher,2,4),(trp_sarranid_veteran_footman,2,3),(trp_sarranid_footman,1,3)]),
# ("kingdom_6_reinforcements_c", "{!}kingdom_6_reinforcements_c", 0, 0, fac_commoners, 0, [(trp_sarranid_horseman,3,5)]),

# end NE 2






##  ("kingdom_1_reinforcements_a", "kingdom_1_reinforcements_a", 0, 0, fac_commoners, 0, [(trp_swadian_footman,3,7),(trp_swadian_skirmisher,5,10),(trp_swadian_militia,11,26)]),
##  ("kingdom_1_reinforcements_b", "kingdom_1_reinforcements_b", 0, 0, fac_commoners, 0, [(trp_swadian_man_at_arms,5,10),(trp_swadian_infantry,5,10),(trp_swadian_crossbowman,3,8)]),
##  ("kingdom_1_reinforcements_c", "kingdom_1_reinforcements_c", 0, 0, fac_commoners, 0, [(trp_swadian_knight,2,6),(trp_swadian_sergeant,2,5),(trp_swadian_sharpshooter,2,5)]),
##
##  ("kingdom_2_reinforcements_a", "kingdom_2_reinforcements_a", 0, 0, fac_commoners, 0, [(trp_vaegir_veteran,3,7),(trp_vaegir_skirmisher,5,10),(trp_vaegir_footman,11,26)]),
##  ("kingdom_2_reinforcements_b", "kingdom_2_reinforcements_b", 0, 0, fac_commoners, 0, [(trp_vaegir_horseman,4,9),(trp_vaegir_infantry,5,10),(trp_vaegir_archer,3,8)]),
##  ("kingdom_2_reinforcements_c", "kingdom_2_reinforcements_c", 0, 0, fac_commoners, 0, [(trp_vaegir_knight,3,7),(trp_vaegir_guard,2,5),(trp_vaegir_marksman,2,5)]),
##
##  ("kingdom_3_reinforcements_a", "kingdom_3_reinforcements_a", 0, 0, fac_commoners, 0, [(trp_khergit_horseman,3,7),(trp_khergit_skirmisher,5,10),(trp_khergit_tribesman,11,26)]),
##  ("kingdom_3_reinforcements_b", "kingdom_3_reinforcements_b", 0, 0, fac_commoners, 0, [(trp_khergit_veteran_horse_archer,4,9),(trp_khergit_horse_archer,5,10),(trp_khergit_horseman,3,8)]),
##  ("kingdom_3_reinforcements_c", "kingdom_3_reinforcements_c", 0, 0, fac_commoners, 0, [(trp_khergit_lancer,3,7),(trp_khergit_veteran_horse_archer,2,5),(trp_khergit_horse_archer,2,5)]),
##
##  ("kingdom_4_reinforcements_a", "kingdom_4_reinforcements_a", 0, 0, fac_commoners, 0, [(trp_nord_trained_footman,3,7),(trp_nord_footman,5,10),(trp_nord_recruit,11,26)]),
##  ("kingdom_4_reinforcements_b", "kingdom_4_reinforcements_b", 0, 0, fac_commoners, 0, [(trp_nord_veteran,4,9),(trp_nord_warrior,5,10),(trp_nord_footman,3,8)]),
##  ("kingdom_4_reinforcements_c", "kingdom_4_reinforcements_c", 0, 0, fac_commoners, 0, [(trp_nord_champion,1,3),(trp_nord_veteran,2,5),(trp_nord_warrior,2,5)]),
##
##  ("kingdom_5_reinforcements_a", "kingdom_5_reinforcements_a", 0, 0, fac_commoners, 0, [(trp_rhodok_spearman,3,7),(trp_rhodok_crossbowman,5,10),(trp_rhodok_tribesman,11,26)]),
##  ("kingdom_5_reinforcements_b", "kingdom_5_reinforcements_b", 0, 0, fac_commoners, 0, [(trp_rhodok_trained_spearman,4,9),(trp_rhodok_spearman,5,10),(trp_rhodok_crossbowman,3,8)]),
##  ("kingdom_5_reinforcements_c", "kingdom_5_reinforcements_c", 0, 0, fac_commoners, 0, [(trp_rhodok_sergeant,3,7),(trp_rhodok_veteran_spearman,2,5),(trp_rhodok_veteran_crossbowman,2,5)]),



("steppe_bandit_lair" ,"Steppe Bandit Lair",icon_bandit_lair|carries_goods(2)|pf_is_static|pf_hide_defenders,0,fac_neutral,bandit_personality,[(trp_steppe_bandit,15,58)]),
("taiga_bandit_lair","Tundra Bandit Lair",icon_bandit_lair|carries_goods(2)|pf_is_static|pf_hide_defenders,0,fac_neutral,bandit_personality,[(trp_taiga_bandit,15,58)]),
("desert_bandit_lair" ,"Desert Bandit Lair",icon_bandit_lair|carries_goods(2)|pf_is_static|pf_hide_defenders,0,fac_neutral,bandit_personality,[(trp_desert_bandit,15,58)]),
("forest_bandit_lair" ,"Forest Bandit Camp",icon_bandit_lair|carries_goods(2)|pf_is_static|pf_hide_defenders,0,fac_neutral,bandit_personality,[(trp_forest_bandit,15,58)]),
("mountain_bandit_lair" ,"Mountain Bandit Hideout",icon_bandit_lair|carries_goods(2)|pf_is_static|pf_hide_defenders,0,fac_neutral,bandit_personality,[(trp_mountain_bandit,15,58)]),
("sea_raider_lair","Sea Raider Landing",icon_bandit_lair|carries_goods(2)|pf_is_static|pf_hide_defenders,0,fac_neutral,bandit_personality,[(trp_sea_raider,15,50)]),
("looter_lair","Kidnappers' Hideout",icon_bandit_lair|carries_goods(2)|pf_is_static|pf_hide_defenders,0,fac_neutral,bandit_personality,[(trp_looter,15,25)]),

("bandit_lair_templates_end","{!}bandit_lair_templates_end",icon_axeman|carries_goods(2)|pf_is_static,0,fac_outlaws,bandit_personality,[(trp_sea_raider,15,50)]),

("leaded_looters","Band of robbers",icon_axeman|carries_goods(8)|pf_quest_party,0,fac_neutral,bandit_personality,[(trp_looter_leader,1,1),(trp_looter,3,3)]),

]
