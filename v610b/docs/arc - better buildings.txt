CURRENT STATE:

  slot_center_has_manor            = 130 #village
  slot_center_has_fish_pond        = 131 #village
  slot_center_has_watch_tower      = 132 #village
  slot_center_has_school           = 133 #village

  # Ne building
  slot_center_has_palisade         = 134 #village
  slot_center_has_horse_ranch      = 135 #village
  slot_center_has_distillery       = 136 #village
  slot_center_has_caltrops         = 137 #village
  slot_center_has_cattle_ranch     = 138 #village
  slot_center_has_messenger_post   = 139 #town, castle, village
  slot_center_has_prisoner_tower   = 140 #town, castle
  slot_center_has_caravan_escort   = 141 #town
  slot_center_has_bookstore        = 142 #town
  slot_center_has_festival_square  = 143 #town
  slot_center_has_blacksmith       = 144 #town, castle
  slot_center_has_prisoner_tower   = 145 #town, castle

AYNIL IDEAS:

	Messenger Post - allows center to send messages about events happening to it or around it (enemy attack, bandit attack, enemies spotted)
	                 city can use messengers to send out orders to it's active patrols
	                 if village is attacked by bandits or spots some enemies nearby, a message is send to player and to the town
	                 if town has a patrol on the ready, or if it has messenger post and can communicate with active patrols, it will send closest patrol to deal with the problem
	Militia Barracks - increases number of city patrols by 1, also improves training rate for Town Guard
	Slaver's Guild - creates additional manhunter patrol around the city, also player can always sell prisoners to the slaver's guild
	Army Barracks - will generate troops for the town garrison, and also train troops of the matching culture
	Watch Tower - increases the observation radius of village, reduces negative effects from bandit raids and lootings
	Caltrops - prevents any horses in village battle
	Ammo Storage - defenders will get their ammo replenished during siege
	City Medical Service - defenders will suffer noticeably less fatalities during siege (effectively a bonus to Surgery), reduces losses to diseases and plagues
	City Firefighters - reduces losses to fires
	Official Church - spreads the official religion of the faction, improves population conversion rate to major culture
	Local Church - greatly improves morale of local population, but may prove troublesome in the long run
	High-Security Prison - reduces escape chance for enemy lords and prisoners
	Mercenary Guild - increases number and variety of mercenaries available for hire
	School - educates and indoctrinates young children, adding to culture conversion rate and improving morale
	Merchants Guild - improves effectiveness of caravans
	Village Manor - allows resting in village and adds storage chest
	Outpost - allows keeping troops in village and appointment of officer responsible for village militia training
	University - ?

CONVERGENCE EFFECTS

	Mercenary Guild + Merchants Guild = increased caravan guard
	Watch Tower + Messenger Post = better and more accurate reporting
	Slavers Guild + Militia Barracks = additional source of income for town guard, increasing it's size

ECONOMY BUILDINGS

	Cattle Ranch
	Horse Ranch
	Distillery
	Blacksmith Forge
	Bookstore
	Mill

BUILDINGS FROM SWORD OF DAMOCLES
    - Village Buildings:
    --- Ambulatory:   Cost 2,500 - Health +10, Weekly Health +1, Health Cap +20, Ideal Health +20
    --- Clayworks:   Cost 2,500 - Prosperity +5, Weekly Prosperity +1, Prosperity Cap +20, Ideal Prosperity +20
    --- Inn:  Cost 2,000 - Weekly Relations +2
    --- Manor:   Cost 1,500 - Weekly Renown +3, Fief's Demesne Cost -2
    --- Monastery:   Cost 2,000 - Weekly Taxes +5%, Weekly Local Faith +2, Weekly Global Faith +2
    --- Mill:  Cost 2,500 - Prosperity +5, Weekly Prosperity +1, Ideal Prosperity +20%, Weekly Taxes +10%
    --- Rustic Blacksmith:   Cost 1,200 - Prosperity +5, Weekly Prosperity +1, Prosperity Cap +10, Ideal Prosperity +10
    --- Shrine:   Cost 1,000 - Weekly Relations +1, Weekly Local Faith +3, Weekly Global Faith +2
    --- Watchtower:   Cost 1,200 - Village's Demesne Cost -1, Village raid time increased
    --- Water Supply:   Cost 1,200 - Health +5, Weekly Health +1, Health Cap +10, Ideal Health +10
    - Town Buildings:
    --- Bank:  Cost 1,200 - Prosperity +5, Weekly Prosperity +1, Prosperity Cap +10, Ideal Prosperity +10
    --- Canalization:  Cost 1,200 - Health +5, Weekly Health +1, Health Cap +10, Ideal Health +10
    --- Congregation (Republic of Marina only):  Cost 4,000 - Can only be built in towns, Enables auto-recruitment of Nobles from player's homeland, +5 Weekly Renown, Fief's Demesne Cost -1
    --- Guild:  Cost 9,000 - Weekly Relations +1, Weekly Prosperity +2, Fief's Demesne Cost -2, Weekly Taxes +10%
    --- Hospital:  Cost 2,500 - Health +10, Weekly Health +1, Health Cap +20, Ideal Health +20
    --- Manufacture:  Cost 2,500 - Prosperity +5, Weekly Prosperity +1, Prosperity Cap +20, Ideal Prosperity +20
    --- Temple:  Cost 2,000 - Weekly Relations +1, Weekly Local Faith +5, Weekly Global Faith +4
    --- University:  Cost 4,000 - Weekly Relations +1, Weekly Renown +15, Decrease Weekly Badboy Rating -1
    - Castle Buildings:
    --- Blacksmith:   Cost 1,000 - Decreases Castle's maintenance cost by 50 (to 100), Makes upgrading troops 50% cheaper 
    --- Chapel:   Cost 500 - Allows upgrade of Zealots into Faith Troops, Weekly Global Faith +1
    --- Knight Chapter:  Cost 6,000 - Enables auto-recruitment of homeland Noble troops, +5Weekly Renown, Fief's Demesne Cost -1
    --- Stables:  Cost varies around 4,000 - Allows Cavalry upgrades, +4 Weekly Renown, Fief's Demesne Cost -1
    - Village/Town/Castle:
    --- Messenger Post:  Cost 1,200 - Provides information about enemy troop movement, Fief�s Demesne Cost -1
    - Town/Castle:
    --- Barracks:  Cost varies on SoD faction - Allows upgrade of foot melee units, Allows auto-garrisoning of foot melee units, Fief�s Demesne Cost -1
    --- Range:  Cost varies on SoD faction - Allows upgrade of foot ranged units, Allows auto-garrisoning of foot ranged units, Fief's Demesne Cost -1
    --- Prisoner Tower:  Cost 1,000 - Captured Lords have lower chances of escape, Weekly Relations +1 (only for Towns)

################################################################################################################################################################
################################################################################################################################################################
################################################################################################################################################################
################################################################################################################################################################
################################################################################################################################################################

    RESULTING NATIVE EXPANSION PLANS.

  USEFUL RESOURCES

Lumber, Iron, Tools, Wool Cloth or Linen, Leatherwork, Pottery, Oil, Velvet, Gold, Gems

Each building requires some gold and resources. If player doesn't have some resources in his inventory, their market price in the closest non-hostile town will
be used with +10% overcharge (+50% for Gold and Gems). Note that player cannot purchase Gold or Gems anywhere, so they become a tryly precious commodity.

  VILLAGE BUILDINGS

Manor. Allow rest in village with troop wages down by 50%.
Fish Pond/Mill. Increase prosperity by 5%.
Watch Tower. Earlier alarm and time to loot village increases by 50%.
School. Relation +1 per week.
Palisade. Increase looting time by 25%. USELESS.
Horse Ranch. Increase prosperity by 2% every 2 weeks, adds horses for sale.
Distillery. Increase prosperity by 2% every month, adds wine and ale for sale.
Caltrops. No cavalry in village battles.
Cattle Ranch. Increase growth rate of cattle, no chance of famine.
Messenger Post. Instant player notification.

  CASTLE BUILDINGS

Messenger Post.
Blacksmith. RENAME TO Armory.
Prisoner Tower

  TOWN BUILDINGS

Messenger Post.
Caravan Escort Outpost. Reinforces outgoing caravans.
Bookstore. USELESS.
Festival Square. Increases town relation and party morale when staying in town.
Blacksmith. RENAME TO Armory. Unlimited ammo for defenders.
Prisoner Tower. Reduced chances of lords and troops escaping captivity.

  NEW BUILDINGS

Training Grounds. Allow generation of troops and auto-training for player's towns and castles.
Mercenary Guild. Extra mercenaries for hire available. Mercenary War Parties have a chance to spawn around town.
Town Guard. Reduces taxes. Generates town guard party to patrol around the town and fight bandits.
Manhunters Office. Generates manhunters party to fight bandits. Player can always sell prisoners in town.
University. Increase renown over time but decrease relation.
Church/Chapel/Shrine. Increase town relation over time.
Tavern/Inn. Increase taxes, prosperity and chance of bandits at night.
Storage Rooms. Greatly increase castle/town chest size.
Blacksmith. Decreases amount of Iron and Tools required for other upgrades. Extra Iron and Tools are available for sale.
Hospital. Reduces taxes. Increases honor (very slowly) and prosperity growth (mostly at lower levels).
